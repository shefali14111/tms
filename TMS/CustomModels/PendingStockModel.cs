﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.CustomModels
{
    public class PendingStockModel
    {
        public string ConsignmentNo { get; set; }
        public string ConsignmentDate { get; set; }
        public string PaymentType { get; set; }
        public string ConsignmentFromID { get; set; }
        public string ConsignmentToID { get; set; }
        public string ConsignorName { get; set; }
        public string ConsigneeName { get; set; }
        public int Article { get; set; }
        public double ActualWeight { get; set; }
        public string CreatedDate { get; set; }
        public string DeliveryType { get; set; }
        public string InvoiceNos { get; set; }
        public int InvoiceQuantity { get; set; }
        public double InvoiceValue { get; set; }
        public string PartNos { get; set; }
        public string PoNos { get; set; }
        public string ASNNos { get; set; }
        public string ConsignmentBranch { get; set; }
    }
}