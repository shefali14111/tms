﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.CustomModels
{
    public class DeliveryBranchModel
    {
        public int DestinationID { get; set; }
        public string DeliveryBranch { get; set; }
        public int TotalLR { get; set; }
        public int TotalArticles { get; set; }
        public double TotalActualWeight { get; set; }
        public double TotalChargedWeight { get; set; }
        public double GrandTotal { get; set; }
    }
}