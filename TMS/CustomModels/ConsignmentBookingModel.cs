﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TMS.EFModel;

namespace TMS.CustomModels
{
    public class ConsignmentBookingModel
    {
        public int ConsignmentID { get; set; }
        public string ConsignmentNo { get; set; }
        public string ConsigneeName { get; set; }
        public string ConsignorName { get; set; }
        public string Consignmentdate { get; set; }
        public string ConsignmentFrom { get; set; }
        public string ConsignmentTo { get; set; }
        public string paymentType { get; set; }
        public Nullable<int> InvoiceQuantity { get; set; }
        public Nullable<int> ChargeWeight { get; set; }
        public string DeliveryTYpe { get; set; }
        public string DeliveryStatus { get; set; }
        public string FilePath { get; set; }
        public bool isFileUpload { get; set; }
        public int ManifestConsignmentID { get; set; }
        public string VendorName { get; set; }
    }
    public class ConsignmentDetailsModel
    {
        public string ConsignmentNo { get; set; }
        public string ConsignmentDate { get; set; }
        public string ConsignmentTime { get; set; }
        public string ConsigneeName { get; set; }
        public string ConsignorName { get; set; }
        public string ConsignmentFrom { get; set; }
        public string ConsignmentTo { get; set; }
        public string Destination { get; set; }
        public string MobileNumber { get; set; }
        public string ContactPersonName { get; set; }
        public string BillingParty { get; set; }
        public string ConsigneeAddress { get; set; }
        public string ConsignorAddress { get; set; }
        public string EwayBillNo { get; set; }
        public string VendorCode { get; set; }
        public string PaymentType { get; set; }
        public string UserName { get; set; }
        public Nullable<decimal> BasicFreight { get; set; }
        public Nullable<double> DocketCharges { get; set; }
        public Nullable<double> FOV { get; set; }
        public Nullable<decimal> Pickup { get; set; }
        public Nullable<double> DoorDelivery { get; set; }
        public Nullable<double> LabourCharges { get; set; }
        public Nullable<double> OtherCharges { get; set; }
        public Nullable<double> TotalAmount { get; set; }
        public Nullable<double> GrandTotal { get; set; }
        public Nullable<decimal> StandardRate { get; set; }
        public Nullable<double> DoorCollection { get; set; }
        public Nullable<decimal> ODACharges { get; set; }
        public Nullable<decimal> HamaliCharges { get; set; }
        public Nullable<decimal> DDCharges { get; set; }
        public Nullable<decimal> OtherAOC { get; set; }
        public string BookingBranch { get; set; }
        public string TransportType { get; set; }
        public string DoorPickup { get; set; }
        public string DeliveryType { get; set; }
        public string ConsigneeGSTNo { get; set; }
        public string ConsignorGSTNo { get; set; }
        public string GoodsDescription { get; set; }
        public string TransportMode { get; set; }
        public string VehicleNo { get; set; }
        public Nullable<double> ActualWeight { get; set; }
        public Nullable<double> ChargeWeight { get; set; }
        public int Article { get; set; }
        public string InvoiceNos { get; set; }
        public string InvoiceDates { get; set; }
        public string InvoiceValue { get; set; }
        public string PartNos { get; set; }
        public string ASNNos { get; set; }
        public string PaymentMode { get; set; }
        public string ChequeNo { get; set; }
        public string Packaging { get; set; }
    }

    public class ToPayAndPaidConsignment
    {
        public int ConsignmentID { get; set; }
        public int ManifestConsignmentID { get; set; }
        public string ConsignmentNo { get; set; }
        public string ConsignmentDate { get; set; }
        public double ConsignmentAmount { get; set; }
        public double GSTAmount { get; set; }
        public string BillingType { get; set; }
        public double ReceivedAmount { get; set; }
        public string FromBranch { get; set; }
        public string ToBranch { get; set; }
        public string ConsigneeName { get; set; }
        public string ConsignorName { get; set; }
        public int TotalArticles { get; set; }
        public double InvoiceValue { get; set; }
        public double ActualWeight { get; set; }
        public double ChargedWeight { get; set; }
        public string DeliveryStatus { get; set; }
        public string DeliveryDate { get; set; }
        public string DeliveredBy { get; set; }
        public string ReceivedPaymentType { get; set; }
        public string ReceivedDate { get; set; }
        public string ChequeNo { get; set; }
        public string ReceivedBy { get; set; }
        public string HOReceivedStatus { get; set; }
        public string HOReceivedBy { get; set; }
        public string HOReceivedDate { get; set; }
        public string BillingParty { get; set; }
        public string ConsignorGSTNo { get; set; }
        public string PostalCode { get; set; }
    }
    public class CustomPaymentModel
    {
        public ConsignmentBookingPayment BookingPayment { get; set; }
        public List<TaxMaster> PaymentTaxes { get; set; }
    }
}