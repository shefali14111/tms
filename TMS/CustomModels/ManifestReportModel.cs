﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.CustomModels
{
    public class ManifestReportModel
    {
        public int ManifestID { get; set; }
        public string ManifestNo { get; set; }
        public string FromLoc { get; set; }
        public string ToLoc { get; set; }
        public string CreatedDate { get; set; }
        public string EstimatedDate { get; set; }
        public string typeofdata { get; set; }
        public string ManifestBranch { get; set; }
        public int TotalArticles { get; set; }
        public double TotalActualWeight { get; set; }
    }

    public class ManifestDetailReportModel
    {
        public string ManifestNo { get; set; }
        public string FromBranch { get; set; }
        public string ToBranch { get; set; }
        public string ManifestDate { get; set; }
        public string VendorName { get; set; }
        public string DeliveryBranch { get; set; }
        public string VehicleNo { get; set; }
        public string ManifestTime { get; set; }
        public string ScheduledHours { get; set; } 
        public string DriverMobileNo { get; set; }
        public double FreightAmount { get; set; }
        public double FreightAdvance { get; set; }
        public double FreightBalance { get; set; }
    }
}