﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.CustomModels
{
    public class OtherVendorReportModel
    {
        public string ExpenseDateStr { get; set; }
        public string TimeStr { get; set; }
        public string InvoiceNo { get; set; }
        public string BranchName { get; set; }
        public string Amount { get; set; }
        public string PaidAmount { get; set; }
    }
}