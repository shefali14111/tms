﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.CustomModels
{
    public class BranchBalance
    {
        public int BranchID { get; set; }
        public string BranchName { get; set; }
        public double Amount { get; set; }
        public double Expense { get; set; }
    }
}