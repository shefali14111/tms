﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TMS.EFModel;

namespace TMS.CustomModels
{
    public class ConsignmentDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<ConsignmentBookingModel> data { get; set; }
    }
    public class VehicleHireDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<VehicleHireModel> data { get; set; }
    }
    public class ManifestDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<ManifestModels> data { get; set; }
    }
    public class PendingStockDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<PendingStockModel> data { get; set; }
    }
    public class DamageStockDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<DamageStockModel> data { get; set; }
    }
    public class DeliveryStockDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<DeliveryStockModel> data { get; set; }
    }
    public class VehicleHireReportDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<VehicleHirePaymentModel> data { get; set; }
    }
    public class ConsignmentBookingReportDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<ConsignmentBookingReportModel> data { get; set; }
    }
    public class ManifestReportDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<ManifestReportModel> data { get; set; }
    }
    public class DeliveryBranchDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<DeliveryBranchModel> data { get; set; }
    }
    public class VendorRateDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<VendorRateModel> data { get; set; }
    }
    public class MHELVendorRateDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<MHELVendorRateMaster> data { get; set; }
    }
    public class MahindraVendorRateDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<MahindraVendorRateMaster> data { get; set; }
    }
    public class ReservedConsignmentDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<ReservedConsignment> data { get; set; }
    }
    public class ToPayAndPaidCollectionDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<ToPayAndPaidConsignment> data { get; set; }
    }
    public class VehicleHireBalancePaymentDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<VehicleHireBalancePayment> data { get; set; }
    }

    public class OtherVendorBalancePaymentDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<VendorExpenseMaster> data { get; set; }
    }
    public class PaymentTypeSalesDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<PaymentTypaSales> data { get; set; }
    }
    public class GSTDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<ToPayAndPaidConsignment> data { get; set; }
    }
    public class VoucherDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<Voucher> data { get; set; }
    }

    public class UserDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<UserModel> data { get; set; }
    }

    public class CompanyDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<CompanyMaster> data { get; set; }
    }
    public class ZoneDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<ZoneMaster> data { get; set; }
    }

    public class BranchDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<BranchMaster> data { get; set; }
    }
    public class OtherBranchDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<OtherBranchMaster> data { get; set; }
    }
    public class VendorDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<VendorMaster> data { get; set; }
    }
    public class VehicleVendorDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<VehicleVendorMaster> data { get; set; }
    }
    public class VehicleDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<VehicleMaster> data { get; set; }
    }
    public class BranchRunningDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<BranchRunningNo> data { get; set; }
    }
    public class ConsignmentBillingDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<ConsignmentBilling> data { get; set; }
    }
    public class ConsignmentVoucherDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<ConsignmentVoucher> data { get; set; }
    }
    public class LRExperienceReportDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<LRExperienceReportModel> data { get; set; }
    }

    public class VendorVehicleReportDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<VendorVehicleModel> data { get; set; }
    }
    public class OtherVendorReportDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<OtherVendorReportModel> data { get; set; }
    }
    public class DepositeDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<DepositeMaster> data { get; set; }
    }

    public class VehicleVendorDepositeDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<VehicleVendorDepositeMaster> data { get; set; }
    }

    public class TransactionDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<TransactionModel> data { get; set; }
    }

    public class ExpenseDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<ExpenseMaster> data { get; set; }
    }
    public class VendorExpenseDataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<VendorExpenseMaster> data { get; set; }
    }
}