﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.CustomModels
{
    public class LRExperienceReportModel
    {
        public string VoucherType { get; set; }
        public string Branch { get; set; }
        public string ManifestNo { get; set; }
        public string ConsignmentNo { get; set; }
        public string ConsignmentDate { get; set; }
        public string FromBranch { get; set; }
        public string ToBranch { get; set; }
        public string ConsigneeName { get; set; }
        public int NoOfArticles { get; set; }
        public double ActualWeight { get; set; }
        public string VendorName { get; set; }
        public string VehicleNo { get; set; }
        public double TotalAmount { get; set; }
        public string Transit { get; set; }
        public string DebitLedger { get; set; }
    }
}