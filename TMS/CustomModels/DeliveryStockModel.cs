﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.CustomModels
{
    public class DeliveryStockModel
    {
        public string ConsignmentNo { get; set; }
        public string ConsignmentDate { get; set; }
        public string PaymentType { get; set; }
        public string ConsignmentFromID { get; set; }
        public string ConsignmentToID { get; set; }
        public string ConsignorName { get; set; }
        public string ConsigneeName { get; set; }
        public int Article { get; set; }
        public string Deliverydatestr { get; set; }
        public int DeliveredArticles { get; set; }
        public string VehicleNo { get; set; }
        public string VendorName { get; set; }
        public string DeliveryRemarks { get; set; }
        public string DeliveryStatus { get; set; }
        public string UserName { get; set; }
        public string Created { get; set; }
        public string ToLocBranch { get; set; }
    }
}