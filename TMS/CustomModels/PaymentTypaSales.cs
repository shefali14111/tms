﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.CustomModels
{
    public class PaymentTypaSales
    {
        public int PaymentTypeID { get; set; }
        public string PaymentType { get; set; }
        public int TotalLR { get; set; }
        public int TotalArticles { get; set; }
        public double ActualWeight { get; set; }
        public double ChargedWeight { get; set; }
        public double InvoiceValue { get; set; }
        public double GrantTotal { get; set; }
    }
}