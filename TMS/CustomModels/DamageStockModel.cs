﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.CustomModels
{
    public class DamageStockModel
    {
        public string ConsignmentNo { get; set; }
        public string ConsignmentDate { get; set; }
        public double InvoiceValue { get; set; }
        public int Article { get; set; }
        public string PaymentType { get; set; }
        public string ConsignmentFromID { get; set; }
        public string ConsignmentToID { get; set; }
        public string ConsignorName { get; set; }
        public string ConsigneeName { get; set; }
        public int DamagedArticles { get; set; }
        public string Status { get; set; }
        public string ManifestCreatedBranch { get; set; }
        public string ManifestNo { get; set; }
        public string UnloadDate { get; set; }
        public string VehicleNo { get; set; }
        public string DeliveryRemarks { get; set; }
        public string CreatedBy { get; set; }
        public string ManifestToBranch { get; set; }
    }
}