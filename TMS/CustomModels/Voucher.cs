﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.CustomModels
{
    public class Voucher
    {
        public int VoucherID { get; set; }
        public string VoucherNo { get; set; }
        public string VoucherDate { get; set; }
        public string VoucherType { get; set; }
        public string CreatedBranch { get; set; }
        public string ManifestNo { get; set; }
        public string VendorName { get; set; }
        public string PaymentType { get; set; }
        public double VoucherAmount { get; set; }
        public string CreatedBy { get; set; }
    }
}