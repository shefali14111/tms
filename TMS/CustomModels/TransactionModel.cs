﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.CustomModels
{
    public class TransactionModel
    {
        public string DateStr { get; set; }
        public string TimeStr { get; set; }
        public string VoucherNo { get; set; }
        public string AccountHead { get; set; }
        public string Particular { get; set; }
        public string Location { get; set; }
        public string THCNo { get; set; }
        public string LRNo { get; set; }
        public int? NoOfArticles { get; set; }
        public double? Weight { get; set; }
        public string VehicleNo { get; set; }
        public string PersonName { get; set; }
        public double? ReceivedAmount { get; set; }
        public double? ExpenseAmount { get; set; }
        public DateTime OrderDate { get; set; }
        public double? BalanceFlow { get; set; }
    }
}