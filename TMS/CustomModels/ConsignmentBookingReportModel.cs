﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.CustomModels
{
    public class ConsignmentBookingReportModel
    {
        public string LrNo { get; set; }
        public string LrDate { get; set; }
        public string LrTime { get; set; }
        public string PaymentTerm { get; set; }
        public string BookingBranch { get; set; }
        public string PickupFrom { get; set; }
        public string DeliveryBranch { get; set; }
        public string Destination { get; set; }
        public string ConsignorName { get; set; }
        public string ConsignorGSTNo { get; set; }
        public string ConsigneeName { get; set; }
        public string ConsigneeGSTNo { get; set; }
        public string BillingParty { get; set; }
        public string InvoiceNo { get; set; }
        public string InvoiceDate { get; set; }
        public string GoodsDescription { get; set; }
        public double InvoiceValue { get; set; }
        public string DeliveryType { get; set; }
        public int NoOfArticles { get; set; }
        public string TypeOfPacking { get; set; }
        public string EWayBillNo { get; set; }
        public double ActualWeight { get; set; }
        public double ChargeWeight { get; set; }
        public string CFT { get; set; }
        public double FreightRate { get; set; }
        public decimal BasicFreight { get; set; }
        public double DocketCharges { get; set; }
        public double FOV { get; set; }
        public decimal PickupCharges { get; set; }
        public decimal DeliveryCharges { get; set; }
        public double HamaliCharges { get; set; }
        public decimal CODCharges { get; set; }
        public double OtherCharges { get; set; }
        public double SubTotal { get; set; }
        public double GrandTotal { get; set; }
        public string VendorCode { get; set; }
        public string PONo { get; set; }
        public string PartNo { get; set; }
        public string VehicleNo { get; set; }
        public int InvoiceQuantity { get; set; }
        public string BookingType { get; set; } //doubghts=transportmodeid
        public string Remarks { get; set; }
        public string MobileNumber { get; set; }
        public string ContactPersonName { get; set; }
    }
}