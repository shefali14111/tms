﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.CustomModels
{
    public class VendorRateModel
    {
        public int VendorRateID { get; set; }
        public string BranchName { get; set; }
        public string BillingParty { get; set; }
        public string Consignor { get; set; }
        public string Consignee { get; set; }
        public string FromBranch { get; set; }
        public string ToBranch { get; set; }
        public string ChargeType { get; set; }
        public double FRIEGHTRATE { get; set; }
        public double DocketCharge { get; set; }
        public double DoorCollection { get; set; }
        public double DeliveryCharge { get; set; }
        public double FOV { get; set; }
        public double HamaliCharge { get; set; }
        public double OtherCharges { get; set; }
        public double MinKG { get; set; }
    }
}