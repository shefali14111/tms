﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.CustomModels
{
    public class VendorVehicleModel
    {
        public string VendorName { get; set; }
        public string HireDate { get; set; }
        public string TimeStr { get; set; }
        public string VehicleNo { get; set; }
        public string BranchName { get; set; }
        public double Amount { get; set; }
        public double PaidAmount { get; set; }
        public string FromBranch { get; set; }
        public string ToBranch { get; set; }
    }
}