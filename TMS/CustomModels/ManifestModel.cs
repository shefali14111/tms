﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.CustomModels
{
    public class ManifestModels
    {
        public int ManifestID { get; set; }
        public int VehicleHireID { get; set; }
        public string ManifestNo { get; set; }
        public string DriverName { get; set; }
        public string DriverMobileNo { get; set; }
        public string LicenseNo { get; set; }
        public string EstimatedDeliveryDate { get; set; }
        public string ArrivalTime { get; set; }
        public string DepartureTime { get; set; }
        public string VehicleNo { get; set; }
        public string FromBranch { get; set; }
        public string ToBranch { get; set; }
        public string ArrivalDate { get; set; }
        public int FromLoc { get; set; }
        public int ToLoc { get; set; }
        public string Created { get; set; }
        public int VehicleID { get; set; }
        public string Status { get; set; }
    }
    public class ManifestConsignmentModels
    {
        public int ManifestConsignmentID { get; set; }
        public string DeliveryStatus { get; set; }
        public string DeliveryDate { get; set; }
        public int DeliveredArticles { get; set; }
        public string DeliveryRemarks { get; set; }
        public string VehicleNo { get; set; }
        public int ManifestID { get; set; }
        public int ConsignmentID { get; set; }
        public string DeliveryTime { get; set; }
      
    }
}