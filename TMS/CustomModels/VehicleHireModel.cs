﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.CustomModels
{
    public class VehicleHireModel
    {
        public int BranchID { get; set; }
        public int VehicleHireID { get; set; }
        public int LocalVehicleHireID { get; set; }
        public int DeliveryVehicleHireID { get; set; }
        public int VehicleID { get; set; }
        public string VehicleNo { get; set; }
        public string HireDate { get; set; }
        public string VendorName { get; set; }
        public double BookingAmount { get; set; }
        public string BookingType { get; set; }
        public string BranchName { get; set; }
    }
}