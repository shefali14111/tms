﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.CustomModels
{
    public class VehicleHireBalancePayment
    {
        public int VehicleHireID { get; set; }
        public string VehicleNo { get; set; }
        public string HireDate { get; set; }
        public string VendorName { get; set; }
        public string DriverMobileNo { get; set; }
        public string DriverName { get; set; }
        public string ManifestNo { get; set; }
        public string HiredBy { get; set; }
        public double BookingAmount { get; set; }
        public double PaidAmount { get; set; }
        public double BalanceAmount { get; set; }
        public string HireType { get; set; }
    }
}