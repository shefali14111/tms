﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.CustomModels
{
    public class ReservedConsignment
    {
        public int BranchReservedID { get; set; }
        public string BranchID { get; set; }
        public string BranchName { get; set; }
        public string ConsignmentNo { get; set; }
    }
}