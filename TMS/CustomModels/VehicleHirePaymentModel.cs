﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMS.CustomModels
{
    public class VehicleHirePaymentModel
    {
        public string ManifestCreatedBranch { get; set; }
        public string BookingDate { get; set; }
        public string ManifestNo { get; set; }
        public string FromBranch { get; set; }
        public string ToBranch { get; set; }
        public double TotalActualWeight { get; set; }
        public int TotalArticles { get; set; }
        public string VendorName { get; set; }
        public string VehicleNo { get; set; }
        public string VehicleType { get; set; }
        public string DriverMobileNo { get; set; }
        public double BookingAmount { get; set; }
        public double AdvancePayment { get; set; }
        public double BalanceAmount { get; set; }
        public string UnloadDate { get; set; }
        public string BalancePaymentDate { get; set; }
        public string HiredBy { get; set; }
        public int ManifestID { get; set; }
        public string VehicleHireBranch { get; set; }
    }
}