﻿using System.Web;
using System.Web.Optimization;

namespace TMS
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new Bundle("~/bundles/jquery").Include(
                        "~/Scripts/jQuery-{version}.js"));

            bundles.Add(new Bundle("~/bundles/modernizr").Include(
            "~/Scripts/modernizr-*"));

            bundles.Add(new Bundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));


            bundles.Add(new Bundle("~/Content/css").Include(
                          "~/Content/bootstrap.css",
                          "~/Content/TMS/TMS.css",
                          "~/Content/TMS/skins/_all-skins.css",
                          "~/Content/font-awesome.css",
                          "~/Content/datepicker3.css",
                          "~/Content/animate.css",
                          "~/Content/site.css",
                          "~/Content/fileinput.css",
                          "~/Scripts/angular-ui-notification/angular-ui-notification.css",
                          "~/Scripts/angular-ui-notification/angular-csp.css",
                          "~/Scripts/ngDropdowns/angular-dropdowns.min.css",
                          "~/Content/angucomplete-alt.css",
                          "~/Scripts/ngTables/datatables.css"));


            bundles.Add(new Bundle("~/bundles/JS")
                .Include("~/Scripts/angular.js")
                .Include("~/Scripts/formErrors.js")
                .Include("~/Scripts/datepicker/bootstrap-datepicker.js")
                .Include("~/Scripts/timepicker/bootstrap-timepicker.js")
                .Include("~/Scripts/slimScroll/jquery.slimscroll.js")
                .Include("~/Scripts/TMS/app.js")
                .Include("~/Scripts/TMS/demo.js")
                .Include("~/Scripts/TMS/fileinput.js")
                .Include("~/Scripts/jquery.maskedinput.min.js")
                .Include("~/Scripts/smart-table.min.js")//smart table
                .Include("~/Scripts/dirPagination.js")//smart table
                .Include("~/Scripts/angucomplete-alt.js")
                .Include("~/Scripts/angular-ui-notification/angular-ui-notification.min.js")
                .Include("~/Scripts/angular-confirm.js")
                .Include("~/Scripts/angular-confirm.min.js")
                .Include("~/Scripts/ui-bootstrap-tpls-1.3.3.min.js")
                .Include("~/Scripts/ngDropdowns/angular-dropdowns.min.js")
                 .Include("~/Scripts/ngTables/datatables.js")
                 .Include("~/Scripts/data-table.js")
                .Include("~/Scripts/bootstrap-multiselect.js")
                .Include("~/Scripts/angularjs-dropdown-multiselect.js")
                .Include("~/ngControllers/ngApplicationController.js")
                .Include("~/ngControllers/ngConsignmentController.js")
                .Include("~/ngControllers/ngManifestController.js")
                .Include("~/ngControllers/ngReportController.js")
                .Include("~/ngControllers/ngLocalManifestController.js")
                .Include("~/ngControllers/ngSettingController.js")
                .Include("~/ngControllers/ngPaymentController.js")
                .Include("~/ngControllers/ngTMSController.js")
                .Include("~/ngControllers/ngVoucherController.js")
                .Include("~/ngControllers/ngMasterController.js")
                .Include("~/Scripts/lodash.js")
              );

            bundles.Add(new Bundle("~/bundles/LoginModule")
                .Include("~/Scripts/angular.js")
                .Include("~/Scripts/angular-ui-notification/angular-ui-notification.min.js")
                .Include("~/ngControllers/ngApplicationLoginModule.js")
                .Include("~/ngControllers/ngLoginController.js")

                );

            bundles.Add(new Bundle("~/bundles/jqueryval").Include(
                       "~/Scripts/jquery.validate*"));

            BundleTable.EnableOptimizations = false;
        }
    }
}
