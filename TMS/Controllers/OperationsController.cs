﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using TMS.EFModel;
using TMS.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using TMS.CustomModels;
using TMS.Helper;
using Newtonsoft.Json.Linq;
using System.Globalization;
using Microsoft.Ajax.Utilities;

namespace TMS.Controllers
{

    public class OperationsController : Controller
    {
        string CONS_TransactionType = "Consignment";
        private const int TOTAL_ROWS = 0;
        string MANIFEST_TransactionType = "Manifest";
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #region Views

        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD })]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public ActionResult Consignment()
        {
            ViewData.Add("BranchId", Convert.ToInt32(HttpContext.Session["BranchID"]));
            return View();
        }
       
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD })]
        public ActionResult AddConsignment(int? BranchReservedId)
        {
            ViewData.Add("BranchId", Convert.ToInt32(HttpContext.Session["BranchID"]));
            ViewData.Add("BranchReservedId", BranchReservedId);
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public ActionResult EditConsignment(int ConsignmentId, bool? RedirectPage)
        {
            ViewData["ConsignmentId"] = ConsignmentId;
            ViewBag.RedirectPage = RedirectPage;
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD,USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public ActionResult VehicleHire()
        {
            ViewData.Add("BranchId", Convert.ToInt32(HttpContext.Session["BranchID"]));
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public ActionResult LocalVehicleHire()
        {
            ViewData.Add("BranchId", Convert.ToInt32(HttpContext.Session["BranchID"]));
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public ActionResult DeliveryVehicleHire()
        {
            ViewData.Add("BranchId", Convert.ToInt32(HttpContext.Session["BranchID"]));
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD })]
        public ActionResult ManifestUnload()
        {
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD })]
        public ActionResult ConsignmentDeliveryStatus()
        {
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD })]
        public ActionResult ManifestDeliveryStatus()
        {
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD })]
        public ActionResult ManifestReport()
        {
            return View();
        }
       
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public ActionResult TrackConsignment()
        {
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public ActionResult ViewConsignment(int ConsignmentId)
        {
            ViewData["ConsignmentId"] = ConsignmentId;
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public ActionResult ReserveConsignment()
        {
            ViewData.Add("BranchId", Convert.ToInt32(HttpContext.Session["BranchID"]));
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public ActionResult POD()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            ViewBag.UserName = Convert.ToString(HttpContext.Session["UserName"]);
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public ActionResult BranchBalance()
        {
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public ActionResult ConsignmentBilling()
        {
            ViewBag.RoleName = Convert.ToString(System.Web.HttpContext.Current.Session["RoleName"]);
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }

        #endregion

        #region Consignment Booking

        public string PrintConsignment(int ConsignmentID)
        {
            string consignmentJSON = JsonConvert.SerializeObject(ConsignmentModel.GetConsignmentDetailsByID(ConsignmentID));
            return consignmentJSON;
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public string ReserveConsignmentNo(int branchId)
        {
            string consignmentNo = ConsignmentModel.ReserveConsignmentNo(branchId);
            return consignmentNo;
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void UpdateConsignmentBooking(ConsignmentBooking cnsgnBooking)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                //  string loginID = UserInfo.EMPLOYEEID;
                ConsignmentModel.UpdateConsignmentBooking(loginID, cnsgnBooking);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int? AddConsignmentBooking(ConsignmentBooking cnsgnBooking)
        {
            try
            {
                log.Info("AddConsignmentBooking");
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                cnsgnBooking.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
                cnsgnBooking.ZoneID = Convert.ToInt32(HttpContext.Session["ZoneID"]);
                cnsgnBooking.ConsignmentDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                int consignmentID;
                if (cnsgnBooking.BranchReservedID == null)
                    consignmentID = ConsignmentModel.AddConsignmentBooking(loginID, cnsgnBooking);
                else
                    consignmentID = ConsignmentModel.AddConsignmentBookingWithoutConsignmentNo(loginID, cnsgnBooking);
                return consignmentID;
            }
            catch (Exception ex)
            {
                log.Info("Exception in AddConsignmentBooking");
                log.Error(ex.Message);
                return null;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public bool CheckEditableConsignment(int ConsignmentId)
        {
            try
            {
                if (Convert.ToString(HttpContext.Session["RoleName"]).ToLower() == "operation head")
                {
                    bool checkForManifest = ConsignmentModel.CheckManifestForConsignment(ConsignmentId);
                    if (checkForManifest == true)
                        return false;
                    else
                        return true;
                }
                else
                    return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public bool CheckAddConsignment()
        {
            try
            {
                bool chkaddLr = true;
                string consignmentTime = MasterModel.GetAttributeValueByKey("CONSIGNMENT_TIME");
                TimeSpan timeFrom= DateTime.ParseExact(consignmentTime.Split('-')[0].Trim(), "h:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
                TimeSpan timeTo = DateTime.ParseExact(consignmentTime.Split('-')[1].Trim(), "h:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
                DateTime currentDate =  TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                TimeSpan timeCurrent = DateTime.ParseExact(currentDate.ToString("h:mm tt"), "h:mm tt", CultureInfo.InvariantCulture).TimeOfDay;
                if (timeFrom <= timeTo)
                {
                    if (timeCurrent >= timeFrom && timeCurrent <= timeTo)
                        chkaddLr=false;
                    else
                        chkaddLr=true;
                }
                else
                {
                    if (timeCurrent >= timeFrom || timeCurrent <= timeTo)
                        chkaddLr=false;
                    else
                        chkaddLr=true;
                }
                if(chkaddLr == true)
                {
                    chkaddLr = ConsignmentModel.CheckConsignmentDeliveryStatus(Convert.ToInt32(HttpContext.Session["BranchID"]));
                }
                return chkaddLr;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool CheckDeliveryStatus()
        {
            return ConsignmentModel.CheckConsignmentDeliveryStatus(Convert.ToInt32(HttpContext.Session["BranchID"]));
        }
            public bool CheckMHELPrintOption()
        {
            try
            {
                string printOption = MasterModel.GetAttributeValueByKey("MAHINDRA_PRINT");
                if(printOption == "true")
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteConsignmentBooking(int consignID)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                ConsignmentModel.DeleteConsignmentBooking(loginID, consignID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult GetReservedConsignment(int draw, int start, int length, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            ReservedConsignmentDataTableData dataTableData = new ReservedConsignmentDataTableData();
            dataTableData.draw = draw;
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            var data = ConsignmentModel.GetReservedConsignmentIndex(branchId, start, length, search, sortColumn, sortDirection);
            dataTableData.data = data.FirstOrDefault().Value;
            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public string DataTableToJSON(DataTable table)
        {
            string JSONString = string.Empty;

            JSONString = JsonConvert.SerializeObject(table);
            return JSONString;
        }
        public string GetLocalVehiclesHired()
        {
            try
            {
                int BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
                string vehicleJSON = DataTableToJSON(ConsignmentModel.GetLocalVehiclesHired(BranchID));
                return vehicleJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetConsignmentBookings()
        {
            try
            {
                int branchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
                string bookingJSON = DataTableToJSON(ConsignmentModel.GetConsignmentBookings(branchID));
                //   var bookings = Json(bookingJSON, JsonRequestBehavior.AllowGet);

                return bookingJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetVendorRateMaster(string vendorrates)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            VendorRateMaster vendorrate = serializer.Deserialize<VendorRateMaster>(vendorrates);
            vendorrate.BranchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            var obj = MasterModel.GetVendorRate(vendorrate);
            return Json(obj, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetMHELVendorRates(string vendorrates)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            MHELVendorRateMaster vendorrate = serializer.Deserialize<MHELVendorRateMaster>(vendorrates);
            var obj = MasterModel.GetMHELVendorRate(vendorrate);
            return Json(obj, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetMahindraVendorRates(string vendorrates)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            MahindraVendorRateMaster vendorrate = serializer.Deserialize<MahindraVendorRateMaster>(vendorrates);
            var obj = MasterModel.GetMahindraVendorRate(vendorrate);
            return Json(obj, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetConsignmentData(int draw, int start, int length, int? branchId)
        {
            try
            {
                log.Info("GetConsignmentData For BranchId=" + branchId);
                string search = Request.QueryString["search[value]"];
                int sortColumn = 2;
                string sortDirection = "desc";
                if (length == -1)
                {
                    length = TOTAL_ROWS;
                }

                // note: we only sort one column at a time
                if (Request.QueryString["order[0][column]"] != null)
                {
                    sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
                }
                if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
                {
                    sortDirection = Request.QueryString["order[0][dir]"];
                }

                ConsignmentDataTableData dataTableData = new ConsignmentDataTableData();
                dataTableData.draw = draw;
                if (branchId == 0)
                    branchId = null;
                var data = ConsignmentModel.GetConsignmentIndex(branchId, start, length, search, sortColumn, sortDirection);
                dataTableData.data = data.FirstOrDefault().Value;
                dataTableData.recordsFiltered = data.FirstOrDefault().Key;
                dataTableData.recordsTotal = data.FirstOrDefault().Key;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Info("Exception in GetConsignmentData");
                log.Error(ex.Message);
                return null;
            }
        }
        public ActionResult GetVehicleHireData(int draw, int start, int length)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 3;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            VehicleHireDataTableData dataTableData = new VehicleHireDataTableData();
            dataTableData.draw = draw;
            int? branchId = null;
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            var data = ManifestModel.GetVehicleHireIndex(branchId, start, length, search, sortColumn, sortDirection);
            dataTableData.data = DatatableHelper.ToList<VehicleHireModel>(data.FirstOrDefault().Value);
            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetLocalVehicleHireData(int draw, int start, int length)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 3;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            VehicleHireDataTableData dataTableData = new VehicleHireDataTableData();
            dataTableData.draw = draw;
            int? branchId = null;
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            var data = LocalManifestModel.GetLocalVehicleHireIndex(branchId, start, length, search, sortColumn, sortDirection);
            dataTableData.data = DatatableHelper.ToList<VehicleHireModel>(data.FirstOrDefault().Value);
            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetDeliveryVehicleHireData(int draw, int start, int length)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 3;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            VehicleHireDataTableData dataTableData = new VehicleHireDataTableData();
            dataTableData.draw = draw;
            int? branchId = null;
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            var data = LocalManifestModel.GetDeliveryVehicleHireIndex(branchId, start, length, search, sortColumn, sortDirection);
            dataTableData.data = DatatableHelper.ToList<VehicleHireModel>(data.FirstOrDefault().Value);
            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetManifestToUnload(int draw, int start, int length)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            ManifestDataTableData dataTableData = new ManifestDataTableData();
            dataTableData.draw = draw;

            var data = ManifestModel.GetManifestUnloadIndex(Convert.ToInt32(HttpContext.Session["BranchID"]), start, length, search, sortColumn, sortDirection);
            dataTableData.data = DatatableHelper.ToList<ManifestModels>(data.FirstOrDefault().Value).ToList();
            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetConsignmentDeliveryData(int draw, int start, int length)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            ConsignmentDataTableData dataTableData = new ConsignmentDataTableData();
            dataTableData.draw = draw;

            var data = ManifestModel.GetConsignmentDeliveryIndex(Convert.ToInt32(HttpContext.Session["BranchID"]), start, length, search, sortColumn, sortDirection);
            dataTableData.data = DatatableHelper.ToList<ConsignmentBookingModel>(data.FirstOrDefault().Value);
            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetDeliveryManifestStatusData(int draw, int start, int length)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            ManifestDataTableData dataTableData = new ManifestDataTableData();
            dataTableData.draw = draw;

            var data = ManifestModel.GetDeliveryManifestStatusData(Convert.ToInt32(HttpContext.Session["BranchID"]), start, length, search, sortColumn, sortDirection);
            dataTableData.data = DatatableHelper.ToList<ManifestModels>(data.FirstOrDefault().Value);
            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetConsignmentBookingByID(int consignmentID)
        {
            try
            {

                var bookings = Json(ConsignmentModel.GetConsignmentBookingByID(consignmentID), JsonRequestBehavior.AllowGet);
                return bookings;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
      
        public string GetTrackConsignmentData(string consignmentNo)
        {
            try
            {
                string consignmentJSON = new JavaScriptSerializer().Serialize(TrackConsignmentModel.GetConsignmentDetails(consignmentNo));
                return consignmentJSON;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetManifestById(int ManifestID)
        {
            string manifestJSON = new JavaScriptSerializer().Serialize(ManifestModel.GetManifestById(ManifestID));
            return manifestJSON;
        }
        public string GetManifestsByVehicleID(int VehicleID,string Created)
        {
           
            string manifestJSON = DataTableToJSON(ManifestModel.GetManifestsByVehicleID(VehicleID, Convert.ToInt32(HttpContext.Session["BranchID"]),DateTime.ParseExact(Created,"dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo)));
            return manifestJSON;
        }

        public string GetManifestConsignmentById(int ManifestConsignmentId)
        {
            string manifestJSON = new JavaScriptSerializer().Serialize(ManifestModel.GetManifestConsignmentById(ManifestConsignmentId));
            return manifestJSON;
        }

        #endregion

        #region Consignment Booking payment
        public double GetTotalFreight(int consignmentID)
        {
            try
            {
                return ConsignmentModel.GetTotalBasicFreight(consignmentID);
            }
            catch (Exception ex)
            { throw ex; }
        }

        public double GetInvoiceTotal(int consignmentID)
        {
            try
            {
                return ConsignmentModel.GetInvoiceTotal(consignmentID);
            }
            catch (Exception ex)
            { throw ex; }
        }
        public void UpdateConsignmentPayment(CustomPaymentModel customPaymentModel)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                //  string loginID = UserInfo.EMPLOYEEID;
                ConsignmentModel.UpdateConsignmentPayment(loginID, customPaymentModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddConsignmentPayment(CustomPaymentModel data)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                ConsignmentModel.AddConsignmentPayment(loginID, data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void DeleteConsignmentBookingPayment(int paymenID)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                ConsignmentModel.DeleteConsignmentBookingPayment(loginID, paymenID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public JsonResult GetConsignmentBookingPaymentByID(int consignmentID)
        {
            try
            {

                var payment = Json(ConsignmentModel.GetConsignmentBookingPaymentByID(consignmentID), JsonRequestBehavior.AllowGet);
                return payment;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetConsignmentPartyInvoiceByID(int partyInvoiceId)
        {
            try
            {

                var payment = Json(ConsignmentModel.GetConsignmentPartyInvoiceByID(partyInvoiceId), JsonRequestBehavior.AllowGet);
                return payment;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        #endregion

        #region Consignment Party Invoice
        [HttpPost]
        public void AddConsignmentPartyInvoice(ConsignmentPartyInvoice newpartyInvoice)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);

                ConsignmentModel.AddConsignmentPartyInvoice(loginID, newpartyInvoice);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public void UpdateConsignmentPartyInvoice(ConsignmentPartyInvoice newpartyInvoice)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);

                ConsignmentModel.UpdateConsignmentPartyInvoice(loginID, newpartyInvoice);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteConsignmentPartyInvoice(int partyInvoiceID)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                ConsignmentModel.DeleteConsignmentPartyInvoice(loginID, partyInvoiceID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string GetConsignmentPartyInvoice(int consignmentID)
        {
            try
            {

                string invoices = DataTableToJSON(ConsignmentModel.GetConsignmentPartyInvoices(consignmentID));
                return invoices;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region VEHICLE MASTER
        public void AddVehicle(VehicleMaster vehicle)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                ManifestModel.AddVehicle(loginID, vehicle);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region MASTER DATA RETREIVAL
        public JsonResult GetVehicleByID(int vehicleID)
        {
            try
            {
                var obj = ConsignmentModel.GetVehicleByID(vehicleID);
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            { throw ex; }
        }

        public JsonResult GetVehicleHirePaymentTypes()
        {
            try
            {
                var obj = ManifestModel.GetVehicleHirePaymentTypes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetTruckBookingTypes()
        {
            try
            {
                var obj = ManifestModel.GetTruckBookingTypes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetDeliveryStatusMaster()
        {
            try
            {
                var obj = ManifestModel.GetDeliveryStatusMaster();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {

                throw x;
            }
        }
        public JsonResult GetVendorTypes()
        {
            try
            {
                var obj = ManifestModel.GetVendorTypes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        public JsonResult GetBranches()
        {
            try
            {
                var obj = ConsignmentModel.GetBranches();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetOtherBranches()
        {
            try
            {
                var obj = ConsignmentModel.GetOtherBranches();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetCargoTypes()
        {
            try
            {
                var obj = ConsignmentModel.GetCargoTypes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        public JsonResult GetVehicles(bool isHired)
        {
            try
            {
                var obj = ConsignmentModel.GetVehicles(isHired);
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetCompanies()
        {
            try
            {
                var obj = ConsignmentModel.GetCompanies();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetDeliveryTypes()
        {
            try
            {
                var obj = ConsignmentModel.GetDeliveryTypes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        public JsonResult GetChargeTypes()
        {
            try
            {
                var obj = ConsignmentModel.GetChargeTypes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetDoorCollections()
        {
            try
            {
                var obj = ConsignmentModel.GetDoorCollections();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetPackagings()
        {
            try
            {
                var obj = ConsignmentModel.GetPackagings();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }


        public JsonResult GetPaymentTypes()
        {
            try
            {
                var obj = ConsignmentModel.GetPaymentTypes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        public JsonResult GetGoods()
        {
            try
            {
                var obj = ConsignmentModel.GetGoods();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        public JsonResult GetTaxes()
        {
            try
            {
                var obj = ConsignmentModel.GetTaxes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetServiceTaxpaidBy()
        {
            try
            {
                var obj = ConsignmentModel.GetServiceTaxpaidBy();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        public JsonResult GetTransportModes()
        {
            try
            {
                var obj = ConsignmentModel.GetTransportModes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }


        public JsonResult GetTransportTypes()
        {
            try
            {

                var obj = ConsignmentModel.GetTransportTypes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetVendors()
        {
            try
            {
                var obj = ConsignmentModel.GetVendors();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetOtherVendors()
        {
            try
            {
                var obj = ConsignmentModel.GetOtherVendors();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetMHELVendors()
        {
            try
            {
                var obj = ConsignmentModel.GetMHELVendors();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetPartNos(int vendorID)
        {
            try
            {
                var obj = MasterModel.GetPartNos(vendorID);
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        public JsonResult GetVehicleVendors()
        {
            try
            {
                var obj = ConsignmentModel.GetVehicleVendors();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        public JsonResult GetPinCodes()
        {
            try
            {
                var obj = ConsignmentModel.GetPinCodes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetODA()
        {
            try
            {
                var obj = ConsignmentModel.GetODA();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetManifestsForBranch()
        {
            try
            {
                int branchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
                var obj = MasterModel.GetManifestsForBranch(branchID);
                var list = DatatableHelper.ToList<VehicleHireManifest>(obj);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        public JsonResult GetAccounts()
        {
            try
            {
                int branchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
                var obj = MasterModel.GetAccounts(branchID);
                var list = DatatableHelper.ToList<AccountMaster>(obj);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        public int GetAttributeValueByKey(string key)
        {
            try
            {
                int invoicecount = 1;
                string atrval = MasterModel.GetAttributeValueByKey(key);
                if (!String.IsNullOrEmpty(atrval))
                    invoicecount = Convert.ToInt32(atrval);
                return invoicecount;
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public string GetNextConsignmentNo(int branchID)
        {
            try
            {
                string consignNo = ConsignmentModel.GetNextRunningNo(branchID, CONS_TransactionType);
                return consignNo;
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        public string GetNextManifestNo(int branchID)
        {
            try
            {
                string consignNo = ConsignmentModel.GetNextRunningNo(branchID, MANIFEST_TransactionType);
                return consignNo;
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public string GetStandardRate(int ChargeType, int FromBranch, int ToBranch)
        {
            decimal standardRate = ConsignmentModel.GetStandardRate(ChargeType, FromBranch, ToBranch);
            return standardRate.ToString();
        }



        public JsonResult GetConsignmentReceivedStatus()
        {
            try
            {

                var obj = ManifestModel.GetConsignmentReceivedStatus();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        public JsonResult GetFOV()
        {
            try
            {

                var obj = ConsignmentModel.GetFOV();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        public string GetDocketCharge()
        {
            decimal docketcharge = 0;
            string atrval = MasterModel.GetAttributeValueByKey("DOCKET_CHARGE");
            if (!String.IsNullOrEmpty(atrval))
                docketcharge = Convert.ToDecimal(atrval);
            return docketcharge.ToString();

        }

        #endregion

        #region VEHICLE HIRE PAYMENT
        public void UpdateVehicleHirePayment(VehicleHirePayment hirepayment)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                //  string loginID = UserInfo.EMPLOYEEID;
                ManifestModel.UpdateVehicleHirePayment(loginID, hirepayment);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void AddVehicleHirePayment(VehicleHirePayment hirepayment)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                hirepayment.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
                hirepayment.HireDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                ManifestModel.AddVehicleHirePayment(loginID, hirepayment);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void DeleteVehicleHire(int vehicleHireID)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                ManifestModel.DeleteVehicleHire(loginID, vehicleHireID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetVehicleHirePayments()
        {
            try
            {
                int branchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
                return DataTableToJSON(ManifestModel.GetVehicleHirePayments(branchID));
                //return  Json(ManifestModel.GetVehicleHirePayments(branchID), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetVehicleHirePaymentByID(int vehicleHireID)
        {
            try
            {

                var bookings = Json(ManifestModel.GetVehicleHirePaymentByID(vehicleHireID), JsonRequestBehavior.AllowGet);
                return bookings;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Delivery Vehicle Hire
        public string GetDeliveryVehicleHire()
        {
            try
            {
                int branchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
                return DataTableToJSON(LocalManifestModel.GetDeliveryVehicleHire(branchID));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void AddDeliveryVehicleHire(DeliveryVehicleHire hirepayment)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                hirepayment.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
                LocalManifestModel.AddDeliveryVehicleHire(loginID, hirepayment);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetDeliveryVehicleHireByID(int deliveryVehicleHireID)
        {
            try
            {

                var bookings = Json(LocalManifestModel.GetDeliveryVehicleHireByID(deliveryVehicleHireID), JsonRequestBehavior.AllowGet);
                return bookings;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void UpdateDeliveryVehicleHire(DeliveryVehicleHire hirepayment)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                LocalManifestModel.UpdateDeliveryVehicleHire(loginID, hirepayment);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteDeliveryVehicleHire(int deliveryVehicleHireID)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                LocalManifestModel.DeleteDeliveryVehicleHire(loginID, deliveryVehicleHireID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetDeliveryVehicleHireManifestByVehicleHireID(int deliveryVehicleHireID)
        {
            try
            {

                var bookings = Json(LocalManifestModel.GetDeliveryVehicleHireManifestByVehicleHireID(deliveryVehicleHireID), JsonRequestBehavior.AllowGet);
                return bookings;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region VEHICLE MANIFEST
        public string GetConsignmentsForManifest(int manifestId)
        {
            try
            {
                int branchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
                string bookingJSON = DataTableToJSON(ManifestModel.GetConsignmentsForManifest(branchID, manifestId));
                //   var bookings = Json(bookingJSON, JsonRequestBehavior.AllowGet);

                return bookingJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
             public string GetConsignmentsForManifestDelivery(int manifestId)
        {
            try
            {
                int branchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
                string bookingJSON = DataTableToJSON(LocalManifestModel.GetConsignmentsForManifestDelivery(branchID, manifestId));

                return bookingJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public void UpdateVehicleHireManifest(VehicleHireManifest manifest)
        {
            try
            {
                //VehicleHireManifest vhm = JsonConvert.DeserializeObject<VehicleHireManifest>(manifest);
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                // manifest.EstimatedDeliveryDate = System.DateTime.Now;//TEMPORARY ADDED
                ManifestModel.UpdateVehicleHireManifest(loginID, manifest);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateManifestArticleAndWeight(int totalArticle, float totalActualWeight, int manifestId, char type)
        {
            try
            {
                ManifestModel.UpdateManifestArticleAndWeight(totalArticle, totalActualWeight, manifestId, type);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int AddVehicleHireManifest(VehicleHireManifest manifest)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                int branchID = Convert.ToInt32(HttpContext.Session["BranchID"]);

                int manifestID = ManifestModel.AddVehicleHireManifest(loginID, branchID, manifest);
                return manifestID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void DeleteVehicleHireManifest(int manifestID)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                ManifestModel.DeleteVehicleHireManifest(loginID, manifestID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetVehicleHireManifestByManifestID(int manifestID)
        {
            try
            {

                return Json(ManifestModel.GetVehicleHireManifestByManifestID(manifestID), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetVehicleHireManifestByVehicleHireID(int vehicleHireID)
        {
            try
            {

                var bookings = Json(ManifestModel.GetVehicleHireManifestByVehicleHireID(vehicleHireID), JsonRequestBehavior.AllowGet);
                return bookings;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Manifest consignments

        public void AddManifestConsignment(List<ManifestConsignmentMapping> manifestConsignments)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);

                ManifestModel.AddManifestConsignment(loginID, manifestConsignments);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetManifestConsignments(int manifestID)
        {
            try
            {

                var manifestConsignments = Json(ManifestModel.GetManifestConsignments(manifestID), JsonRequestBehavior.AllowGet);
                return manifestConsignments;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region LOCAL VEHICLE HIRE PAYMENT
        public void UpdateLocalVehicleHirePayment(LocalVehicleHirePayment hirepayment)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                //  string loginID = UserInfo.EMPLOYEEID;
                LocalManifestModel.UpdateLocalVehicleHirePayment(loginID, hirepayment);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void AddLocalVehicleHirePayment(LocalVehicleHirePayment hirepayment)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                hirepayment.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
                hirepayment.HireDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                LocalManifestModel.AddLocalVehicleHirePayment(loginID, hirepayment);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void DeleteLocalVehicleHire(int vehicleHireID)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                LocalManifestModel.DeleteLocalVehicleHire(loginID, vehicleHireID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetLocalVehicleHirePayments()
        {
            try
            {
                int branchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
                return DataTableToJSON(LocalManifestModel.GetLocalVehicleHirePayments(branchID));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetLocalVehicleHirePaymentByID(int vehicleHireID)
        {
            try
            {

                var bookings = Json(LocalManifestModel.GetLocalVehicleHirePaymentByID(vehicleHireID), JsonRequestBehavior.AllowGet);
                return bookings;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region LOCAL VEHICLE MANIFEST
        public string GetConsignmentsForLocalManifest(int manifestId)
        {
            try
            {
                int branchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
                string bookingJSON = DataTableToJSON(LocalManifestModel.GetConsignmentsForLocalManifest(branchID, manifestId));
                //   var bookings = Json(bookingJSON, JsonRequestBehavior.AllowGet);

                return bookingJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateLocalVehicleHireManifest(LocalVehicleHireManifest manifest)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);

                LocalManifestModel.UpdateLocalVehicleHireManifest(loginID, manifest);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int AddLocalVehicleHireManifest(LocalVehicleHireManifest manifest)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                int branchID = Convert.ToInt32(HttpContext.Session["BranchID"]);

                int manifestID = LocalManifestModel.AddLocalVehicleHireManifest(loginID, branchID, manifest);
                return manifestID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void DeleteLocalVehicleHireManifest(int manifestID)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                LocalManifestModel.DeleteLocalVehicleHireManifest(loginID, manifestID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetLocalVehicleHireManifestByManifestID(int manifestID)
        {
            try
            {

                return Json(LocalManifestModel.GetLocalVehicleHireManifestByManifestID(manifestID), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetLocalVehicleHireManifestByVehicleHireID(int vehicleHireID)
        {
            try
            {

                var bookings = Json(LocalManifestModel.GetLocalVehicleHireManifestByVehicleHireID(vehicleHireID), JsonRequestBehavior.AllowGet);
                return bookings;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region LOCAL MANIFEST CONSIGNMENTS
        public void AddLocalManifestConsignment(List<LocalManifestConsignmentMapping> manifestConsignments)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);

                LocalManifestModel.AddLocalManifestConsignment(loginID, manifestConsignments);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetLocalManifestConsignments(int manifestID)
        {
            try
            {

                var manifestConsignments = Json(LocalManifestModel.GetLocalManifestConsignments(manifestID), JsonRequestBehavior.AllowGet);
                return manifestConsignments;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region UNLOAD MANIFEST CONSIGNMENTS
        public string GetManifestsForUnloadByToBranchID()
        {
            try
            {
                int branchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
                string bookingJSON = DataTableToJSON(ManifestModel.GetManifestsForUnloadByToBranchID(branchID));
                return bookingJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetUnloadManifestConsignments(int manifestID)
        {
            try
            {
                return DataTableToJSON(ManifestModel.GetUnloadManifestConsignments(manifestID));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetUnloadManifestConsignmentsByVehicleID(int VehicleID,string Created)
        {
            try
            {
                return DataTableToJSON(ManifestModel.GetUnloadManifestConsignmentsByVehicleID(VehicleID, Convert.ToInt32(HttpContext.Session["BranchID"]), DateTime.ParseExact(Created, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo)));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateManifestConsignmentStatus(List<ManifestConsignmentMapping> manifestConsignmentList)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                ManifestModel.UpdateManifestConsignmentStatus(loginID, manifestConsignmentList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Delivery MANIFEST CONSIGNMENTS

        public string GetManifestConsignmentsForDelivery()
        {
            try
            {
                int branchID = Convert.ToInt32(HttpContext.Session["BranchID"]);

                return DataTableToJSON(ManifestModel.GetManifestConsignmentsForDelivery(branchID));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateManifestConsignmentDeliveryStatus(ManifestConsignmentMapping manifestConsignment)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                ManifestModel.UpdateManifestConsignmentDeliveryStatus(loginID, manifestConsignment);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public JsonResult UploadFiles(HttpPostedFileBase myFile, long ConsignmentId, string FilePath)
        {
            try
            {
                string path = Server.MapPath("~/FileUpload/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string filename = Path.GetFileNameWithoutExtension(myFile.FileName) + "_" + Guid.NewGuid() + Path.GetExtension(myFile.FileName);
                path = path + filename;
                System.Drawing.Image image = System.Drawing.Image.FromStream(myFile.InputStream, true, true);
                if (image.Width > 200)
                {
                    Bitmap bits = ResizeImage(image, 200, 200);
                    bits.Save(path);
                }
                else
                    image.Save(path);
                //delete old file
                string prevFile = Server.MapPath("~/FileUpload/") + Path.GetFileName(FilePath);
                if (System.IO.File.Exists(prevFile))
                    System.IO.File.Delete(prevFile);
                ManifestModel.UpdateManifestConsignmentFile(ConsignmentId, "/FileUpload/" + filename);
                return Json("/FileUpload/" + filename);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int AddDeliveryVehicleHireManifest(DeliveryVehicleHireManifest manifest)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                int branchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
                int manifestID = LocalManifestModel.AddDeliveryVehicleHireManifest(loginID, branchID, manifest);
                return manifestID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetConsignmentsForDeliveryManifest(int manifestId)
        {
            try
            {
                int branchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
                string bookingJSON = DataTableToJSON(LocalManifestModel.GetConsignmentsForDeliveryManifest(branchID, manifestId));

                return bookingJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void AddDeliveryManifestConsignment(List<DeliveryManifestConsignmentMapping> manifestConsignments)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);

                LocalManifestModel.AddDeliveryManifestConsignment(loginID, manifestConsignments);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void UpdateDeliveryVehicleHireManifest(DeliveryVehicleHireManifest manifest)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                LocalManifestModel.UpdateDeliveryVehicleHireManifest(loginID, manifest);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public ActionResult GetPODData(int draw, int start, int length, string fromDate, string toDate, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            ToPayAndPaidCollectionDataTableData dataTableData = new ToPayAndPaidCollectionDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            var data = new Dictionary<int, List<ToPayAndPaidConsignment>>();

            data = ConsignmentModel.GetPODData(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
            dataTableData.data = data.FirstOrDefault().Value;

            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public void UpdatePODStatus(string status, int ManifestConsignmentID)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                ConsignmentModel.UpdatePODStatus(loginID, status, ManifestConsignmentID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Manifest Report
        public string GetManifestsForReport(int fromID, int toID, string createdDate)
        {
            try
            {
                System.Nullable<int> fromLoc = null;
                System.Nullable<int> toLoc = null;
                if (fromID > 0)
                    fromLoc = fromID;
                if (toID > 0)
                    toLoc = toID;
                IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
                System.Nullable<DateTime> dtcreated = null;
                if (createdDate.Length > 0)
                { dtcreated = Convert.ToDateTime(createdDate, provider); }
                return DataTableToJSON(ManifestModel.GetManifestsForReport(fromLoc, toLoc, dtcreated));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Consignment Billing
        public ActionResult GetConsignmentBilling(int draw, int start, int length, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            ConsignmentBillingDataTableData dataTableData = new ConsignmentBillingDataTableData();
            dataTableData.draw = draw;
            if (branchId == 0)
                branchId = null;
            var data = ConsignmentModel.GetConsignmentBillingIndex(branchId, start, length, search, sortColumn, sortDirection);
            dataTableData.data = DatatableHelper.ToList<ConsignmentBilling>(data.FirstOrDefault().Value);
            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllVendors()
        {
            try
            {
                var obj = ConsignmentModel.GetAllVendors();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        public int? SaveConsignmentBilling(ConsignmentBilling cnsgnBilling)
        {
            try
            {
                log.Info("SaveConsignmentBilling");
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                cnsgnBilling.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
                if (cnsgnBilling.BranchID == 0)
                    cnsgnBilling.BranchID = null;
                cnsgnBilling.BillingDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                int consignmentID = ConsignmentModel.SaveConsignmentBilling(loginID, cnsgnBilling);
                return consignmentID;
            }
            catch (Exception ex)
            {
                log.Info("Exception in SaveConsignmentBilling");
                log.Error(ex.Message);
                return null;
            }
        }
        public string GetConsignmentsForBilling(int VendorID, int ConsignmentBillingID)
        {
            try
            {
                int? branchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
                string bookingJSON = DataTableToJSON(ConsignmentModel.GetConsignmentsForBilling(branchID, VendorID, ConsignmentBillingID));

                return bookingJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetBillingConsignmentsWithDetails(int ConsignmentBillingID)
        {
            try
            {
                var data = ConsignmentModel.GetBillingConsignmentsWithDetails(ConsignmentBillingID);
                string bookingJSON = DataTableToJSON(data.Tables[0]);
                //DataTable dtBilling = data.Tables[1];
                //bookingJSON = bookingJSON + "#" + Convert.ToString(dtBilling.Rows[0][0]) + "#" + Convert.ToString(dtBilling.Rows[0][1]);
                return bookingJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetConsignmentBillingByID(int ConsignmentBillingId)
        {
            try
            {

                var bookings = Json(ConsignmentModel.GetConsignmentBillingByID(ConsignmentBillingId), JsonRequestBehavior.AllowGet);
                return bookings;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void UpdateVendorBalancePayment(int ConsignmentBillingID, float BalanceAmount)
        {
            try
            {

                ConsignmentModel.UpdateVendorBalancePayment(ConsignmentBillingID, BalanceAmount);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void SaveBillingConsignments(List<BillingConsignment> billingConsignments)
        {
            try
            {

                ConsignmentModel.SaveBillingConsignments(billingConsignments);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void UpdateConsignmentBillingTotalAmount(int ConsignmentBillingID, float totalAmount)
        {
            try
            {

                ConsignmentModel.UpdateConsignmentBillingTotalAmount(ConsignmentBillingID, totalAmount);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetBillingConsignments(int ConsignmentBillingID)
        {
            try
            {
                string bookingJSON = DataTableToJSON(ConsignmentModel.GetBillingConsignments(ConsignmentBillingID));
                return bookingJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetAllUnPaidBills()
        {
            try
            {
                var obj = ConsignmentModel.GetAllUnPaidBills();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        #endregion

        #region Common Methods
        public static Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
        {

            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }

            }
            return destImage;
        }


        #endregion

        public double GetBalanceAmount(int BranchID)
        {
            try
            {
                var balanceAmount=MasterModel.GetBalanceAmount(BranchID);
                return balanceAmount;
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        public double GetVehicleVendorBalanceAmount(int BranchID,int VehicleVendorID)
        {
            try
            {
                var balanceAmount = MasterModel.GetVehicleVendorBalanceAmount(BranchID,VehicleVendorID);
                return balanceAmount;
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        public JsonResult GetBranchBalanceData()
        {
            try
            {
                var obj = MasterModel.GetBranchBalanceData();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        public void UpdateManifestDeliveryStatus(int ManifestID,int Status)
        {
            try
            {
                ManifestModel.UpdateManifestDeliveryStatus(ManifestID, Status);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }


}