﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TMS.CustomModels;
using TMS.EFModel;
using TMS.Models;

namespace TMS.Controllers
{
    public class PaymentController : Controller
    {
        #region Views
        private const int TOTAL_ROWS = 0;
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public ActionResult ToPayAndPaidCollection()
        {
            ViewData.Add("BranchId", Convert.ToInt32(HttpContext.Session["BranchID"]));
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public ActionResult VehicleHireBalancePayment()
        {
            ViewData.Add("BranchId", Convert.ToInt32(HttpContext.Session["BranchID"]));
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public ActionResult OtherVendorBalancePayment()
        {
            ViewData.Add("BranchId", Convert.ToInt32(HttpContext.Session["BranchID"]));
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD})]
        public ActionResult Deposite()
        {
            ViewData.Add("BranchId", Convert.ToInt32(HttpContext.Session["BranchID"]));
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD })]
        public ActionResult VehicleVendorDeposite()
        {
            ViewData.Add("BranchId", Convert.ToInt32(HttpContext.Session["BranchID"]));
            return View();
        }
        #endregion

        #region To Pay Collection
        public ActionResult GetToPayAndPaidConsignments(int draw, int start, int length, string fromDate, string toDate, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            ToPayAndPaidCollectionDataTableData dataTableData = new ToPayAndPaidCollectionDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            var data = new Dictionary<int, List<ToPayAndPaidConsignment>>();

            data = PaymentModel.GetToPayConsignments(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
            dataTableData.data = data.FirstOrDefault().Value;

            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public string GetConsignmentPaymentDetails(int ConsignmentID)
        {
            string paymentJSON = JsonConvert.SerializeObject(PaymentModel.GetConsignmentPaymentDetails(ConsignmentID));
            return paymentJSON;
        }

        public void AddPaymentForConsignment(PaymentMaster PaymentMaster)
        {
            int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
            int PaymentId = PaymentModel.AddPaymentForConsignment(PaymentMaster, loginID);
        }
        #endregion

   
        #region Vehicle Hire Balance Payment
        public ActionResult GetVehicleHireBalancePayment(int draw, int start, int length, string fromDate, string toDate,int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            VehicleHireBalancePaymentDataTableData dataTableData = new VehicleHireBalancePaymentDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
          
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            var data = new Dictionary<int, List<VehicleHireBalancePayment>>();

            data = PaymentModel.GetVehicleHireBalancePayment(branchId, start, length, search, sortColumn, sortDirection);
            dataTableData.data = data.FirstOrDefault().Value;

            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public string GetManifestPaymentDetails(int VehicleHireID,string HireType)
        {
            string paymentJSON = JsonConvert.SerializeObject(PaymentModel.GetManifestPaymentDetails(VehicleHireID, HireType));
            return paymentJSON;
        }

        public void AddPaymentForManifest(BalancePaymentMaster VehicleHireBalancePayment)
        {
            int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
            int PaymentId = PaymentModel.AddPaymentForManifest(VehicleHireBalancePayment, loginID);
        }

        #endregion

        #region Deposite
        public ActionResult GetDeposites(int draw, int start, int length, string fromDate, string toDate, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            DepositeDataTableData dataTableData = new DepositeDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            var data = new Dictionary<int, List<DepositeMaster>>();

            data = PaymentModel.GetDeposites(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
            dataTableData.data = data.FirstOrDefault().Value;

            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public void AddDeposite(DepositeMaster DepositeMaster)
        {
            try
            {
                PaymentModel.AddDeposite(DepositeMaster);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteDeposite(int DepositeID)
        {
            try
            {
                PaymentModel.DeleteDeposite(DepositeID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Vehicle Vendor Deposite
        public ActionResult GetVehicleVendorDeposites(int draw, int start, int length, string fromDate, string toDate, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            VehicleVendorDepositeDataTableData dataTableData = new VehicleVendorDepositeDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            var data = new Dictionary<int, List<VehicleVendorDepositeMaster>>();

            data = PaymentModel.GetVehicleVendorDeposites(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
            dataTableData.data = data.FirstOrDefault().Value;

            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public void AddVehicleVendorDeposite(VehicleVendorDepositeMaster DepositeMaster)
        {
            try
            {
                PaymentModel.AddVehicleVendorDeposite(DepositeMaster);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteVehicleVendorDeposite(int DepositeID)
        {
            try
            {
                PaymentModel.DeleteVehicleVendorDeposite(DepositeID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        #region Other Vendor Balance Payment
        public ActionResult GetOtherVendorBalancePayment(int draw, int start, int length, string fromDate, string toDate, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            OtherVendorBalancePaymentDataTableData dataTableData = new OtherVendorBalancePaymentDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }

            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            var data = new Dictionary<int, List<VendorExpenseMaster>>();

            data = PaymentModel.GetOtherVendorBalancePayment(branchId, start, length, search, sortColumn, sortDirection);
            dataTableData.data = data.FirstOrDefault().Value;

            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public string GetVendorExpenseDetails(int VendorExpenseID)
        {
            string paymentJSON = JsonConvert.SerializeObject(PaymentModel.GetVendorExpenseDetails(VendorExpenseID));
            return paymentJSON;
        }

        public void AddPaymentForOtherVendor(OtherVendorBalancePaymentMaster OtherVendorBalancePayment)
        {
            int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
            int PaymentId = PaymentModel.AddPaymentForOtherVendor(OtherVendorBalancePayment, loginID);
        }

        #endregion
    }
}