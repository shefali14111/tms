﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TMS.CustomModels;
using TMS.Models;

namespace TMS.Controllers
{
    public class TMSController : Controller
    {
        private const int TOTAL_ROWS = 0;
        [Authorize]
        public ActionResult Home()
        {
            ViewData.Add("BranchId", Convert.ToInt32(HttpContext.Session["BranchID"]));
            return View();
        }
        public string GetPaymentTypeSalesData(int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
          
            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            PaymentTypeSalesDataTableData dataTableData = new PaymentTypeSalesDataTableData();
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
           
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            var data = new Dictionary<int, List<PaymentTypaSales>>();
            string yearVal = MasterModel.GetAttributeValueByKey("FINANCIAL_YEAR");
            dtfrom = DateTime.ParseExact(yearVal.Split('-')[0].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            dtto = DateTime.ParseExact(yearVal.Split('-')[1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            data = PaymentModel.GetPaymentTypeSalesData(branchId, 0, 20, "", 0, "", dtfrom, dtto);
            string consignmentJSON = JsonConvert.SerializeObject(data.Values.FirstOrDefault());
            return consignmentJSON;
        }
        public string GetPaymentTypeSalesDataDaily(int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
          
            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            PaymentTypeSalesDataTableData dataTableData = new PaymentTypeSalesDataTableData();
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;

            string fromDate = TimeZoneInfo.ConvertTime(DateTime.Now.Date, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time")).ToString("dd/MM/yyyy");
           string toDate = TimeZoneInfo.ConvertTime(DateTime.Now.Date, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time")).ToString("dd/MM/yyyy");

            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            var data = new Dictionary<int, List<PaymentTypaSales>>();

            data = PaymentModel.GetPaymentTypeSalesData(branchId, 0, 20, "", 0, "", dtfrom, dtto);
            string consignmentJSON = JsonConvert.SerializeObject(data.Values.FirstOrDefault());
            return consignmentJSON;
        }
    }
}