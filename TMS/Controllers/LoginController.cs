﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Mvc;
using System.Web.Security;
using TMS.EFModel;
using TMS.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace TMS.Controllers
{

    public class LoginController : Controller
    {
        
        // GET: Login
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            HttpContext.Session["UserID"] = null;
            HttpContext.Session["UserName"] = null;
            HttpContext.Session["BranchID"] = null;
            HttpContext.Session["ZoneID"] = null;
            HttpContext.Session["RoleID"] = null;
            HttpContext.Session["RoleName"] = null;
            HttpContext.Session["BranchName"] = null;
            return RedirectToAction("Login", "Login");
        }
        [HttpPost]
        public ActionResult LoadHome()
        {
            // dispayUserInfo();
            return Json(Url.Action("Home", "TMS"));
        }

       
        public ActionResult AccessDenied()
        {
            ViewBag.Message = "Access Denied";

            return View();
        }

        public JsonResult ValidateUser(string emailID, String userPword)
        {
            bool isValid = false;
            try
            {
                UserMaster loginUser = LoginModel.ValidateUser(emailID, userPword);
                if (loginUser != null)
                {
                    HttpContext.Session["UserID"] = loginUser.UserID;
                    HttpContext.Session["UserName"] = loginUser.UserName;
                    HttpContext.Session["BranchID"] = loginUser.BranchID;
                    HttpContext.Session["ZoneID"] = loginUser.ZoneID;
                    HttpContext.Session["RoleID"] = loginUser.RoleID;
                    HttpContext.Session["RoleName"] = loginUser.RoleName;
                    HttpContext.Session["BranchName"] = loginUser.BranchName;
                    if (loginUser.ReportingManagerID != null)
                        HttpContext.Session["ReportingManagerID"] = (int)loginUser.ReportingManagerID;

                   FormsAuthentication.SetAuthCookie(emailID, true);
                    isValid = true;
                 
                }
                
                return Json(isValid,JsonRequestBehavior.AllowGet );
            }
            catch (Exception ex)
            { throw ex; }
        }

       
    }
}