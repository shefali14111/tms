﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using TMS.CustomModels;
using TMS.Models;

namespace TMS.Controllers
{
    [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
    public class ReportController : Controller
    {
        private const int TOTAL_ROWS = 10;
        public ActionResult DeliveryStockReport()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }

        public ActionResult PendingStockReport()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }

        public ActionResult DamageStockReport()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }
        public ActionResult VehicleHirePaymentReport()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }
        public ActionResult ConsignmentBookingReport()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }
        public ActionResult ManifestReport()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }
        public ActionResult DestinationBranchReport()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }
        public ActionResult ToPayAndPaidCollectionReport()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }
        public ActionResult ToPayAndPaidPendingReport()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }
        public ActionResult PaymentTypeWiseSalesReport()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }
        public ActionResult VendorSalesReport()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }
        public ActionResult LRExperience()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }
        public ActionResult VendorVehicleReport()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }
        public ActionResult TransactionReport()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }
        public ActionResult OtherVendorReport()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }
        public ActionResult VendorSalesDetail(int PaymentTypeID, int VendorId, string FromDate, string ToDate)
        {
            ViewBag.VendorId = VendorId;
            ViewBag.PaymentTypeID = PaymentTypeID;
            ViewBag.VendorName = MasterModel.GetVendorByID(VendorId).FirstOrDefault().VendorName;
            ViewBag.PaymentType = MasterModel.GetPaymentTypeById(PaymentTypeID);
            ViewBag.FromDate = FromDate;
            ViewBag.ToDate = ToDate;
            return View();
        }
        public ActionResult MHELVendorSalesReport()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }
        public ActionResult MHELVendorSalesDetail(int PaymentTypeID, int VendorId, string FromDate, string ToDate)
        {
            ViewBag.VendorId = VendorId;
            ViewBag.PaymentTypeID = PaymentTypeID;
            ViewBag.VendorName = MasterModel.GetMHELVendorByID(VendorId).FirstOrDefault().VendorName;
            ViewBag.PaymentType = MasterModel.GetPaymentTypeById(PaymentTypeID);
            ViewBag.FromDate = FromDate;
            ViewBag.ToDate = ToDate;
            return View();
        }
        public ActionResult PaymentTypeSalesDetail(int PaymentTypeID, int BranchId, string FromDate, string ToDate)
        {
            ViewBag.BranchId = BranchId;
            ViewBag.PaymentTypeID = PaymentTypeID;
            ViewBag.BranchName = MasterModel.GetBranchNameByBranchId(BranchId);
            ViewBag.PaymentType = MasterModel.GetPaymentTypeById(PaymentTypeID);
            ViewBag.FromDate = FromDate;
            ViewBag.ToDate = ToDate;
            return View();
        }
        public ActionResult GSTReport()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }
        public ActionResult BilledConsignmentReport()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }
        public ActionResult UnBilledConsignmentReport()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }
        public ActionResult CancelledConsignmentReport()
        {
            ViewBag.BranchID = Convert.ToInt32(HttpContext.Session["BranchID"]);
            return View();
        }
        public ActionResult ExportDeliveryStockReport(string Fromdate, string Todate, int BranchId)
        {
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(Fromdate) && !String.IsNullOrEmpty(Todate))
            {
                dtfrom = Convert.ToDateTime(Fromdate, provider);
                dtto = Convert.ToDateTime(Todate, provider);
            }
            int? branchId = null;
            if (BranchId != 0) branchId = BranchId;
            DataTable dtDelivery = ManifestModel.GetDeliveryStockReport(branchId, dtfrom, dtto);
            ExportToExcel(dtDelivery, "DeliveryReport");
            return Json(new { successCode = "1" });
        }
        public ActionResult GetDeliveryStockData(int draw, int start, int length, string fromDate, string toDate, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            DeliveryStockDataTableData dataTableData = new DeliveryStockDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            var data = new Dictionary<int, List<DeliveryStockModel>>();
            if (dtfrom != null && dtto != null)
            {
                data = ManifestModel.GetDeliveryStockReportIndex(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
                dataTableData.data = data.FirstOrDefault().Value;
            }
            else
            {
                dataTableData.data = new List<DeliveryStockModel>();
            }
            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ExportPendingStockReport(string Fromdate, string Todate, int BranchId)
        {
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(Fromdate) && !String.IsNullOrEmpty(Todate))
            {
                dtfrom = Convert.ToDateTime(Fromdate, provider);
                dtto = Convert.ToDateTime(Todate, provider);
            }
            int? branchId = null;
            if (BranchId != 0) branchId = BranchId;
            DataTable dtPending = ManifestModel.GetPendingStockReport(branchId, dtfrom, dtto);
            ExportToExcel(dtPending, "PendingStockReport");
            return Json(new { successCode = "1" });
        }
        public ActionResult GetPendingStockData(int draw, int start, int length, string fromDate, string toDate, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            PendingStockDataTableData dataTableData = new PendingStockDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            var data = new Dictionary<int, List<PendingStockModel>>();

            data = ManifestModel.GetPendingStockReportIndex(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
            dataTableData.data = data.FirstOrDefault().Value;


            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportDamageStockReport(string Fromdate, string Todate, int BranchId)
        {
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(Fromdate) && !String.IsNullOrEmpty(Todate))
            {
                dtfrom = Convert.ToDateTime(Fromdate, provider);
                dtto = Convert.ToDateTime(Todate, provider);
            }
            int? branchId = null;
            if (BranchId != 0) branchId = BranchId;
            DataTable dtDamage = ManifestModel.GetDamageStockReport(branchId, dtfrom, dtto);
            ExportToExcel(dtDamage, "DamageStockReport");
            return Json(new { successCode = "1" });
        }
        public ActionResult GetDamageStockData(int draw, int start, int length, string fromDate, string toDate, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            DamageStockDataTableData dataTableData = new DamageStockDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;

            var data = new Dictionary<int, List<DamageStockModel>>();
            if (dtfrom != null && dtto != null)
            {
                data = ManifestModel.GetDamageStockReportIndex(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
                dataTableData.data = data.FirstOrDefault().Value;
            }
            else
            {
                dataTableData.data = new List<DamageStockModel>();
            }
            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportVehicleHireReport(string Fromdate, string Todate, int BranchId)
        {
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(Fromdate) && !String.IsNullOrEmpty(Todate))
            {
                dtfrom = Convert.ToDateTime(Fromdate, provider);
                dtto = Convert.ToDateTime(Todate, provider);
            }
            int? branchId = null;
            if (BranchId != 0) branchId = BranchId;
            DataTable dtDamage = ManifestModel.GetVehicleHirePaymentReport(branchId, dtfrom, dtto);
            ExportToExcel(dtDamage, "VehicleHireReport");
            return Json(new { successCode = "1" });
        }
        public ActionResult GetVehicleHireData(int draw, int start, int length, string fromDate, string toDate, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            VehicleHireReportDataTableData dataTableData = new VehicleHireReportDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;

            var data = new Dictionary<int, List<VehicleHirePaymentModel>>();
            if (dtfrom != null && dtto != null)
            {
                data = ManifestModel.GetVehicleHirePaymentReportIndex(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
                dataTableData.data = data.FirstOrDefault().Value;
            }
            else
            {
                dataTableData.data = new List<VehicleHirePaymentModel>();
            }
            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportConsignmentBookingReport(string Fromdate, string Todate, int BranchId)
        {
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(Fromdate) && !String.IsNullOrEmpty(Todate))
            {
                dtfrom = Convert.ToDateTime(Fromdate, provider);
                dtto = Convert.ToDateTime(Todate, provider);
            }
            int? branchId = null;
            if (BranchId != 0) branchId = BranchId;
            DataTable dtDamage = ManifestModel.GetConsignmentBookingReport(branchId, dtfrom, dtto);
            ExportToExcel(dtDamage, "ConsignmentBookingReport");
            return Json(new { successCode = "1" });
        }
        public ActionResult GetConsignmentBookingData(int draw, int start, int length, string fromDate, string toDate, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            ConsignmentBookingReportDataTableData dataTableData = new ConsignmentBookingReportDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;

            var data = new Dictionary<int, List<ConsignmentBookingReportModel>>();
            if (dtfrom != null && dtto != null)
            {
                data = ManifestModel.GetConsignmentBookingReportIndex(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
                dataTableData.data = data.FirstOrDefault().Value;
            }
            else
            {
                dataTableData.data = new List<ConsignmentBookingReportModel>();
            }

            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetManifestData(int draw, int start, int length, string from, string to, string created, string type, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            ManifestReportDataTableData dataTableData = new ManifestReportDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtCreated = null;
            if (!String.IsNullOrEmpty(created))
            {
                dtCreated = Convert.ToDateTime(created, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            var data = ManifestModel.GetManifestReportIndex(branchId, start, length, search, sortColumn, sortDirection, dtCreated, from, to, type);
            dataTableData.data = data.FirstOrDefault().Value;
            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public string ViewManifestReport(int ManifestID, string Type)
        {
            string manifestJSON = JsonConvert.SerializeObject(ManifestModel.ManifestDetailReportModel(ManifestID, Type));
            return manifestJSON;
        }
        public string GetConsignmentsForManifestReport(int ManifestID, string Type)
        {
            try
            {
                string bookingJSON = DataTableToJSON(ManifestModel.GetConsignmentsForManifestReport(ManifestID, Type));
                return bookingJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public ActionResult GetDeliveryBranchData(int draw, int start, int length, string fromDate, string toDate, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            DeliveryBranchDataTableData dataTableData = new DeliveryBranchDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            var data = new Dictionary<int, List<DeliveryBranchModel>>();
            if (branchId != null)
            {
                data = ManifestModel.GetDeliveryBranchReportIndex(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
                dataTableData.data = data.FirstOrDefault().Value;
            }
            else
            {
                dataTableData.data = new List<DeliveryBranchModel>();
            }

            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public string GetConsignmentsForDestination(int DestinationID, string FromDate, string ToDate)
        {
            try
            {
                IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
                System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
                if (!String.IsNullOrEmpty(FromDate) && !String.IsNullOrEmpty(ToDate))
                {
                    dtfrom = Convert.ToDateTime(FromDate, provider);
                    dtto = Convert.ToDateTime(ToDate, provider);
                }
                string bookingJSON = DataTableToJSON(ConsignmentModel.GetConsignmentsForDestination(DestinationID, dtfrom, dtto));
                return bookingJSON;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult GetToPayAndPaidPendingData(int draw, int start, int length, string fromDate, string toDate, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            ToPayAndPaidCollectionDataTableData dataTableData = new ToPayAndPaidCollectionDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            var data = new Dictionary<int, List<ToPayAndPaidConsignment>>();

            data = PaymentModel.GetToPayConsignments(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
            dataTableData.data = data.FirstOrDefault().Value;

            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportToPayAndPaidPendingReport(string Fromdate, string Todate, int BranchId)
        {
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(Fromdate) && !String.IsNullOrEmpty(Todate))
            {
                dtfrom = Convert.ToDateTime(Fromdate, provider);
                dtto = Convert.ToDateTime(Todate, provider);
            }
            int? branchId = null;
            if (BranchId != 0) branchId = BranchId;
            DataTable dtPaymentPending = PaymentModel.GetToPayConsignmentsReport(branchId, dtfrom, dtto);
            ExportToExcel(dtPaymentPending, "ToPayAndPaidPendingReport");
            return Json(new { successCode = "1" });
        }

        public ActionResult GetToPayAndPaidCollectionData(int draw, int start, int length, string fromDate, string toDate, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            ToPayAndPaidCollectionDataTableData dataTableData = new ToPayAndPaidCollectionDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            var data = new Dictionary<int, List<ToPayAndPaidConsignment>>();

            data = PaymentModel.GetPaymentForToPayAndPaid(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
            dataTableData.data = data.FirstOrDefault().Value;

            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportToPayAndPaidCollectionReport(string Fromdate, string Todate, int BranchId)
        {
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(Fromdate) && !String.IsNullOrEmpty(Todate))
            {
                dtfrom = Convert.ToDateTime(Fromdate, provider);
                dtto = Convert.ToDateTime(Todate, provider);
            }
            int? branchId = null;
            if (BranchId != 0) branchId = BranchId;
            DataTable dtPaymentPending = PaymentModel.GetPaymentConsignmentReport(branchId, dtfrom, dtto);
            ExportToExcel(dtPaymentPending, "ToPayAndPaidPendingReport");
            return Json(new { successCode = "1" });
        }

        public ActionResult GetPaymentTypeSalesData(int draw, int start, int length, string fromDate, string toDate, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            PaymentTypeSalesDataTableData dataTableData = new PaymentTypeSalesDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            var data = new Dictionary<int, List<PaymentTypaSales>>();
            //if (branchId != null)
            //{
                data = PaymentModel.GetPaymentTypeSalesData(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
                dataTableData.data = data.FirstOrDefault().Value;
            //}
            //else
            //{
            //    dataTableData.data = new List<PaymentTypaSales>();
            //}

            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetPaymentTypeSalesDataDaily(int draw, int start, int length, string fromDate, string toDate, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            PaymentTypeSalesDataTableData dataTableData = new PaymentTypeSalesDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;

            fromDate = TimeZoneInfo.ConvertTime(DateTime.Now.Date, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time")).ToString("dd/MM/yyyy");
            toDate = TimeZoneInfo.ConvertTime(DateTime.Now.Date, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time")).ToString("dd/MM/yyyy");

            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            var data = new Dictionary<int, List<PaymentTypaSales>>();
            if (branchId != null)
            {
                data = PaymentModel.GetPaymentTypeSalesData(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
                dataTableData.data = data.FirstOrDefault().Value;
            }
            else
            {
                dataTableData.data = new List<PaymentTypaSales>();
            }

            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetPaymentTypeSalesDetails(int draw, int start, int length, string fromDate, string toDate, int? branchId, int? paymentTypeId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            ToPayAndPaidCollectionDataTableData dataTableData = new ToPayAndPaidCollectionDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            var data = new Dictionary<int, List<ToPayAndPaidConsignment>>();
            if (paymentTypeId != null)
            {
                data = ConsignmentModel.GetPaymentWiseConsignments(branchId, start, length, search, sortColumn, sortDirection, paymentTypeId, dtfrom, dtto);
                dataTableData.data = data.FirstOrDefault().Value;

                dataTableData.recordsFiltered = data.FirstOrDefault().Key;
                dataTableData.recordsTotal = data.FirstOrDefault().Key;
            }
            else
            {
                dataTableData.data = new List<ToPayAndPaidConsignment>();
            }

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportPaymentTypeSalesDetail(string Fromdate, string Todate, int BranchId, int PaymentTypeId)
        {
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(Fromdate) && !String.IsNullOrEmpty(Todate))
            {
                dtfrom = Convert.ToDateTime(Fromdate, provider);
                dtto = Convert.ToDateTime(Todate, provider);
            }
            int? branchId = null;
            if (BranchId != 0) branchId = BranchId;
            DataTable dtDelivery = ConsignmentModel.GetPaymentWiseConsignmentsReport(branchId, dtfrom, dtto, PaymentTypeId);
            ExportToExcel(dtDelivery, "PaymentTypeSalesDetail");
            return Json(new { successCode = "1" });
        }
        public ActionResult ExportGSTReport(string Fromdate, string Todate, int BranchId)
        {
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(Fromdate) && !String.IsNullOrEmpty(Todate))
            {
                dtfrom = Convert.ToDateTime(Fromdate, provider);
                dtto = Convert.ToDateTime(Todate, provider);
            }
            int? branchId = null;
            if (BranchId != 0) branchId = BranchId;
            DataTable dtDamage = ConsignmentModel.GetGSTData(branchId, dtfrom, dtto);
            ExportToExcel(dtDamage, "GSTReport");
            return Json(new { successCode = "1" });
        }
        public ActionResult GetGSTData(int draw, int start, int length, string fromDate, string toDate, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            GSTDataTableData dataTableData = new GSTDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;

            var data = new Dictionary<int, List<ToPayAndPaidConsignment>>();
            if (branchId != null)
            {
                data = ConsignmentModel.GetGSTData(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
                dataTableData.data = data.FirstOrDefault().Value;
            }
            else
            {
                dataTableData.data = new List<ToPayAndPaidConsignment>();
            }
            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportLRExperiencegReport(string Fromdate, string Todate, int BranchId)
        {
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            int? branchId = null;
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(Fromdate) && !String.IsNullOrEmpty(Todate))
            {
                dtfrom = Convert.ToDateTime(Fromdate, provider);
                dtto = Convert.ToDateTime(Todate, provider);
            }
            if (BranchId != 0) branchId = BranchId;
            DataTable dtDamage = ManifestModel.GetLRExperienceReport(branchId, dtfrom, dtto);
            ExportToExcel(dtDamage, "LRWiseExperienceReport");
            return Json(new { successCode = "1" });
        }
        public ActionResult GetLRExperiencegData(int draw, int start, int length, int? branchId, string fromDate, string toDate)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            LRExperienceReportDataTableData dataTableData = new LRExperienceReportDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
          
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            var data = new Dictionary<int, List<LRExperienceReportModel>>();
            if (branchId!=null)
            {
                data = ManifestModel.GetLRExperienceReportIndex(branchId, start, length, search, sortColumn, sortDirection,dtfrom,dtto);
                dataTableData.data = data.FirstOrDefault().Value;
            }
            else
            {
                dataTableData.data = new List<LRExperienceReportModel>();
            }

            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;
            var jsonResult = Json(dataTableData, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult ExportVendorVehicleReport(string Fromdate, string Todate, int BranchId, int VendorId)
        {
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            int? branchId = null;
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(Fromdate) && !String.IsNullOrEmpty(Todate))
            {
                dtfrom = Convert.ToDateTime(Fromdate, provider);
                dtto = Convert.ToDateTime(Todate, provider);
            }
            if (BranchId != 0) branchId = BranchId;
            DataTable dtDamage = ManifestModel.GetVendorVehicleReport(branchId, dtfrom, dtto, VendorId);
            ExportToExcel(dtDamage, "VendorVehicleReport");
            return Json(new { successCode = "1" });
        }
        public ActionResult GetVendorVehicleData(int draw, int start, int length, int? branchId,int? vendorId, string fromDate, string toDate)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            VendorVehicleReportDataTableData dataTableData = new VendorVehicleReportDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);

            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            var data = new Dictionary<int, List<VendorVehicleModel>>();
            if (vendorId != null)
            {
                data = ManifestModel.GetVendorVehicleReportIndex(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto,vendorId.Value);
                dataTableData.data = data.FirstOrDefault().Value;
            }
            else
            {
                dataTableData.data = new List<VendorVehicleModel>();
            }

            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        #region Common Methods
        public string DataTableToJSON(DataTable table)
        {
            string JSONString = string.Empty;

            JSONString = JsonConvert.SerializeObject(table);
            return JSONString;
        }
        public void ExportToExcel(DataTable dt, string FileName)
        {
            if (dt.Rows.Count > 0)
            {
                if (System.Web.HttpContext.Current.Response.RedirectLocation == null)
                {
                    string filename = FileName + ".xls";
                    System.IO.StringWriter tw = new System.IO.StringWriter();
                    System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
                    DataGrid dgGrid = new DataGrid();
                    dgGrid.DataSource = dt;
                    dgGrid.DataBind();
                    //Get the HTML for the control.
                    dgGrid.RenderControl(hw);
                    //Write the HTML back to the browser.
                    //Response.ContentType = application/vnd.ms-excel;                                         
                    HttpContext.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "");
                    HttpContext.Response.ContentType = "application/vnd.ms-excel";
                    HttpContext.Response.Write(tw.ToString());
                    HttpContext.Response.Flush();
                    HttpContext.Response.End();

                }

            }

        }

        #region Vendor Sales Report
        public ActionResult GetPaymentTypeVendorSalesData(int draw, int start, int length, string fromDate, string toDate, int? vendorId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            PaymentTypeSalesDataTableData dataTableData = new PaymentTypeSalesDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
           
            var data = new Dictionary<int, List<PaymentTypaSales>>();
            if (vendorId != null)
            {
                data = PaymentModel.GetPaymentTypeVendorSalesData(vendorId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
                dataTableData.data = data.FirstOrDefault().Value;
            }
            else
            {
                dataTableData.data = new List<PaymentTypaSales>();
            }

            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetVendorSalesDetails(int draw, int start, int length, string fromDate, string toDate, int? vendorId, int? paymentTypeId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            ToPayAndPaidCollectionDataTableData dataTableData = new ToPayAndPaidCollectionDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
           
            var data = new Dictionary<int, List<ToPayAndPaidConsignment>>();
            if (vendorId != null)
            {
                data = ConsignmentModel.GetPaymentWiseVendorConsignments(vendorId, start, length, search, sortColumn, sortDirection, paymentTypeId, dtfrom, dtto);
                dataTableData.data = data.FirstOrDefault().Value;

                dataTableData.recordsFiltered = data.FirstOrDefault().Key;
                dataTableData.recordsTotal = data.FirstOrDefault().Key;
            }
            else
            {
                dataTableData.data = new List<ToPayAndPaidConsignment>();
            }

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportPaymentTypeVendorSalesDetail(string Fromdate, string Todate, int VendorId, int PaymentTypeId)
        {
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(Fromdate) && !String.IsNullOrEmpty(Todate))
            {
                dtfrom = Convert.ToDateTime(Fromdate, provider);
                dtto = Convert.ToDateTime(Todate, provider);
            }
           
            DataTable dtDelivery = ConsignmentModel.GetPaymentWiseVendorConsignmentsReport(VendorId, dtfrom, dtto, PaymentTypeId);
            ExportToExcel(dtDelivery, "VendorSalesDetail");
            return Json(new { successCode = "1" });
        }
        #endregion
        #region MHEL Vendor Sales Report
        public ActionResult GetPaymentTypeMHELVendorSalesData(int draw, int start, int length, string fromDate, string toDate, int? vendorId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            PaymentTypeSalesDataTableData dataTableData = new PaymentTypeSalesDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }

            var data = new Dictionary<int, List<PaymentTypaSales>>();
            if (vendorId != null)
            {
                data = PaymentModel.GetPaymentTypeMHELVendorSalesData(vendorId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
                dataTableData.data = data.FirstOrDefault().Value;
            }
            else
            {
                dataTableData.data = new List<PaymentTypaSales>();
            }

            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetMHELVendorSalesDetails(int draw, int start, int length, string fromDate, string toDate, int? vendorId, int? paymentTypeId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            ToPayAndPaidCollectionDataTableData dataTableData = new ToPayAndPaidCollectionDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }

            var data = new Dictionary<int, List<ToPayAndPaidConsignment>>();
            if (vendorId != null)
            {
                data = ConsignmentModel.GetPaymentWiseMHELVendorConsignments(vendorId, start, length, search, sortColumn, sortDirection, paymentTypeId, dtfrom, dtto);
                dataTableData.data = data.FirstOrDefault().Value;

                dataTableData.recordsFiltered = data.FirstOrDefault().Key;
                dataTableData.recordsTotal = data.FirstOrDefault().Key;
            }
            else
            {
                dataTableData.data = new List<ToPayAndPaidConsignment>();
            }

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportPaymentTypeMHELVendorSalesDetail(string Fromdate, string Todate, int VendorId, int PaymentTypeId)
        {
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(Fromdate) && !String.IsNullOrEmpty(Todate))
            {
                dtfrom = Convert.ToDateTime(Fromdate, provider);
                dtto = Convert.ToDateTime(Todate, provider);
            }

            DataTable dtDelivery = ConsignmentModel.GetPaymentWiseMHELVendorConsignmentsReport(VendorId, dtfrom, dtto, PaymentTypeId);
            ExportToExcel(dtDelivery, "MHELVendorSalesDetail");
            return Json(new { successCode = "1" });
        }
        #endregion

        #region Billed Consignment Report
        public ActionResult ExportBilledConsignmentBookingReport(string Fromdate, string Todate, int BranchId)
        {
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(Fromdate) && !String.IsNullOrEmpty(Todate))
            {
                dtfrom = Convert.ToDateTime(Fromdate, provider);
                dtto = Convert.ToDateTime(Todate, provider);
            }
            int? branchId = null;
            if (BranchId != 0) branchId = BranchId;
            DataTable dtDamage = ConsignmentModel.GetBilledConsignmentBookingReport(branchId, dtfrom, dtto);
            ExportToExcel(dtDamage, "ConsignmentBookingReport");
            return Json(new { successCode = "1" });
        }
        public ActionResult GetBilledConsignmentBookingData(int draw, int start, int length, string fromDate, string toDate, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            ConsignmentBookingReportDataTableData dataTableData = new ConsignmentBookingReportDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;

            var data = new Dictionary<int, List<ConsignmentBookingReportModel>>();
            if (dtfrom != null && dtto != null)
            {
                data = ConsignmentModel.GetBilledConsignmentBookingReportIndex(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
                dataTableData.data = data.FirstOrDefault().Value;
            }
            else
            {
                dataTableData.data = new List<ConsignmentBookingReportModel>();
            }

            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Billed Consignment Report
        public ActionResult ExportUnBilledConsignmentBookingReport(string Fromdate, string Todate, int BranchId)
        {
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(Fromdate) && !String.IsNullOrEmpty(Todate))
            {
                dtfrom = Convert.ToDateTime(Fromdate, provider);
                dtto = Convert.ToDateTime(Todate, provider);
            }
            int? branchId = null;
            if (BranchId != 0) branchId = BranchId;
            DataTable dtDamage = ConsignmentModel.GetUnbilledConsignmentBookingReport(branchId, dtfrom, dtto);
            ExportToExcel(dtDamage, "ConsignmentBookingReport");
            return Json(new { successCode = "1" });
        }
        public ActionResult GetUnBilledConsignmentBookingData(int draw, int start, int length, string fromDate, string toDate, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            ConsignmentBookingReportDataTableData dataTableData = new ConsignmentBookingReportDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;

            var data = new Dictionary<int, List<ConsignmentBookingReportModel>>();
            if (dtfrom != null && dtto != null)
            {
                data = ConsignmentModel.GetUnbilledConsignmentBookingReportIndex(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
                dataTableData.data = data.FirstOrDefault().Value;
            }
            else
            {
                dataTableData.data = new List<ConsignmentBookingReportModel>();
            }

            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Cancelled Consignment Report
        public ActionResult ExportCancelledConsignmentBookingReport(string Fromdate, string Todate, int BranchId)
        {
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(Fromdate) && !String.IsNullOrEmpty(Todate))
            {
                dtfrom = Convert.ToDateTime(Fromdate, provider);
                dtto = Convert.ToDateTime(Todate, provider);
            }
            int? branchId = null;
            if (BranchId != 0) branchId = BranchId;
            DataTable dtDamage = ConsignmentModel.GetCancelledConsignmentBookingReport(branchId, dtfrom, dtto);
            ExportToExcel(dtDamage, "ConsignmentBookingReport");
            return Json(new { successCode = "1" });
        }
        public ActionResult GetCancelledConsignmentBookingData(int draw, int start, int length, string fromDate, string toDate, int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            ConsignmentBookingReportDataTableData dataTableData = new ConsignmentBookingReportDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;

            var data = new Dictionary<int, List<ConsignmentBookingReportModel>>();
            if (dtfrom != null && dtto != null)
            {
                data = ConsignmentModel.GetCancelledConsignmentBookingReportIndex(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
                dataTableData.data = data.FirstOrDefault().Value;
            }
            else
            {
                dataTableData.data = new List<ConsignmentBookingReportModel>();
            }

            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Transaction Report
        public ActionResult GetTransactionData(int draw, int start, int length, string fromDate, string toDate,int? branchId)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            TransactionDataTableData dataTableData = new TransactionDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }

            var data = new Dictionary<int, List<TransactionModel>>();
            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            if (branchId != null)
            {
                data = PaymentModel.GetTransactionReportIndex(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto);
                dataTableData.data = data.FirstOrDefault().Value;
            }
            else
            {
                dataTableData.data = new List<TransactionModel>();
            }
            if(data.Count == 0)
                dataTableData.data = new List<TransactionModel>();
            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData , JsonRequestBehavior.AllowGet);
        }
      
        public ActionResult ExportTransactionDetail(string Fromdate, string Todate, int BranchId)
        {
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(Fromdate) && !String.IsNullOrEmpty(Todate))
            {
                dtfrom = Convert.ToDateTime(Fromdate, provider);
                dtto = Convert.ToDateTime(Todate, provider);
            }

            DataTable dtDelivery = PaymentModel.GetTransactionReport(BranchId,dtfrom, dtto);
            ExportToExcel(dtDelivery, "TransactionReport");
            return Json(new { successCode = "1" });
        }
        #endregion

        #region Other Vendor Expense
        public ActionResult ExportOtherVendorReport(string Fromdate, string Todate, int BranchId, int VendorId)
        {
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
            int? branchId = null;
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(Fromdate) && !String.IsNullOrEmpty(Todate))
            {
                dtfrom = Convert.ToDateTime(Fromdate, provider);
                dtto = Convert.ToDateTime(Todate, provider);
            }
            if (BranchId != 0) branchId = BranchId;
            DataTable dtDamage = ManifestModel.GetOtherVendorExpenseReport(branchId, dtfrom, dtto, VendorId);
            ExportToExcel(dtDamage, "OtherVendorReport");
            return Json(new { successCode = "1" });
        }
        public ActionResult GetOtherVendorData(int draw, int start, int length, int? branchId, int? vendorId, string fromDate, string toDate)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            OtherVendorReportDataTableData dataTableData = new OtherVendorReportDataTableData();
            dataTableData.draw = draw;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);

            if (Convert.ToInt32(HttpContext.Session["BranchID"]) != 0)
                branchId = Convert.ToInt32(HttpContext.Session["BranchID"]);
            if (branchId == 0)
                branchId = null;
            System.Nullable<DateTime> dtfrom = null; System.Nullable<DateTime> dtto = null;
            if (!String.IsNullOrEmpty(fromDate) && !String.IsNullOrEmpty(toDate))
            {
                dtfrom = Convert.ToDateTime(fromDate, provider);
                dtto = Convert.ToDateTime(toDate, provider);
            }
            var data = new Dictionary<int, List<OtherVendorReportModel>>();
            if (vendorId != null)
            {
                data = ManifestModel.GetOtherVendorReportIndex(branchId, start, length, search, sortColumn, sortDirection, dtfrom, dtto, vendorId.Value);
                dataTableData.data = data.FirstOrDefault().Value;
            }
            else
            {
                dataTableData.data = new List<OtherVendorReportModel>();
            }

            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion
    }
}