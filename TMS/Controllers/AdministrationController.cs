﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TMS.CustomModels;
using TMS.EFModel;
using TMS.Models;

namespace TMS.Controllers
{

    [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN,USERROLE.SUPERADMIN })]
    public class AdministrationController : Controller
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const int TOTAL_ROWS = 0;
        // GET: Administration Views
        public ActionResult UserMaster()
        {
            return View();
        }
        public ActionResult UserRoles()
        {
            return View();
        }
        public ActionResult Packaging()
        {
            return View();
        }
        public ActionResult ODA()
        {
            return View();
        }
        public ActionResult PaymentType()
        {
            return View();
        }
        public ActionResult VehicleHirePaymentType()
        {
            return View();
        }
        public ActionResult CargoType()
        {
            return View();
        }
        public ActionResult ChargeType()
        {
            return View();
        }
        public ActionResult Account()
        {
            return View();
        }
        public ActionResult Attribute()
        {
            return View();
        }
        public ActionResult Goods()
        {
            return View();
        }
        public ActionResult Branches()
        {
            return View();
        }
        public ActionResult OtherBranches()
        {
            return View();
        }
        public ActionResult OtherVendors()
        {
            return View();
        }
        public ActionResult Vendors()
        {
            return View();
        }
        public ActionResult VehicleVendors()
        {
            return View();
        }
        public ActionResult Vehicles()
        {
            return View();
        }
        public ActionResult PinCodes()
        {
            return View();
        }
        public ActionResult FOV()
        {
            return View();
        }
        public ActionResult BranchRunningNo()
        {
            return View();
        }
        public ActionResult Company()
        {
            return View();
        }
        public ActionResult Tax()
        {
            return View();
        }
        public ActionResult Zone()
        {
            return View();
        }
        public ActionResult PartNo()
        {
            return View();
        }
        // User Master
        public ActionResult GetUserData(int draw, int start, int length)
        {
            try
            {

                log.Info("GetUserData");
                string search = Request.QueryString["search[value]"];
                int sortColumn = 2;
                string sortDirection = "desc";
                if (length == -1)
                {
                    length = TOTAL_ROWS;
                }

                if (Request.QueryString["order[0][column]"] != null)
                {
                    sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
                }
                if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
                {
                    sortDirection = Request.QueryString["order[0][dir]"];
                }

                UserDataTableData dataTableData = new UserDataTableData();
                dataTableData.draw = draw;

                var data = MasterModel.GetUserIndex(start, length, search, sortColumn, sortDirection);
                dataTableData.data = data.FirstOrDefault().Value;
                dataTableData.recordsFiltered = data.FirstOrDefault().Key;
                dataTableData.recordsTotal = data.FirstOrDefault().Key;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Info("Exception in GetUserData");
                log.Error(ex.Message);
                return null;
            }
        }
        public JsonResult GetAdminUsers()
        {
            try
            {
                var obj = MasterModel.GetAdminUsers();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN,USERROLE.SUPERADMIN })]
        public JsonResult AddUser(UserMaster user)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                MasterModel.AddUser(user, loginID);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteUser(int UserID)
        {
            try
            {
                MasterModel.DeleteUser(UserID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult GetUserByID(int UserID)
        {
            try
            {

                var voucher = Json(MasterModel.GetUserByID(UserID), JsonRequestBehavior.AllowGet);
                return voucher;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void UpdateUser(UserMaster user)
        {
            try
            {
                MasterModel.UpdateUser(user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Zone
        public JsonResult GetZones()
        {
            try
            {
                var obj = MasterModel.GetZones();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        //Role
        public JsonResult GetRoles()
        {
            try
            {
                var obj = MasterModel.GetRoles();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddRole(RoleMaster role)
        {
            try
            {
                MasterModel.AddRole(role);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteRole(int RoleID)
        {
            try
            {
                MasterModel.DeleteRole(RoleID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult UpdateRole(RoleMaster role)
        {
            try
            {
                MasterModel.UpdateRole(role);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }

        //Packaging
        public JsonResult GetPackaging()
        {
            try
            {
                var obj = MasterModel.GetPackagings();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddPackaging(PackagingMaster package)
        {
            try
            {
                MasterModel.AddPackaging(package);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeletePackaging(int PackagingID)
        {
            try
            {
                MasterModel.DeletePackaging(PackagingID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult UpdatePackaging(PackagingMaster package)
        {
            try
            {
                MasterModel.UpdatePackaging(package);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }

        //ODA
        public JsonResult GetODA()
        {
            try
            {
                var obj = MasterModel.GetODA();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddODA(ODAMaster oda)
        {
            try
            {
                MasterModel.AddODA(oda);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteODA(int ODAID)
        {
            try
            {
                MasterModel.DeleteODA(ODAID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult UpdateODA(ODAMaster oda)
        {
            try
            {
                MasterModel.UpdateODA(oda);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        //Payment Type
        public JsonResult GetPaymentType()
        {
            try
            {
                var obj = MasterModel.GetPaymentTypes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddPaymentType(PaymentTypeMaster payment)
        {
            try
            {
                MasterModel.AddPaymentType(payment);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeletePaymentType(int PaymentTypeID)
        {
            try
            {
                MasterModel.DeletePaymentType(PaymentTypeID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult UpdatePaymentType(PaymentTypeMaster payment)
        {
            try
            {
                MasterModel.UpdatePaymentType(payment);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }

        //Vehicle Hire Payment Type
        public JsonResult GetVehicleHirePaymentType()
        {
            try
            {
                var obj = MasterModel.GetVehicleHirePaymentType();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddVehicleHirePaymentType(VehicleHirePaymentTypeMaster payment)
        {
            try
            {
                MasterModel.AddVehicleHirePaymentType(payment);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteVehicleHirePaymentType(int PaymentTypeID)
        {
            try
            {
                MasterModel.DeleteVehicleHirePaymentType(PaymentTypeID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult UpdateVehicleHirePaymentType(VehicleHirePaymentTypeMaster payment)
        {
            try
            {
                MasterModel.UpdateVehicleHirePaymentType(payment);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }

        //Cargo Type
        public JsonResult GetCargoType()
        {
            try
            {
                var obj = MasterModel.GetCargoType();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddCargoType(CargoTypeMaster cargo)
        {
            try
            {
                MasterModel.AddCargoType(cargo);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteCargoType(int CargoTypeID)
        {
            try
            {
                MasterModel.DeleteCargoType(CargoTypeID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult UpdateCargoType(CargoTypeMaster cargo)
        {
            try
            {
                MasterModel.UpdateCargoType(cargo);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }

        //Charge Type
        public JsonResult GetChargeType()
        {
            try
            {
                var obj = MasterModel.GetChargeType();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddChargeType(ChargeTypeMaster charge)
        {
            try
            {
                MasterModel.AddChargeType(charge);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteChargeType(int ChargeTypeID)
        {
            try
            {
                MasterModel.DeleteChargeType(ChargeTypeID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult UpdateChargeType(ChargeTypeMaster charge)
        {
            try
            {
                MasterModel.UpdateChargeType(charge);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }

        //Account
        public JsonResult GetAccount()
        {
            try
            {
                var obj = MasterModel.GetAccount();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddAccount(AccountMaster acct)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                MasterModel.AddAccount(acct, loginID);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteAccount(int AccountID)
        {
            try
            {
                MasterModel.DeleteAccount(AccountID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult UpdateAccount(AccountMaster acct)
        {
            try
            {
                MasterModel.UpdateAccount(acct);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }

        //Attribute
        public JsonResult GetAttribute()
        {
            try
            {
                var obj = MasterModel.GetAttribute();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddAttribute(AttributeMaster acct)
        {
            try
            {
                acct.Key = acct.Name.Replace(' ', '_').ToUpper();
                MasterModel.AddAttribute(acct);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteAttribute(int AttributeID)
        {
            try
            {
                MasterModel.DeleteAttribute(AttributeID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult UpdateAttribute(AttributeMaster acct)
        {
            try
            {
                MasterModel.UpdateAttribute(acct);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }

        //Goods
        public JsonResult GetGoods()
        {
            try
            {
                var obj = MasterModel.GetGoods();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddGoods(GoodsMaster goodsdata)
        {
            try
            {
                MasterModel.AddGoods(goodsdata);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteGoods(int GoodsID)
        {
            try
            {
                MasterModel.DeleteGoods(GoodsID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult UpdateGoods(GoodsMaster goodsdata)
        {
            try
            {
                MasterModel.UpdateGoods(goodsdata);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }

        // Branch Master
        public ActionResult GetBranchData(int draw, int start, int length)
        {
            try
            {

                log.Info("GetBranchData");
                string search = Request.QueryString["search[value]"];
                int sortColumn = 2;
                string sortDirection = "desc";
                if (length == -1)
                {
                    length = TOTAL_ROWS;
                }

                if (Request.QueryString["order[0][column]"] != null)
                {
                    sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
                }
                if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
                {
                    sortDirection = Request.QueryString["order[0][dir]"];
                }

                BranchDataTableData dataTableData = new BranchDataTableData();
                dataTableData.draw = draw;

                var data = MasterModel.GetBranchIndex(start, length, search, sortColumn, sortDirection);
                dataTableData.data = data.FirstOrDefault().Value;
                dataTableData.recordsFiltered = data.FirstOrDefault().Key;
                dataTableData.recordsTotal = data.FirstOrDefault().Key;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Info("Exception in GetBranchData");
                log.Error(ex.Message);
                return null;
            }
        }

        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddBranch(BranchMaster branchdata)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                int count = MasterModel.AddBranch(branchdata, loginID);
                if (count == 0)
                    return Json("Succeess");
                else
                    return Json("AlreadyExist");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteBranch(int BranchID)
        {
            try
            {
                MasterModel.DeleteBranch(BranchID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult GetBranchByID(int BranchID)
        {
            try
            {

                var voucher = Json(MasterModel.GetBranchByID(BranchID), JsonRequestBehavior.AllowGet);
                return voucher;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void UpdateBranch(BranchMaster branchdata)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                MasterModel.UpdateBranch(branchdata, loginID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Other Branch Master
        public ActionResult GetOtherBranchData(int draw, int start, int length)
        {
            try
            {

                log.Info("GetBranchData");
                string search = Request.QueryString["search[value]"];
                int sortColumn = 2;
                string sortDirection = "desc";
                if (length == -1)
                {
                    length = TOTAL_ROWS;
                }

                if (Request.QueryString["order[0][column]"] != null)
                {
                    sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
                }
                if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
                {
                    sortDirection = Request.QueryString["order[0][dir]"];
                }

                OtherBranchDataTableData dataTableData = new OtherBranchDataTableData();
                dataTableData.draw = draw;

                var data = MasterModel.GetOtherBranchIndex(start, length, search, sortColumn, sortDirection);
                dataTableData.data = data.FirstOrDefault().Value;
                dataTableData.recordsFiltered = data.FirstOrDefault().Key;
                dataTableData.recordsTotal = data.FirstOrDefault().Key;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Info("Exception in GetBranchData");
                log.Error(ex.Message);
                return null;
            }
        }

        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddOtherBranch(OtherBranchMaster branchdata)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                int count = MasterModel.AddOtherBranch(branchdata, loginID);
                if (count == 0)
                    return Json("Succeess");
                else
                    return Json("AlreadyExist");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteOtherBranch(int OtherBranchID)
        {
            try
            {
                MasterModel.DeleteOtherBranch(OtherBranchID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult GetOtherBranchByID(int OtherBranchID)
        {
            try
            {

                var voucher = Json(MasterModel.GetOtherBranchByID(OtherBranchID), JsonRequestBehavior.AllowGet);
                return voucher;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void UpdateOtherBranch(OtherBranchMaster branchdata)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                MasterModel.UpdateOtherBranch(branchdata, loginID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // Other Vendor Master
        public ActionResult GetOtherVendorData(int draw, int start, int length)
        {
            try
            {

                log.Info("GetOtherVendorData");
                string search = Request.QueryString["search[value]"];
                int sortColumn = 2;
                string sortDirection = "desc";
                if (length == -1)
                {
                    length = TOTAL_ROWS;
                }

                if (Request.QueryString["order[0][column]"] != null)
                {
                    sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
                }
                if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
                {
                    sortDirection = Request.QueryString["order[0][dir]"];
                }

                VendorDataTableData dataTableData = new VendorDataTableData();
                dataTableData.draw = draw;

                var data = MasterModel.GetOtherVendorIndex(start, length, search, sortColumn, sortDirection);
                dataTableData.data = data.FirstOrDefault().Value;
                dataTableData.recordsFiltered = data.FirstOrDefault().Key;
                dataTableData.recordsTotal = data.FirstOrDefault().Key;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Info("Exception in GetUserData");
                log.Error(ex.Message);
                return null;
            }
        }

        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddOtherVendor(OtherVendorMaster vendordata)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                int count = MasterModel.AddOtherVendor(vendordata, loginID);
                if (count == 0)
                    return Json("Succeess");
                else
                    return Json("AlreadyExist");

            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteOtherVendor(int VendorID)
        {
            try
            {
                MasterModel.DeleteOtherVendor(VendorID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult GetOtherVendorByID(int VendorID)
        {
            try
            {

                var vendor = Json(MasterModel.GetOtherVendorByID(VendorID), JsonRequestBehavior.AllowGet);
                return vendor;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult UpdateOtherVendor(OtherVendorMaster vendordata)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                int count = MasterModel.UpdateOtherVendor(vendordata, loginID);
                if (count == 0)
                    return Json("Succeess");
                else
                    return Json("AlreadyExist");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // Vendor Master
        public ActionResult GetVendorData(int draw, int start, int length)
        {
            try
            {

                log.Info("GetVendorData");
                string search = Request.QueryString["search[value]"];
                int sortColumn = 2;
                string sortDirection = "desc";
                if (length == -1)
                {
                    length = TOTAL_ROWS;
                }

                if (Request.QueryString["order[0][column]"] != null)
                {
                    sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
                }
                if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
                {
                    sortDirection = Request.QueryString["order[0][dir]"];
                }

                VendorDataTableData dataTableData = new VendorDataTableData();
                dataTableData.draw = draw;

                var data = MasterModel.GetVendorIndex(start, length, search, sortColumn, sortDirection);
                dataTableData.data = data.FirstOrDefault().Value;
                dataTableData.recordsFiltered = data.FirstOrDefault().Key;
                dataTableData.recordsTotal = data.FirstOrDefault().Key;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Info("Exception in GetUserData");
                log.Error(ex.Message);
                return null;
            }
        }

        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddVendor(VendorMaster vendordata)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                int count = MasterModel.AddVendor(vendordata, loginID);
                if (count == 0)
                    return Json("Succeess");
                else
                    return Json("AlreadyExist");

            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteVendor(int VendorID)
        {
            try
            {
                MasterModel.DeleteVendor(VendorID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult GetVendorByID(int VendorID)
        {
            try
            {

                var vendor = Json(MasterModel.GetVendorByID(VendorID), JsonRequestBehavior.AllowGet);
                return vendor;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult UpdateVendor(VendorMaster vendordata)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                int count = MasterModel.UpdateVendor(vendordata, loginID);
                if (count == 0)
                    return Json("Succeess");
                else
                    return Json("AlreadyExist");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Vehicle Vendor Master
        public ActionResult GetVehicleVendorData(int draw, int start, int length)
        {
            try
            {

                log.Info("GetVehicleVendorData");
                string search = Request.QueryString["search[value]"];
                int sortColumn = 2;
                string sortDirection = "desc";
                if (length == -1)
                {
                    length = TOTAL_ROWS;
                }

                if (Request.QueryString["order[0][column]"] != null)
                {
                    sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
                }
                if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
                {
                    sortDirection = Request.QueryString["order[0][dir]"];
                }

                VehicleVendorDataTableData dataTableData = new VehicleVendorDataTableData();
                dataTableData.draw = draw;

                var data = MasterModel.GetVehicleVendorIndex(start, length, search, sortColumn, sortDirection);
                dataTableData.data = data.FirstOrDefault().Value;
                dataTableData.recordsFiltered = data.FirstOrDefault().Key;
                dataTableData.recordsTotal = data.FirstOrDefault().Key;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Info("Exception in GetVehicleVendorData");
                log.Error(ex.Message);
                return null;
            }
        }

        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddVehicleVendor(VehicleVendorMaster vendordata)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                int count = MasterModel.AddVehicleVendor(vendordata, loginID);
                if (count == 0)
                    return Json("Succeess");
                else
                    return Json("AlreadyExist");

            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteVehicleVendor(int VehicleVendorID)
        {
            try
            {
                MasterModel.DeleteVehicleVendor(VehicleVendorID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult GetVehicleVendorByID(int VehicleVendorID)
        {
            try
            {

                var vendor = Json(MasterModel.GetVehicleVendorByID(VehicleVendorID), JsonRequestBehavior.AllowGet);
                return vendor;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult UpdateVehicleVendor(VehicleVendorMaster vendordata)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                int count = MasterModel.UpdateVehicleVendor(vendordata, loginID);
                if (count == 0)
                    return Json("Succeess");
                else
                    return Json("AlreadyExist");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Vehicle Master
        public ActionResult GetVehicleData(int draw, int start, int length)
        {
            try
            {

                log.Info("GetVehicleData");
                string search = Request.QueryString["search[value]"];
                int sortColumn = 2;
                string sortDirection = "desc";
                if (length == -1)
                {
                    length = TOTAL_ROWS;
                }

                if (Request.QueryString["order[0][column]"] != null)
                {
                    sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
                }
                if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
                {
                    sortDirection = Request.QueryString["order[0][dir]"];
                }

                VehicleDataTableData dataTableData = new VehicleDataTableData();
                dataTableData.draw = draw;

                var data = MasterModel.GetVehicleIndex(start, length, search, sortColumn, sortDirection);
                dataTableData.data = data.FirstOrDefault().Value;
                dataTableData.recordsFiltered = data.FirstOrDefault().Key;
                dataTableData.recordsTotal = data.FirstOrDefault().Key;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Info("Exception in GetVehicleData");
                log.Error(ex.Message);
                return null;
            }
        }

        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddVehicle(VehicleMaster vehicledata)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                MasterModel.AddVehicle(vehicledata, loginID);
                return Json("Succeess");

            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteVehicle(int VehicleID)
        {
            try
            {
                MasterModel.DeleteVehicle(VehicleID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult GetVehicleByID(int VehicleID)
        {
            try
            {

                var vendor = Json(MasterModel.GetVehicleByID(VehicleID), JsonRequestBehavior.AllowGet);
                return vendor;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult UpdateVehicle(VehicleMaster vehicledata)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                MasterModel.UpdateVehicle(vehicledata, loginID);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //PinCodes
        public JsonResult GetPinCodes()
        {
            try
            {
                var obj = MasterModel.GetPinCodes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddPinCode(PinCodeMaster pin)
        {
            try
            {
                MasterModel.AddPinCode(pin);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN })]
        public void DeletePinCode(int PinCodeId)
        {
            try
            {
                MasterModel.DeletePinCode(PinCodeId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult UpdatePinCode(PinCodeMaster pin)
        {
            try
            {
                MasterModel.UpdatePinCode(pin);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        //FOV
        public JsonResult GetFOV()
        {
            try
            {
                var obj = MasterModel.GetFOV();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddFOV(FOVMaster fovdata)
        {
            try
            {
                MasterModel.AddFOV(fovdata);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteFOV(int FOVId)
        {
            try
            {
                MasterModel.DeleteFOV(FOVId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult UpdateFOV(FOVMaster fovdata)
        {
            try
            {
                MasterModel.UpdateFOV(fovdata);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }

        //Branch Running
        public ActionResult GetBranchRunningData(int draw, int start, int length)
        {
            try
            {

                log.Info("GetBranchRunningData");
                string search = Request.QueryString["search[value]"];
                int sortColumn = 2;
                string sortDirection = "desc";
                if (length == -1)
                {
                    length = TOTAL_ROWS;
                }

                if (Request.QueryString["order[0][column]"] != null)
                {
                    sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
                }
                if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
                {
                    sortDirection = Request.QueryString["order[0][dir]"];
                }

                BranchRunningDataTableData dataTableData = new BranchRunningDataTableData();
                dataTableData.draw = draw;

                var data = MasterModel.GetBranchRunningIndex(start, length, search, sortColumn, sortDirection);
                dataTableData.data = data.FirstOrDefault().Value;
                dataTableData.recordsFiltered = data.FirstOrDefault().Key;
                dataTableData.recordsTotal = data.FirstOrDefault().Key;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Info("Exception in GetBranchRunningData");
                log.Error(ex.Message);
                return null;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddBranchRunning(BranchRunningNo runningno)
        {
            try
            {
                int count=MasterModel.AddBranchRunning(runningno);
                if (count == 0)
                    return Json("Succeess");
                else
                    return Json("AlreadyExist");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteBranchRunning(int BranchRunningID)
        {
            try
            {
                MasterModel.DeleteBranchRunning(BranchRunningID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult UpdateBranchRunning(BranchRunningNo runningno)
        {
            try
            {
               int count=MasterModel.UpdateBranchRunning(runningno);
                if (count == 0)
                    return Json("Succeess");
                else
                    return Json("AlreadyExist");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
       
        //Departments
        public JsonResult GetDepartments()
        {
            try
            {
                var obj = MasterModel.GetDepartments();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        public string DataTableToJSON(DataTable table)
        {
            string JSONString = string.Empty;

            JSONString = JsonConvert.SerializeObject(table);
            return JSONString;
        }

        // Company Master
        public ActionResult GetCompanyData(int draw, int start, int length)
        {
            try
            {

                log.Info("GetCompanyData");
                string search = Request.QueryString["search[value]"];
                int sortColumn = 2;
                string sortDirection = "desc";
                if (length == -1)
                {
                    length = TOTAL_ROWS;
                }

                if (Request.QueryString["order[0][column]"] != null)
                {
                    sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
                }
                if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
                {
                    sortDirection = Request.QueryString["order[0][dir]"];
                }

                CompanyDataTableData dataTableData = new CompanyDataTableData();
                dataTableData.draw = draw;

                var data = MasterModel.GetCompanyIndex(start, length, search, sortColumn, sortDirection);
                dataTableData.data = data.FirstOrDefault().Value;
                dataTableData.recordsFiltered = data.FirstOrDefault().Key;
                dataTableData.recordsTotal = data.FirstOrDefault().Key;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Info("Exception in GetCompanyData");
                log.Error(ex.Message);
                return null;
            }
        }
      
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddCompany(CompanyMaster com)
        {
            try
            {
                MasterModel.AddCompany(com);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteCompany(int CompanyID)
        {
            try
            {
                MasterModel.DeleteCompany(CompanyID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult GetCompanyByID(int CompanyID)
        {
            try
            {

                var voucher = Json(MasterModel.GetCompanyByID(CompanyID), JsonRequestBehavior.AllowGet);
                return voucher;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void UpdateCompany(CompanyMaster com)
        {
            try
            {
                MasterModel.UpdateCompany(com);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Tax
        public JsonResult GetTaxes()
        {
            try
            {
                var obj = MasterModel.GetTaxes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddTax(TaxMaster taxdata)
        {
            try
            {
                MasterModel.AddTax(taxdata);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteTax(int TaxId)
        {
            try
            {
                MasterModel.DeleteTax(TaxId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult UpdateTax(TaxMaster taxdata)
        {
            try
            {
                MasterModel.UpdateTax(taxdata);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }

        // Zone Master
        public ActionResult GetZoneData(int draw, int start, int length)
        {
            try
            {

                log.Info("GetZoneData");
                string search = Request.QueryString["search[value]"];
                int sortColumn = 2;
                string sortDirection = "desc";
                if (length == -1)
                {
                    length = TOTAL_ROWS;
                }

                if (Request.QueryString["order[0][column]"] != null)
                {
                    sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
                }
                if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
                {
                    sortDirection = Request.QueryString["order[0][dir]"];
                }

                ZoneDataTableData dataTableData = new ZoneDataTableData();
                dataTableData.draw = draw;

                var data = MasterModel.GetZoneIndex(start, length, search, sortColumn, sortDirection);
                dataTableData.data = data.FirstOrDefault().Value;
                dataTableData.recordsFiltered = data.FirstOrDefault().Key;
                dataTableData.recordsTotal = data.FirstOrDefault().Key;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Info("Exception in GetZoneData");
                log.Error(ex.Message);
                return null;
            }
        }

        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddZone(ZoneMaster zonedata)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                MasterModel.AddZone(zonedata, loginID);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteZone(int ZoneID)
        {
            try
            {
                MasterModel.DeleteZone(ZoneID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult GetZoneByID(int ZoneID)
        {
            try
            {

                var voucher = Json(MasterModel.GetZoneByID(ZoneID), JsonRequestBehavior.AllowGet);
                return voucher;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void UpdateZone(ZoneMaster zonedata)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                MasterModel.UpdateZone(zonedata, loginID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Part Nos
        public JsonResult GetPartNos()
        {
            try
            {
                var obj = MasterModel.GetMasterPartNos();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddPartNo(PartNoMaster partnodata)
        {
            try
            {
                MasterModel.AddPartNo(partnodata);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeletePartNo(int PartNoId)
        {
            try
            {
                MasterModel.DeletePartNo(PartNoId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult UpdatePartNo(PartNoMaster partnodata)
        {
            try
            {
                MasterModel.UpdatePartNo(partnodata);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }


    }
}