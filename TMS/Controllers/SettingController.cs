﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TMS.CustomModels;
using TMS.EFModel;
using TMS.Models;

namespace TMS.Controllers
{
    public class SettingController : Controller
    {
        private const int TOTAL_ROWS = 0;
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public ActionResult VendorRates()
        {
            return View();
        }
        public ActionResult MHELVendorRates()
        {
            return View();
        }
        public ActionResult MahindraVendorRates()
        {
            return View();
        }
        public ActionResult MasterSetting()
        {
            return View();
        }
        #region Vendor Rate
        public ActionResult GetVendorRates(int draw, int start, int length)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            VendorRateDataTableData dataTableData = new VendorRateDataTableData();
            dataTableData.draw = draw;
          
            var data = SettingModels.GetVendorRatesIndex(start, length, search, sortColumn, sortDirection);
            dataTableData.data = data.FirstOrDefault().Value;
            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public int AddVendorRate(VendorRateMaster vendorRate)
        {
            try
            {
                int isSucceed = SettingModels.AddVendorRate(vendorRate);
                return isSucceed;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteVendorRate(int VendorRateID)
        {
            try
            {
                SettingModels.DeleteVendorRate(VendorRateID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region MHEL Vendor Rate
        public ActionResult GetMHELVendorRates(int draw, int start, int length)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            MHELVendorRateDataTableData dataTableData = new MHELVendorRateDataTableData();
            dataTableData.draw = draw;

            var data = SettingModels.GetMHELVendorRatesIndex(start, length, search, sortColumn, sortDirection);
            dataTableData.data = data.FirstOrDefault().Value;
            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public int AddMHELVendorRate(MHELVendorRateMaster vendorRate)
        {
            try
            {
                int isSucceed = SettingModels.AddMHELVendorRate(vendorRate);
                return isSucceed;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteMHELVendorRate(int MHELVendorRateID)
        {
            try
            {
                SettingModels.DeleteMHELVendorRate(MHELVendorRateID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetMHELVendorRateById(int MHELVendorRateID)
        {
            try
            {

                var voucher = Json(SettingModels.GetMHELVendorRateById(MHELVendorRateID), JsonRequestBehavior.AllowGet);
                return voucher;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateMHELVendorRate(MHELVendorRateMaster vendorRate)
        {
            try
            {
                int isSucceed = SettingModels.UpdateMHELVendorRate(vendorRate);
                return isSucceed;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Mahindra Vendor Rate
        public ActionResult GetMahindraVendorRates(int draw, int start, int length)
        {
            string search = Request.QueryString["search[value]"];
            int sortColumn = 2;
            string sortDirection = "desc";
            if (length == -1)
            {
                length = TOTAL_ROWS;
            }

            // note: we only sort one column at a time
            if (Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
            }
            if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = Request.QueryString["order[0][dir]"];
            }

            MahindraVendorRateDataTableData dataTableData = new MahindraVendorRateDataTableData();
            dataTableData.draw = draw;

            var data = SettingModels.GetMahindraVendorRatesIndex(start, length, search, sortColumn, sortDirection);
            dataTableData.data = data.FirstOrDefault().Value;
            dataTableData.recordsFiltered = data.FirstOrDefault().Key;
            dataTableData.recordsTotal = data.FirstOrDefault().Key;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        public int AddMahindraVendorRate(MahindraVendorRateMaster vendorRate)
        {
            try
            {
                int isSucceed = SettingModels.AddMahindraVendorRate(vendorRate);
                return isSucceed;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteMahindraVendorRate(int MahindraVendorRateID)
        {
            try
            {
                SettingModels.DeleteMahindraVendorRate(MahindraVendorRateID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetMahindraVendorRateById(int MahindraVendorRateID)
        {
            try
            {

                var voucher = Json(SettingModels.GetMahindraVendorRateById(MahindraVendorRateID), JsonRequestBehavior.AllowGet);
                return voucher;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateMahindraVendorRate(MahindraVendorRateMaster vendorRate)
        {
            try
            {
                int isSucceed = SettingModels.UpdateMahindraVendorRate(vendorRate);
                return isSucceed;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Master Settings
        public JsonResult GetAttributes()
        {
            try
            {
                var obj = MasterModel.GetAttribute();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        public JsonResult UpdateMasterAttribute(string KeyName,string ValueData)
        {
            try
            {
                MasterModel.UpdateMasterAttribute(KeyName, ValueData);
                return Json("Success");
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        #endregion
    }
}