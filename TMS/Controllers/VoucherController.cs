﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TMS.CustomModels;
using TMS.EFModel;
using TMS.Models;

namespace TMS.Controllers
{
    public class VoucherController : Controller
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const int TOTAL_ROWS = 0;
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD })]
        public ActionResult Voucher()
        {
            ViewData.Add("BranchId", Convert.ToInt32(HttpContext.Session["BranchID"]));
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public ActionResult ConsignmentPayment()
        {
            ViewData.Add("BranchId", Convert.ToInt32(HttpContext.Session["BranchID"]));
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public ActionResult Expense()
        {
            ViewData.Add("BranchId", Convert.ToInt32(HttpContext.Session["BranchID"]));
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD })]
        public ActionResult VendorExpense()
        {
            ViewData.Add("BranchId", Convert.ToInt32(HttpContext.Session["BranchID"]));
            return View();
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD })]
        public ActionResult GetVoucherData(int draw, int start, int length, int? branchId)
        {
            try
            {

                log.Info("GetVoucherData For BranchId=" + branchId);
                string search = Request.QueryString["search[value]"];
                int sortColumn = 2;
                string sortDirection = "desc";
                if (length == -1)
                {
                    length = TOTAL_ROWS;
                }

                // note: we only sort one column at a time
                if (Request.QueryString["order[0][column]"] != null)
                {
                    sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
                }
                if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
                {
                    sortDirection = Request.QueryString["order[0][dir]"];
                }

                VoucherDataTableData dataTableData = new VoucherDataTableData();
                dataTableData.draw = draw;
                if (branchId == 0)
                    branchId = null;
                var data = SettingModels.GetVoucherIndex(branchId, start, length, search, sortColumn, sortDirection);
                dataTableData.data = data.FirstOrDefault().Value;
                dataTableData.recordsFiltered = data.FirstOrDefault().Key;
                dataTableData.recordsTotal = data.FirstOrDefault().Key;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Info("Exception in GetVoucherData");
                log.Error(ex.Message);
                return null;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD })]
        public JsonResult AddVoucher(VoucherMaster voucher)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                SettingModels.AddVoucher(voucher, loginID);
                return Json("Succeess");
            }
            catch(Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD })]
        public void DeleteVoucher(int VoucherID)
        {
            try
            {
                SettingModels.DeleteVoucher(VoucherID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD })]
        public void UpdateVoucher(VoucherMaster voucher)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                SettingModels.UpdateVoucher(voucher,loginID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD })]
        public JsonResult GetVoucherByID(int VoucherID)
        {
            try
            {

                var voucher = Json(SettingModels.GetVoucherByID(VoucherID), JsonRequestBehavior.AllowGet);
                return voucher;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Consignment Payment
        public JsonResult SaveConsignmentPayment(ConsignmentVoucher voucher)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                SettingModels.AddConsignmentPayment(voucher, loginID);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        public ActionResult GetConsignmentPaymentData(int draw, int start, int length)
        {
            try
            {

                log.Info("GetConsignmentPaymentData");
                string search = Request.QueryString["search[value]"];
                int sortColumn = 2;
                string sortDirection = "desc";
                if (length == -1)
                {
                    length = TOTAL_ROWS;
                }

                // note: we only sort one column at a time
                if (Request.QueryString["order[0][column]"] != null)
                {
                    sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
                }
                if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
                {
                    sortDirection = Request.QueryString["order[0][dir]"];
                }

                ConsignmentVoucherDataTableData dataTableData = new ConsignmentVoucherDataTableData();
                dataTableData.draw = draw;
              
                var data = SettingModels.GetConsignmentPaymentIndex(start, length, search, sortColumn, sortDirection);
                dataTableData.data = data.FirstOrDefault().Value;
                dataTableData.recordsFiltered = data.FirstOrDefault().Key;
                dataTableData.recordsTotal = data.FirstOrDefault().Key;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Info("Exception in GetVoucherData");
                log.Error(ex.Message);
                return null;
            }
        }
        public void DeleteConsignmentPayment(int ConsignmentVoucherID)
        {
            try
            {
                SettingModels.DeleteConsignmentPayment(ConsignmentVoucherID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Expense
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public ActionResult GetExpenseData(int draw, int start, int length, int? branchId)
        {
            try
            {

                log.Info("GetExpenseData For BranchId=" + branchId);
                string search = Request.QueryString["search[value]"];
                int sortColumn = 2;
                string sortDirection = "desc";
                if (length == -1)
                {
                    length = TOTAL_ROWS;
                }

                // note: we only sort one column at a time
                if (Request.QueryString["order[0][column]"] != null)
                {
                    sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
                }
                if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
                {
                    sortDirection = Request.QueryString["order[0][dir]"];
                }

                ExpenseDataTableData dataTableData = new ExpenseDataTableData();
                dataTableData.draw = draw;
                if (branchId == 0)
                    branchId = null;
                var data = SettingModels.GetExpenseIndex(branchId, start, length, search, sortColumn, sortDirection);
                dataTableData.data = data.FirstOrDefault().Value;
                dataTableData.recordsFiltered = data.FirstOrDefault().Key;
                dataTableData.recordsTotal = data.FirstOrDefault().Key;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Info("Exception in GetVoucherData");
                log.Error(ex.Message);
                return null;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult AddExpense(ExpenseMaster expense)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                SettingModels.AddExpense(expense, loginID);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void DeleteExpense(int ExpenseID)
        {
            try
            {
                SettingModels.DeleteExpense(ExpenseID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public void UpdateExpense(ExpenseMaster expense)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                SettingModels.UpdateExpense(expense, loginID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD, USERROLE.ADMIN, USERROLE.SUPERADMIN })]
        public JsonResult GetExpenseByID(int ExpenseID)
        {
            try
            {

                var voucher = Json(SettingModels.GetExpenseByID(ExpenseID), JsonRequestBehavior.AllowGet);
                return voucher;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetExpenseTypes()
        {
            try
            {
                var obj = MasterModel.GetExpenseTypes();
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        #endregion

        #region Vendor Expense
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD })]
        public ActionResult GetVendorExpenseData(int draw, int start, int length, int? branchId)
        {
            try
            {

                log.Info("GetVendorExpenseData For BranchId=" + branchId);
                string search = Request.QueryString["search[value]"];
                int sortColumn = 2;
                string sortDirection = "desc";
                if (length == -1)
                {
                    length = TOTAL_ROWS;
                }

                // note: we only sort one column at a time
                if (Request.QueryString["order[0][column]"] != null)
                {
                    sortColumn = int.Parse(Request.QueryString["order[0][column]"]);
                }
                if (sortColumn != 0 && Request.QueryString["order[0][dir]"] != null)
                {
                    sortDirection = Request.QueryString["order[0][dir]"];
                }

                VendorExpenseDataTableData dataTableData = new VendorExpenseDataTableData();
                dataTableData.draw = draw;
                if (branchId == 0)
                    branchId = null;
                var data = SettingModels.GetVendorExpenseIndex(branchId, start, length, search, sortColumn, sortDirection);
                dataTableData.data = data.FirstOrDefault().Value;
                dataTableData.recordsFiltered = data.FirstOrDefault().Key;
                dataTableData.recordsTotal = data.FirstOrDefault().Key;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Info("Exception in GetVoucherData");
                log.Error(ex.Message);
                return null;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD })]
        public JsonResult AddVendorExpense(VendorExpenseMaster expense)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                SettingModels.AddVendorExpense(expense, loginID);
                return Json("Succeess");
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json("Error");
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD })]
        public void DeleteVendorExpense(int ExpenseID)
        {
            try
            {
                SettingModels.DeleteVendorExpense(ExpenseID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD })]
        public void UpdateVendorExpense(VendorExpenseMaster expense)
        {
            try
            {
                int loginID = Convert.ToInt32(HttpContext.Session["UserID"]);
                SettingModels.UpdateVendorExpense(expense, loginID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Authorize(AccessLevels = new USERROLE[] { USERROLE.OPERATIONHEAD })]
        public JsonResult GetVendorExpenseByID(int ExpenseID)
        {
            try
            {

                var voucher = Json(SettingModels.GetVendorExpenseByID(ExpenseID), JsonRequestBehavior.AllowGet);
                return voucher;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}