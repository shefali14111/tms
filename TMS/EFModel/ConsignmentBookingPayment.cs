//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TMS.EFModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class ConsignmentBookingPayment
    {
        public int BookingPaymentID { get; set; }
        public int ConsignmentID { get; set; }
        public int GoodsDescription { get; set; }
        public Nullable<bool> InsuranceFlag { get; set; }
        public Nullable<double> DocketCharges { get; set; }
        public Nullable<double> FOV { get; set; }
        public Nullable<double> DoorCollection { get; set; }
        public Nullable<double> Octroicharges { get; set; }
        public Nullable<double> DoorDelivery { get; set; }
        public Nullable<double> OctroiSurcharges { get; set; }
        public Nullable<double> LabourCharges { get; set; }
        public Nullable<double> OtherCharges { get; set; }
        public Nullable<double> TotalAmount { get; set; }
        public Nullable<bool> TaxFlag { get; set; }
        public Nullable<int> ServiceTaxID { get; set; }
        public Nullable<bool> RoadPermitAttached { get; set; }
        public string RoadPermitType { get; set; }
        public string Remark { get; set; }
        public Nullable<double> GrandTotal { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> Modified { get; set; }
        public Nullable<bool> Status { get; set; }
        public Nullable<int> PaymentTypeID { get; set; }
        public string BankName { get; set; }
        public string BranchName_ { get; set; }
        public string IFSCCode { get; set; }
        public string PanNo { get; set; }
        public string BranchName { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeDate { get; set; }
        public string EwayBillNo { get; set; }
        public string VendorCode { get; set; }
        public string Documents { get; set; }
        public Nullable<int> FOVId { get; set; }
        public Nullable<bool> PaymentCollected { get; set; }
        public Nullable<decimal> StandardRate { get; set; }
        public Nullable<decimal> BasicFreight { get; set; }
        public Nullable<decimal> Pickup { get; set; }
        public Nullable<decimal> HamaliCharges { get; set; }
        public Nullable<decimal> DDCharges { get; set; }
        public Nullable<decimal> ODACharges { get; set; }
        public Nullable<decimal> OtherAOC { get; set; }
    }
}
