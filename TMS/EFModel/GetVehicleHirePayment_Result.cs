//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TMS.EFModel
{
    using System;
    
    public partial class GetVehicleHirePayment_Result
    {
        public int BranchID { get; set; }
        public int VehicleHireID { get; set; }
        public string HireDate { get; set; }
        public int TruckBookingType { get; set; }
        public int VehicleID { get; set; }
        public string VendorName { get; set; }
        public Nullable<int> VendorType { get; set; }
        public double BookingAmount { get; set; }
        public Nullable<double> AdvancePayment { get; set; }
        public Nullable<double> BalanceAmount { get; set; }
        public Nullable<int> PaymentTypeID { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string IFSCCode { get; set; }
        public string PanNo { get; set; }
        public string Remarks { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> Modified { get; set; }
        public string BookingType { get; set; }
        public string VehicleNo { get; set; }
        public string loadingCapacity { get; set; }
        public string MaxloadingCapacity { get; set; }
    }
}
