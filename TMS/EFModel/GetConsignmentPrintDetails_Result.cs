//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TMS.EFModel
{
    using System;
    
    public partial class GetConsignmentPrintDetails_Result
    {
        public int ConsignmentID { get; set; }
        public string ConsignmentNo { get; set; }
        public string BookingBranch { get; set; }
        public string BillingParty { get; set; }
        public string ConsigneeName { get; set; }
        public string ConsigneeAddress { get; set; }
        public string ConsigneeNo { get; set; }
        public string ConsignorName { get; set; }
        public string ConsignorAddress { get; set; }
        public string ConsignorNo { get; set; }
        public Nullable<System.DateTime> ConsignmentDate { get; set; }
        public string ConsignmentFrom { get; set; }
        public string ConsignmentTo { get; set; }
        public string BillingParty1 { get; set; }
        public string TransportType { get; set; }
        public string paymentType { get; set; }
        public string GoodsDescription { get; set; }
        public string DeliveryTYpe { get; set; }
        public Nullable<double> DocketCharges { get; set; }
        public string DoorCollection { get; set; }
        public Nullable<double> DoorCollectionCharges { get; set; }
        public Nullable<double> TotalAmount { get; set; }
        public Nullable<double> FOV { get; set; }
        public Nullable<double> DoorDelivery { get; set; }
        public Nullable<double> LabourCharges { get; set; }
        public Nullable<double> OtherCharges { get; set; }
        public Nullable<double> Octroicharges { get; set; }
        public Nullable<double> GrandTotal { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<double> TaxPercent { get; set; }
        public string VehicleNo { get; set; }
        public string TransportMode { get; set; }
    }
}
