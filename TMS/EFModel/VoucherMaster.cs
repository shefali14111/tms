//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TMS.EFModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class VoucherMaster
    {
        public int VoucherID { get; set; }
        public string VoucherNo { get; set; }
        public string VoucherDate { get; set; }
        public int VoucherType { get; set; }
        public int DocumentType { get; set; }
        public string BillNo { get; set; }
        public Nullable<int> CreditLedger { get; set; }
        public Nullable<double> CreditAmount { get; set; }
        public Nullable<int> DebitLedger { get; set; }
        public Nullable<double> DebitAmount { get; set; }
        public string Narration { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> Modified { get; set; }
        public int BranchID { get; set; }
        public int AccountID { get; set; }
        public string VendorName { get; set; }
        public int VendorID { get; set; }
        public int PaymentTypeID { get; set; }
        public double VoucherAmount { get; set; }
        public int UserID { get; set; }
        public int ManifestID { get; set; }
        public string Particulars { get; set; }
        public string HireType { get; set; }
    }
}
