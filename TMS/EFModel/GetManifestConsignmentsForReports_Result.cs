//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TMS.EFModel
{
    using System;
    
    public partial class GetManifestConsignmentsForReports_Result
    {
        public int ConsignmentID { get; set; }
        public string ConsignmentNo { get; set; }
        public string ConsigneeName { get; set; }
        public string ConsignorName { get; set; }
        public Nullable<System.DateTime> ConsignmentDate { get; set; }
        public string paymentType { get; set; }
        public Nullable<int> Articles { get; set; }
        public Nullable<double> ActualWeight { get; set; }
        public string ConsignmentFrom { get; set; }
        public string ConsignmentTo { get; set; }
    }
}
