//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TMS.EFModel
{
    using System;
    
    public partial class BranchRunningNos_SP_Result
    {
        public int RunningNoID { get; set; }
        public int BranchID { get; set; }
        public int LastRunningNo { get; set; }
        public string TransactionType { get; set; }
        public string Branch { get; set; }
    }
}
