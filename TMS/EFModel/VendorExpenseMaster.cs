//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TMS.EFModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class VendorExpenseMaster
    {
        public int VendorExpenseID { get; set; }
        public int BranchID { get; set; }
        public System.DateTime ExpenseDate { get; set; }
        public int VendorID { get; set; }
        public string InvoiceNo { get; set; }
        public double BillAmount { get; set; }
        public Nullable<double> AdvancePayment { get; set; }
        public Nullable<double> BalanceAmount { get; set; }
        public Nullable<int> PaymentTypeID { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string Particulars { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> Modified { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeDate { get; set; }
        public string ExpenseDateStr { get; set; }
        public string PaymentType { get; set; }
        public string VendorName { get; set; }
        public double PaidAmount { get; set; }
    }
}
