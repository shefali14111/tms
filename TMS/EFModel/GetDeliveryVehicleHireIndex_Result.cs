//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TMS.EFModel
{
    using System;
    
    public partial class GetDeliveryVehicleHireIndex_Result
    {
        public int BranchID { get; set; }
        public int DeliveryVehicleHireID { get; set; }
        public int VehicleID { get; set; }
        public string VehicleNo { get; set; }
        public string HireDate { get; set; }
        public string VendorName { get; set; }
        public double BookingAmount { get; set; }
        public string BookingType { get; set; }
    }
}
