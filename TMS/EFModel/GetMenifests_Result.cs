//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TMS.EFModel
{
    using System;
    
    public partial class GetMenifests_Result
    {
        public int ManifestID { get; set; }
        public string ManifestNo { get; set; }
        public string Created { get; set; }
        public string DepartureTime { get; set; }
        public string FromLoc { get; set; }
        public string ToLoc { get; set; }
        public string ArrivalDate { get; set; }
        public string ArrivalTime { get; set; }
    }
}
