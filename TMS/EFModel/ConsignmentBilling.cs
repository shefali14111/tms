//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TMS.EFModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class ConsignmentBilling
    {
        public int ConsignmentBillingID { get; set; }
        public string BillingNo { get; set; }
        public Nullable<System.DateTime> BillingDate { get; set; }
        public int VendorID { get; set; }
        public string BillingTime { get; set; }
        public Nullable<double> BillingTotalAmount { get; set; }
        public string Remarks { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> Modified { get; set; }
        public Nullable<System.DateTime> ArrivalDate { get; set; }
        public string VendorName { get; set; }
        public Nullable<int> BranchID { get; set; }

        public string BillingDateStr { get; set; }
    }
}
