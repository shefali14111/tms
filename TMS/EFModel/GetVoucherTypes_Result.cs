//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TMS.EFModel
{
    using System;
    
    public partial class GetVoucherTypes_Result
    {
        public int VoucherTypeID { get; set; }
        public string VoucherType { get; set; }
        public string Desciption { get; set; }
    }
}
