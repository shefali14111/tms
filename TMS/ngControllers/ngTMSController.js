﻿App.controller('TMSController_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, $timeout) {

    var panes = $scope.panes = [{ name: "Today", selected: true, title: "Today's Sale" }, { name: "All", selected: false, title: "Current Financial Year Sale"  }];

    $scope.select = function (pane) {
        angular.forEach(panes, function (pane) {
            pane.selected = false;
        });
        pane.selected = true;
    }

    $scope.addPane = function (pane) {
        if (panes.length == 0) $scope.select(pane);
        panes.push(pane);
    }
    $scope.Branches = [];
    $scope.GetBranches = function () {
        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    brnches.push(response.data[i]);
                }
                $scope.Branches = brnches;

            },
                function errorCallback(response) {
                    alert(response);
                });

    }
    $scope.GetBranches();
    $scope.GetPaymentTypeWiseSalesDaily = function(){
        DailySales = [];
        $http({
            method: 'GET',
            url: '/TMS/GetPaymentTypeSalesDataDaily',
            params: { 'branchId': $("#ddlBranch").val() },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {

                for (i = 0; i < response.data.length; i++) {
                    DailySales.push(response.data[i]);
                }

                $scope.DailySales = DailySales;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetPaymentTypeWiseSalesAll = function () {
        AllSales = [];
        $http({
            method: 'GET',
            url: '/TMS/GetPaymentTypeSalesData',
            params: { 'branchId': $("#ddlBranch").val() },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {

                for (i = 0; i < response.data.length; i++) {
                    AllSales.push(response.data[i]);
                }

                $scope.AllSales = AllSales;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }

    $scope.GetPaymentTypeWiseSalesDaily();
    $scope.GetPaymentTypeWiseSalesAll();
    $scope.GetPaymentTypeWiseSalesReport = function () {
        $scope.GetPaymentTypeWiseSalesAll();
        $scope.GetPaymentTypeWiseSalesDaily();
    };

}])
