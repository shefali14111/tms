﻿
App.controller('Consignment_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder) {

    $scope.consignmentBooking = {
        ConsignmentID: '',
        ConsignmentNo: '',
        ConsigneeName: '',
        ConsigneeAddress: '',
        CompanyID: '',
        BranchID: '',
        ZoneID: '',
        ConsigneeID: '',
        ConsignorName: '',
        ConsignorAddress: '',
        ConsignorID: '',
        ConsignmentDate: $scope.currentDate,
        PaymentTypeID: '',
        ConsignmentFromID: '',
        ConsignmentToID: '',
        DeliveryTypeID: '',
        BillingPartyID: '',
        TransportTypeID: '',
        ServiceTaxId: '',
        DoorCollectionID: '',
        TransportModeID: '',
        CargotTypeID: '',
        CreatedBy: '',
        Created: '',
        ModifiedBy: '',
        Modified: '',
        Status: '',
        VehicleID: '',
        MobileNumber: '',
        ContactPersonName: ''
    };
    $scope.MHELPrint = true;
    $scope.activeConsignmentBooking = [];
    $scope.GetMHELPrintOption = function () {
        $.get("/Operations/CheckMHELPrintOption")
            .done(function (response) {
                if (response == "False") {
                    $scope.MHELPrint = false;
                }
                else {
                    $scope.MHELPrint = true;
                }
            });
    }
    $scope.GetMHELPrintOption();
    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Operations/GetConsignmentData",
            type: 'GET',
            data: function (d) {
                d.branchId = $("#hdnBranchID").val();
            }
        })
        .withOption('aaSorting', [[2, "desc"]])
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { search: "", searchPlaceholder: "Search..." })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('ConsignmentID').withTitle('ConsignmentID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('ConsignmentNo').withTitle('Consignment No').notSortable(),
        DTColumnBuilder.newColumn('Consignmentdate').withTitle('Consignment Date'),
        DTColumnBuilder.newColumn('ConsigneeName').withTitle('Consignee Name').notSortable(),
        DTColumnBuilder.newColumn('ConsignorName').withTitle('Consignor Name').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentFrom').withTitle('From').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentTo').withTitle('To').notSortable(),
        DTColumnBuilder.newColumn('VendorName').withTitle('Payment Type').notSortable(),
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            var strLinks = '<a data-ng-click="EditBooking(' + row.ConsignmentID + ')" class="pull-left font20 marleft15" > <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
            if ($("#hdnBranchID").val() == "0") {
                strLinks = strLinks + '<a class="pull-left marleft15 font20 delete" ng-click="DeleteBooking(' + row.ConsignmentID + ')" > <i class="fa fa-trash-o" aria-hidden="true"></i></a>';
            }
            if (row.VendorName.toLowerCase().indexOf('mahindra') > -1) {
                if ($scope.MHELPrint == true) {
                    strLinks = strLinks + '<a ng-click=PrintBooking(' + row.ConsignmentID + ') class="pull-left marleft15 font20" > <i class="fa fa-print" aria-hidden="true"></i></a >';
                }

                strLinks = strLinks + ' <a data-ng-click="ViewBooking(' + row.ConsignmentID + ')" class="pull-left font20 marleft15"><i class="fa fa-eye" aria-hidden="true"></i></a>';

            }
            else {
                strLinks = strLinks + '<a ng-click=PrintBooking(' + row.ConsignmentID + ') class="pull-left marleft15 font20" > <i class="fa fa-print" aria-hidden="true"></i></a > <a data-ng-click="ViewBooking(' + row.ConsignmentID + ')" class="pull-left font20 marleft15"><i class="fa fa-eye" aria-hidden="true"></i></a>';
            }
            return strLinks;

        })
    ];

    $scope.AddBookings = function () {
        $.get("/Operations/CheckAddConsignment")
            .done(function (response) {

                if (response == "False") {
                    Notification.error({ message: "Request is not allowed.", delay: 5000 });
                }
                else {
                    window.location.href = "/Operations/AddConsignment";
                }
            });

    }
    $scope.EditBooking = function (consignmentId) {
        $.get("/Operations/CheckEditableConsignment",
            { 'ConsignmentId': consignmentId })
            .done(function (response) {

                if (response == "False") {
                    Notification.error({ message: "Request is not authorized. Please contact Administrator.", delay: 5000 });
                }
                else {
                    window.location.href = "/Operations/EditConsignment?ConsignmentiD=" + consignmentId + "&RedirectPage=" + false;
                }
            });
    }
    $scope.ViewBooking = function (consignmentId) {

        window.location.href = "/Operations/ViewConsignment?ConsignmentId=" + consignmentId

    }
    $scope.DeleteBooking = function (consignmentID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Operations/DeleteConsignmentBooking',
                params: { 'consignID': consignmentID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Consignment Booking deleted successfully.", delay: 5000 });
                    $('#tblConsignment').DataTable().ajax.reload();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
    $scope.PrintConsignments = {
        ConsignmentNo: '',
        ConsignmentDate: '',
        ConsignmentTime: '',
        ConsigneeName: '',
        ConsignorName: '',
        ConsignmentFrom: '',
        ConsignmentTo: '',
        Destination: '',
        MobileNumber: '',
        ContactPersonName: '',
        BillingParty: '',
        ConsigneeAddress: '',
        ConsignorAddress: '',
        EwayBillNo: '',
        VendorCode: '',
        PaymentType: '',
        BasicFreight: '',
        DocketCharges: '',
        FOV: '',
        Pickup: '',
        DoorDelivery: '',
        LabourCharges: '',
        OtherCharges: '',
        TotalAmount: '',
        TaxAmount: '',
        GrandTotal: '',
        StandardRate: '',
        DoorCollection: '',
        ODACharges: '',
        HamaliCharges: '',
        DDCharges: '',
        OtherAOC: '',
        InvoiceNos: '',
        InvoiceDates: '',
        InvoiceValue: '',
        PartNos: '',
        ASNNos: '',
        PaymentMode: '',
        ChequeNo: ''
    };

    $scope.PrintBooking = function (consignmentID) {
        PrintConsignment = [];
        $http({
            method: 'GET',
            url: '/Operations/PrintConsignment',
            params: { 'ConsignmentID': consignmentID },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {

                for (i = 0; i < response.data.length; i++) {
                    PrintConsignment.push(response.data[i]);
                }

                $scope.PrintConsignments.ConsignmentNo = PrintConsignment[0].ConsignmentNo;
                $scope.PrintConsignments.ConsignmentDate = PrintConsignment[0].ConsignmentDate;
                $scope.PrintConsignments.ConsignmentTime = PrintConsignment[0].ConsignmentTime;
                $scope.PrintConsignments.ConsigneeName = PrintConsignment[0].ConsigneeName;
                $scope.PrintConsignments.ConsignorName = PrintConsignment[0].ConsignorName;
                $scope.PrintConsignments.ConsignmentFrom = PrintConsignment[0].ConsignmentFrom;
                $scope.PrintConsignments.ConsignmentTo = PrintConsignment[0].ConsignmentTo;
                $scope.PrintConsignments.Destination = PrintConsignment[0].Destination;
                $scope.PrintConsignments.MobileNumber = PrintConsignment[0].MobileNumber;
                $scope.PrintConsignments.ContactPersonName = PrintConsignment[0].ContactPersonName;
                $scope.PrintConsignments.BillingParty = PrintConsignment[0].BillingParty;
                $scope.PrintConsignments.ConsigneeAddress = PrintConsignment[0].ConsigneeAddress;
                $scope.PrintConsignments.ConsignorAddress = PrintConsignment[0].ConsignorAddress;
                $scope.PrintConsignments.EwayBillNo = PrintConsignment[0].EwayBillNo;
                $scope.PrintConsignments.VendorCode = PrintConsignment[0].VendorCode;
                $scope.PrintConsignments.PaymentType = PrintConsignment[0].PaymentType;
                $scope.PrintConsignments.BasicFreight = PrintConsignment[0].BasicFreight;
                $scope.PrintConsignments.DocketCharges = PrintConsignment[0].DocketCharges;
                $scope.PrintConsignments.FOV = PrintConsignment[0].FOV;
                $scope.PrintConsignments.Pickup = PrintConsignment[0].Pickup;
                $scope.PrintConsignments.DoorDelivery = PrintConsignment[0].DoorDelivery;
                $scope.PrintConsignments.LabourCharges = PrintConsignment[0].LabourCharges;
                $scope.PrintConsignments.OtherCharges = PrintConsignment[0].OtherCharges;
                $scope.PrintConsignments.TotalAmount = parseFloat(PrintConsignment[0].TotalAmount).toFixed(2);
                $scope.PrintConsignments.GrandTotal = parseFloat(PrintConsignment[0].GrandTotal).toFixed(2);
                $scope.PrintConsignments.StandardRate = PrintConsignment[0].StandardRate;
                $scope.PrintConsignments.DoorCollection = PrintConsignment[0].DoorCollection;
                $scope.PrintConsignments.ODACharges = PrintConsignment[0].ODACharges;
                $scope.PrintConsignments.HamaliCharges = PrintConsignment[0].HamaliCharges;
                $scope.PrintConsignments.DDCharges = PrintConsignment[0].DDCharges;
                $scope.PrintConsignments.OtherAOC = PrintConsignment[0].OtherAOC;
                $scope.PrintConsignments.BookingBranch = PrintConsignment[0].BookingBranch;
                $scope.PrintConsignments.TransportType = PrintConsignment[0].TransportType;
                $scope.PrintConsignments.DoorPickup = PrintConsignment[0].DoorPickup;
                $scope.PrintConsignments.DeliveryType = PrintConsignment[0].DeliveryType;
                $scope.PrintConsignments.ConsigneeGSTNo = PrintConsignment[0].ConsigneeGSTNo;
                $scope.PrintConsignments.ConsignorGSTNo = PrintConsignment[0].ConsignorGSTNo;
                $scope.PrintConsignments.GoodsDescription = PrintConsignment[0].GoodsDescription;
                $scope.PrintConsignments.PaymentWay = PrintConsignment[0].PaymentWay;
                $scope.PrintConsignments.EwayBillNo = PrintConsignment[0].EwayBillNo;
                $scope.PrintConsignments.TransportMode = PrintConsignment[0].TransportMode;
                $scope.PrintConsignments.VehicleNo = PrintConsignment[0].VehicleNo;
                $scope.PrintConsignments.ActualWeight = PrintConsignment[0].ActualWeight;
                $scope.PrintConsignments.ChargeWeight = PrintConsignment[0].ChargeWeight;
                $scope.PrintConsignments.Article = PrintConsignment[0].Article;
                $scope.PrintConsignments.InvoiceNos = PrintConsignment[0].InvoiceNos;
                $scope.PrintConsignments.InvoiceDates = PrintConsignment[0].InvoiceDates;
                $scope.PrintConsignments.InvoiceValue = PrintConsignment[0].InvoiceValue;
                $scope.PrintConsignments.PartNos = PrintConsignment[0].PartNos;
                $scope.PrintConsignments.ASNNos = PrintConsignment[0].ASNNos;
                $scope.PrintConsignments.CreatedBy = PrintConsignment[0].UserName;
                $scope.PrintConsignments.PaymentMode = PrintConsignment[0].PaymentMode;
                $scope.PrintConsignments.ChequeNo = PrintConsignment[0].ChequeNo;
                $scope.PrintConsignments.TaxAmount = parseFloat(parseFloat(PrintConsignment[0].GrandTotal).toFixed(2)) - parseFloat(parseFloat(PrintConsignment[0].TotalAmount).toFixed(2));
                $scope.PrintConsignments.ConsignmentNo = PrintConsignment[0].ConsignmentNo;
                $scope.PrintConsignments.Packaging = PrintConsignment[0].Packaging;
                setTimeout(function () { $scope.printDiv(); }, 1000);

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.printDiv = function () {
        var printContents = document.getElementById("printarea").innerHTML;
        var popupWin = window.open();
        popupWin.document.open();
        popupWin.document.write('<html><body style="font-size:15px;font-family:arial" onload="window.print()">' + printContents + '</body></html>');
        popupWin.document.close();
    }
}]);
App.controller('TrackConsignment_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", function ($scope, $http, $filter, filterFilter, Notification) {
    $scope.success = false;
    $scope.ConsignmentData = null;
    $scope.TrackConsignment = function () {
        var consignmentNo = $("#txtConsignmentNo").val();
        $http({
            method: 'GET',
            params: { 'consignmentNo': consignmentNo },
            url: '/Operations/GetTrackConsignmentData',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        }).then(function successCallback(response) {
            if (response.data != null) {
                $scope.ConsignmentData = response.data;
                $scope.success = true;
            }
        },
            function errorCallback(response) {
                alert(response);
            });
    }
}]);

App.controller('AddConsignment_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", function ($scope, $http, $filter, filterFilter, Notification) {
    $scope.FormatDate = function (jsonDate) {
        if (jsonDate == null) {
            return "";
        }
        else {
            var date = new Date(parseInt(jsonDate.substr(6)));
            return date;
        }
    }
    $scope.GetConsignment = function () {
        $scope.currentDate = $filter('date')(new Date(), 'dd/MM/yyyy');
        var date = new Date();
        var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
        var am_pm = date.getHours() >= 12 ? "PM" : "AM";
        hours = hours < 10 ? "0" + hours : hours;
        var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
        time = hours + ":" + minutes + ":" + seconds + " " + am_pm;
        $scope.currentTime = time;

        $scope.consignmentBooking = {
            ConsignmentID: '',
            ConsignmentNo: '',
            ConsignmentDate: $scope.currentDate,
            ConsignmentTime: $scope.currentTime,
            PaymentTypeID: '',
            ConsignmentFromID: '',
            ConsignmentToID: '',
            DestinationID: '',
            DeliveryTypeID: '',
            BillingPartyID: '',
            TransportTypeID: '',
            ServiceTaxId: '',
            CargotTypeID: '',
            TransportModeID: '',
            DoorCollectionID: '',
            VehicleID: '',
            PinCodeID: '',
            ODAID: '',
            ConsignorID: '',
            ConsignorName: '',
            ConsignorGSTNo: '',
            ConsigneeID: '',
            ConsigneeName: '',
            ConsigneeGSTNo: '',
            ContactPersonName: '',
            MobileNumber: '',
            ConsignorAddress: '',
            ConsigneeAddress: '',
            BranchReservedID: $("#hdnBranchReservedID").val()
        };
        $scope.BookingPayment = {
            BookingPaymentID: '',
            ConsignmentID: '',
            GoodsDescription: '',
            EwayBillNo: '',
            VendorCode: '',
            InsuranceFlag: '',
            RoadPermitAttached: '',
            Documents: '',
            FOVId: '',
            PaymentTypeID: '',
            BankName: '',
            BranchName: '',
            ChequeNo: '',
            ChequeDate: '',
            PaymentCollected: '',
            Remark: '',
            StandardRate: '',
            BasicFreight: '',
            DocketCharges: '',
            Pickup: '',
            FOV: 0,
            HamaliCharges: '',
            DDCharges: '',
            ODACharges: '',
            OtherCharges: '',
            OtherAOC: '',
            TotalAmount: '',
            SGST: '',
            CGST: '',
            IGST: '',
            GrandTotal: ''
        };
        $scope.GetPaymentTypes();
        $scope.GetOtherBranches();
        $scope.GetBranches();
        $scope.GetDeliveryTypes();
        $scope.GetVendors();
        $scope.GetTransportTypes();
        $scope.GetServiceTaxpaidBy();
        $scope.GetCargoTypes();
        $scope.GetTransportModes();
        $scope.GetDoorCollections();
        $scope.GetVehicles();
        $scope.GetPinCodes();
        $scope.GetODA();

    };

    $scope.GetPaymentTypes = function () {

        var pmtTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetPaymentTypes',
            async: true,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        }).then(function successCallback(response) {
            for (i = 0; i < response.data.length; i++) {
                pmtTypes.push(response.data[i]);
            }
            $scope.PaymentTypes = pmtTypes;

        },
            function errorCallback(response) {
                alert(response);
            });
    };
    $scope.GetOtherBranches = function () {

        var otherBranches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetOtherBranches',
            async: true,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    otherBranches.push(response.data[i]);
                }
                $scope.OtherBranches = otherBranches;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetBranches = function () {

        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            async: true,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    brnches.push(response.data[i]);
                }
                $scope.Branches = brnches;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetDeliveryTypes = function () {

        var delvTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetDeliveryTypes',
            async: true,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    delvTypes.push(response.data[i]);
                }
                $scope.DeliveryTypes = delvTypes;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetVendors = function () {

        var vendor = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVendors',
            async: true,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    vendor.push(response.data[i]);
                }
                $scope.Vendors = vendor;
                $scope.consignorVendors = vendor;
                $scope.BillingVendors = vendor;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetTransportTypes = function () {

        var trnptType = [];
        $http({
            method: 'GET',
            url: '/Operations/GetTransportTypes',
            async: true,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    trnptType.push(response.data[i]);
                }
                $scope.TransportTypes = trnptType;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetServiceTaxpaidBy = function () {

        var taxes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetServiceTaxpaidBy',
            async: true,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    taxes.push(response.data[i]);
                }
                $scope.ServiceTaxPaidBy = taxes;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetCargoTypes = function () {

        var crgoTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetCargoTypes',
            async: true,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    crgoTypes.push(response.data[i]);
                }
                $scope.CargoTypes = crgoTypes;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetTransportModes = function () {

        var transmode = [];
        $http({
            method: 'GET',
            url: '/Operations/GetTransportModes',
            async: true,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    transmode.push(response.data[i]);
                }
                $scope.Transportmodes = transmode;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetDoorCollections = function () {

        var dcColl = [];
        $http({
            method: 'GET',
            url: '/Operations/GetDoorCollections',
            async: true,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    dcColl.push(response.data[i]);
                }
                $scope.DoorCollections = dcColl;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetVehicles = function () {
        var vehList = [];
        $http({
            method: 'GET',
            url: '/Operations/GetLocalVehiclesHired',
            async: true,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    vehList.push(response.data[i]);
                }
                $scope.Vehicles = vehList;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetPinCodes = function () {
        var pinCodeList = [];
        $http({
            method: 'GET',
            url: '/Operations/GetPinCodes',
            async: true,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    pinCodeList.push(response.data[i]);
                }
                $scope.PinCodes = pinCodeList;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetODA = function () {
        var ODAList = [];
        $http({
            method: 'GET',
            url: '/Operations/GetODA',
            async: true,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    ODAList.push(response.data[i]);
                }
                $scope.ODA = ODAList;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }

    $scope.GetConsignorAddress = function () {
        try {
            var cnName = $scope.consignmentBooking.ConsignorID;
            var vendorCon = filterFilter($scope.consignorVendors, { VendorID: cnName }, true);
            if (vendorCon.length > 0) {
                $scope.consignmentBooking.ConsignorAddress = vendorCon[0].Address;
                $scope.consignmentBooking.ConsignorName = vendorCon[0].VendorName;
                $scope.consignmentBooking.ConsignorGSTNo = vendorCon[0].PostalCode;
                if ($("#ddlPaymentType option:selected").text().toLowerCase() == "paid") {
                    $scope.consignmentBooking.BillingPartyID = cnName;
                }

            }

        }
        catch (ex) {
            throw ex;
        }
    }
    $scope.GetConsigneeAddress = function () {
        try {

            var cnName = $scope.consignmentBooking.ConsigneeID;
            var vendorConsinee = filterFilter($scope.Vendors, { VendorID: cnName }, true);
            if (vendorConsinee.length > 0) {
                $scope.consignmentBooking.ConsigneeAddress = vendorConsinee[0].Address;
                $scope.consignmentBooking.ConsigneeName = vendorConsinee[0].VendorName;
                $scope.consignmentBooking.ConsigneeGSTNo = vendorConsinee[0].PostalCode;
                if ($("#ddlPaymentType option:selected").text().toLowerCase() == "to pay") {
                    $scope.consignmentBooking.BillingPartyID = $scope.consignmentBooking.ConsigneeID;
                }
            }
        }
        catch (ex) {
            throw ex;
        }
    }
    $scope.isShowAfterConsignment = false;
    $scope.isShowAfterAddConsignment = false;
    $scope.isDisableConsignmentBooking = false;

    $scope.isBillingParty = false;
    $scope.ShowBillingParty = function () {
        var ptID = $scope.consignmentBooking.PaymentTypeID;
        var objPaymentType = filterFilter($scope.PaymentTypes, { PaymentTypeID: ptID });
        if (objPaymentType != null) {
            if (objPaymentType[0].PaymentType.toLowerCase() == 'billing' || objPaymentType[0].PaymentType.toLowerCase() == 'mhel' || objPaymentType[0].PaymentType.toLowerCase() == 'mahindra') {
                $scope.isBillingParty = true;
                if (objPaymentType[0].PaymentType.toLowerCase() == 'mhel' || objPaymentType[0].PaymentType.toLowerCase() == 'mahindra') {
                    var vendor = [];
                    $http({
                        method: 'GET',
                        url: '/Operations/GetMHELVendors',
                        headers: { 'Content-Type': 'application/json; charset=utf-8' }
                    })
                        .then(function successCallback(response) {
                            for (i = 0; i < response.data.length; i++) {
                                vendor.push(response.data[i]);
                            }
                            $scope.BillingVendors = vendor;
                        },
                            function errorCallback(response) {
                                alert(response);
                            });
                }
                else {
                    var vendor = [];
                    $http({
                        method: 'GET',
                        url: '/Operations/GetVendors',
                        headers: { 'Content-Type': 'application/json; charset=utf-8' }
                    })
                        .then(function successCallback(response) {
                            for (i = 0; i < response.data.length; i++) {
                                vendor.push(response.data[i]);
                            }
                            $scope.BillingVendors = vendor;
                        },
                            function errorCallback(response) {
                                alert(response);
                            });
                }
            }
            else { $scope.isBillingParty = false; }
        }
        if ($("#ddlPaymentType option:selected").text().toLowerCase() == "paid") {
            $scope.consignmentBooking.BillingPartyID = $scope.consignmentBooking.ConsignorID;
        }
        if ($("#ddlPaymentType option:selected").text().toLowerCase() == "to pay") {
            $scope.consignmentBooking.BillingPartyID = $scope.consignmentBooking.ConsigneeID;
        }
        if ($("#ddlPaymentType option:selected").text().toLowerCase() == "foc") {
            $scope.consignmentBooking.BillingPartyID = '';
        }
    }

    $scope.isDoorCollection = false;
    $scope.ShowVehicles = function () {
        var doorCollectionID = $scope.consignmentBooking.DoorCollectionID;
        var objDoorColl = filterFilter($scope.DoorCollections, { DoorCollectionID: doorCollectionID }, true);
        if (objDoorColl != null) {
            if (objDoorColl[0].DoorCollection == 'Yes') {
                $scope.isDoorCollection = true;
            }
            else { $scope.isDoorCollection = false; }
        }
    }

    $scope.isShowDocument = false;
    $scope.ShowDocuments = function () {
        var roadPermitAttached = $scope.consignmentBooking.RoadPermitAttached;

        if (roadPermitAttached != "true") {
            $scope.isShowDocument = false;
        }
        else { $scope.isShowDocument = true; }

    }

    $scope.isShowODA = false;
    $scope.ShowODA = function () {
        var pincode = $("#dpdnPincode option:selected").text();
        if (pincode == 'ODA') {
            $scope.isShowODA = true;
        }
        else { $scope.isShowODA = false; }

    }


    $scope.SaveBookings = function () {
        $("#btnAddConsignment").attr("disabled", true);
        if ($scope.frmBookings.$valid) {

            // set billing party
            var ptID = $scope.consignmentBooking.PaymentTypeID;
            var objPaymentType = filterFilter($scope.PaymentTypes, { PaymentTypeID: ptID });
            if (objPaymentType != null) {
                if (objPaymentType[0].PaymentType.toLowerCase() == 'paid') {
                    $scope.consignmentBooking.BillingPartyID = $scope.consignmentBooking.ConsignorID
                }
                if (objPaymentType[0].PaymentType.toLowerCase() == 'to pay') {
                    $scope.consignmentBooking.BillingPartyID = $scope.consignmentBooking.ConsigneeID
                }

            }
            if ($scope.consignmentBooking.ConsignmentID == '') {
                $http({
                    method: 'POST',
                    url: '/Operations/AddConsignmentBooking',
                    data: JSON.stringify($scope.consignmentBooking),
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
                    .then(function successCallback(response) {
                        console.log(response);

                        Notification.success({ message: "Consignment Booking saved successfully.", delay: 5000 });
                        var consignmentID = response.data;

                        $scope.consignmentBooking.ConsignmentID = consignmentID;
                        $scope.isShowAfterAddConsignment = true;
                        $scope.isDisableConsignmentBooking = true;
                        $scope.GetInvoiceCount();
                        $("#btnAddConsignment").attr("disabled", true);
                    }, function errorCallback(response) {
                        Notification.error({ message: response.message, delay: 5000 });
                    });
            }
            else {
                $http({
                    method: 'POST',
                    url: '/Operations/UpdateConsignmentBooking',
                    data: JSON.stringify($scope.consignmentBooking),
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
                    .then(function successCallback(response) {
                        Notification.success({ message: "Consignment Booking saved successfully.", delay: 5000 });
                        $("#btnAddConsignment").attr("disabled", true);
                    }, function errorCallback(response) {
                        Notification.error({ message: response.message, delay: 5000 });
                    });
            }



        }
        else {
            $("#btnAddConsignment").attr("disabled", false);
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.clearBookings = function () {
        $scope.consignmentBooking = {
            ConsignmentID: '',
            ConsignmentNo: '',
            ConsignmentDate: $scope.currentDate,
            ConsignmentTime: $scope.currentTime,
            PaymentTypeID: '',
            ConsignmentFromID: '',
            ConsignmentToID: '',
            DestinationID: '',
            DeliveryTypeID: '',
            BillingPartyID: '',
            TransportTypeID: '',
            ServiceTaxId: '',
            CargotTypeID: '',
            TransportModeID: '',
            DoorCollectionID: '',
            VehicleID: '',
            PinCodeID: '',
            ODAID: '',
            ConsignorID: '',
            ConsignorName: '',
            ConsignorGSTNo: '',
            ConsigneeID: '',
            ConsigneeName: '',
            ConsigneeGSTNo: '',
            ContactPersonName: '',
            MobileNumber: '',
            ConsignorAddress: '',
            ConsigneeAddress: ''
        };
    }

    /* Invoice */

    $scope.GetInvoiceTotal = function (consignmentID) {
        $http({
            method: 'GET',
            url: '/Operations/GetInvoiceTotal',
            params: { 'consignmentID': consignmentID },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {

                var invoiceTotal = response.data;
                $scope.BookingPayment.FOV = (invoiceTotal * 0.2) / 100
            },
                function errorCallback(response) {
                    alert(response);
                });

    }
    $scope.GetInvoiceCount = function () {
        $scope.IsDisableAddInvoice = false;
        $http({
            method: 'GET',
            url: '/Operations/GetAttributeValueByKey',
            params: { 'key': 'INVOICE_COUNT' },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                $scope.InvoiceCount = response.data;
                if ($scope.partyInvoiceCollection != undefined && $scope.partyInvoiceCollection.length == response.data) {
                    $scope.IsDisableAddInvoice = true;
                }
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetPackagings = function () {

        var pkgs = [];
        $http({
            method: 'GET',
            url: '/Operations/GetPackagings',
            async: true,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    pkgs.push(response.data[i]);
                }
                $scope.Packagings = pkgs;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.ChargeTypes = [];
    $scope.GetChargeTypes = function () {

        var chrgTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetChargeTypes',
            async: true,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    chrgTypes.push(response.data[i]);
                }
                $scope.ChargeTypes = chrgTypes;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetPartNos = function () {
        var partnos = [];
        $http({
            method: 'GET',
            url: '/Operations/GetPartNos',
            async: true,
            params: { 'vendorID': $scope.consignmentBooking.ConsignorID },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    partnos.push(response.data[i]);
                }
                $scope.PartNos = partnos;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.partyInvoiceCollection = [];
    $scope.activePartyInvoices = '';
    $scope.showPartyInvoiceModal = function () {

        if ($scope.consignmentBooking.ConsignmentID != '') {
            $('#txtInvoiceDate').datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                forceParse: false
            });
            $scope.GetPackagings();
            $scope.GetChargeTypes();

            $scope.ConsignmentPartyInvoice = {
                PartyInvoiceID: '',
                ConsignmentID: $scope.consignmentBooking.ConsignmentID,
                InvoiceNo: '',
                InvoiceDate: '',
                InvoiceQuantity: '',
                InvoiceValue: '',
                PONo: '',
                PartNo: '',
                ASNNo: '',
                Article: '',
                PackagingID: '',
                ChargeType: '',
                ActualWeight: '',
                ChargeWeight: '',
                StandardRate: '',
                RatePerKG: '',
                BasicFreight: '',
                LxWxH: '',
                Status: ''
            };
            //check payment type
            var ptID = $scope.consignmentBooking.PaymentTypeID;
            var objPaymentType = filterFilter($scope.PaymentTypes, { PaymentTypeID: ptID });
            if (objPaymentType != null) {
                if (objPaymentType[0].PaymentType.toLowerCase() == 'mhel') {
                    $scope.GetPartNos();
                    $scope.IsMHEL = true;
                    $scope.IsMahindra = false;
                    $scope.ConsignmentPartyInvoice.ChargeType = 1;
                }
                else if (objPaymentType[0].PaymentType.toLowerCase() == 'mahindra') {
                    $scope.IsMahindra = true;
                    $scope.IsMHEL = false;
                    $scope.ConsignmentPartyInvoice.ChargeType = 1;

                }
                else {
                    $scope.IsMHEL = false;
                    $scope.IsMahindra = false;
                }
            }
            $('#PartyInvoiceModal').modal('show'); $("#btnSaveInvoice").attr("disabled", false)
        }
        else {
            Notification.error({ message: 'Please add consignment booking for party Invoices', delay: 5000 });
        }
    }
    $scope.editPartyInvoiceModal = function (partyinvoiceid) {

        if ($scope.consignmentBooking.ConsignmentID != '') {
            $('#txtInvoiceDate').datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                forceParse: false
            });
            $scope.GetPackagings();
            $scope.GetChargeTypes();

            $scope.ConsignmentPartyInvoice = {
                PartyInvoiceID: '',
                ConsignmentID: $scope.consignmentBooking.ConsignmentID,
                InvoiceNo: '',
                InvoiceDate: '',
                InvoiceQuantity: '',
                InvoiceValue: '',
                PONo: '',
                PartNo: '',
                ASNNo: '',
                Article: '',
                PackagingID: '',
                ChargeType: '',
                ActualWeight: '',
                ChargeWeight: '',
                StandardRate: '',
                RatePerKG: '',
                BasicFreight: '',
                LxWxH: '',
                Status: ''
            };
            //get party invoice

            $http({
                method: 'GET',
                params: { 'partyInvoiceId': partyinvoiceid, },
                url: '/Operations/GetConsignmentPartyInvoiceByID',
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {

                    if (response.data.length > 0) {
                        $scope.ConsignmentPartyInvoice = {
                            PartyInvoiceID: response.data[0].PartyInvoiceID,
                            ConsignmentID: $scope.consignmentBooking.ConsignmentID,
                            InvoiceNo: response.data[0].InvoiceNo,
                            InvoiceDate: $filter('date')($scope.FormatDate(response.data[0].InvoiceDate), 'dd/MM/yyyy'),
                            InvoiceQuantity: response.data[0].InvoiceQuantity,
                            InvoiceValue: response.data[0].InvoiceValue,
                            PONo: response.data[0].PONo,
                            PartNo: response.data[0].PartNo,
                            ASNNo: response.data[0].ASNNo,
                            Article: response.data[0].Article,
                            PackagingID: response.data[0].PackagingID,
                            ChargeType: response.data[0].ChargeType,
                            ActualWeight: response.data[0].ActualWeight,
                            ChargeWeight: response.data[0].ChargeWeight,
                            StandardRate: response.data[0].StandardRate,
                            RatePerKG: response.data[0].RatePerKG,
                            BasicFreight: response.data[0].BasicFreight,
                            LxWxH: response.data[0].LxWxH,
                            Status: response.data[0].Status
                        };
                    }


                },
                    function errorCallback(response) {
                        alert(response);
                    });

            //check payment type
            var ptID = $scope.consignmentBooking.PaymentTypeID;
            var objPaymentType = filterFilter($scope.PaymentTypes, { PaymentTypeID: ptID });
            if (objPaymentType != null) {
                if (objPaymentType[0].PaymentType.toLowerCase() == 'mhel') {
                    $scope.GetPartNos();
                    $scope.IsMHEL = true;
                    $scope.IsMahindra = false;
                    $scope.ConsignmentPartyInvoice.ChargeType = 1;
                }
                else if (objPaymentType[0].PaymentType.toLowerCase() == 'mahindra') {
                    $scope.IsMahindra = true;
                    $scope.IsMHEL = false;
                    $scope.ConsignmentPartyInvoice.ChargeType = 1;

                }
                else {
                    $scope.IsMHEL = false;
                    $scope.IsMahindra = false;
                }
            }
            $('#PartyInvoiceModal').modal('show');
        }
        else {
            Notification.error({ message: 'Please add consignment booking for party Invoices', delay: 5000 });
        }
    }
    $scope.GetVendorRates = function () {

        var vendorrates = {};
        vendorrates.BillingPartyId = parseInt($scope.consignmentBooking.BillingPartyID);
        vendorrates.ConsignorId = parseInt($scope.consignmentBooking.ConsignorID);
        vendorrates.ConsigneeId = parseInt($scope.consignmentBooking.ConsigneeID);
        vendorrates.FROM = parseInt($scope.consignmentBooking.ConsignmentFromID);
        vendorrates.TO = parseInt($scope.consignmentBooking.ConsignmentToID);
        vendorrates.ChargeTypeId = parseInt($scope.ConsignmentPartyInvoice.ChargeType);

        $http({
            method: 'GET',
            url: '/Operations/GetVendorRateMaster',
            params: { 'vendorrates': JSON.stringify(vendorrates) },
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            async: false
        })
            .success(function (response) {
                var vendorrateList = [];
                for (i = 0; i < response.length; i++) {
                    vendorrateList.push(response[i]);
                }
                $scope.vendorrate = vendorrateList;
                if (response.length > 0) {

                    $scope.ConsignmentPartyInvoice.RatePerKG = $scope.vendorrate[0].FRIEGHTRATE;
                    $scope.BookingPayment.DocketCharges = $scope.vendorrate[0].DocketCharge;
                    //Check DOORCOLLECTION
                    if ($scope.consignmentBooking.DoorCollectionID == 3) {
                        if ($scope.vendorrate[0].DoorCollection != '')
                            $scope.BookingPayment.Pickup = $scope.vendorrate[0].DoorCollection;
                        else
                            $scope.BookingPayment.Pickup = 0;
                    }
                    //if godown
                    if ($scope.consignmentBooking.DeliveryTypeID != 1) {
                        $scope.BookingPayment.DDCharges = $scope.vendorrate[0].DeliveryCharge;
                    }
                    $scope.BookingPayment.FOV = $scope.vendorrate[0].FOV;
                    $scope.BookingPayment.HamaliCharges = $scope.vendorrate[0].HamaliCharge;
                    $scope.BookingPayment.OtherCharges = $scope.vendorrate[0].OtherCharges;
                    $scope.noteditableratemaster = true;
                    $scope.isDisableFOV = true;
                    $scope.CalculateBasicFreight("basic");
                    $scope.CalculateTotalAmount();
                }
                else {
                    $scope.noteditableratemaster = false;
                }
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetMHELVendorRates = function () {

        var vendorrates = {};
        vendorrates.BillingPartyId = parseInt($scope.consignmentBooking.BillingPartyID);
        vendorrates.ConsignorId = parseInt($scope.consignmentBooking.ConsignorID);
        vendorrates.ConsigneeId = parseInt($scope.consignmentBooking.ConsigneeID);
        vendorrates.FROM = parseInt($scope.consignmentBooking.ConsignmentFromID);
        vendorrates.TO = parseInt($scope.consignmentBooking.ConsignmentToID);
        vendorrates.ChargeTypeId = parseInt($scope.ConsignmentPartyInvoice.ChargeType);
        var ptID = $scope.ConsignmentPartyInvoice.PartNo;
        var objPartNo = filterFilter($scope.PartNos, { PartNoID: ptID });
        if (objPartNo != null) {
            vendorrates.PartNo = objPartNo[0].PartNo;
            vendorrates.PerPieceWeight = parseFloat(objPartNo[0].PerPriceWeight);
        }
        $http({
            method: 'GET',
            url: '/Operations/GetMHELVendorRates',
            params: { 'vendorrates': JSON.stringify(vendorrates) },
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            async: false
        })
            .success(function (response) {
                var vendorrateList = [];
                for (i = 0; i < response.length; i++) {
                    vendorrateList.push(response[i]);
                }
                $scope.vendorrate = vendorrateList;
                if (response.length > 0) {
                    var minKG = $scope.vendorrate[0].MinKG;
                    if ($scope.ConsignmentPartyInvoice.ActualWeight < parseFloat(minKG)) {
                        $scope.ConsignmentPartyInvoice.ActualWeight = minKG;
                        $scope.ConsignmentPartyInvoice.ChargeWeight = minKG;
                    }

                    $scope.ConsignmentPartyInvoice.RatePerKG = $scope.vendorrate[0].FRIEGHTRATE;
                    $scope.BookingPayment.DocketCharges = $scope.vendorrate[0].DocketCharge;
                    //Check DOORCOLLECTION
                    if ($scope.consignmentBooking.DoorCollectionID == 3) {
                        if ($scope.vendorrate[0].DoorCollection != '')
                            $scope.BookingPayment.Pickup = $scope.vendorrate[0].DoorCollection;
                        else
                            $scope.BookingPayment.Pickup = 0;
                    }
                    //if godown
                    if ($scope.consignmentBooking.DeliveryTypeID != 1) {
                        $scope.BookingPayment.DDCharges = $scope.vendorrate[0].DeliveryCharge;
                    }
                    $scope.BookingPayment.FOV = $scope.vendorrate[0].FOV;
                    $scope.BookingPayment.HamaliCharges = $scope.vendorrate[0].HamaliCharge;
                    $scope.BookingPayment.OtherCharges = $scope.vendorrate[0].OtherCharges;
                    $scope.noteditableratemaster = true;
                    $scope.isDisableFOV = true;
                    $scope.ConsignmentPartyInvoice.BasicFreight = parseFloat($scope.vendorrate[0].FRIEGHTRATE) * parseFloat($scope.ConsignmentPartyInvoice.ActualWeight);
                    $scope.CalculateTotalAmount();
                }
                else {
                    $scope.noteditableratemaster = false;
                }
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetMahindraVendorRates = function () {
        var ptID = $scope.consignmentBooking.PaymentTypeID;
        var objPaymentType = filterFilter($scope.PaymentTypes, { PaymentTypeID: ptID });
        if (objPaymentType[0].PaymentType.toLowerCase() == "mahindra") {
            var vendorrates = {};
            vendorrates.BillingPartyId = parseInt($scope.consignmentBooking.BillingPartyID);
            vendorrates.ConsignorId = parseInt($scope.consignmentBooking.ConsignorID);
            vendorrates.ConsigneeId = parseInt($scope.consignmentBooking.ConsigneeID);
            vendorrates.FROM = parseInt($scope.consignmentBooking.ConsignmentFromID);
            vendorrates.TO = parseInt($scope.consignmentBooking.ConsignmentToID);
            vendorrates.ChargeTypeId = parseInt($scope.ConsignmentPartyInvoice.ChargeType);
            vendorrates.Boxes = parseInt($scope.ConsignmentPartyInvoice.InvoiceQuantity);
            $http({
                method: 'GET',
                url: '/Operations/GetMahindraVendorRates',
                params: { 'vendorrates': JSON.stringify(vendorrates) },
                headers: { 'Content-Type': 'application/json; charset=utf-8' },
                async: false
            })
                .success(function (response) {
                    var vendorrateList = [];
                    for (i = 0; i < response.length; i++) {
                        vendorrateList.push(response[i]);
                    }
                    $scope.vendorrate = vendorrateList;
                    if (response.length > 0) {
                        var minKG = $scope.vendorrate[0].MinKG;
                        $scope.ConsignmentPartyInvoice.ActualWeight = $scope.vendorrate[0].ChargedWeight;
                        $scope.ConsignmentPartyInvoice.ChargeWeight = $scope.vendorrate[0].ChargedWeight;
                        $scope.ConsignmentPartyInvoice.LxWxH = $scope.vendorrate[0].L + '×' + $scope.vendorrate[0].W + '×' + $scope.vendorrate[0].H;
                        if (parseFloat($scope.ConsignmentPartyInvoice.ActualWeight) < parseFloat(minKG)) {
                            $scope.ConsignmentPartyInvoice.ActualWeight = minKG;
                            $scope.ConsignmentPartyInvoice.ChargeWeight = minKG;
                        }
                        $scope.ConsignmentPartyInvoice.RatePerKG = $scope.vendorrate[0].FRIEGHTRATE;
                        $scope.ConsignmentPartyInvoice.RatePerKG = $scope.vendorrate[0].FRIEGHTRATE;

                        $scope.BookingPayment.DocketCharges = $scope.vendorrate[0].DocketCharge;

                        //Check DOORCOLLECTION
                        if ($scope.consignmentBooking.DoorCollectionID == 3) {
                            if ($scope.vendorrate[0].DoorCollection != '')
                                $scope.BookingPayment.Pickup = $scope.vendorrate[0].DoorCollection;
                            else
                                $scope.BookingPayment.Pickup = 0;
                        }
                        //if godown
                        if ($scope.consignmentBooking.DeliveryTypeID != 1) {
                            $scope.BookingPayment.DDCharges = $scope.vendorrate[0].DeliveryCharge;
                        }
                        $scope.BookingPayment.FOV = $scope.vendorrate[0].FOV;
                        $scope.BookingPayment.HamaliCharges = $scope.vendorrate[0].HamaliCharge;
                        $scope.BookingPayment.OtherCharges = $scope.vendorrate[0].OtherCharges;
                        $scope.noteditableratemaster = true;
                        $scope.isDisableFOV = true;
                        $scope.ConsignmentPartyInvoice.BasicFreight = parseFloat($scope.vendorrate[0].FRIEGHTRATE) * parseFloat($scope.ConsignmentPartyInvoice.ActualWeight);
                        $scope.CalculateTotalAmount();
                    }
                    else {
                        $scope.noteditableratemaster = false;
                    }
                },
                    function errorCallback(response) {
                        alert(response);
                    });
        }
    }
    $scope.CalculateBasicFreight = function (action) {

        var chargetypeSelected = $scope.ConsignmentPartyInvoice.ChargeType;
        var chargeType = filterFilter($scope.ChargeTypes, { ChargeTypeID: chargetypeSelected });
        var basicFreight = 0;
        if (chargeType.length > 0) {
            var chargeCode = chargeType[0].ChargeType;
            switch (chargeCode) {
                case 'Per Kg':
                    {
                        var chargeWt = 0;
                        if ($scope.ConsignmentPartyInvoice.ChargeWeight != '')
                            chargeWt = $scope.ConsignmentPartyInvoice.ChargeWeight;
                        var rateperKG = 0;
                        if ($scope.ConsignmentPartyInvoice.RatePerKG != '')
                            rateperKG = $scope.ConsignmentPartyInvoice.RatePerKG

                        basicFreight = chargeWt * rateperKG;
                        break;
                    }
                case 'Article':
                    {

                        var noOfArticles = 0;
                        if ($scope.ConsignmentPartyInvoice.Article != '')
                            noOfArticles = $scope.ConsignmentPartyInvoice.Article;
                        var rateperKG = 0;
                        if ($scope.ConsignmentPartyInvoice.RatePerKG != '')
                            rateperKG = $scope.ConsignmentPartyInvoice.RatePerKG

                        basicFreight = noOfArticles * rateperKG;
                        break;
                    }
                case 'CFT':
                    {
                        var noOfArticles = 0;
                        if ($scope.ConsignmentPartyInvoice.Article != '')
                            noOfArticles = $scope.ConsignmentPartyInvoice.Article;
                        var LWH = 0;
                        if ($scope.ConsignmentPartyInvoice.LxWxH != '')
                            LWH = $scope.ConsignmentPartyInvoice.LxWxH

                        basicFreight = noOfArticles * (LWH / 1728) * 12;
                        break;
                    }

            }
        }

        $scope.ConsignmentPartyInvoice.BasicFreight = basicFreight;
        if (action == "chargetype") {
            var standardrate = 0;
            var chargetypeSelected = $scope.ConsignmentPartyInvoice.ChargeType;
            var fromBranch = $scope.consignmentBooking.ConsignmentFromID;
            var toBranch = $scope.consignmentBooking.ConsignmentToID;

            $http({
                method: 'GET',
                url: '/Operations/GetStandardRate',
                params: { 'ChargeType': chargetypeSelected, 'FromBranch': fromBranch, 'ToBranch': toBranch },
                headers: { 'Content-Type': 'application/json; charset=utf-8' },
                async: false
            })
                .success(function (response) {
                    standardrate = response;
                    $scope.ConsignmentPartyInvoice.StandardRate = parseFloat(standardrate);
                },
                    function errorCallback(response) {
                        alert(response);
                    });

            $scope.ConsignmentPartyInvoice.StandardRate = parseFloat(standardrate);
            $scope.GetVendorRates();
        }
        if (action == "partno") {
            var ptID = $scope.ConsignmentPartyInvoice.PartNo;
            var objPartNo = filterFilter($scope.PartNos, { PartNoID: ptID });
            if (objPartNo != null) {
                var weightperpiece = parseFloat(objPartNo[0].PerPriceWeight);
                var totalWeight = parseFloat($scope.ConsignmentPartyInvoice.InvoiceQuantity) * weightperpiece;
                $scope.ConsignmentPartyInvoice.ActualWeight = totalWeight;
                $scope.ConsignmentPartyInvoice.ChargeWeight = totalWeight;
            }
            $scope.GetMHELVendorRates();
        }

    }
    $scope.SavePartyInvoices = function () {
        $("#btnSaveInvoice").attr("disabled", true)
        if ($scope.frmInvoice.$valid && $scope.ConsignmentPartyInvoice.ConsignmentID != '') {
            var aryDate = ($scope.ConsignmentPartyInvoice.InvoiceDate.toString().split('/'));
            $scope.ConsignmentPartyInvoice.InvoiceDate = aryDate[2] + "-" + aryDate[1] + "-" + aryDate[0];
            var ptID = $scope.consignmentBooking.PaymentTypeID;
            var objPaymentType = filterFilter($scope.PaymentTypes, { PaymentTypeID: ptID });
            if (objPaymentType != null) {
                if (objPaymentType[0].PaymentType.toLowerCase() == 'mhel') {
                    var objPartNo = filterFilter($scope.PartNos, { PartNoID: $scope.consignmentBooking.PartNoID });
                    if (objPartNo != null) {
                        $scope.ConsignmentPartyInvoice.PartNo = objPartNo[0].PartNo;
                    }
                }
            }
            if ($scope.ConsignmentPartyInvoice.PartyInvoiceID == '') {
                $http({
                    method: 'POST',
                    url: '/Operations/AddConsignmentPartyInvoice',
                    dataType: 'json',
                    traditional: true,
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify($scope.ConsignmentPartyInvoice),
                })
                    .then(function successCallback(response) {
                        Notification.success({ message: "Consignment Party Invoices saved successfully. You need to save payment data to add invoice into payment.", delay: 5000 });
                        $scope.clearInvoice();
                        $scope.loadPartyInvoice($scope.ConsignmentPartyInvoice.ConsignmentID, 'add');
                        $scope.closeInvoiceModal();
                        $scope.isShowAfterConsignment = true;

                    }, function errorCallback(response) {
                        Notification.error({ message: response.message, delay: 5000 });
                    });
            }
            else {
                $http({
                    method: 'POST',
                    url: '/Operations/UpdateConsignmentPartyInvoice',
                    dataType: 'json',
                    traditional: true,
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify($scope.ConsignmentPartyInvoice),
                })
                    .then(function successCallback(response) {
                        Notification.success({ message: "Consignment Party Invoices updated successfully.", delay: 5000 });
                        $scope.clearInvoice();
                        $scope.loadPartyInvoice($scope.ConsignmentPartyInvoice.ConsignmentID, 'add');
                        $scope.closeInvoiceModal();
                        $scope.isShowAfterConsignment = true;

                    }, function errorCallback(response) {
                        Notification.error({ message: response.message, delay: 5000 });
                    });
            }

        }
        else {
            $("#btnSaveInvoice").attr("disabled", false)
            Notification.error({ message: 'Please fill all required fields!', delay: 5000 });
        }

    }
    $scope.clearInvoice = function () {
        $scope.ConsignmentPartyInvoice = {
            PartyInvoiceID: '',
            ConsignmentID: $scope.consignmentBooking.ConsignmentID,
            InvoiceNo: '',
            InvoiceDate: '',
            InvoiceQuantity: '',
            InvoiceValue: '',
            PONo: '',
            PartNo: '',
            ASNNo: '',
            Article: '',
            PackagingID: '',
            ChargeType: '',
            ActualWeight: '',
            ChargeWeight: '',
            StandardRate: '',
            RatePerKG: '',
            BasicFreight: '',
            LxWxH: '',
            Status: ''
        };
    }
    $scope.loadPartyInvoice = function (consignmentID, action) {

        var invoices = [];
        $http({
            method: 'GET',
            params: { 'consignmentID': consignmentID, },
            url: '/Operations/GetConsignmentPartyInvoice',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                if (response.data.length > 0) {
                    $scope.isShowAfterConsignment = true;
                }
                else {
                    $scope.isShowAfterConsignment = false;
                }
                for (i = 0; i < response.data.length; i++) {
                    invoices.push(response.data[i]);
                }
                $scope.activePartyInvoices = invoices;
                $scope.partyInvoiceCollection = invoices;

                if ($scope.partyInvoiceCollection.length == $scope.InvoiceCount) {
                    $scope.IsDisableAddInvoice = true;
                }
                else { $scope.IsDisableAddInvoice = false; }
                if (action == "update") {
                    $scope.GetInvoiceCount();
                    $scope.initializePayment();
                    setTimeout(function () { $scope.GetBookingPaymentByID($scope.consignmentBooking.ConsignmentID); }, 5000);
                }
                else {
                    $scope.initializePayment();
                }
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetBookingPaymentByID = function (consignmentid) {

        $http({
            method: 'GET',
            params: { 'consignmentID': consignmentid, },
            url: '/Operations/GetConsignmentBookingPaymentByID',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {

                if (response.data.length > 0) {
                    $('#chqDate').datepicker({
                        format: 'dd/mm/yyyy',
                        autoclose: true,
                        forceParse: false
                    });
                    $scope.BookingPayment = {
                        BookingPaymentID: response.data[0].BookingPaymentID,
                        ConsignmentID: response.data[0].ConsignmentID,
                        GoodsDescription: response.data[0].GoodsDescription,
                        InsuranceFlag: response.data[0].InsuranceFlag,
                        StandardRate: response.data[0].StandardRate,
                        BasicFreight: response.data[0].BasicFreight,
                        DocketCharges: response.data[0].DocketCharges,
                        DoorCollection: response.data[0].DoorCollection,
                        DoorDelivery: response.data[0].DoorDelivery,
                        LabourCharges: response.data[0].LabourCharges,
                        ODACharges: response.data[0].ODACharges,
                        FOV: response.data[0].FOV,
                        HamaliCharges: response.data[0].HamaliCharges,
                        DDCharges: response.data[0].DDCharges,
                        OtherCharges: response.data[0].OtherCharges,
                        OtherAOC: response.data[0].OtherAOC,
                        TotalAmount: response.data[0].TotalAmount,
                        ServiceTaxID: response.data[0].ServiceTaxID,
                        OtherTax: response.data[0].OtherTax,
                        GrandTotal: response.data[0].GrandTotal,
                        EwayBillNo: response.data[0].EwayBillNo,
                        VendorCode: response.data[0].VendorCode,
                        PaymentTypeID: response.data[0].PaymentTypeID,
                        BankName: response.data[0].BankName,
                        BranchName: response.data[0].BranchName,
                        ChequeNo: response.data[0].ChequeNo,
                        ChequeDate: response.data[0].ChequeDate,
                        PaymentCollected: response.data[0].PaymentCollected,
                        RoadPermitAttached: response.data[0].RoadPermitAttached,
                        Remark: response.data[0].Remark,
                        Pickup: response.data[0].Pickup,
                        FOVId: response.data[0].FOVId
                    };
                    $scope.CalculateFOV();
                    setTimeout(function () {
                        if ($scope.BookingPayment.FOVId != "") {
                            $scope.CalculateFOVCharge();
                        }
                        $scope.SelectPaymentType();
                        setTimeout(function () {
                            $scope.CalculateTotalAmount();
                        }, 5000);
                    }, 2000);

                }


            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.closeInvoiceModal = function () {
        $scope.clearInvoice();
        $('#PartyInvoiceModal').modal('hide');
    }
    $scope.DeletePartyInvoice = function (partyInvoiceID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Operations/DeleteConsignmentPartyInvoice',
                params: { 'partyInvoiceID': partyInvoiceID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    $scope.loadPartyInvoice($scope.consignmentBooking.ConsignmentID);
                }, function errorCallback(response) {

                });
        }
    }

    /*Payment Booking*/

    $scope.CalculateGrandTotal = function () {
        var grandTotal = $scope.BookingPayment.TotalAmount;
        var totalAmt = $scope.BookingPayment.TotalAmount;
        var maxTaxAmount = parseFloat($scope.TaxAmountMax);
        if (totalAmt != "" && totalAmt != 0) {
            if (parseFloat(totalAmt) > maxTaxAmount && $scope.consignmentBooking.ConsignorGSTNo == "") {
                var taxes = [];
                $http({
                    method: 'GET',
                    url: '/Operations/GetTaxes',
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
                    .then(function successCallback(response) {
                        for (i = 0; i < response.data.length; i++) {
                            taxes.push(response.data[i]);
                        }
                        $scope.paymentTaxes = taxes;
                        for (var i = 0; i < $scope.paymentTaxes.length; i++) {
                            var taxVal = (totalAmt * $scope.paymentTaxes[i].TaxPercent) / 100;
                            $scope.paymentTaxes[i].TaxAmount = taxVal;
                            grandTotal = grandTotal + taxVal;
                        }

                        $scope.BookingPayment.GrandTotal = grandTotal;
                    },
                        function errorCallback(response) {
                            alert(response);
                        });
            }
            else {
                $scope.paymentTaxes = [];
                $scope.BookingPayment.GrandTotal = totalAmt
            }
        }

    }
    $scope.CalculateTotalAmount = function () {

        var basicfreight = (($scope.BookingPayment.BasicFreight == '') ? 0 : $scope.BookingPayment.BasicFreight);
        var pickup = (($scope.BookingPayment.Pickup == '') ? 0 : $scope.BookingPayment.Pickup);
        var fov = (($scope.BookingPayment.FOV == '' || isNaN($scope.BookingPayment.FOV)) ? 0 : $scope.BookingPayment.FOV);
        var hamalicharge = (($scope.BookingPayment.HamaliCharges == '') ? 0 : $scope.BookingPayment.HamaliCharges);
        var docketcharge = (($scope.BookingPayment.DocketCharges == '') ? 0 : $scope.BookingPayment.DocketCharges);
        var ddfreight = (($scope.BookingPayment.DDCharges == '') ? 0 : $scope.BookingPayment.DDCharges);
        var odafreight = (($scope.BookingPayment.ODACharges == '') ? 0 : $scope.BookingPayment.ODACharges);
        var othCharges = (($scope.BookingPayment.OtherCharges == '') ? 0 : $scope.BookingPayment.OtherCharges);
        var aoccharge = (($scope.BookingPayment.OtherAOC == '') ? 0 : $scope.BookingPayment.OtherAOC);
        var totalAmt = eval(basicfreight) + eval(pickup) + eval(fov) + eval(hamalicharge) +
            eval(docketcharge) + eval(ddfreight) + eval(odafreight) + eval(othCharges) + eval(aoccharge);

        $scope.BookingPayment.TotalAmount = totalAmt;
        $scope.CalculateGrandTotal();
    }
    $scope.GetDocketCharge = function () {
        var docketcharge = 0;
        $http({
            method: 'GET',
            url: '/Operations/GetDocketCharge',
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            async: true
        })
            .success(function (response) {
                docketcharge = response;
                $scope.BookingPayment.DocketCharges = parseFloat(docketcharge);
                $scope.CalculateTotalAmount();
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GoodsDesc = [];
    $scope.GetGoods = function () {
        var goods = [];
        $http({
            method: 'GET',
            url: '/Operations/GetGoods',
            async: true,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    goods.push(response.data[i]);
                }
                $scope.GoodsDesc = goods;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.bankPaymentTypes = [];
    $scope.GetbankPaymentTypes = function () {

        var pmtTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVehicleHirePaymentTypes',
            async: true,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    pmtTypes.push(response.data[i]);
                }
                $scope.bankPaymentTypes = pmtTypes;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetTaxAmountMax = function () {
        $http({
            method: 'GET',
            url: '/Operations/GetAttributeValueByKey',
            params: { 'key': 'TAX_MAX_AMOUNT' },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                $scope.TaxAmountMax = response.data;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.initializePayment = function () {
        $('#chqDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false
        });
        $scope.GetTaxAmountMax();
        $scope.paymentTaxes = [];
        $scope.isDisableFOV = true;
        $scope.isDisableDocument = true;
        $scope.isDisabledDDFreight = true;
        var totalStandardRate = 0;
        var totalBasicFreight = 0;
        for (count = 0; count < $scope.activePartyInvoices.length; count++) {
            totalStandardRate += parseFloat($scope.activePartyInvoices[count].StandardRate);
            totalBasicFreight += parseFloat($scope.activePartyInvoices[count].BasicFreight);
        }
        if (totalStandardRate = null)
            totalStandardRate = 0;
        $scope.BookingPayment.StandardRate = totalStandardRate;
        $scope.BookingPayment.BasicFreight = totalBasicFreight;
        $scope.BookingPayment.ConsignmentID = $scope.consignmentBooking.ConsignmentID;
        var cnName = $scope.consignmentBooking.ConsignorID;
        var vendorCon = filterFilter($scope.Vendors, { VendorID: cnName }, true);
        $scope.BookingPayment.VendorCode = vendorCon[0].VendorCode;
        if ($("#dpdnDCColl option:selected").text().toLowerCase() == "yes") {
            $scope.isDisablePickup = false;
        }
        else { $scope.isDisablePickup = true; }
        if ($("#dpdnDeliveryType option:selected").text().toLowerCase() == "godown") {
            $scope.isDisabledDDFreight = true;
        }
        else { $scope.isDisabledDDFreight = false; }
        var paymenttype = filterFilter($scope.PaymentTypes, { PaymentTypeID: $scope.consignmentBooking.PaymentTypeID });
        if (paymenttype[0].PaymentType.toLowerCase() == "billing") {
            $scope.BookingPayment.PaymentTypeID = 2;
            $scope.isDisableCBPPaymentType = true;
            //$scope.isDisablePaymentCollection = false;
            $scope.BookingPayment.PaymentCollected = false;
        }
        else if (paymenttype[0].PaymentType.toLowerCase() == "paid") {
            $scope.BookingPayment.PaymentCollected = true;
            //$scope.isDisablePaymentCollection = true;
            $scope.selectPaymentCollection();
            $scope.isDisableCBPPaymentType = false;
        }
        else {
            $scope.BookingPayment.PaymentCollected = false;
            $scope.isDisableCBPPaymentType = false;
            $scope.isDisablePaymentCollection = false;
        }
        if ($scope.BookingPayment.DocketCharge == '' || $scope.BookingPayment.DocketCharge == '0') {
            $scope.GetDocketCharge();
        }
        else {
            $scope.CalculateTotalAmount();
        }
        $scope.GetGoods();
        $scope.GetbankPaymentTypes();

    }
    $scope.CalculateFOV = function () {

        if ($scope.BookingPayment.InsuranceFlag == true || $scope.BookingPayment.InsuranceFlag == 'true') {
            $scope.isDisableFOV = false;
            $scope.BookingPayment.FOV = 0;
            $scope.GetFOV();

        }
        else {
            $scope.isDisableFOV = true;
            $scope.BookingPayment.FOV = 0;
        }
        $scope.CalculateTotalAmount();
    }
    $scope.CalculateFOVCharge = function () {
        var percent = $("#drpFOV option:selected").text();
        var totalInvoiceValue = 0;
        for (count = 0; count < $scope.activePartyInvoices.length; count++) {
            totalInvoiceValue += parseFloat($scope.activePartyInvoices[count].InvoiceValue);
        }

        $scope.BookingPayment.FOV = parseFloat(totalInvoiceValue) * (parseFloat(percent) / 100);
        $scope.CalculateTotalAmount();
    }
    $scope.FOVs = [];
    $scope.GetFOV = function () {

        var fov = [];
        $http({
            method: 'GET',
            url: '/Operations/GetFOV',
            async: true,
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    fov.push(response.data[i]);
                }
                $scope.FOVs = fov;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.showDocument = function () {
        if ($scope.BookingPayment.RoadPermitAttached == "true") {
            $scope.isDisableDocument = false;
        }
        else {
            $scope.isDisableDocument = true;
        }
    }
    $scope.SelectPaymentType = function () {
        if ($("#ddlPaymentTypeForPayment option:selected").text().toLowerCase() == "cash" || $("#ddlPaymentTypeForPayment option:selected").text().toLowerCase() == "billing") {
            $scope.isDisableCheque = true;
        }
        else {
            $scope.isDisableCheque = false;
        }
        if ($("#ddlPaymentTypeForPayment option:selected").text().toLowerCase() == "cheque") {
            $scope.isRequiredCheque = true;
        }
        else {
            $scope.isRequiredCheque = false;
        }
    }
    $scope.selectPaymentCollection = function () {
        if ($("#drpPaymentCollected option:selected").text().toLowerCase() == "no") {
            $scope.isDisableCheque = true;
            $scope.isDisableCBPPaymentType = false;
        }
    }
    $scope.SaveBookingPayments = function () {
        $("#btnAddPayment").attr("disabled", true);
        if ($scope.BookingPayment.ConsignmentID == '' || !($scope.frmBookingPayment.$valid)) {
            Notification.error({ message: "Consignment booking is not added !", delay: 5000 });
            isValidConsignment = false;
        }
        if ($scope.BookingPayment.InsuranceFlag == '') {
            $scope.BookingPayment.InsuranceFlag = false;
        }
        if ($scope.BookingPayment.RoadPermitAttached == '') {
            $scope.BookingPayment.RoadPermitAttached = false;
        }
        if ($scope.frmBookingPayment.$valid) {
            if ($scope.BookingPayment.BookingPaymentID == '') {

                var myData = {};
                myData["BookingPayment"] = $scope.BookingPayment;
                myData["PaymentTaxes"] = $scope.paymentTaxes;

                $http({
                    method: 'POST',
                    url: '/Operations/AddConsignmentPayment',
                    data: JSON.stringify(myData),
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
                    .then(function successCallback(response) {
                        window.location.href = "/Operations/Consignment";
                    }, function errorCallback(response) {
                        Notification.error({ message: response.message, delay: 5000 });
                    });
            }



            else {
                var myData = {};
                myData["BookingPayment"] = $scope.BookingPayment;
                myData["PaymentTaxes"] = $scope.paymentTaxes;
                $http({
                    method: 'POST',
                    url: '/Operations/UpdateConsignmentPayment',
                    data: JSON.stringify(myData),
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
                    .then(function successCallback(response) {
                        console.log(response);
                        Notification.success({ message: "Consignment Booking Payment saved successfully.", delay: 5000 });
                    }, function errorCallback(response) {
                        Notification.error({ message: response.message, delay: 5000 });
                    });
            }
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            $("#btnAddPayment").attr("disabled", false);
            return;
        }

    }
    $scope.clearBookingPayments = function () {
        $scope.BookingPayment = {
            BookingPaymentID: '',
            ConsignmentID: '',
            GoodsDescription: '',
            InsuranceFlag: '',
            StandardRate: '',
            BasicFreight: '',
            DocketCharges: '',
            DoorCollection: '',
            DoorDelivery: 0,
            LabourCharges: '',
            ODACharges: '',
            FOV: '',
            HamaliCharges: '',
            DDCharges: 0,
            OtherCharges: '',
            OtherAOC: '',
            TotalAmount: '',
            ServiceTaxID: '',
            OtherTax: '',
            GrandTotal: '',
            EwayBillNo: '',
            VendorCode: '',
            PaymentTypeID: '',
            BankName: '',
            BranchName: '',
            ChequeNo: '',
            ChequeDate: '',
            PaymentCollected: '',
            Remark: '',
            RoadPermitAttached: '',
            TaxPercentage: '',
            TaxId: ''
        };
    }

    /* Edit Consignment */
    $scope.RedirectToConsignmentBilling = function () {
        window.location.href = "/Operations/ConsignmentBilling";
    }
    $scope.GetConsignmentToEdit = function (consignmentId, action) {
        if (action = 'view') {
            $scope.IsView = true;
        }
        else {
            $scope.IsView = false;
        }
        $scope.GetPaymentTypes();
        $scope.GetOtherBranches();
        $scope.GetBranches();
        $scope.GetDeliveryTypes();
        $scope.GetVendors();
        $scope.BillingVendors = [];
        $scope.GetTransportTypes();
        $scope.GetServiceTaxpaidBy();
        $scope.GetCargoTypes();
        $scope.GetTransportModes();
        $scope.GetDoorCollections();
        $scope.GetVehicles();
        $scope.GetPinCodes();
        $scope.GetODA();

        $scope.consignmentBooking = {
            ConsignmentID: '',
            ConsignmentNo: '',
            ConsignmentDate: '',
            ConsignmentTime: '',
            PaymentTypeID: '',
            ConsignmentFromID: '',
            ConsignmentToID: '',
            DestinationID: '',
            DeliveryTypeID: '',
            BillingPartyID: '',
            TransportTypeID: '',
            ServiceTaxId: '',
            CargotTypeID: '',
            TransportModeID: '',
            DoorCollectionID: '',
            VehicleID: '',
            PinCodeID: '',
            ODAID: '',
            ConsignorID: '',
            ConsignorName: '',
            ConsignorGSTNo: '',
            ConsigneeID: '',
            ConsigneeName: '',
            ConsigneeGSTNo: '',
            ContactPersonName: '',
            MobileNumber: '',
            ConsignorAddress: '',
            ConsigneeAddress: ''

        };
        $scope.BookingPayment = {
            BookingPaymentID: '',
            ConsignmentID: '',
            GoodsDescription: '',
            EwayBillNo: '',
            VendorCode: '',
            InsuranceFlag: '',
            RoadPermitAttached: '',
            Documents: '',
            FOVId: '',
            PaymentTypeID: '',
            BankName: '',
            BranchName: '',
            ChequeNo: '',
            ChequeDate: '',
            PaymentCollected: '',
            Remark: '',
            StandardRate: '',
            BasicFreight: '',
            DocketCharges: '',
            Pickup: '',
            FOV: 0,
            HamaliCharges: '',
            DDCharges: '',
            ODACharges: '',
            OtherCharges: '',
            OtherAOC: '',
            TotalAmount: '',
            SGST: '',
            CGST: '',
            IGST: '',
            GrandTotal: ''
        };
        var booking = [];
        $http({
            method: 'GET',
            url: '/Operations/GetConsignmentBookingByID',
            params: { 'consignmentID': consignmentId },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    booking.push(response.data[i]);
                }

                $scope.consignmentBooking.ConsignmentID = booking[0].ConsignmentID;
                $scope.consignmentBooking.ConsignmentDate = booking[0].date;
                $scope.consignmentBooking.ConsignmentTime = booking[0].ConsignmentTime;
                $scope.consignmentBooking.ConsignmentNo = booking[0].ConsignmentNo;
                $scope.consignmentBooking.ConsigneeName = booking[0].ConsigneeName;
                $scope.consignmentBooking.ConsigneeAddress = booking[0].ConsigneeAddress;
                $scope.consignmentBooking.CompanyID = booking[0].CompanyID;
                $scope.consignmentBooking.BranchID = booking[0].BranchID;
                $scope.consignmentBooking.ZoneID = booking[0].ZoneID;
                $scope.consignmentBooking.ConsigneeID = booking[0].ConsigneeID;
                $scope.consignmentBooking.ConsignorName = booking[0].ConsignorName;
                $scope.consignmentBooking.ConsignorAddress = booking[0].ConsignorAddress;
                $scope.consignmentBooking.ConsignorID = booking[0].ConsignorID;
                //$scope.consignmentBooking.ConsignmentDate = $filter('date')($scope.FormatDate(booking[0].ConsignmentDate), 'dd/MM/yyyy');
                $scope.consignmentBooking.PaymentTypeID = booking[0].PaymentTypeID;
                $scope.consignmentBooking.ConsignmentFromID = booking[0].ConsignmentFromID;
                $scope.consignmentBooking.ConsignmentToID = booking[0].ConsignmentToID;
                $scope.consignmentBooking.DeliveryTypeID = booking[0].DeliveryTypeID;
                $scope.consignmentBooking.BillingPartyID = booking[0].BillingPartyID;
                $scope.consignmentBooking.TransportTypeID = booking[0].TransportTypeID;
                $scope.consignmentBooking.ServiceTaxId = booking[0].ServiceTaxId;
                $scope.consignmentBooking.DoorCollectionID = booking[0].DoorCollectionID;
                $scope.consignmentBooking.TransportModeID = booking[0].TransportModeID;
                $scope.consignmentBooking.CargotTypeID = booking[0].CargotTypeID;
                $scope.consignmentBooking.DestinationID = booking[0].DestinationID;
                if (booking[0].VehicleID != null)
                    $scope.consignmentBooking.VehicleID = booking[0].VehicleID;
                $scope.consignmentBooking.MobileNumber = booking[0].MobileNumber;
                $scope.consignmentBooking.ContactPersonName = booking[0].ContactPersonName;
                $scope.consignmentBooking.PinCodeID = booking[0].PinCodeID;
                $scope.consignmentBooking.ODAID = booking[0].ODAID;

                $scope.ShowVehicles();
                $scope.GetPartNos();
                setTimeout(function () {
                    $scope.ShowBillingParty();
                    $scope.GetConsigneeAddress();
                    $scope.GetConsignorAddress();
                    $scope.loadPartyInvoice($scope.consignmentBooking.ConsignmentID, 'update');
                    $scope.consignmentBooking.BillingPartyID = booking[0].BillingPartyID;
                }, 3000);
            },
                function errorCallback(response) {
                    alert(response);
                });

    }

}]);

App.controller('ReserveConsignment_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder) {

    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Operations/GetReservedConsignment",
            type: 'GET',
            data: function (d) {
                d.branchId = $("#hdnBranchID").val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('BranchReservedID').withTitle('BranchReservedID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('BranchID').withTitle('BranchID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('BranchName').withTitle('Branch').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentNo').withTitle('Consignment No').notSortable(),

        DTColumnBuilder.newColumn('Action').notSortable().renderWith(function (data, type, row, meta) {
            return '<a class="pull-left marleft15 font15 delete" ng-click="CreateConsignment(' + row.BranchReservedID + ')" > Create Consignment</a>';
        })

    ];
    $scope.CreateConsignment = function (BranchReservedID) {
        window.location.href = "/Operations/AddConsignment?BranchReservedId=" + BranchReservedID;
    }
    $scope.GetBranches = function () {

        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    brnches.push(response.data[i]);
                }
                $scope.Branches = brnches;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.ReserveLRNo = function () {
        var branchId = "";
        if ($("#hdnBranchID").val() == "0")
            branchId = $("#ddlBranch").val();
        else
            branchId = $("#hdnBranchID").val();
        if (branchId != "") {
            $http({
                method: 'POST',
                url: '/Operations/ReserveConsignmentNo',
                data: { 'branchId': parseInt(branchId) },
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    if (response.data != "") {
                        Notification.success({ message: "Consignment reserved successfully.", delay: 5000 });
                        var dtinstance = $scope.dtInstance.DataTable;
                        dtinstance.ajax.reload(function (json) { }, true);
                    }
                    else {
                        Notification.error({ message: "Network Error! Please try again later.", delay: 5000 });
                    }
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }

        else {
            Notification.error({ message: "Please select branch.", delay: 5000 });
            return;
        }
    }
}]);

App.controller('ConsignmentBilling_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "Excel", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, Excel, $timeout) {

    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Operations/GetConsignmentBilling",
            type: 'GET',
            data: function (d) {
                d.branchId = $("#hdnBranchID").val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('ConsignmentBillingID').withTitle('ConsignmentBillingID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('BillingNo').withTitle('BillingNo').notSortable(),
        DTColumnBuilder.newColumn('BillingDateStr').withTitle('Billing Date').notSortable(),
        DTColumnBuilder.newColumn('VendorName').withTitle('Customer').notSortable(),
        DTColumnBuilder.newColumn('BillingTotalAmount').withTitle('Total Amount').notSortable(),
        DTColumnBuilder.newColumn('Action').notSortable().renderWith(function (data, type, row, meta) {
            var str = 
                 '<a class="font15 delete" style="margin-left:30px;" ng-click="ViewConsignmentBilling(' + row.ConsignmentBillingID + ')" > View</a>';
            if ($("#hdnRoleName").val() == "SuperAdmin")
                str = str + '<a class="pull-left marleft15 font15 delete" ng-click="AddConsignmentBilling(' + row.ConsignmentBillingID + ')" > + Add Consignment</a>';
            return str;
        })

    ];

    $scope.SetCurrentDateTime = function () {
        $scope.currentDate = $filter('date')(new Date(), 'dd/MM/yyyy');
        var date = new Date();
        var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
        var am_pm = date.getHours() >= 12 ? "PM" : "AM";
        hours = hours < 10 ? "0" + hours : hours;
        var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
        time = hours + ":" + minutes + ":" + seconds + " " + am_pm;
        $scope.currentTime = time;
    }
    $scope.SetCurrentDateTime();
    $scope.ConsignmentBilling = {
        ConsignmentBillingID: '',
        BillingNo: '',
        BillingDate: $scope.currentDate,
        VendorID: '',
        BillingTime: $scope.currentTime,
        BillingTotalAmount: '0',
        Remarks: ''
    }
    $scope.clearConsignmentBilling = function () {
        $scope.SetCurrentDateTime();
        $scope.ConsignmentBilling = {
            ConsignmentBillingID: '',
            BillingNo: '',
            BillingDate: $scope.currentDate,
            VendorID: '',
            BillingTime: $scope.currentTime,
            BillingTotalAmount: '0',
            Remarks: ''
        }
    }
    $scope.GetAllVendors = function () {

        var vendor = [];
        $http({
            method: 'GET',
            url: '/Operations/GetAllVendors',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    vendor.push(response.data[i]);
                }
                $scope.Vendors = vendor;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetConsignmentBillingByID = function (ConsignmentBillingId) {
        var billing = [];
        $http({
            method: 'GET',
            params: { 'ConsignmentBillingId': ConsignmentBillingId },
            url: '/Operations/GetConsignmentBillingByID',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    billing.push(response.data[i]);
                }
                $scope.ConsignmentBilling.ConsignmentBillingID = billing[0].ConsignmentBillingID;
                $scope.ConsignmentBilling.BillingNo = billing[0].BillingNo;
                $scope.ConsignmentBilling.BillingDate = billing[0].BillingDateStr;
                $scope.ConsignmentBilling.VendorID = billing[0].VendorID;
                $scope.ConsignmentBilling.BillingTime = billing[0].BillingTime;
                $scope.ConsignmentBilling.BillingTotalAmount = billing[0].BillingTotalAmount;
                $scope.ConsignmentBilling.Remarks = billing[0].Remarks;
                $scope.AddConsignment();
                $scope.GetBillingConsignment();
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetBillingConsignment = function () {
        $http({
            method: 'GET',
            params: { 'ConsignmentBillingID': $scope.ConsignmentBilling.ConsignmentBillingID },
            url: '/Operations/GetBillingConsignments',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {

                    for (j = 0; j < $scope.activeConsignmentBooking.length; j++) {
                        if ($scope.activeConsignmentBooking[j].ConsignmentID == response.data[i].ConsignmentID) {
                            $scope.activeConsignmentBooking[j].checkSelected = true;
                            break;
                        }
                    }
                }
                $scope.selectConsignment();
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.AddConsignmentBilling = function (ConsignmentBillingId) {
        $scope.clearConsignmentBilling();
        $scope.GetAllVendors();
        $scope.isDisableAddConsignment = true;
        $scope.showBillingConsignments = false;
        if (ConsignmentBillingId != '' && ConsignmentBillingId != undefined) {
            $scope.GetConsignmentBillingByID(ConsignmentBillingId)
            $scope.isDisableAddConsignment = false;

        }
        $('#AddConsignmentBillingModal').modal('show');
    }
 
    $scope.ViewConsignmentBilling = function (ConsignmentBillingId) {
        $scope.clearConsignmentBilling();
        var billing = [];
        var billingconsignment = [];
        $http({
            method: 'GET',
            params: { 'ConsignmentBillingId': ConsignmentBillingId },
            url: '/Operations/GetConsignmentBillingByID',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    billing.push(response.data[i]);
                }
                $scope.ConsignmentBilling.ConsignmentBillingID = billing[0].ConsignmentBillingID;
                $scope.ConsignmentBilling.BillingNo = billing[0].BillingNo;
                $scope.ConsignmentBilling.BillingDate = billing[0].BillingDateStr;
                $scope.ConsignmentBilling.VendorID = billing[0].VendorID;
                $scope.ConsignmentBilling.BillingTime = billing[0].BillingTime;
                $scope.ConsignmentBilling.BillingTotalAmount = billing[0].BillingTotalAmount;
                $scope.ConsignmentBilling.Remarks = billing[0].Remarks;
                $scope.ConsignmentBilling.VendorName = billing[0].VendorName;
                $http({
                    method: 'GET',
                    params: { 'ConsignmentBillingID': $scope.ConsignmentBilling.ConsignmentBillingID },
                    url: '/Operations/GetBillingConsignmentsWithDetails',
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
                    .then(function successCallback(response) {
                        var totalArticle = 0;
                        var totalActualWeight = 0;
                        for (i = 0; i < response.data.length; i++) {
                            if (response.data[i].Taxes != '' && response.data[i].Taxes != null) {
                                response.data[i].Taxes = ((parseFloat(response.data[i].TotalAmount) * parseFloat(response.data[i].Taxes)) / 100)
                            }
                            else
                                response.data[i].Taxes = 0;
                            totalArticle = totalArticle + parseInt(response.data[i].Article);
                            totalActualWeight = totalActualWeight + parseFloat(response.data[i].ActualWeight);
                            billingconsignment.push(response.data[i]);
                        }
                        $scope.TotalArticles = totalArticle;
                        $scope.TotalActualWeight = totalActualWeight;
                        $scope.BillingConsignmentData = billingconsignment;
                    },
                        function errorCallback(response) {
                            alert(response);
                        });
            },
                function errorCallback(response) {
                    alert(response);
                });
        $('#ViewConsignmentBillingModal').modal('show');
    }
    $scope.closeViewConsignmentBillingModal = function () {
        $('#ViewConsignmentBillingModal').modal('hide');
    }
    $scope.closeConsignmentBillingModal = function () {
        $('#AddConsignmentBillingModal').modal('hide');
    }
    $scope.SaveConsignmentBilling = function () {
        if ($scope.frmConsignmentBilling.$valid) {
            if ($scope.ConsignmentBilling.ConsignmentBillingID == null || $scope.ConsignmentBilling.ConsignmentBillingID == '') {
                $http({
                    method: 'POST',
                    url: '/Operations/SaveConsignmentBilling',
                    data: JSON.stringify($scope.ConsignmentBilling),
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
                    .then(function successCallback(response) {
                        Notification.success({ message: "Consignment Bill saved successfully.", delay: 5000 });
                        $scope.ConsignmentBilling.ConsignmentBillingID = response.data;
                        if ($scope.ConsignmentBilling.ConsignmentBillingID != null && $scope.ConsignmentBilling.ConsignmentBillingID != '') {
                            $scope.isDisableAddConsignment = false;

                        }
                        var dtinstance = $scope.dtInstance.DataTable;
                        dtinstance.ajax.reload(function (json) { }, true);
                    }, function errorCallback(response) {
                        alert(JSON.stringify(response.statusText));
                    });
            }
            else {
                $http({
                    method: 'POST',
                    url: '/Operations/SaveConsignmentBilling',
                    data: JSON.stringify($scope.ConsignmentBilling),
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
                    .then(function successCallback(response) {
                        Notification.success({ message: "Consignment Bill updated successfully.", delay: 5000 });

                        var dtinstance = $scope.dtInstance.DataTable;
                        dtinstance.ajax.reload(function (json) { }, true);
                    }, function errorCallback(response) {
                        alert(JSON.stringify(response.statusText));
                    });
            }
        }
        else {

            Notification.error({ message: "Please fill all the required fields on the form.", delay: 5000 });
            return;
        }
    }
    $scope.AddConsignment = function () {
        $scope.activeConsignmentBookingCollection = [];
        $scope.activeConsignmentBooking = '';
        var bookings = [];
        $http({
            method: 'GET',
            params: { 'VendorID': $scope.ConsignmentBilling.VendorID, 'ConsignmentBillingID': $scope.ConsignmentBilling.ConsignmentBillingID },
            url: '/Operations/GetConsignmentsForBilling',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    if (response.data[i].Taxes != '' && response.data[i].Taxes != null) {
                        response.data[i].Taxes = ((parseFloat(response.data[i].TotalAmount) * parseFloat(response.data[i].Taxes)) / 100)
                    }
                    else
                        response.data[i].Taxes = 0;
                    bookings.push(response.data[i]);
                    bookings[i].checkSelected = false;
                }
                if (response.data.length > 0) {
                    $scope.showBillingConsignments = true;
                }
                $scope.activeConsignmentBooking = bookings;
                $scope.activeConsignmentBookingCollection = bookings;
                $scope.TotalArticles = 0;
                $scope.TotalActualWeight = 0;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.selectConsignment = function () {

        var totalArticle = 0;
        var totalActualWeight = 0;
        var totalAmount = 0;
        for (i = 0; i < $scope.activeConsignmentBooking.length; i++) {
            if ($scope.activeConsignmentBooking[i].checkSelected == true) {
                totalArticle = totalArticle + parseInt($scope.activeConsignmentBooking[i].Article);
                totalActualWeight = totalActualWeight + parseFloat($scope.activeConsignmentBooking[i].ActualWeight);
                totalAmount = totalAmount + parseFloat($scope.activeConsignmentBooking[i].GrandTotal);
            }
        }
        $scope.TotalArticles = totalArticle;
        $scope.TotalActualWeight = totalActualWeight;
        $scope.ConsignmentBilling.BillingTotalAmount = totalAmount;
    }

    $scope.EditBooking = function (consignmentId) {
        $.get("/Operations/CheckEditableConsignment",
            { 'ConsignmentId': consignmentId })
            .done(function (response) {

                if (response == "False") {
                    Notification.error({ message: "Request is not authorized. Please contact Administrator.", delay: 5000 });
                }
                else {
                    window.location.href = "/Operations/EditConsignment?ConsignmentiD=" + consignmentId + "&RedirectPage=" + true;
                }
            });
    }
    $scope.SaveBillingConsignments = function () {
        var newBillingConsignments = [];
        for (i = 0; i < $scope.activeConsignmentBooking.length; i++) {
            if ($scope.activeConsignmentBooking[i].checkSelected == true) {

                var blConsignment = {
                    ConsignmentBillingID: $scope.ConsignmentBilling.ConsignmentBillingID,
                    ConsignmentID: $scope.activeConsignmentBooking[i].ConsignmentID,
                }
                newBillingConsignments.push(blConsignment);
            }


        }
        $http({
            method: 'POST',
            url: '/Operations/SaveBillingConsignments',
            dataType: 'json',
            traditional: true,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(newBillingConsignments)

        })
            .then(function successCallback(response) {
                $scope.UpdateConsignmentBillingTotalAmount();
            }, function errorCallback(response) {
                Notification.error({ message: response.message, delay: 5000 });
            });

    }

    $scope.UpdateConsignmentBillingTotalAmount = function () {
        $http({
            method: 'POST',
            url: '/Operations/UpdateConsignmentBillingTotalAmount',
            dataType: 'json',
            traditional: true,
            contentType: 'application/json; charset=utf-8',
            params: { 'ConsignmentBillingID': $scope.ConsignmentBilling.ConsignmentBillingID, 'totalAmount': parseFloat($scope.ConsignmentBilling.BillingTotalAmount) }

        })
            .then(function successCallback(response) {
                Notification.success({ message: "Billing consignments saved successfully.", delay: 5000 });


            }, function errorCallback(response) {
                Notification.error({ message: response.message, delay: 5000 });
            });
    }
    $scope.exportToExcel = function (divId) {
        $scope.exportHref = Excel.tableToExcel(divId, 'VendorBill');
        $timeout(function () {
            var link = document.createElement('a');
            link.download = "VendorBill.xls";
            link.href = $scope.exportHref;
            link.click();
        }, 100);
    }

}]);
