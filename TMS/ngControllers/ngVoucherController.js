﻿App.controller('Voucher_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder) {
    $scope.Branches = [];
    $scope.GetBranches = function () {
        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    brnches.push(response.data[i]);
                }
                $scope.Branches = brnches;

            },
                function errorCallback(response) {
                    alert(response);
                });

    }
    $scope.GetBranches();
    $scope.currentDate = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.InitVoucher = function () {
        $scope.Voucher = {
            VoucherID: '',
            VoucherDate: $scope.currentDate,
            VoucherType: '',
            BranchID: $("#hdnBranchID").val(),
            BankName: '',
            BranchName: '',
            ChequeNo: '',
            ChequeDate: '',
            AccountID: '',
            VendorName: '',
            VendorID: '',
            PaymentTypeID: '',
            VoucherAmount: '',
            ManifestID: '',
            IsVendor: '',
            Particulars: '',
            HireType:''
        };
    }
    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Voucher/GetVoucherData",
            type: 'GET',
            data: function (d) {
                d.branchId = $("#hdnBranchID").val();
            }
        })
        .withOption('aaSorting', [[2, "desc"]])
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { search: "", searchPlaceholder: "Search..." })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('VoucherID').withTitle('VoucherID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('VoucherNo').withTitle('Voucher No').notSortable(),
        DTColumnBuilder.newColumn('VoucherDate').withTitle('Voucher Date'),
        //DTColumnBuilder.newColumn('VoucherType').withTitle('Voucher Type').notSortable(),
        DTColumnBuilder.newColumn('CreatedBranch').withTitle('Branch').notSortable(),
        DTColumnBuilder.newColumn('ManifestNo').withTitle('Manifest No').notSortable(),
        DTColumnBuilder.newColumn('VendorName').withTitle('Vendor Name').notSortable(),
        DTColumnBuilder.newColumn('PaymentType').withTitle('Payment Type').notSortable(),
        DTColumnBuilder.newColumn('VoucherAmount').withTitle('Amount').notSortable(),
        DTColumnBuilder.newColumn('CreatedBy').withTitle('Created By').notSortable(),
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            var strLinks = '<a ng-click="EditVoucherData(' + row.VoucherID + ')" class="pull-left font20 marleft15" > <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
            strLinks = strLinks + '<a class="pull-left marleft15 font20 delete" ng-click="DeleteVoucher(' + row.VoucherID + ')" > <i class="fa fa-trash-o" aria-hidden="true"></i></a>';
            return strLinks;

        })
    ];

    $scope.GetManifests = function () {
        var manifests = [];
        $http({
            method: 'GET',
            url: '/Operations/GetManifestsForBranch',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    manifests.push(response.data[i]);
                }
                $scope.Manifests = manifests;

            },
                function errorCallback(response) {
                    alert(response);
                });

    }
    $scope.GetPaymentTypes = function () {

        var pmtTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVehicleHirePaymentTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    pmtTypes.push(response.data[i]);
                }
                $scope.PaymentTypes = pmtTypes;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }

    $scope.GetVendors = function () {

        var vendor = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVehicleVendors',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    vendor.push(response.data[i]);
                }
                $scope.Vendors = vendor;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetAccounts = function () {

        var accounts = [];
        $http({
            method: 'GET',
            url: '/Operations/GetAccounts',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    accounts.push(response.data[i]);
                }
                $scope.Accounts = accounts;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.AddVoucher = function () {
        $scope.InitVoucher();
        $scope.isDisableVendorDropdown = true;
        $scope.isDisableVendorText = true;
        $('#chqDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false
        });
        $scope.GetManifests();
        $scope.GetPaymentTypes();
        $scope.GetAccounts();
        $scope.GetVendors();
        $("#VoucherModal").modal("show");
    }
    $scope.closeVoucher = function () {
        $scope.InitVoucher();
        $("#VoucherModal").modal("hide");
    }


    $scope.ChangeEditPaymentType = function () {
        var selectedtext = $("#ddlEditPaymentType option:selected").text();
        if (selectedtext.toLowerCase() == "cheque") {
            $scope.isDisableCheque = false;
        }
        else {
            $scope.isDisableCheque = true;
        }
    }
    $scope.ChangePaymentType = function () {
        var selectedtext = $("#ddlPaymentType option:selected").text();
        if (selectedtext.toLowerCase() == "cheque") {
            $scope.isDisableCheque = false;
        }
        else {
            $scope.isDisableCheque = true;
        }
    }
    $scope.SaveVoucher = function () {
        var manifest = $scope.Voucher.ManifestID;
        $scope.Voucher.ManifestID = manifest.split('&')[0];
        $scope.Voucher.HireType = manifest.split('&')[1];
        if ($scope.frmVoucher.$valid) {
            if ($scope.Voucher.VendorID != '') {

                var vendor = filterFilter($scope.Vendors, { VendorID: $scope.Voucher.VendorID },true);
                $scope.Voucher.VendorName = vendor[0].VendorName;

            }
            $http({
                method: 'POST',
                url: '/Voucher/AddVoucher',
                data: JSON.stringify($scope.Voucher),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);

                    Notification.success({ message: "Voucher saved successfully.", delay: 5000 });
                    $scope.closeVoucher();
                    var dtinstance = $scope.dtInstance.DataTable;
                    $scope.dtOptions.ajax.data = function (d) {
                        d.branchId = $("#hdnBranchID").val();
                    };
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.InitEditVoucher = function () {
        $scope.EditVoucher = {
            VoucherID: null,
            VoucherNo: '',
            BankName: '',
            BranchName: '',
            ChequeNo: '',
            ChequeDate: '',
            AccountID: '',
            VendorName: '',
            VendorID: '',
            PaymentTypeID: '',
            VoucherAmount: '',
            ManifestID: '',
            IsVendor: '',
            Particulars: ''
        };
    }

    $scope.GetVoucherByID = function (VoucherId) {
        $scope.EditVoucher = {
            VoucherID: VoucherId,
            VoucherNo: '',
            BankName: '',
            BranchName: '',
            ChequeNo: '',
            ChequeDate: '',
            AccountID: '',
            VendorName: '',
            VendorID: '',
            PaymentTypeID: '',
            VoucherAmount: '',
            ManifestID: '',
            IsVendor: '',
            Particulars: '',
            HireType:''
        };
        var voucher = [];
        $http({
            method: 'GET',
            url: '/Voucher/GetVoucherByID',
            params: { 'VoucherID': VoucherId },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    voucher.push(response.data[i]);
                }

                $scope.EditVoucher.VoucherID = voucher[0].VoucherID;
                $scope.EditVoucher.VoucherNo = voucher[0].VoucherNo;
                $scope.EditVoucher.VoucherDate = voucher[0].VoucherDate;
                $scope.EditVoucher.AccountID = voucher[0].AccountID;
                $scope.EditVoucher.ManifestID = voucher[0].ManifestID;
                $scope.EditVoucher.VendorID = voucher[0].VendorID;
                $scope.EditVoucher.VendorName = voucher[0].VendorName;
                $scope.EditVoucher.PaymentTypeID = voucher[0].PaymentTypeID;
                $scope.EditVoucher.BankName = voucher[0].BankName;
                $scope.EditVoucher.BranchName = voucher[0].BranchName;
                $scope.EditVoucher.ChequeNo = voucher[0].ChequeNo;
                $scope.EditVoucher.ChequeDate = $filter('date')(voucher[0].ChequeDate, 'dd/MM/yyyy');
                $scope.EditVoucher.VoucherAmount = voucher[0].VoucherAmount;
                $scope.EditVoucher.Particulars = voucher[0].Particulars;
                $scope.EditVoucher.HireType = voucher[0].HireType;
                $("#EditVoucherModal").modal("show");
                //change payment type

                var paymenttypes = filterFilter($scope.PaymentTypes, { PaymentTypeID: $scope.EditVoucher.PaymentTypeID });
                var selectedtext = paymenttypes[0].PaymentType;
                if (selectedtext.toLowerCase() == "cheque") {
                    $scope.isDisableCheque = false;
                }
                else {
                    $scope.isDisableCheque = true;
                }
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.EditVoucherData = function (VoucherID) {

        $scope.isDisableVendorDropdown = true;
        $scope.isDisableVendorText = true;

        $scope.GetManifests();
        $scope.GetPaymentTypes();
        $scope.GetAccounts();
        $scope.GetVendors();
        $('#chqEditDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false
        });
        setTimeout(function () { $scope.GetVoucherByID(VoucherID); }, 1000);

    }

    $scope.UpdateVoucher = function () {

        var vendor = filterFilter($scope.Vendors, { VendorID: $scope.EditVoucher.VendorID },true);
        $scope.EditVoucher.VendorName = vendor[0].VendorName;
        var manifest = $scope.EditVoucher.ManifestID;
        $scope.EditVoucher.ManifestID = manifest.split('&')[0];
        $scope.EditVoucher.HireType = manifest.split('&')[1];

        if ($("#ddlEditPaymentType option:selected").text().toLowerCase() != 'cheque') {
            $scope.EditVoucher.ChequeNo = '';
            $scope.EditVoucher.ChequeDate = null;
        }
        if ($scope.frmEditVoucher.$valid) {
            $http({
                method: 'POST',
                url: '/Voucher/UpdateVoucher',
                data: JSON.stringify($scope.EditVoucher),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeUpdateVoucher();
                    Notification.success({ message: "Voucher updated successfully.", delay: 5000 });
                    var dtinstance = $scope.dtInstance.DataTable;
                    $scope.dtOptions.ajax.data = function (d) {
                        d.branchId = $("#hdnBranchID").val();
                    };
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    
    $scope.DeleteVoucher = function (VoucherID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Voucher/DeleteVoucher',
                params: { 'VoucherID': VoucherID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Voucher deleted successfully.", delay: 5000 });
                    var dtinstance = $scope.dtInstance.DataTable;
                    $scope.dtOptions.ajax.data = function (d) {
                        d.branchId = $("#hdnBranchID").val();
                    };
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
    $scope.closeUpdateVoucher = function () {
        $("#EditVoucherModal").modal("hide");
    }
}]);

App.controller('ConsignmentPayment_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder) {
  
    $scope.currentDate = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.InitConsignmentPayment = function () {
        $scope.ConsignmentVoucher = {
            ConsignmentVoucherID: '',
            VoucherDate: $scope.currentDate,
            ConsignmentBillingID: '',
            BankName:'',
            BranchName: '',
            ChequeNo: '',
            ChequeDate: '',
            PaymentTypeID: '',
            VoucherAmount: '',
            Remarks: '',
            VoucherNo: '',
            TDS: '',
            Trade: '',
            ShortAmount:''
        };
    }
    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Voucher/GetConsignmentPaymentData",
            type: 'GET'
           
        })
        .withOption('aaSorting', [[2, "desc"]])
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { search: "", searchPlaceholder: "Search..." })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('ConsignmentVoucherID').withTitle('VoucherID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('VoucherNo').withTitle('Voucher No').notSortable(),
        DTColumnBuilder.newColumn('VoucherDate').withTitle('Voucher Date'),
        DTColumnBuilder.newColumn('BillingNo').withTitle('Bill No').notSortable(),
        DTColumnBuilder.newColumn('PaymentType').withTitle('Payment Type').notSortable(),
        DTColumnBuilder.newColumn('VoucherAmount').withTitle('Amount').notSortable(),
        DTColumnBuilder.newColumn('CreatedBy').withTitle('Created By').notSortable(),
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            var strLinks = '<a class="pull-left marleft15 font20 delete" ng-click="DeleteConsignmentPayment(' + row.ConsignmentVoucherID + ')" > <i class="fa fa-trash-o" aria-hidden="true"></i></a>';
            return strLinks;

        })
    ];

    $scope.GetPaymentTypes = function () {

        var pmtTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVehicleHirePaymentTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    pmtTypes.push(response.data[i]);
                }
                $scope.PaymentTypes = pmtTypes;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    
    $scope.GetAllBills = function () {

        var bill = [];
        $http({
            method: 'GET',
            url: '/Operations/GetAllUnPaidBills',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    bill.push(response.data[i]);
                }
                $scope.Bills = bill;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.AddBillPayment = function () {
        $scope.InitConsignmentPayment();
        $('#chqDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false
        });
        $scope.GetPaymentTypes();
        $scope.GetAllBills();
        $scope.HideConsignment = true;
        $("#ConsignmentPaymentModal").modal("show");
    }
    $scope.closeConsignmentPayment = function () {
        $scope.InitConsignmentPayment();
        $("#ConsignmentPaymentModal").modal("hide");
    }


    $scope.ChangePaymentType = function () {
        var selectedtext = $("#ddlPaymentType option:selected").text();
        if (selectedtext.toLowerCase() == "cheque") {
            $scope.isDisableCheque = false;
        }
        else {
            $scope.isDisableCheque = true;
        }
    }
    $scope.changeBill = function () {
        var billingconsignment = [];
        $scope.BillingConsignmentData = [];
        var billid = $("#ddlBill").val();
        $http({
            method: 'GET',
            params: { 'ConsignmentBillingID': billid },
            url: '/Operations/GetBillingConsignmentsWithDetails',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                var totalArticle = 0;
                var totalActualWeight = 0;
                var totalamount = 0;
                var jsondata = response.data.toString().split('#')[0];
                $scope.VendorName = response.data.toString().split('#')[1];
                $scope.BalanceAmount = response.data.toString().split('#')[2];
                if ($scope.BalanceAmount == '')
                    $scope.BalanceAmount = 0;
                var responsejson = JSON.parse(jsondata);
                for (i = 0; i < responsejson.length; i++) {
                    $scope.UseBalanceAmount = false;
                    $scope.HideConsignment = false; 
                    if (responsejson.Taxes != '' && responsejson.Taxes != null) {
                        responsejson[i].Taxes = ((parseFloat(responsejson[i].TotalAmount) * parseFloat(responsejson[i].Taxes)) / 100)
                    }
                    else
                        responsejson[i].Taxes = 0;
                    totalArticle = totalArticle + parseInt(responsejson[i].Article);
                    totalamount = totalamount + parseFloat(responsejson[i].GrandTotal);
                    totalActualWeight = totalActualWeight + parseFloat(responsejson[i].ActualWeight);
                    billingconsignment.push(responsejson[i]);
                }
                if (responsejson[0].PaidAmount == null)
                    responsejson[0].PaidAmount = 0;
                $scope.RemainingAmount = parseFloat(totalamount) - parseFloat(responsejson[0].PaidAmount);
                $scope.TotalArticles = totalArticle;
                $scope.TotalActualWeight = totalActualWeight;
                $scope.BillingConsignmentData = billingconsignment;
                $scope.TotalAmount = totalamount;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.calculateReceivedAmount = function () {
        if ($scope.ConsignmentVoucher.VoucherAmount != '') {
            var voucheramount = parseFloat($scope.ConsignmentVoucher.VoucherAmount);
            var remainingamount = parseFloat($scope.RemainingAmount);
            var amount = 0;
            if (voucheramount < remainingamount) {
                amount = voucheramount;
            }
            else {
                amount = remainingamount;
            }
            var tds = (($scope.ConsignmentVoucher.TDS == '') ? 0 : $scope.ConsignmentVoucher.TDS);
            var trade = (($scope.ConsignmentVoucher.Trade == '') ? 0 : $scope.ConsignmentVoucher.Trade);
            var short = (($scope.ConsignmentVoucher.ShortAmount == '') ? 0 : $scope.ConsignmentVoucher.ShortAmount);
            var tdsamount = (amount * parseFloat(tds)) / 100;
            var tradeamount = (amount * parseFloat(trade)) / 100;
            $scope.ReceivedAmount = amount - tdsamount - tradeamount - parseFloat(short);
        }
    }
    $scope.changeBalancePayment = function () {
        if ($scope.BalanceAmount == '')
            $scope.BalanceAmount = 0;
        if ($("#chkBalance").is(":checked")) {
            $scope.RemainingAmount = parseFloat($scope.RemainingAmount) - parseFloat($scope.BalanceAmount);
        }
        else {
            $scope.RemainingAmount = parseFloat($scope.RemainingAmount) + parseFloat($scope.BalanceAmount);
        }
    }
    $scope.SaveConsignmentPayment = function () {
        if ($scope.frmConsignmentPayment.$valid) {
           
            $http({
                method: 'POST',
                url: '/Voucher/SaveConsignmentPayment',
                data: JSON.stringify($scope.ConsignmentVoucher),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);
                    Notification.success({ message: "Consignment Payment saved successfully.", delay: 5000 });
                    if (parseFloat($scope.ConsignmentVoucher.VoucherAmount) > parseFloat($scope.RemainingAmount)) {
                        $scope.UpdateVendorBalancePayment();
                    }
                    $scope.closeConsignmentPayment();
                    var dtinstance = $scope.dtInstance.DataTable;
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }

    $scope.UpdateVendorBalancePayment = function () {
        var balancepayment = 0;
        if (parseFloat($scope.ConsignmentVoucher.VoucherAmount) > parseFloat($scope.RemainingAmount)) {
            balancepayment = parseFloat($scope.ConsignmentVoucher.VoucherAmount) - parseFloat($scope.RemainingAmount);
        }
        if (balancepayment > 0) {
            $http({
                method: 'POST',
                url: '/Operations/UpdateVendorBalancePayment',
                dataType: 'json',
                traditional: true,
                contentType: 'application/json; charset=utf-8',
                params: { 'ConsignmentBillingID': $scope.ConsignmentVoucher.ConsignmentBillingID, 'BalanceAmount': balancepayment }

            })
                .then(function successCallback(response) {
                    
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
    }
  
    $scope.DeleteConsignmentPayment = function (ConsignmentVoucherID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Voucher/DeleteConsignmentPayment',
                params: { 'ConsignmentVoucherID': ConsignmentVoucherID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Consignment Payment deleted successfully.", delay: 5000 });
                    var dtinstance = $scope.dtInstance.DataTable;
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
  
}]);

App.controller('Expense_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder) {
    $scope.Branches = [];
    $scope.GetBranches = function () {
        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    brnches.push(response.data[i]);
                }
                $scope.Branches = brnches;

            },
                function errorCallback(response) {
                    alert(response);
                });

    }
    $scope.GetBranches();
    $scope.currentDate = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.InitExpense = function () {
        $scope.Expense = {
            ExpenseID: '',
            ExpenseDate: $scope.currentDate,
            ExpenseType: '',
            BranchID: $("#hdnBranchID").val(),
            BankName: '',
            BranchName: '',
            ChequeNo: '',
            ChequeDate: '',
            PaidTo: '',
            PaymentTypeID: '',
            Amount: '',
            Particulars: ''
        };
    }
    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Voucher/GetExpenseData",
            type: 'GET',
            data: function (d) {
                d.branchId = $("#hdnBranchID").val();
            }
        })
        .withOption('aaSorting', [[2, "desc"]])
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { search: "", searchPlaceholder: "Search..." })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('ExpenseID').withTitle('ExpenseID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('ExpenseDateStr').withTitle('Expense Date'),
        DTColumnBuilder.newColumn('ExpenseTypeStr').withTitle('Expense Type').notSortable(),
        DTColumnBuilder.newColumn('PaidTo').withTitle('PaidTo').notSortable(),
        DTColumnBuilder.newColumn('PaymentType').withTitle('Payment Type').notSortable(),
        DTColumnBuilder.newColumn('Amount').withTitle('Amount').notSortable(),
        DTColumnBuilder.newColumn('CreatedBranch').withTitle('Branch').notSortable(),
        
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            var strLinks = '<a ng-click="EditExpenseData(' + row.ExpenseID + ')" class="pull-left font20 marleft15" > <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
            strLinks = strLinks + '<a class="pull-left marleft15 font20 delete" ng-click="DeleteExpense(' + row.ExpenseID + ')" > <i class="fa fa-trash-o" aria-hidden="true"></i></a>';
            return strLinks;

        })
    ];
    $scope.Branches = [];
    $scope.GetBranches = function () {
        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    brnches.push(response.data[i]);
                }
                $scope.Branches = brnches;

            },
                function errorCallback(response) {
                    alert(response);
                });

    }
  
    $scope.GetPaymentTypes = function () {

        var pmtTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVehicleHirePaymentTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    pmtTypes.push(response.data[i]);
                }
                $scope.PaymentTypes = pmtTypes;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }

    $scope.GetExpenseTypes = function () {

        var accounts = [];
        $http({
            method: 'GET',
            url: '/Voucher/GetExpenseTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    accounts.push(response.data[i]);
                }
                $scope.ExpenseTypes = accounts;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.AddExpense = function () {
        $scope.InitExpense();
        $('#chqDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false
        });
        $scope.GetBranches();
        $scope.GetPaymentTypes();
        $scope.GetExpenseTypes();
        $("#ExpenseModal").modal("show");
    }
    $scope.closeExpense = function () {
        $scope.InitExpense();
        $("#ExpenseModal").modal("hide");
    }


    $scope.ChangeEditPaymentType = function () {
        var selectedtext = $("#ddlEditPaymentType option:selected").text();
        if (selectedtext.toLowerCase() == "cheque") {
            $scope.isDisableCheque = false;
        }
        else {
            $scope.isDisableCheque = true;
        }
    }
    $scope.ChangePaymentType = function () {
        var selectedtext = $("#ddlPaymentType option:selected").text();
        if (selectedtext.toLowerCase() == "cheque") {
            $scope.isDisableCheque = false;
        }
        else {
            $scope.isDisableCheque = true;
        }
    }
    $scope.SaveExpense = function () {
        if ($scope.frmExpense.$valid) {
          
            $http({
                method: 'POST',
                url: '/Voucher/AddExpense',
                data: JSON.stringify($scope.Expense),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);

                    Notification.success({ message: "Expense saved successfully.", delay: 5000 });
                    $scope.closeExpense();
                    var dtinstance = $scope.dtInstance.DataTable;
                    $scope.dtOptions.ajax.data = function (d) {
                        d.branchId = $("#hdnBranchID").val();
                    };
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.InitEditExpense = function () {
        $scope.EditExpense = {
            ExpenseID: '',
            ExpenseDate: $scope.currentDate,
            ExpenseType: '',
            BranchID: $("#hdnBranchID").val(),
            BankName: '',
            BranchName: '',
            ChequeNo: '',
            ChequeDate: '',
            PaidTo: '',
            PaymentTypeID: '',
            Amount: '',
            Particulars: ''
        };
    }

    $scope.GetExpenseByID = function (ExpenseId) {
        $scope.EditExpense = {
            ExpenseID: ExpenseId,
            ExpenseDate: $scope.currentDate,
            ExpenseType: '',
            BranchID: $("#hdnBranchID").val(),
            BankName: '',
            BranchName: '',
            ChequeNo: '',
            ChequeDate: '',
            PaidTo: '',
            PaymentTypeID: '',
            Amount: '',
            Particulars: ''
        };
        var expense = [];
        $http({
            method: 'GET',
            url: '/Voucher/GetExpenseByID',
            params: { 'ExpenseID': ExpenseId },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    expense.push(response.data[i]);
                }

                $scope.EditExpense.ExpenseID = expense[0].ExpenseID;
                $scope.EditExpense.ExpenseDate = expense[0].ExpenseDateStr;
                $scope.EditExpense.ExpenseType = expense[0].ExpenseType;
                $scope.EditExpense.PaidTo = expense[0].PaidTo;
                $scope.EditExpense.BankName = expense[0].BankName;
                $scope.EditExpense.BranchName = expense[0].BranchName;
                $scope.EditExpense.ChequeNo = expense[0].ChequeNo;
                $scope.EditExpense.PaymentTypeID = expense[0].PaymentTypeID;
                $scope.EditExpense.Amount = expense[0].Amount;
                $scope.EditExpense.Particulars = expense[0].Particulars;
                $scope.EditExpense.ChequeDate = expense[0].ChequeDate;

                $("#EditExpenseModal").modal("show");
                //change payment type

                var paymenttypes = filterFilter($scope.PaymentTypes, { PaymentTypeID: $scope.EditExpense.PaymentTypeID });
                var selectedtext = paymenttypes[0].PaymentType;
                if (selectedtext.toLowerCase() == "cheque") {
                    $scope.isDisableCheque = false;
                }
                else {
                    $scope.isDisableCheque = true;
                }
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.EditExpenseData = function (ExpenseID) {
        $scope.GetPaymentTypes();
        $scope.GetExpenseTypes();
        $('#chqEditDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false
        });
        setTimeout(function () { $scope.GetExpenseByID(ExpenseID); }, 1000);

    }

    $scope.UpdateExpense = function () {

     
        if ($("#ddlEditPaymentType option:selected").text().toLowerCase() != 'cheque') {
            $scope.EditExpense.ChequeNo = '';
            $scope.EditExpense.ChequeDate = null;
        }
        if ($scope.frmEditExpense.$valid) {
            $http({
                method: 'POST',
                url: '/Voucher/UpdateExpense',
                data: JSON.stringify($scope.EditExpense),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeUpdateExpense();
                    Notification.success({ message: "Expense updated successfully.", delay: 5000 });
                    var dtinstance = $scope.dtInstance.DataTable;
                    $scope.dtOptions.ajax.data = function (d) {
                        d.branchId = $("#hdnBranchID").val();
                    };
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }

    $scope.DeleteExpense = function (ExpenseID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Voucher/DeleteExpense',
                params: { 'ExpenseID': ExpenseID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Expense deleted successfully.", delay: 5000 });
                    var dtinstance = $scope.dtInstance.DataTable;
                    $scope.dtOptions.ajax.data = function (d) {
                        d.branchId = $("#hdnBranchID").val();
                    };
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
    $scope.closeUpdateExpense = function () {
        $("#EditExpenseModal").modal("hide");
    }
}]);

App.controller('VendorExpense_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder) {
    $scope.Branches = [];
    $scope.GetBranches = function () {
        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    brnches.push(response.data[i]);
                }
                $scope.Branches = brnches;

            },
                function errorCallback(response) {
                    alert(response);
                });

    }
    $scope.GetBranches();
    $scope.currentDate = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.InitExpense = function () {
        $scope.Expense = {
            VendorExpenseID: '',
            ExpenseDate: $scope.currentDate,
            BranchID: $("#hdnBranchID").val(),
            VendorID: '',
            InvoiceNo: '',
            BillAmount: '',
            AdvancePayment: '',
            BalanceAmount: '',
            PaymentTypeID: '',
            BankName: '',
            BranchName: '',
            ChequeNo: '',
            ChequeDate: '',
            Particulars: ''
        };
    }
    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Voucher/GetVendorExpenseData",
            type: 'GET',
            data: function (d) {
                d.branchId = $("#hdnBranchID").val();
            }
        })
        .withOption('aaSorting', [[2, "desc"]])
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { search: "", searchPlaceholder: "Search..." })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('VendorExpenseID').withTitle('VendorExpenseID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('ExpenseDateStr').withTitle('Expense Date'),
        DTColumnBuilder.newColumn('PaymentType').withTitle('Payment Type').notSortable(),
        DTColumnBuilder.newColumn('BillAmount').withTitle('Bill Amount').notSortable(),
        DTColumnBuilder.newColumn('BalanceAmount').withTitle('Balance Amount').notSortable(),
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            var strLinks = '<a ng-click="EditExpenseData(' + row.VendorExpenseID + ')" class="pull-left font20 marleft15" > <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
            strLinks = strLinks + '<a class="pull-left marleft15 font20 delete" ng-click="DeleteExpense(' + row.VendorExpenseID + ')" > <i class="fa fa-trash-o" aria-hidden="true"></i></a>';
            return strLinks;

        })
    ];

    $scope.GetPaymentTypes = function () {

        var pmtTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVehicleHirePaymentTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    pmtTypes.push(response.data[i]);
                }
                $scope.PaymentTypes = pmtTypes;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }

    $scope.GetVendors = function () {

        var vendor = [];
        $http({
            method: 'GET',
            url: '/Operations/GetOtherVendors',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    vendor.push(response.data[i]);
                }
                $scope.Vendors = vendor;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.calculateBalanceAmount = function () {
        var bookingAmt = 0;
        if ($scope.Expense.BillAmount != '')
            bookingAmt = parseFloat($scope.Expense.BillAmount);
        var advPayment = 0;
        if ($scope.Expense.AdvancePayment != '')
            advPayment = parseFloat($scope.Expense.AdvancePayment);
        if (advPayment > bookingAmt) {
            Notification.error({ message: 'Advance payment can not be greater than booking amount !', delay: 5000 });
        }
        var balanceAmt = eval(bookingAmt - advPayment);
        $scope.Expense.BalanceAmount = balanceAmt;

    }
    $scope.AddExpense = function () {
        $scope.InitExpense();
        $('#chqDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false
        });
        $scope.GetPaymentTypes();
        $scope.GetVendors();
        $("#ExpenseModal").modal("show");
    }
    $scope.closeExpense = function () {
        $scope.InitExpense();
        $("#ExpenseModal").modal("hide");
    }


    $scope.ChangeEditPaymentType = function () {
        var selectedtext = $("#ddlEditPaymentType option:selected").text();
        if (selectedtext.toLowerCase() == "cheque") {
            $scope.isDisableCheque = false;
        }
        else {
            $scope.isDisableCheque = true;
        }
    }
    $scope.ChangePaymentType = function () {
        var selectedtext = $("#ddlPaymentType option:selected").text();
        if (selectedtext.toLowerCase() == "cheque") {
            $scope.isDisableCheque = false;
        }
        else {
            $scope.isDisableCheque = true;
        }
    }
    $scope.SaveExpense = function () {
        if ($scope.frmExpense.$valid) {

            $http({
                method: 'POST',
                url: '/Voucher/AddVendorExpense',
                data: JSON.stringify($scope.Expense),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);

                    Notification.success({ message: "Vendor Expense saved successfully.", delay: 5000 });
                    $scope.closeExpense();
                    var dtinstance = $scope.dtInstance.DataTable;
                    $scope.dtOptions.ajax.data = function (d) {
                        d.branchId = $("#hdnBranchID").val();
                    };
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.InitEditExpense = function () {
        $scope.EditExpense = {
            VendorExpenseID: '',
            ExpenseDate: $scope.currentDate,
            BranchID: $("#hdnBranchID").val(),
            VendorID: '',
            InvoiceNo: '',
            BillAmount: '',
            AdvancePayment: '',
            BalanceAmount: '',
            PaymentTypeID: '',
            BankName: '',
            BranchName: '',
            ChequeNo: '',
            ChequeDate: '',
            Particulars: ''
        };
    }

    $scope.GetExpenseByID = function (ExpenseId) {
        $scope.EditExpense = {
            VendorExpenseID: ExpenseId,
            ExpenseDate: $scope.currentDate,
            BranchID: $("#hdnBranchID").val(),
            VendorID: '',
            InvoiceNo: '',
            BillAmount: '',
            AdvancePayment: '',
            BalanceAmount: '',
            PaymentTypeID: '',
            BankName: '',
            BranchName: '',
            ChequeNo: '',
            ChequeDate: '',
            Particulars: ''
        };
        var expense = [];
        $http({
            method: 'GET',
            url: '/Voucher/GetVendorExpenseByID',
            params: { 'ExpenseID': ExpenseId },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    expense.push(response.data[i]);
                }

                $scope.EditExpense.VendorExpenseID = expense[0].VendorExpenseID;
                $scope.EditExpense.ExpenseDate = expense[0].ExpenseDateStr;
                $scope.EditExpense.BranchID = expense[0].BranchID;
                $scope.EditExpense.VendorID = expense[0].VendorID;
                $scope.EditExpense.InvoiceNo = expense[0].InvoiceNo;
                $scope.EditExpense.BillAmount = expense[0].BillAmount;
                $scope.EditExpense.AdvancePayment = expense[0].AdvancePayment;
                $scope.EditExpense.BalanceAmount = expense[0].BalanceAmount;
                $scope.EditExpense.BankName = expense[0].BankName;
                $scope.EditExpense.BranchName = expense[0].BranchName;
                $scope.EditExpense.ChequeNo = expense[0].ChequeNo;
                $scope.EditExpense.PaymentTypeID = expense[0].PaymentTypeID;
                $scope.EditExpense.Particulars = expense[0].Particulars;
                $scope.EditExpense.ChequeDate = expense[0].ChequeDate;

                $("#EditExpenseModal").modal("show");
                //change payment type

                var paymenttypes = filterFilter($scope.PaymentTypes, { PaymentTypeID: $scope.EditExpense.PaymentTypeID });
                var selectedtext = paymenttypes[0].PaymentType;
                if (selectedtext.toLowerCase() == "cheque") {
                    $scope.isDisableCheque = false;
                }
                else {
                    $scope.isDisableCheque = true;
                }
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.EditExpenseData = function (ExpenseID) {
        $scope.GetPaymentTypes();
        $scope.GetVendors();
        $('#chqEditDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false
        });
        setTimeout(function () { $scope.GetExpenseByID(ExpenseID); }, 1000);

    }

    $scope.UpdateExpense = function () {


        if ($("#ddlEditPaymentType option:selected").text().toLowerCase() != 'cheque') {
            $scope.EditExpense.ChequeNo = '';
            $scope.EditExpense.ChequeDate = null;
        }
        if ($scope.frmEditExpense.$valid) {
            $http({
                method: 'POST',
                url: '/Voucher/UpdateVendorExpense',
                data: JSON.stringify($scope.EditExpense),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeUpdateExpense();
                    Notification.success({ message: "Expense updated successfully.", delay: 5000 });
                    var dtinstance = $scope.dtInstance.DataTable;
                    $scope.dtOptions.ajax.data = function (d) {
                        d.branchId = $("#hdnBranchID").val();
                    };
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }

    $scope.DeleteExpense = function (ExpenseID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Voucher/DeleteVendorExpense',
                params: { 'ExpenseID': ExpenseID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Expense deleted successfully.", delay: 5000 });
                    var dtinstance = $scope.dtInstance.DataTable;
                    $scope.dtOptions.ajax.data = function (d) {
                        d.branchId = $("#hdnBranchID").val();
                    };
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
    $scope.closeUpdateExpense = function () {
        $("#EditExpenseModal").modal("hide");
    }
}]);