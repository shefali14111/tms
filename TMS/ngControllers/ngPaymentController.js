﻿ App.controller('ToPayAndPaidCollection_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "Excel", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, Excel, $timeout) {
     $scope.Branches = [];
     $scope.GetBranches = function () {
         var brnches = [];
         $http({
             method: 'GET',
             url: '/Operations/GetBranches',
             headers: { 'Content-Type': 'application/json; charset=utf-8' }
         })
             .then(function successCallback(response) {
                 for (i = 0; i < response.data.length; i++) {
                     brnches.push(response.data[i]);
                 }
                 $scope.Branches = brnches;

             },
                 function errorCallback(response) {
                     alert(response);
                 });

     }
     $scope.GetBranches();
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: "/Payment/GetToPayAndPaidConsignments",
            type: 'GET',
            data: function (d) {
                d.branchId = $("#ddlBranch").val();
            }
          
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)

        .withOption('destroy', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('ConsignmentID').withTitle('ConsignmentID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('ConsignmentNo').withTitle('Consignment No').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentDate').withTitle('Consignment Date').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentAmount').withTitle('Consignment Amount').notSortable(),
        DTColumnBuilder.newColumn('BillingType').withTitle('Payment Type').notSortable(),
      
        DTColumnBuilder.newColumn('Action').notSortable().renderWith(function (data, type, row, meta) {
            return '<a style="font-size:18px" data-ng-click="PayForConsignment(' + row.ConsignmentID + ')" class="pull-left font20 marleft15"><i class="fa fa-money" aria-hidden="true"></i>&nbsp;&nbsp;Make Payment</a>'
        })
    ];
    $scope.closeModal = function () {
        $scope.ToPayConsignment = {
            ConsignmentNo: '',
            ConsignmentDate: '',
            ConsignmentAmount: '',
            ReceivedAmount: 0,
            PaymentTypeID: '',
            BankName: '',
            BranchName: '',
            ChequeNo: '',
            ChequeDate: '',
            Remarks: '',
            ReceivedBy: '',
            ReceivedDate: $scope.currentDate,
            ReceivedTime: $scope.currentTime,
            PaidAmount: 0,
            BranchID:''
        };
        $('#ToPayModal').modal('hide');
    }
    $scope.PayForConsignment = function (ConsignmentID) {
        $scope.currentDate = $filter('date')(new Date(), 'dd/MM/yyyy');
        var date = new Date();
        var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
        var am_pm = date.getHours() >= 12 ? "PM" : "AM";
        hours = hours < 10 ? "0" + hours : hours;
        var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
        time = hours + ":" + minutes + ":" + seconds + " " + am_pm;
        $scope.currentTime = time;
        $('#chqDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false
        });
        $scope.ToPayConsignment = {
            ConsignmentID: ConsignmentID,
            ConsignmentNo: '',
            ConsignmentDate: '',
            ConsignmentAmount: '',
            ReceivedAmount: 0,
            PaymentTypeID: '',
            BankName: '',
            BranchName: '',
            ChequeNo: '',
            ChequeDate: '',
            Remarks: '',
            ReceivedBy: '',
            ReceivedDate: $scope.currentDate,
            ReceivedTime: $scope.currentTime,
            PaidAmount:0
        };
        var ToPayConsignmentDetails = [];
        $http({
            method: 'GET',
            url: '/Payment/GetConsignmentPaymentDetails',
            params: { 'ConsignmentID': ConsignmentID },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {

                for (i = 0; i < response.data.length; i++) {
                    ToPayConsignmentDetails.push(response.data[i]);
                }
                $scope.ToPayConsignment.ConsignmentNo = ToPayConsignmentDetails[0].ConsignmentNo;
                $scope.ToPayConsignment.ConsignmentDate = ToPayConsignmentDetails[0].ConsignmentDate;
                $scope.ToPayConsignment.ConsignmentAmount = ToPayConsignmentDetails[0].ConsignmentAmount;
                $scope.ToPayConsignment.PaidAmount = ToPayConsignmentDetails[0].ReceivedAmount || 0; 
                $scope.ToPayConsignment.ReceivedAmount = $scope.ToPayConsignment.PaidAmount == 0 ? ToPayConsignmentDetails[0].ConsignmentAmount : (parseFloat(ToPayConsignmentDetails[0].ConsignmentAmount) - parseFloat($scope.ToPayConsignment.PaidAmount))
                $scope.GetbankPaymentTypes();
                $scope.ValidateValue = $scope.ToPayConsignment.PaidAmount == 0 ? ToPayConsignmentDetails[0].ConsignmentAmount : (parseFloat(ToPayConsignmentDetails[0].ConsignmentAmount) - parseFloat($scope.ToPayConsignment.PaidAmount));
                $('#ToPayModal').modal('show');
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.selectAmount = function () {
        if ($("#txtAmount").val() != "" && $("#txtAmount").val() != "0") {
            var amount = parseFloat($("#txtAmount").val());
            if (amount > $scope.ValidateValue) {
                $scope.IsValidPay = false;
            }
            else {
                $scope.IsValidPay = true;
            }
        }
    }
    $scope.bankPaymentTypes = [];
    $scope.GetbankPaymentTypes = function () {

        var pmtTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVehicleHirePaymentTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    pmtTypes.push(response.data[i]);
                }
                $scope.bankPaymentTypes = pmtTypes;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.SelectPaymentType = function () {
        if ($("#ddlPaymentTypeForPayment option:selected").text().toLowerCase() == "cash" || $("#ddlPaymentTypeForPayment option:selected").text().toLowerCase() == "billing") {
            $scope.isDisableCheque = true;
        }
        else {
            $scope.isDisableCheque = false;
        }
        if ($("#ddlPaymentTypeForPayment option:selected").text().toLowerCase() == "cheque") {
            $scope.isRequiredCheque = true;
        }
        else {
            $scope.isRequiredCheque = false;
        }
      
    }
     $scope.GetToPayAndPaidCollection = function () {

         var branchId = $('#ddlBranch').val();

         var dtinstance = $scope.dtInstance.DataTable;
         $scope.dtOptions.ajax.data = function (d) {

             d.branchId = branchId;
         };
         dtinstance.ajax.reload(function (json) { }, true);

     }
     $scope.AddPaymentForConsignment = function () {
         $scope.ToPayConsignment.BranchID = $("#ddlBranch").val();
        if ($scope.IsValidPay == false) {
            Notification.error({ message: 'Please enter valid amount.', delay: 5000 });
        }
        else if ($scope.frmToPay.$valid && $scope.ToPayConsignment.ConsignmentID != '') {
            $http({
                method: 'POST',
                url: '/Payment/AddPaymentForConsignment',
                dataType: 'json',
                traditional: true,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify($scope.ToPayConsignment),
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Consignment payment done successfully.", delay: 5000 });
                    $scope.closeModal();
                    var dtinstance = $scope.dtInstance.DataTable;
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });

        }
        else {
            Notification.error({ message: 'Please fill all required fields!', delay: 5000 });
        }
    }
}]);

App.controller('VehicleHireBalancePayment_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "Excel", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, Excel, $timeout) {
    $scope.Branches = [];
    $scope.GetBranches = function () {
        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    brnches.push(response.data[i]);
                }
                $scope.Branches = brnches;

            },
                function errorCallback(response) {
                    alert(response);
                });

    }
    $scope.GetBranches();
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: "/Payment/GetVehicleHireBalancePayment",
            type: 'GET',
            data: function (d) {
                d.branchId = $("#ddlBranch").val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)

        .withOption('destroy', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('VehicleHireID').withTitle('VehicleHireID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('VehicleNo').withTitle('Vehicle No').notSortable(),
        DTColumnBuilder.newColumn('HireDate').withTitle('Hire Date').notSortable(),
        DTColumnBuilder.newColumn('VendorName').withTitle('Vendor Name').notSortable(),
        DTColumnBuilder.newColumn('ManifestNo').withTitle('Manifest No').notSortable(),
        DTColumnBuilder.newColumn('HiredBy').withTitle('Hired By').notSortable(),
        DTColumnBuilder.newColumn('BookingAmount').withTitle('Booking Amount').notSortable(),
        DTColumnBuilder.newColumn('BalanceAmount').withTitle('Balance Amount').notSortable(),
        DTColumnBuilder.newColumn('HireType').withTitle('').notVisible(),
        DTColumnBuilder.newColumn('Action').notSortable().renderWith(function (data, type, row, meta) {
            return '<a style="font-size:18px" data-ng-click="PayForVehicleHire(' + row.VehicleHireID + ',\''+row.HireType+'\')" class="pull-left font20 marleft15"><i class="fa fa-money" aria-hidden="true"></i>&nbsp;&nbsp;Make Payment</a>'
        })
    ];
    $scope.closeModal = function () {
        $scope.VehicleHireBalance = {
            VehicleHirePaymentId: '',
            BalanceAmount: 0,
            Remarks: '',
            PaymentDate: $scope.currentDate,
            PaymentTime: $scope.currentTime,
            VehicleNo: '',
            VehicleHireDate: '',
            VendorName: '',
            DriverName: '',
            DriverMobileNo: '',
            ManifestNo: '',
            HiredBy: '',
            BookingAmount: '',
            BalanceAmount: '',
            Remarks: '',
            PaymentTypeID: '',
            BankName: '',
            BranchName: '',
            ChequeNo: '',
            ChequeDate: '',
            Remarks: '',
        };
        $('#ToPayModal').modal('hide');
    }
  
    $scope.PayForVehicleHire = function (VehicleHireID,HireType) {
        $('#chqDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false
        });
        $scope.currentDate = $filter('date')(new Date(), 'dd/MM/yyyy');
        var date = new Date();
        var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
        var am_pm = date.getHours() >= 12 ? "PM" : "AM";
        hours = hours < 10 ? "0" + hours : hours;
        var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
        time = hours + ":" + minutes + ":" + seconds + " " + am_pm;
        $scope.currentTime = time;
        $scope.HireType = HireType;
        $scope.VehicleHireBalance = {
            VehicleHirePaymentId: VehicleHireID,
            VehicleHireID: VehicleHireID,
            BalanceAmount: 0,
            Remarks: '',
            PaymentDate: $scope.currentDate,
            PaymentTime: $scope.currentTime,
            VehicleNo: '',
            HireDate: '',
            VendorName: '',
            DriverName: '',
            DriverMobileNo: '',
            ManifestNo: '',
            HiredBy: '',
            BookingAmount: 0,
            PaidAmount:0,
            BalanceAmount: 0,
            Remarks: '',
            PaymentTypeID: '',
            BankName: '',
            BranchName: '',
            ChequeNo: '',
            ChequeDate: '',
            Remarks: '',
        };
        var BalancePaymentDetails = [];
        $http({
            method: 'GET',
            url: '/Payment/GetManifestPaymentDetails',
            params: { 'VehicleHireID': VehicleHireID, 'HireType': HireType},
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {

                for (i = 0; i < response.data.length; i++) {
                    BalancePaymentDetails.push(response.data[i]);
                }
                $scope.VehicleHireBalance.HireType = $scope.HireType;
                $scope.VehicleHireBalance.VehicleNo = BalancePaymentDetails[0].VehicleNo;
                $scope.VehicleHireBalance.HireDate = BalancePaymentDetails[0].HireDate;
                $scope.VehicleHireBalance.VendorName = BalancePaymentDetails[0].VendorName;
                $scope.VehicleHireBalance.DriverName = BalancePaymentDetails[0].DriverName;
                $scope.VehicleHireBalance.DriverMobileNo = BalancePaymentDetails[0].DriverMobileNo;
                $scope.VehicleHireBalance.ManifestNo = BalancePaymentDetails[0].ManifestNo;
                $scope.VehicleHireBalance.HiredBy = BalancePaymentDetails[0].HiredBy;
                $scope.VehicleHireBalance.BookingAmount = BalancePaymentDetails[0].BookingAmount;
                $scope.VehicleHireBalance.BalanceAmount = parseFloat(BalancePaymentDetails[0].BookingAmount) - parseFloat(BalancePaymentDetails[0].PaidAmount);
                $scope.VehicleHireBalance.PaidAmount = BalancePaymentDetails[0].PaidAmount;
                $scope.ValidateValue = parseFloat(BalancePaymentDetails[0].BookingAmount) - parseFloat(BalancePaymentDetails[0].PaidAmount);
                $scope.GetbankPaymentTypes();
                $('#ToPayModal').modal('show');
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.bankPaymentTypes = [];
    $scope.GetbankPaymentTypes = function () {

        var pmtTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVehicleHirePaymentTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    pmtTypes.push(response.data[i]);
                }
                $scope.bankPaymentTypes = pmtTypes;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.selectAmount = function () {
        if ($("#txtBalanceAmount").val() != "" && $("#txtBalanceAmount").val() != "0") {
            var amount = parseFloat($("#txtBalanceAmount").val());
            if (amount > $scope.ValidateValue) {
                $scope.IsValidPay = false;
            }
            else {
                $scope.IsValidPay = true;
            }
        }
    }
    $scope.SelectPaymentType = function () {
        if ($("#ddlPaymentTypeForPayment option:selected").text().toLowerCase() == "cash" || $("#ddlPaymentTypeForPayment option:selected").text().toLowerCase() == "billing") {
            $scope.isDisableCheque = true;
        }
        else {
            $scope.isDisableCheque = false;
        }
        if ($("#ddlPaymentTypeForPayment option:selected").text().toLowerCase() == "cheque") {
            $scope.isRequiredCheque = true;
        }
        else {
            $scope.isRequiredCheque = false;
        }

    }
    $scope.GetVehicleBalancePayment = function () {
      
        var branchId = $('#ddlBranch').val();
       
            var dtinstance = $scope.dtInstance.DataTable;
            $scope.dtOptions.ajax.data = function (d) {
               
                d.branchId = branchId;
            };
            dtinstance.ajax.reload(function (json) { }, true);
      
    }
    $scope.AddPaymentForVehicleHire = function () {
        if ($scope.IsValidPay == false) {
            Notification.error({ message: 'Please enter valid amount.', delay: 5000 });
        }
        else if ($scope.frmToPay.$valid && $scope.VehicleHireBalance.VehicleHireID != '') {
            $http({
                method: 'POST',
                url: '/Payment/AddPaymentForManifest',
                dataType: 'json',
                traditional: true,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify($scope.VehicleHireBalance),
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Consignment Party Invoices saved successfully.", delay: 5000 });
                    $scope.closeModal();
                    var dtinstance = $scope.dtInstance.DataTable;
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });

        }
        else {
            Notification.error({ message: 'Please fill all required fields!', delay: 5000 });
        }
    }
}]);

App.controller('Deposite_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "Excel", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, Excel, $timeout) {
    $scope.Branches = [];
    $scope.GetBranches = function () {
        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    brnches.push(response.data[i]);
                }
                $scope.Branches = brnches;

            },
                function errorCallback(response) {
                    alert(response);
                });

    }
    $scope.GetBranches();
    $scope.currentDate = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: "/Payment/GetDeposites",
            type: 'GET',
            data: function (d) {
                d.branchId = $("#ddlBranch").val();
            }

        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)

        .withOption('destroy', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('DepositeID').withTitle('DepositeID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('DepositeDate').withTitle('Date').notSortable(),
        DTColumnBuilder.newColumn('DepositeAmount').withTitle('Deposite Amount').notSortable(),
        DTColumnBuilder.newColumn('BranchName').withTitle('Branch').notSortable(),

        DTColumnBuilder.newColumn('Action').notSortable().renderWith(function (data, type, row, meta) {
            return '<a ng-click="DeleteDeposite(' + row.DepositeID + ')" class="pull-left font20 marleft15" > <i class="fa fa-trash-o" aria-hidden="true"></i></a>'
        })
    ];
    $scope.closeModal = function () {
        $scope.Deposite = {
            DepositeID: '',
            Date: '',
            DepositeAmount: 0,
            BranchID:''
        };
        $('#DepositeModal').modal('hide');
    }
    $scope.GetBalanceAmount = function () {
        $http({
            method: 'GET',
            url: '/Operations/GetBalanceAmount',
            params: { 'BranchID': $scope.Deposite.BranchID },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                $scope.BalanceAmount = response.data;
              
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.AddDepositeModal = function () {
        $scope.Deposite = {
            DepositeID: '',
            Date: $scope.currentDate,
            DepositeAmount: 0,
            BranchID: $("#ddlBranch").val()
        };
        $scope.GetBalanceAmount();
        $('#DepositeModal').modal('show');
    }
    $scope.AddDeposite = function () {
 
         if ($scope.frmDeposite.$valid) {
            $http({
                method: 'POST',
                url: '/Payment/AddDeposite',
                dataType: 'json',
                traditional: true,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify($scope.Deposite),
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Deposite done successfully.", delay: 5000 });
                    $scope.closeModal();
                    var dtinstance = $scope.dtInstance.DataTable;
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });

        }
        else {
            Notification.error({ message: 'Please fill all required fields!', delay: 5000 });
        }
    }
    $scope.DeleteDeposite = function (DepositeID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Payment/DeleteDeposite',
                params: { 'DepositeID': DepositeID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Deposite deleted successfully.", delay: 5000 });
                    var dtinstance = $scope.dtInstance.DataTable;
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
}]);

App.controller('VehicleVendorDeposite_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "Excel", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, Excel, $timeout) {
    $scope.Branches = [];
    $scope.GetBranches = function () {
        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    brnches.push(response.data[i]);
                }
                $scope.Branches = brnches;

            },
                function errorCallback(response) {
                    alert(response);
                });

    }
    $scope.GetBranches();
    $scope.GetVendors = function () {

        var vendor = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVehicleVendors',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    vendor.push(response.data[i]);
                }
                $scope.Vendors = vendor;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetVendors();
    $scope.currentDate = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: "/Payment/GetVehicleVendorDeposites",
            type: 'GET',
            data: function (d) {
                d.branchId = $("#ddlBranch").val();
            }

        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)

        .withOption('destroy', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('DepositeID').withTitle('DepositeID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('DepositeDate').withTitle('Date').notSortable(),
        DTColumnBuilder.newColumn('DepositeAmount').withTitle('Deposite Amount').notSortable(),
        DTColumnBuilder.newColumn('BranchName').withTitle('Branch').notSortable(),
        DTColumnBuilder.newColumn('VendorName').withTitle('Vendor').notSortable(),
        DTColumnBuilder.newColumn('Action').notSortable().renderWith(function (data, type, row, meta) {
            return '<a ng-click="DeleteDeposite(' + row.DepositeID + ')" class="pull-left font20 marleft15" > <i class="fa fa-trash-o" aria-hidden="true"></i></a>'
        })
    ];
    $scope.closeModal = function () {
        $scope.Deposite = {
            DepositeID: '',
            Date: '',
            DepositeAmount: 0,
            BranchID: '',
            VehicleVendorID:''
        };
        $('#DepositeModal').modal('hide');
    }
    $scope.GetVendorBalanceAmount = function () {
        $http({
            method: 'GET',
            url: '/Operations/GetVehicleVendorBalanceAmount',
            params: { 'BranchID': $scope.Deposite.BranchID, 'VehicleVendorID': $scope.Deposite.VehicleVendorID},
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                $scope.BalanceAmount = response.data;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.AddDepositeModal = function () {
        $scope.Deposite = {
            DepositeID: '',
            Date: $scope.currentDate,
            DepositeAmount: 0,
            BranchID: $("#ddlBranch").val(),
            VehicleVendorID:''
        };
      
        $('#DepositeModal').modal('show');
    }
    $scope.AddDeposite = function () {

        if ($scope.frmDeposite.$valid) {
            $http({
                method: 'POST',
                url: '/Payment/AddVehicleVendorDeposite',
                dataType: 'json',
                traditional: true,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify($scope.Deposite),
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Deposite done successfully.", delay: 5000 });
                    $scope.closeModal();
                    var dtinstance = $scope.dtInstance.DataTable;
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });

        }
        else {
            Notification.error({ message: 'Please fill all required fields!', delay: 5000 });
        }
    }
    $scope.DeleteDeposite = function (DepositeID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Payment/DeleteVehicleVendorDeposite',
                params: { 'DepositeID': DepositeID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Deposite deleted successfully.", delay: 5000 });
                    var dtinstance = $scope.dtInstance.DataTable;
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
}]);

App.controller('OtherVendorBalancePayment_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "Excel", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, Excel, $timeout) {
    $scope.Branches = [];
    $scope.GetBranches = function () {
        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    brnches.push(response.data[i]);
                }
                $scope.Branches = brnches;

            },
                function errorCallback(response) {
                    alert(response);
                });

    }
    $scope.GetBranches();
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: "/Payment/GetOtherVendorBalancePayment",
            type: 'GET',
            data: function (d) {
                d.branchId = $("#ddlBranch").val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)

        .withOption('destroy', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('VendorExpenseID').withTitle('VendorExpenseID').notSortable(),
        DTColumnBuilder.newColumn('ExpenseDateStr').withTitle('Expense Date').notSortable(),
        DTColumnBuilder.newColumn('VendorName').withTitle('Vendor Name').notSortable(),
        DTColumnBuilder.newColumn('BillAmount').withTitle('Total Bill Amount').notSortable(),
        DTColumnBuilder.newColumn('AdvancePayment').withTitle('Advance Payment').notSortable(),
        DTColumnBuilder.newColumn('BalanceAmount').withTitle('BalanceAmount'),
        DTColumnBuilder.newColumn('Action').notSortable().renderWith(function (data, type, row, meta) {
            return '<a style="font-size:18px" data-ng-click="PayForOtherVendor(' + row.VendorExpenseID +')" class="pull-left font20 marleft15"><i class="fa fa-money" aria-hidden="true"></i>&nbsp;&nbsp;Make Payment</a>'
        })
    ];
    $scope.closeModal = function () {
        $scope.OtherVendorBalance = {
            VendorExpenseID: '',
            ExpenseDate: '',
            VendorName: '',
            PaymentDate: $scope.currentDate,
            PaymentTime: $scope.currentTime,
            InvoiceNo: '',
            BillAmount: '',
            AdvancePayment: '',
            BalanceAmount: '',
            PaymentTypeID: '',
            BankName: '',
            BranchName: '',
            ChequeNo: '',
            ChequeDate: '',
            Remarks: '',
        };
        $('#OtherVendorPaymentModal').modal('hide');
    }

    $scope.PayForOtherVendor = function (VendorExpenseID) {
        $('#chqDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false
        });
        $scope.currentDate = $filter('date')(new Date(), 'dd/MM/yyyy');
        var date = new Date();
        var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
        var am_pm = date.getHours() >= 12 ? "PM" : "AM";
        hours = hours < 10 ? "0" + hours : hours;
        var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
        time = hours + ":" + minutes + ":" + seconds + " " + am_pm;
        $scope.currentTime = time;
        $scope.OtherVendorBalance = {
            VendorExpenseID: VendorExpenseID,
            ExpenseDate: '',
            VendorName: '',
            PaymentDate: $scope.currentDate,
            PaymentTime: $scope.currentTime,
            InvoiceNo: '',
            BillAmount: '',
            AdvancePayment: '',
            BalanceAmount: '',
            PaymentTypeID: '',
            BankName: '',
            BranchName: '',
            ChequeNo: '',
            ChequeDate: '',
            Remarks: '',
        };
        var BalancePaymentDetails = [];
        $http({
            method: 'GET',
            url: '/Payment/GetVendorExpenseDetails',
            params: { 'VendorExpenseID': VendorExpenseID },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {

                for (i = 0; i < response.data.length; i++) {
                    BalancePaymentDetails.push(response.data[i]);
                }
                $scope.OtherVendorBalance.VendorExpenseID = BalancePaymentDetails[0].VendorExpenseID;
                $scope.OtherVendorBalance.ExpenseDate = BalancePaymentDetails[0].ExpenseDateStr;
                $scope.OtherVendorBalance.VendorName = BalancePaymentDetails[0].VendorName;
                $scope.OtherVendorBalance.InvoiceNo = BalancePaymentDetails[0].InvoiceNo;
                $scope.OtherVendorBalance.BillAmount = BalancePaymentDetails[0].BillAmount;
                $scope.OtherVendorBalance.AdvancePayment = BalancePaymentDetails[0].AdvancePayment;
                $scope.OtherVendorBalance.PaidAmount = BalancePaymentDetails[0].PaidAmount; 
                $scope.OtherVendorBalance.BalanceAmount = parseFloat(BalancePaymentDetails[0].BillAmount) - parseFloat(BalancePaymentDetails[0].PaidAmount);
 
                $scope.ValidateValue = parseFloat(BalancePaymentDetails[0].BillAmount) - parseFloat(BalancePaymentDetails[0].PaidAmount);
                $scope.GetbankPaymentTypes();
                $('#OtherVendorPaymentModal').modal('show');
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.bankPaymentTypes = [];
    $scope.GetbankPaymentTypes = function () {

        var pmtTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVehicleHirePaymentTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    pmtTypes.push(response.data[i]);
                }
                $scope.bankPaymentTypes = pmtTypes;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.selectAmount = function () {
        if ($("#txtBalanceAmount").val() != "" && $("#txtBalanceAmount").val() != "0") {
            var amount = parseFloat($("#txtBalanceAmount").val());
            if (amount > $scope.ValidateValue) {
                $scope.IsValidPay = false;
            }
            else {
                $scope.IsValidPay = true;
            }
        }
    }
    $scope.SelectPaymentType = function () {
        if ($("#ddlPaymentTypeForPayment option:selected").text().toLowerCase() == "cash" || $("#ddlPaymentTypeForPayment option:selected").text().toLowerCase() == "billing") {
            $scope.isDisableCheque = true;
        }
        else {
            $scope.isDisableCheque = false;
        }
        if ($("#ddlPaymentTypeForPayment option:selected").text().toLowerCase() == "cheque") {
            $scope.isRequiredCheque = true;
        }
        else {
            $scope.isRequiredCheque = false;
        }

    }
    $scope.GetOtherVendorBalancePayment = function () {

        var branchId = $('#ddlBranch').val();

        var dtinstance = $scope.dtInstance.DataTable;
        $scope.dtOptions.ajax.data = function (d) {

            d.branchId = branchId;
        };
        dtinstance.ajax.reload(function (json) { }, true);

    }
    $scope.AddPaymentForOtherVendor = function () {
        if ($scope.IsValidPay == false) {
            Notification.error({ message: 'Please enter valid amount.', delay: 5000 });
        }
        else if ($scope.frmOtherVendorPaymentData.$valid && $scope.OtherVendorBalance.VendorExpenseID != '') {
            $http({
                method: 'POST',
                url: '/Payment/AddPaymentForOtherVendor',
                dataType: 'json',
                traditional: true,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify($scope.OtherVendorBalance),
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Payment saved successfully.", delay: 5000 });
                    $scope.closeModal();
                    var dtinstance = $scope.dtInstance.DataTable;
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });

        }
        else {
            Notification.error({ message: 'Please fill all required fields!', delay: 5000 });
        }
    }
}]);