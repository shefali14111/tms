﻿App.factory('BranchData', ["$http", function ($http) {
    var Branches = [];
    return {
        GetData: function () {
            $http({
                method: 'GET',
                url: '/Operations/GetBranches',
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    for (i = 0; i < response.data.length; i++) {
                        Branches.push(response.data[i]);
                    }

                },
                    function errorCallback(response) {
                        alert(response);
                    });
            return Branches;
        }
    };
}])
App.controller('DeliveryStockReport_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "BranchData", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, BranchData, $timeout) {
    $scope.Branches = BranchData.GetData();
    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Report/GetDeliveryStockData",
            type: 'GET',
            data: function (d) {
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
                d.branchId = $("#ddlBranch").val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('ConsignmentNo').withTitle('Lr No').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentDate').withTitle('Lr Date').notSortable(),
        DTColumnBuilder.newColumn('PaymentType').withTitle('Payment Type').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentFromID').withTitle('From').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentToID').withTitle('To').notSortable(),
        DTColumnBuilder.newColumn('ConsignorName').withTitle('Consignor').notSortable(),
        DTColumnBuilder.newColumn('ConsigneeName').withTitle('Consignee').notSortable(),
        DTColumnBuilder.newColumn('Article').withTitle('No Of Article').notSortable(),
        DTColumnBuilder.newColumn('Deliverydatestr').withTitle('Delivery Date').notSortable(),
        DTColumnBuilder.newColumn('DeliveredArticles').withTitle('Delivered Article').notSortable(),
        DTColumnBuilder.newColumn('VehicleNo').withTitle('Vehicle No').notSortable(),
        DTColumnBuilder.newColumn('VendorName').withTitle('Vendor Name').notSortable(),
        DTColumnBuilder.newColumn('DeliveryRemarks').withTitle('Remarks').notSortable(),
        DTColumnBuilder.newColumn('DeliveryStatus').withTitle('Status').notSortable(),
        DTColumnBuilder.newColumn('UserName').withTitle('Delivery Done By').notSortable(),
        DTColumnBuilder.newColumn('Created').withTitle('Unload Date').notSortable(),
        DTColumnBuilder.newColumn('ToLocBranch').withTitle('Manifest To Branch').notSortable()

    ];


    $scope.InitializeDatePicker = function () {
        $('#txtFromDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#txtToDate').datepicker('setStartDate', startDate);
        });
        $('#txtToDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#txtFromDate').datepicker('setEndDate', FromEndDate);
        });
    }

    $scope.GetDeliveryStockReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $("#ddlBranch").val();
        if (fromdate == "" || todate == "") {
            Notification.error({ message: "Please select all criteria.", delay: 5000 });
        }
        else {
            var dtinstance = $scope.dtInstance.DataTable;
            $scope.dtOptions.ajax.data = function (d) {
                d.fromDate = fromdate;
                d.toDate = todate;
                d.branchId = branchId;
            };
            dtinstance.ajax.reload(function (json) { }, true);
        }
    }
    $scope.ExportDeliveryStockData = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $("#ddlBranch").val();
        if (branchId != "") {
            $timeout(function () {
                var link = document.createElement('a');
                link.download = "DeliveryStockReport.xls";
                link.href = "/Report/ExportDeliveryStockReport?Fromdate=" + fromdate + "&Todate=" + todate + "&BranchId=" + branchId;
                link.click();
            }, 100);
        }
        else {
            Notification.error({ message: "Please select all criteria.", delay: 5000 });
        }
    }
}]);

App.controller('PendingStockReport_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "BranchData", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, BranchData, $timeout) {
    $scope.Branches = BranchData.GetData();
    $scope.pendingStockCollection = [];

    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Report/GetPendingStockData",
            type: 'GET',
            data: function (d) {
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
                d.branchId = $("#ddlBranch").val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('ConsignmentNo').withTitle('Lr No').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentDate').withTitle('Lr Date').notSortable(),
        DTColumnBuilder.newColumn('PaymentType').withTitle('Payment Type').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentFromID').withTitle('From').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentToID').withTitle('To').notSortable(),
        DTColumnBuilder.newColumn('ConsignorName').withTitle('Consignor').notSortable(),
        DTColumnBuilder.newColumn('ConsigneeName').withTitle('Consignee').notSortable(),
        DTColumnBuilder.newColumn('DeliveryType').withTitle('Delivery Type').notSortable(),
        DTColumnBuilder.newColumn('InvoiceNos').withTitle('Invoice Nos').notSortable(),
        DTColumnBuilder.newColumn('InvoiceQuantity').withTitle('Quantity').notSortable(),
        DTColumnBuilder.newColumn('InvoiceValue').withTitle('InvoiceValue').notSortable(),
        DTColumnBuilder.newColumn('Article').withTitle('Article').notSortable(),
        DTColumnBuilder.newColumn('ActualWeight').withTitle('Actual Weight').notSortable(),
        DTColumnBuilder.newColumn('CreatedDate').withTitle('Created').notSortable(),
        DTColumnBuilder.newColumn('PartNos').withTitle('Part Nos').notSortable(),
        DTColumnBuilder.newColumn('ASNNos').withTitle('ASN Nos').notSortable(),
        DTColumnBuilder.newColumn('PoNos').withTitle('Po Nos').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentBranch').withTitle('Pending Stock Branch').notSortable()
    ];
    $scope.InitializeDatePicker = function () {
        $('#txtFromDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#txtToDate').datepicker('setStartDate', startDate);
        });
        $('#txtToDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#txtFromDate').datepicker('setEndDate', FromEndDate);
        });
    }
    $scope.GetPendingStockReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $('#ddlBranch').val();
        if (fromdate == "" || todate == "") {
            Notification.error({ message: "Please select all criteria.", delay: 5000 });
        }
        else {
            var dtinstance = $scope.dtInstance.DataTable;
            $scope.dtOptions.ajax.data = function (d) {
                d.fromDate = fromdate;
                d.toDate = todate;
                d.branchId = branchId;
            };
            dtinstance.ajax.reload(function (json) { }, true);
        }
    }
    $scope.ExportPendingStockReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $("#ddlBranch").val();
        if (branchId != "") {
            $timeout(function () {
                var link = document.createElement('a');
                link.download = "PendingStockReport.xls";
                link.href = "/Report/ExportPendingStockReport?Fromdate=" + fromdate + "&Todate=" + todate + "&BranchId=" + branchId;
                link.click();
            }, 100);
        }
        else {
            Notification.error({ message: "Please select criteria.", delay: 5000 });
        }
    }
}]);

App.controller('DamageStockReport_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "BranchData", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, BranchData, $timeout) {
    $scope.Branches = BranchData.GetData();
    $scope.damageStockCollection = [];

    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Report/GetDamageStockData",
            type: 'GET',
            data: function (d) {
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
                d.branchId = $("#ddlBranch").val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('ConsignmentNo').withTitle('Lr No').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentDate').withTitle('Lr Date').notSortable(),
        DTColumnBuilder.newColumn('InvoiceValue').withTitle('Invoice Value').notSortable(),
        DTColumnBuilder.newColumn('Article').withTitle('No Of Article').notSortable(),
        DTColumnBuilder.newColumn('PaymentType').withTitle('Billing Type').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentFromID').withTitle('From').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentToID').withTitle('To').notSortable(),
        DTColumnBuilder.newColumn('ConsignorName').withTitle('Consignor').notSortable(),
        DTColumnBuilder.newColumn('ConsigneeName').withTitle('Consignee').notSortable(),
        DTColumnBuilder.newColumn('DamagedArticles').withTitle('Damage Articles').notSortable(),
        DTColumnBuilder.newColumn('Status').withTitle('Status').notSortable(),
        DTColumnBuilder.newColumn('ManifestCreatedBranch').withTitle('Manifest Created Branch').notSortable(),
        DTColumnBuilder.newColumn('ManifestNo').withTitle('Manifest No').notSortable(),
        DTColumnBuilder.newColumn('UnloadDate').withTitle('Unload Date').notSortable(),
        DTColumnBuilder.newColumn('VehicleNo').withTitle('Vehicle No').notSortable(),
        DTColumnBuilder.newColumn('DeliveryRemarks').withTitle('Remarks').notSortable(),
        DTColumnBuilder.newColumn('CreatedBy').withTitle('Delivery Done By').notSortable(),
        DTColumnBuilder.newColumn('ManifestToBranch').withTitle('Manifest To Branch').notSortable()
    ];

    $scope.InitializeDatePicker = function () {
        $('#txtFromDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#txtToDate').datepicker('setStartDate', startDate);
        });
        $('#txtToDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#txtFromDate').datepicker('setEndDate', FromEndDate);
        });
    }
    $scope.GetDamageStockReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $('#ddlBranch').val();
        if (fromdate == "" || todate == "") {
            Notification.error({ message: "Please select all criteria.", delay: 5000 });
        }
        else {
            var dtinstance = $scope.dtInstance.DataTable;
            $scope.dtOptions.ajax.data = function (d) {
                d.fromDate = fromdate;
                d.toDate = todate;
                d.branchId = branchId;
            };
            dtinstance.ajax.reload(function (json) { }, true);
        }
    }
    $scope.ExportDamageStockReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $("#ddlBranch").val();
        if (branchId != "") {
            $timeout(function () {
                var link = document.createElement('a');
                link.download = "DamageStockReport.xls";
                link.href = "/Report/ExportDamageStockReport?Fromdate=" + fromdate + "&Todate=" + todate + "&BranchId=" + branchId;
                link.click();
            }, 100);
        }
        else {
            Notification.error({ message: "Please select criteria.", delay: 5000 });
        }
    }
}]);

App.controller('VehicleHirePaymentReport_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "Excel", "$timeout", "BranchData", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, Excel, $timeout, BranchData, $timeout) {
    $scope.Branches = BranchData.GetData();
    $scope.vehicleHireCollection = [];

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: "/Report/GetVehicleHireData",
            type: 'GET',
            data: function (d) {
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
                d.branchId = $("#ddlBranch").val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)

        .withOption('destroy', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};
    var index = 1;
    countIndex = function () {
        return index++;
    }

    $scope.dtColumns = [
        DTColumnBuilder.newColumn(countIndex, "Sr No").notSortable(),
        DTColumnBuilder.newColumn('ManifestCreatedBranch').withTitle('Manifest Created Branch').notSortable(),
        DTColumnBuilder.newColumn('BookingDate').withTitle('Booking Date').notSortable(),
        DTColumnBuilder.newColumn('ManifestID').withTitle('Manifest Id').notSortable().notVisible(),
        DTColumnBuilder.newColumn('ManifestNo').withTitle('Manifest No').notSortable('Manifest No').renderWith(function (data, type, row, meta) {
            return '<a style="text-decoration:underline" data-ng-click="ViewManifestReport(' + row.ManifestID + ',\'V\')">' + row.ManifestNo + '</a>'
        }),
        DTColumnBuilder.newColumn('FromBranch').withTitle('FROM').notSortable(),
        DTColumnBuilder.newColumn('ToBranch').withTitle('TO').notSortable(),
        DTColumnBuilder.newColumn('TotalActualWeight').withTitle('Weight (IN KGS)').notSortable(),
        DTColumnBuilder.newColumn('TotalArticles').withTitle('No Of Article').notSortable(),
        DTColumnBuilder.newColumn('VendorName').withTitle('Vendor Name').notSortable(),
        DTColumnBuilder.newColumn('VehicleNo').withTitle('Vehicle No').notSortable(),
        DTColumnBuilder.newColumn('VehicleType').withTitle('Vehicle Type').notSortable(),
        DTColumnBuilder.newColumn('DriverMobileNo').withTitle('Driver Mobile No').notSortable(),
        DTColumnBuilder.newColumn('BookingAmount').withTitle('Total Freight').notSortable(),
        DTColumnBuilder.newColumn('AdvancePayment').withTitle('Advance Paid Amount').notSortable(),
        DTColumnBuilder.newColumn('BalanceAmount').withTitle('Balance Amount').notSortable(),
        DTColumnBuilder.newColumn('UnloadDate').withTitle('Unload Date').notSortable(),
        DTColumnBuilder.newColumn('BalancePaymentDate').withTitle('Balance Payment Date').notSortable(),
        DTColumnBuilder.newColumn('HiredBy').withTitle('Vehicle Hire Done By').notSortable(),
        DTColumnBuilder.newColumn('VehicleHireBranch').withTitle('Vehicle Hire Payment Branch').notSortable()

    ];

    $scope.InitializeDatePicker = function () {
        $('#txtFromDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#txtToDate').datepicker('setStartDate', startDate);
        });
        $('#txtToDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#txtFromDate').datepicker('setEndDate', FromEndDate);
        });
    }
    $scope.GetVehicleHirePaymentReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $("#ddlBranch").val();
        if (fromdate == "" || todate == "") {
            Notification.error({ message: "Please select all criteria.", delay: 5000 });
        }
        else {
            var dtinstance = $scope.dtInstance.DataTable;
            $scope.dtOptions.ajax.data = function (d) {
                d.fromDate = fromdate;
                d.toDate = todate;
                d.branchId = branchId;
            };
            dtinstance.ajax.reload(function (json) { }, true);
        }
    }
    $scope.ExportVehicleHireReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $("#ddlBranch").val();
        if (branchId != "") {
            $timeout(function () {
                var link = document.createElement('a');
                link.download = "VehicleHireReport.xls";
                link.href = "/Report/ExportVehicleHireReport?Fromdate=" + fromdate + "&Todate=" + todate + "&BranchId=" + branchId;
                link.click();
            }, 100);
        }
        else {
            Notification.error({ message: "Please select criteria.", delay: 5000 });
        }
    }
    $scope.ViewManifestReport = function (ManifestID, Type) {
        $scope.Manifest = {
            ManifestID: '',
            ManifestNo: '',
            FromBranch: '',
            ToBranch: '',
            ManifestDate: '',
            VendorName: '',
            VehicleNo: '',
            ManifestTime: '',
            ScheduledHours: '',
            DriverMobileNo: '',
            FreightAmount: '',
            FreightAdvance: '',
            FreightBalance: ''

        };
        ManifestReport = [];
        $http({
            method: 'GET',
            url: '/Report/ViewManifestReport',
            params: { 'ManifestID': ManifestID, 'Type': Type },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {

                for (i = 0; i < response.data.length; i++) {
                    ManifestReport.push(response.data[i]);
                }
                $scope.Manifest.ManifestID = ManifestID;
                $scope.Manifest.ManifestNo = ManifestReport[0].ManifestNo;
                $scope.Manifest.FromBranch = ManifestReport[0].FromBranch;
                $scope.Manifest.ToBranch = ManifestReport[0].ToBranch;
                $scope.Manifest.ManifestDate = ManifestReport[0].ManifestDate;
                $scope.Manifest.VendorName = ManifestReport[0].VendorName;
                $scope.Manifest.VehicleNo = ManifestReport[0].VehicleNo;
                $scope.Manifest.ManifestTime = ManifestReport[0].ManifestTime;
                $scope.Manifest.ScheduledHours = ManifestReport[0].ScheduledHours;
                $scope.Manifest.DriverMobileNo = ManifestReport[0].DriverMobileNo;
                $scope.Manifest.FreightAmount = ManifestReport[0].FreightAmount;
                $scope.Manifest.FreightAdvance = ManifestReport[0].FreightAdvance;
                $scope.Manifest.FreightBalance = ManifestReport[0].FreightBalance;
                $scope.GetConsignmentsForManifest(ManifestID, Type);
                $('#ManifestReportDetailModal').modal('show');
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.closeManifestReport = function () {
        $scope.clearManifestReport();
        $('#ManifestReportDetailModal').modal('hide');
    }
    $scope.GetConsignmentsForManifest = function (ManifestID, Type) {
        $scope.ConsignmentBookings = [];
        var bookings = [];
        $http({
            method: 'GET',
            url: '/Report/GetConsignmentsForManifestReport',
            params: { 'ManifestID': ManifestID, 'Type': Type },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {

                for (i = 0; i < response.data.length; i++) {
                    bookings.push(response.data[i]);
                }
                $scope.ConsignmentBookings = bookings;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.clearManifestReport = function () {
        $scope.Manifest = {
            ManifestNo: '',
            FromBranch: '',
            ToBranch: '',
            ManifestDate: '',
            VendorName: '',
            VehicleNo: '',
            ManifestTime: '',
            ScheduledHours: '',
            DriverMobileNo: '',
            FreightAmount: '',
            FreightAdvance: '',
            FreightBalance: ''

        };
        $scope.ConsignmentBookings = [];

    }
    $scope.exportToExcel = function (divId) {
        $scope.exportHref = Excel.tableToExcel(divId, 'ManifestReport');
        $timeout(function () {
            var link = document.createElement('a');
            link.download = "ManifestReport.xls";
            link.href = $scope.exportHref;
            link.click();
        }, 100);
    }
    $scope.printDiv = function (divId) {
        var printContents = document.getElementById(divId).innerHTML;
        var htmlToPrint = '' +
            '<style type="text/css">' +
            'table th, table td {' +
            'border:1px solid #000;' +
            'border-collapse: collapse;' +
            'padding:0.5em;' +
            '}' +
            '</style>';
        printContents += htmlToPrint;
        var popupWin = window.open();
        popupWin.document.open();
        popupWin.document.write('<html><body style="font-size:15px;font-family:arial" onload="window.print()">' + printContents + '</body></html>');
        popupWin.document.close();
    }
}]);

App.controller('ConsignmentBookingReport_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "BranchData", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, BranchData, $timeout) {
    $scope.Branches = BranchData.GetData();
    $scope.consignmentBookingCollection = [];

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: "/Report/GetConsignmentBookingData",
            type: 'GET',
            data: function (d) {
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
                d.branchId = $('#ddlBranch').val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)

        .withOption('destroy', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [

        DTColumnBuilder.newColumn('LrNo').withTitle('Lr No').notSortable(),
        DTColumnBuilder.newColumn('LrDate').withTitle('Lr Date').notSortable(),
        DTColumnBuilder.newColumn('LrTime').withTitle('Time Of Consignment Note').notSortable(),
        DTColumnBuilder.newColumn('PaymentTerm').withTitle('Payment Term').notSortable(),
        DTColumnBuilder.newColumn('BookingBranch').withTitle('BookingBranch').notSortable(),
        DTColumnBuilder.newColumn('PickupFrom').withTitle('Pickup From').notSortable(),
        DTColumnBuilder.newColumn('DeliveryBranch').withTitle('DeliveryBranch').notSortable(),
        DTColumnBuilder.newColumn('Destination').withTitle('Destination').notSortable(),
        DTColumnBuilder.newColumn('ConsignorName').withTitle('Consignor Name').notSortable(),
        DTColumnBuilder.newColumn('ConsignorGSTNo').withTitle('GST No Of Consignor').notSortable(),
        DTColumnBuilder.newColumn('ConsigneeName').withTitle('Consignee Name').notSortable(),
        DTColumnBuilder.newColumn('ConsigneeGSTNo').withTitle('GST No Of Consignee').notSortable(),
        DTColumnBuilder.newColumn('BillingParty').withTitle('Billing Party').notSortable(),
        DTColumnBuilder.newColumn('InvoiceNo').withTitle('Invoice No').notSortable(),
        DTColumnBuilder.newColumn('InvoiceDate').withTitle('Invoice Date').notSortable(),
        DTColumnBuilder.newColumn('GoodsDescription').withTitle('Goods Description').notSortable(),
        DTColumnBuilder.newColumn('InvoiceValue').withTitle('Invoice Value').notSortable(),
        DTColumnBuilder.newColumn('DeliveryType').withTitle('Delivery Type').notSortable(),
        DTColumnBuilder.newColumn('NoOfArticles').withTitle('No Of Articles').notSortable(),
        DTColumnBuilder.newColumn('TypeOfPacking').withTitle('Type Of Packing').notSortable(),
        DTColumnBuilder.newColumn('EWayBillNo').withTitle('E-Way Bill No').notSortable(),
        DTColumnBuilder.newColumn('ActualWeight').withTitle('ActualWeight(Kg.)').notSortable(),
        DTColumnBuilder.newColumn('ChargeWeight').withTitle('Charge Weight(Kg.)').notSortable(),
        DTColumnBuilder.newColumn('CFT').withTitle('CFT(L x W x H)').notSortable(),
        DTColumnBuilder.newColumn('FreightRate').withTitle('Freight Rate per bag/per kg').notSortable(),
        DTColumnBuilder.newColumn('BasicFreight').withTitle('Basic Freight').notSortable(),
        DTColumnBuilder.newColumn('DocketCharges').withTitle('Docket Charges').notSortable(),
        DTColumnBuilder.newColumn('FOV').withTitle('FOV').notSortable(),
        DTColumnBuilder.newColumn('PickupCharges').withTitle('Pickup Charges').notSortable(),
        DTColumnBuilder.newColumn('DeliveryCharges').withTitle('Delivery Charges').notSortable(),
        DTColumnBuilder.newColumn('HamaliCharges').withTitle('Hamali Charges').notSortable(),
        DTColumnBuilder.newColumn('CODCharges').withTitle('COD Charges').notSortable(),
        DTColumnBuilder.newColumn('OtherCharges').withTitle('Other Charges').notSortable(),
        DTColumnBuilder.newColumn('SubTotal').withTitle('Sub Total').notSortable(),
        DTColumnBuilder.newColumn('GrandTotal').withTitle('Grand Total').notSortable(),
        DTColumnBuilder.newColumn('VendorCode').withTitle('BA/Vendor Code').notSortable(),
        DTColumnBuilder.newColumn('PONo').withTitle('PO No/ASN No').notSortable(),
        DTColumnBuilder.newColumn('PartNo').withTitle('Part No').notSortable(),
        DTColumnBuilder.newColumn('VehicleNo').withTitle('Vehicle No').notSortable(),
        DTColumnBuilder.newColumn('InvoiceQuantity').withTitle('Invoice Quantity').notSortable(),
        DTColumnBuilder.newColumn('BookingType').withTitle('Booking Type').notSortable(),
        DTColumnBuilder.newColumn('Remarks').withTitle('Remarks').notSortable(),
        DTColumnBuilder.newColumn('ContactPersonName').withTitle('Contact Person').notSortable(),
        DTColumnBuilder.newColumn('MobileNumber').withTitle('Contact No').notSortable()
    ];

    $scope.InitializeDatePicker = function () {
        $('#txtFromDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#txtToDate').datepicker('setStartDate', startDate);
        });
        $('#txtToDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#txtFromDate').datepicker('setEndDate', FromEndDate);
        });
    }
    $scope.GetConsignmentBookingReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $('#ddlBranch').val();
        if (fromdate == "" || todate == "") {
            Notification.error({ message: "Please select all criteria.", delay: 5000 });
        }
        else {
            var dtinstance = $scope.dtInstance.DataTable;
            $scope.dtOptions.ajax.data = function (d) {
                d.fromDate = fromdate;
                d.toDate = todate;
                d.branchId = branchId;
            };
            dtinstance.ajax.reload(function (json) { }, true);
        }
    }
    $scope.ExportConsignmentBookingReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $("#ddlBranch").val();
        if (branchId != "") {
            $timeout(function () {
                var link = document.createElement('a');
                link.download = "ConsignmentBookingReport.xls";
                link.href = "/Report/ExportConsignmentBookingReport?Fromdate=" + fromdate + "&Todate=" + todate + "&BranchId=" + branchId;
                link.click();
            }, 100);
        }
        else {
            Notification.error({ message: "Please select criteria.", delay: 5000 });
        }
    }
}]);

App.factory('Excel', function ($window) {
    var uri = 'data:application/vnd.ms-excel;base64,',
        template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64 = function (s) { return $window.btoa(unescape(encodeURIComponent(s))); },
        format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) };
    return {
        tableToExcel: function (tableId, worksheetName) {
            console.log($(tableId).html())
            var table = $(tableId),
                ctx = { worksheet: worksheetName, table: table.html() },
                href = uri + base64(format(template, ctx));
            console.log(format(template, ctx))
            return href;
        }
    };
})
App.controller('ManifestReport_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "Excel", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, Excel, $timeout) {

    $scope.manifestCollection = [];
    $scope.Branches = [];
    $scope.GetBranches = function () {
        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    brnches.push(response.data[i]);
                }
                $scope.Branches = brnches;

            },
                function errorCallback(response) {
                    alert(response);
                });

    }
    $scope.fromDate = ""; $scope.toDate = "";
    $scope.reload = function () {
        $scope.dtRebind();
    };

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: "/Report/GetManifestData",
            type: 'GET',
            data: function (d) {
                d.from = $('#drpFrom').val();
                d.to = $('#drpTo').val();
                d.created = $('#txtCreated').val();
                d.type = $('#drpType').val();
                d.branchId = $('#ddlBranch').val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)

        .withOption('destroy', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtRebind = function () {
        $scope.dtInstance.ajax.reload();
    }
    $scope.dtColumns = [
        DTColumnBuilder.newColumn('ManifestID').withTitle('Manifest Id').notVisible().notSortable(),
        DTColumnBuilder.newColumn('ManifestNo').withTitle('Manifest No').notSortable(),
        DTColumnBuilder.newColumn('FromLoc').withTitle('From Location').notSortable(),
        DTColumnBuilder.newColumn('ToLoc').withTitle('To Location').notSortable(),
        DTColumnBuilder.newColumn('CreatedDate').withTitle('Date').notSortable(),
        DTColumnBuilder.newColumn('EstimatedDate').withTitle('Estimated Delivery Date').notSortable(),
        DTColumnBuilder.newColumn('typeofdata').withTitle('typeofdata').notVisible().notSortable(),
        DTColumnBuilder.newColumn('TotalArticles').withTitle('Total Articles').notSortable(),
        DTColumnBuilder.newColumn('TotalActualWeight').withTitle('Total Actual Weight').notSortable(),
        DTColumnBuilder.newColumn('Action').notSortable().renderWith(function (data, type, row, meta) {
            return '<a style="font-size:18px" data-ng-click="ViewManifestReport(' + row.ManifestID + ',\'' + row.typeofdata + '\')" class="pull-left font20 marleft15">View Report</a>'
        })
    ];
    $scope.ViewManifestReport = function (ManifestID, Type) {
        $scope.Manifest = {
            ManifestID: '',
            ManifestNo: '',
            FromBranch: '',
            ToBranch: '',
            ManifestDate: '',
            VendorName: '',
            VehicleNo: '',
            ManifestTime: '',
            ScheduledHours: '',
            DriverMobileNo: '',
            FreightAmount: '',
            FreightAdvance: '',
            FreightBalance: ''

        };
        ManifestReport = [];
        $http({
            method: 'GET',
            url: '/Report/ViewManifestReport',
            params: { 'ManifestID': ManifestID, 'Type': Type },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {

                for (i = 0; i < response.data.length; i++) {
                    ManifestReport.push(response.data[i]);
                }
                $scope.Manifest.ManifestID = ManifestID;
                $scope.Manifest.ManifestNo = ManifestReport[0].ManifestNo;
                $scope.Manifest.FromBranch = ManifestReport[0].FromBranch;
                $scope.Manifest.ToBranch = ManifestReport[0].ToBranch;
                $scope.Manifest.ManifestDate = ManifestReport[0].ManifestDate;
                $scope.Manifest.VendorName = ManifestReport[0].VendorName;
                $scope.Manifest.VehicleNo = ManifestReport[0].VehicleNo;
                $scope.Manifest.ManifestTime = ManifestReport[0].ManifestTime;
                $scope.Manifest.DeliveryBranch = ManifestReport[0].DeliveryBranch || '-';
                $scope.Manifest.DriverMobileNo = ManifestReport[0].DriverMobileNo;
                $scope.Manifest.FreightAmount = ManifestReport[0].FreightAmount;
                $scope.Manifest.FreightAdvance = ManifestReport[0].FreightAdvance;
                $scope.Manifest.FreightBalance = ManifestReport[0].FreightBalance;
                $scope.GetConsignmentsForManifest(ManifestID, Type);
                $('#ManifestReportDetailModal').modal('show');
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetManifestsForReport = function () {
        var fromdate = $('#drpFrom').val();
        var todate = $('#drpTo').val();
        var created = $('#txtCreated').val();
        var type = $('#drpType').val();
        var branchId = $('#ddlBranch').val();
        var dtinstance = $scope.dtInstance.DataTable;
        $scope.dtOptions.ajax.data = function (d) {
            d.from = fromdate;
            d.to = todate;
            d.created = created;
            d.type = type;
            d.branchId = branchId
        };
        dtinstance.ajax.reload(function (json) { }, true);
    }
    $scope.ResetManifestsForReport = function () {
        $('#drpFrom').val("");
        $('#drpTo').val("");
        $('#txtCreated').val("");
        $('#drpType').val("");
        if ($("#hdnBranchID").val() == "0") {
            $("#ddlBranch").val("0");
        }
        var fromdate = $('#drpFrom').val();
        var todate = $('#drpTo').val();
        var created = $('#txtCreated').val();
        var type = $('#drpType').val();
        var branchId = $('#ddlBranch').val();
        var dtinstance = $scope.dtInstance.DataTable;
        $scope.dtOptions.ajax.data = function (d) {
            d.from = fromdate;
            d.to = todate;
            d.created = created;
            d.type = type;
            d.branchId = branchId;
        };
        dtinstance.ajax.reload(function (json) { }, true);
    }
    $scope.GetConsignmentsForManifest = function (ManifestID, Type) {
        $scope.ConsignmentBookings = [];
        var bookings = [];
        $http({
            method: 'GET',
            url: '/Report/GetConsignmentsForManifestReport',
            params: { 'ManifestID': ManifestID, 'Type': Type },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {

                for (i = 0; i < response.data.length; i++) {
                    bookings.push(response.data[i]);
                }
                $scope.ConsignmentBookings = bookings;
                $scope.CountActualWeightAndArticle();
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.CountActualWeightAndArticle = function () {
        var totalArticle = 0;
        var totalActualWeight = 0;
        for (i = 0; i < $scope.ConsignmentBookings.length; i++) {
            totalArticle = totalArticle + parseInt($scope.ConsignmentBookings[i].Articles);
            totalActualWeight = totalActualWeight + parseFloat($scope.ConsignmentBookings[i].ActualWeight);
        }

        $scope.TotalArticles = totalArticle;
        $scope.TotalActualWeight = totalActualWeight;
    }
    $scope.closeManifestReport = function () {
        $scope.clearManifestReport();
        $('#ManifestReportDetailModal').modal('hide');
    }
    $scope.clearManifestReport = function () {
        $scope.Manifest = {
            ManifestNo: '',
            FromBranch: '',
            ToBranch: '',
            ManifestDate: '',
            VendorName: '',
            VehicleNo: '',
            ManifestTime: '',
            ScheduledHours: '',
            DriverMobileNo: '',
            FreightAmount: '',
            FreightAdvance: '',
            FreightBalance: ''

        };
        $scope.ConsignmentBookings = [];

    }

    $scope.InitializeDatePicker = function () {
        $scope.GetBranches();
        $('#txtCreated').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        });

    }
    $scope.exportToExcel = function (divId) {
        $scope.exportHref = Excel.tableToExcel(divId, 'ManifestReport');
        $timeout(function () {
            var link = document.createElement('a');
            link.download = "ManifestReport.xls";
            link.href = $scope.exportHref;
            link.click();
        }, 100);
    }
    $scope.printDiv = function (divId) {
        var printContents = document.getElementById(divId).innerHTML;
        var htmlToPrint = '' +
            '<style type="text/css">' +
            'table th, table td {' +
            'border:1px solid #000;' +
            'border-collapse: collapse;' +
            'padding:0.5em;' +
            '}' +
            '</style>';
        printContents += htmlToPrint;
        var popupWin = window.open();
        popupWin.document.open();
        popupWin.document.write('<html><body style="font-size:15px;font-family:arial" onload="window.print()">' + printContents + '</body></html>');
        popupWin.document.close();
    }
}]);


App.controller('DestinationBranchReport_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "BranchData", "Excel", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, BranchData, Excel, $timeout) {

    $scope.Branches = BranchData.GetData();
    $scope.deliveryBranchCollection = [];
    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Report/GetDeliveryBranchData",
            type: 'GET',
            data: function (d) {
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
                d.branchId = $("#ddlBranch").val();
            }
        })
        .withOption('aaSorting', [[1, "desc"]])
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('DestinationID').withTitle('DestinationID').notSortable(),
        DTColumnBuilder.newColumn('DeliveryBranch').withTitle('Delivery Branch').notSortable(),
        DTColumnBuilder.newColumn('TotalLR').withTitle('Total No. Of LR').notSortable(),
        DTColumnBuilder.newColumn('TotalArticles').withTitle('Total No. Of Articles').notSortable(),
        DTColumnBuilder.newColumn('TotalActualWeight').withTitle('Total Actual Weight').notSortable(),
        DTColumnBuilder.newColumn('TotalChargedWeight').withTitle('Total Charge Weight').notSortable(),
        DTColumnBuilder.newColumn('GrandTotal').withTitle('Grand Total').notSortable(),
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            return '<a data-ng-click="GetDetails(' + row.DestinationID + ',\'' + row.DeliveryBranch + '\',\'' + row.TotalLR + '\',\'' + row.TotalArticles + '\',\'' + row.TotalActualWeight + '\',\'' + row.TotalChargedWeight + '\',\'' + row.GrandTotal + '\')" class="pull-left font15 marleft15">Details</a>'
        })

    ];

    $scope.GetDetails = function (DestinationID, DeliveryBranch, TotalLR, TotalArticles, TotalActualWeight, TotalChargedWeight, GrandTotal) {
        $scope.DeliveryBranch = DeliveryBranch;
        $scope.TotalLR = TotalLR;
        $scope.TotalArticles = TotalArticles;
        $scope.TotalActualWeight = TotalActualWeight;
        $scope.TotalChargedWeight = TotalChargedWeight;
        $scope.GrandTotal = GrandTotal;
        $scope.ConsignmentBookings = [];
        var bookings = [];
        $http({
            method: 'GET',
            url: '/Report/GetConsignmentsForDestination',
            params: {
                'DestinationID': DestinationID, 'FromDate': $("#txtFromDate").val(), 'ToDate': $("#txtToDate").val()},
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {

                for (i = 0; i < response.data.length; i++) {
                    bookings.push(response.data[i]);
                }
                $scope.ConsignmentBookings = bookings;
                $("#DestinationDetailModal").modal("show");
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.InitializeDatePicker = function () {
        $('#txtFromDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#txtToDate').datepicker('setStartDate', startDate);
        });
        $('#txtToDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#txtFromDate').datepicker('setEndDate', FromEndDate);
        });
    }

    $scope.GetDeliveryBranchReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $("#ddlBranch").val();
        if (branchId == "") {
            Notification.error({ message: "Please select branch.", delay: 5000 });
        }
        else {
            var dtinstance = $scope.dtInstance.DataTable;
            $scope.dtOptions.ajax.data = function (d) {
                d.fromDate = fromdate;
                d.toDate = todate;
                d.branchId = branchId;
            };
            dtinstance.ajax.reload(function (json) { }, true);
        }
    }
    $scope.ResetDeliveryBranchReport = function () {
        $('#txtFromDate').val("");
        $('#txtToDate').val("");
        if ($("#hdnBranchID").val() == "0") {
            $("#ddlBranch").val("0");
        }
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $("#ddlBranch").val();
        var dtinstance = $scope.dtInstance.DataTable;
        $scope.dtOptions.ajax.data = function (d) {
            d.fromDate = fromdate;
            d.toDate = todate;
            d.branchId = branchId;
        };
        dtinstance.ajax.reload(function (json) { }, true);
    }
    $scope.exportToExcel = function (divId) {
        $scope.exportHref = Excel.tableToExcel(divId, 'DelieveryBranchReport');
        $timeout(function () {
            var link = document.createElement('a');
            link.download = "DelieveryBranchReport.xls";
            link.href = $scope.exportHref;
            link.click();
        }, 100);
    }
    $scope.printDiv = function (divId) {
        var printContents = document.getElementById(divId).innerHTML;
        var htmlToPrint = '' +
            '<style type="text/css">' +
            'table th, table td {' +
            'border:1px solid #000;' +
            'border-collapse: collapse;' +
            'padding:0.5em;' +
            '}' +
            '</style>';
        printContents += htmlToPrint;
        var popupWin = window.open();
        popupWin.document.open();
        popupWin.document.write('<html><body style="font-size:15px;font-family:arial" onload="window.print()">' + printContents + '</body></html>');
        popupWin.document.close();
    }
    $scope.closeManifestReport = function () {
        $scope.clearDelieveryReport();
        $('#DestinationDetailModal').modal('hide');
    }
    $scope.clearDelieveryReport = function () {

        $scope.ConsignmentBookings = [];

    }
}]);

App.controller('ToPayAndPaidPendingReport_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "BranchData", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, BranchData, $timeout) {
    $scope.Branches = BranchData.GetData();
    $scope.damageStockCollection = [];

    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Report/GetToPayAndPaidPendingData",
            type: 'GET',
            data: function (d) {
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
                d.branchId = $("#ddlBranch").val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('ConsignmentNo').withTitle('Lr No').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentDate').withTitle('Lr Date').notSortable(),
        DTColumnBuilder.newColumn('BillingType').withTitle('Billing Type').notSortable(),
        DTColumnBuilder.newColumn('FromBranch').withTitle('From').notSortable(),
        DTColumnBuilder.newColumn('ToBranch').withTitle('To').notSortable(),
        DTColumnBuilder.newColumn('ConsignorName').withTitle('Consignor').notSortable(),
        DTColumnBuilder.newColumn('ConsigneeName').withTitle('Consignee').notSortable(),
        DTColumnBuilder.newColumn('TotalArticles').withTitle('Articles').notSortable(),
        DTColumnBuilder.newColumn('ActualWeight').withTitle('ActualWeight(kg.)').notSortable(),
        DTColumnBuilder.newColumn('InvoiceValue').withTitle('Invoice Value').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentAmount').withTitle('Consignment Amount').notSortable(),
        DTColumnBuilder.newColumn('DeliveryStatus').withTitle('Delivery Status').notSortable(),
        DTColumnBuilder.newColumn('DeliveryDate').withTitle('Delivery Date').notSortable(),
        DTColumnBuilder.newColumn('DeliveredBy').withTitle('Delivered By').notSortable()

    ];

    $scope.InitializeDatePicker = function () {
        $('#txtFromDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#txtToDate').datepicker('setStartDate', startDate);
        });
        $('#txtToDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#txtFromDate').datepicker('setEndDate', FromEndDate);
        });
    }
    $scope.GetToPayAndPaidPendingReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $('#ddlBranch').val();
        if (fromdate == "" || todate == "") {
            Notification.error({ message: "Please select all criteria.", delay: 5000 });
        }
        else {
            var dtinstance = $scope.dtInstance.DataTable;
            $scope.dtOptions.ajax.data = function (d) {
                d.fromDate = fromdate;
                d.toDate = todate;
                d.branchId = branchId;
            };
            dtinstance.ajax.reload(function (json) { }, true);
        }
    }
    $scope.ExportToPayAndPaidPendingReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $("#ddlBranch").val();
        if (branchId != "") {
            $timeout(function () {
                var link = document.createElement('a');
                link.download = "ToPayAndPaidPendingReport.xls";
                link.href = "/Report/ExportToPayAndPaidPendingReport?Fromdate=" + fromdate + "&Todate=" + todate + "&BranchId=" + branchId;
                link.click();
            }, 100);
        }
        else {
            Notification.error({ message: "Please select criteria.", delay: 5000 });
        }
    }
    $scope.ResetToPayPendingReport = function () {
        $('#txtFromDate').val("");
        $('#txtToDate').val("");
        if ($("#hdnBranchID").val() == "0") {
            $("#ddlBranch").val("0");
        }
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $("#ddlBranch").val();
        var dtinstance = $scope.dtInstance.DataTable;
        $scope.dtOptions.ajax.data = function (d) {
            d.fromDate = fromdate;
            d.toDate = todate;
            d.branchId = branchId;
        };
        dtinstance.ajax.reload(function (json) { }, true);
    }
}]);

App.controller('ToPayAndPaidCollectionReport_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "BranchData", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, BranchData, $timeout) {
    $scope.Branches = BranchData.GetData();
    $scope.damageStockCollection = [];

    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Report/GetToPayAndPaidCollectionData",
            type: 'GET',
            data: function (d) {
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
                d.branchId = $("#ddlBranch").val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('ConsignmentNo').withTitle('Lr No').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentDate').withTitle('Lr Date').notSortable(),
        DTColumnBuilder.newColumn('BillingType').withTitle('Billing Type').notSortable(),
        DTColumnBuilder.newColumn('FromBranch').withTitle('From').notSortable(),
        DTColumnBuilder.newColumn('ToBranch').withTitle('To').notSortable(),
        DTColumnBuilder.newColumn('ConsignorName').withTitle('Consignor').notSortable(),
        DTColumnBuilder.newColumn('ConsigneeName').withTitle('Consignee').notSortable(),
        DTColumnBuilder.newColumn('TotalArticles').withTitle('Articles').notSortable(),
        DTColumnBuilder.newColumn('ActualWeight').withTitle('ActualWeight(kg.)').notSortable(),
        DTColumnBuilder.newColumn('InvoiceValue').withTitle('Invoice Value').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentAmount').withTitle('Consignment Amount').notSortable(),
        DTColumnBuilder.newColumn('ReceivedPaymentType').withTitle('Received Payment Type').notSortable(),
        DTColumnBuilder.newColumn('ReceivedAmount').withTitle('Received Amount').notSortable(),
        DTColumnBuilder.newColumn('ReceivedDate').withTitle('Received Date').notSortable(),
        DTColumnBuilder.newColumn('ChequeNo').withTitle('Cheque No').notSortable(),
        DTColumnBuilder.newColumn('ReceivedBy').withTitle('Received By').notSortable()

    ];

    $scope.InitializeDatePicker = function () {
        $('#txtFromDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#txtToDate').datepicker('setStartDate', startDate);
        });
        $('#txtToDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#txtFromDate').datepicker('setEndDate', FromEndDate);
        });
    }
    $scope.GetToPayAndPaidReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $('#ddlBranch').val();
        if (fromdate == "" || todate == "") {
            Notification.error({ message: "Please select all criteria.", delay: 5000 });
        }
        else {
            var dtinstance = $scope.dtInstance.DataTable;
            $scope.dtOptions.ajax.data = function (d) {
                d.fromDate = fromdate;
                d.toDate = todate;
                d.branchId = branchId;
            };
            dtinstance.ajax.reload(function (json) { }, true);
        }
    }
    $scope.ExportToPayAndPaidReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $("#ddlBranch").val();
        if (branchId != "") {
            $timeout(function () {
                var link = document.createElement('a');
                link.download = "ToPayAndPaidCollectionReport.xls";
                link.href = "/Report/ExportToPayAndPaidCollectionReport?Fromdate=" + fromdate + "&Todate=" + todate + "&BranchId=" + branchId;
                link.click();
            }, 100);
        }
        else {
            Notification.error({ message: "Please select criteria.", delay: 5000 });
        }
    }
    $scope.ResetToPayCollectionReport = function () {
        $('#txtFromDate').val("");
        $('#txtToDate').val("");
        if ($("#hdnBranchID").val() == "0") {
            $("#ddlBranch").val("0");
        }
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $("#ddlBranch").val();
        var dtinstance = $scope.dtInstance.DataTable;
        $scope.dtOptions.ajax.data = function (d) {
            d.fromDate = fromdate;
            d.toDate = todate;
            d.branchId = branchId;
        };
        dtinstance.ajax.reload(function (json) { }, true);
    }
}]);

App.controller('PaymentTypeWiseSalesReport_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "BranchData", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, BranchData, $timeout) {
    $scope.Branches = BranchData.GetData();

    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Report/GetPaymentTypeSalesData",
            type: 'GET',
            data: function (d) {
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
                d.branchId = $("#ddlBranch").val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('searching', false)
        .withOption('lengthChange', false)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('PaymentTypeID').withTitle('PaymentTypeID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('PaymentType').withTitle('Payment Type').notSortable(),
        DTColumnBuilder.newColumn('TotalLR').withTitle('Total LR').notSortable(),
        DTColumnBuilder.newColumn('TotalArticles').withTitle('Total No. Of Articles').notSortable(),
        DTColumnBuilder.newColumn('ActualWeight').withTitle('Actual Weight(kg.)').notSortable(),
        DTColumnBuilder.newColumn('ChargedWeight').withTitle('Charged Weight(kg.)').notSortable(),
        DTColumnBuilder.newColumn('InvoiceValue').withTitle('Invoice Value').notSortable(),
        DTColumnBuilder.newColumn('GrantTotal').withTitle('Grand Total').notSortable(),
        DTColumnBuilder.newColumn('Action').notSortable().renderWith(function (data, type, row, meta) {
            if (row.PaymentTypeID != 0) {
                return '<a style="font-size:18px" data-ng-click="GetDetailSalesOfPaymentType(' + row.PaymentTypeID + ',' + $("#ddlBranch").val() + ')" class="pull-left font20 marleft15">Details</a>'
            }
            else
                return ''
        })

    ];
    $scope.GetDetailSalesOfPaymentType = function (PaymentTypeID, branchId) {
        window.location.href = "/Report/PaymentTypeSalesDetail?PaymentTypeID=" + PaymentTypeID + "&BranchId=" + branchId + "&FromDate=" + $('#txtFromDate').val() + "&ToDate=" + $('#txtToDate').val();
    }
    $scope.InitializeDatePicker = function () {
        $('#txtFromDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#txtToDate').datepicker('setStartDate', startDate);
        });
        $('#txtToDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#txtFromDate').datepicker('setEndDate', FromEndDate);
        });
    }
    $scope.GetPaymentTypeWiseSalesReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $('#ddlBranch').val();
        if (branchId == "") {
            Notification.error({ message: "Please select branch.", delay: 5000 });
        }
        else {
            var dtinstance = $scope.dtInstance.DataTable;
            $scope.dtOptions.ajax.data = function (d) {
                d.fromDate = fromdate;
                d.toDate = todate;
                d.branchId = branchId;
            };
            dtinstance.ajax.reload(function (json) { }, true);
        }
    }

    $scope.ResetPaymentTypeWiseSalesReport = function () {
        $('#txtFromDate').val("");
        $('#txtToDate').val("");
        if ($("#hdnBranchID").val() == "0") {
            $("#ddlBranch").val("0");
        }
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $("#ddlBranch").val();
        var dtinstance = $scope.dtInstance.DataTable;
        $scope.dtOptions.ajax.data = function (d) {
            d.fromDate = fromdate;
            d.toDate = todate;
            d.branchId = branchId;
        };
        dtinstance.ajax.reload(function (json) { }, true);
    }
}]);

App.controller('PaymentTypeSalesDetail_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "BranchData", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, BranchData, $timeout) {

    $scope.dtOptions1 = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Report/GetPaymentTypeSalesDetails",
            type: 'GET',
            data: function (d) {
                d.fromDate = $('#hdnFromDate').val();
                d.toDate = $('#hdnToDate').val();
                d.branchId = $("#hdnBranchID").val();
                d.paymentTypeId = $("#hdnPaymentTypeID").val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('searching', false)
        .withOption('lengthChange', false)
        .withOption('info', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance1 = {};

    $scope.dtColumns1 = [
        DTColumnBuilder.newColumn('ConsignmentNo').withTitle('Lr No').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentDate').withTitle('Lr Date').notSortable(),
        DTColumnBuilder.newColumn('FromBranch').withTitle('From').notSortable(),
        DTColumnBuilder.newColumn('ToBranch').withTitle('To').notSortable(),
        DTColumnBuilder.newColumn('ConsignorName').withTitle('Consignor').notSortable(),
        DTColumnBuilder.newColumn('ConsigneeName').withTitle('Consignee').notSortable(),
        DTColumnBuilder.newColumn('TotalArticles').withTitle('No. Of Articles').notSortable(),
        DTColumnBuilder.newColumn('ActualWeight').withTitle('Actual Weight').notSortable(),
        DTColumnBuilder.newColumn('ChargedWeight').withTitle('Charged Weight').notSortable(),
        DTColumnBuilder.newColumn('InvoiceValue').withTitle('Invoice Value').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentAmount').withTitle('Consignment Amount').notSortable()
    ];
    $scope.ExportPaymentTypeSalesDetails = function () {
        var fromdate = $("#hdnFromDate").val();
        var todate = $("#hdnToDate").val();
        var branchId = $("#hdnBranchID").val();
        var paymentTypeId = $("#hdnPaymentTypeID").val();
        if (fromdate == undefined) fromdate = '';
        if (todate == undefined) todate = '';
        $timeout(function () {
            var link = document.createElement('a');
            link.download = "PaymentTypeSalesDetail.xls";
            link.href = "/Report/ExportPaymentTypeSalesDetail?Fromdate=" + fromdate + "&Todate=" + todate + "&BranchId=" + branchId + "&PaymentTypeId=" + paymentTypeId;
            link.click();
        }, 100);

    }
}]);

App.controller('GSTReport_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "BranchData", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, BranchData, $timeout) {
    $scope.Branches = BranchData.GetData();
  
    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Report/GetGSTData",
            type: 'GET',
            data: function (d) {
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
                d.branchId = $("#ddlBranch").val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('ConsignmentNo').withTitle('Lr No').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentDate').withTitle('Lr Date').notSortable(),
        DTColumnBuilder.newColumn('BillingType').withTitle('Billing Type').notSortable(),
        DTColumnBuilder.newColumn('FromBranch').withTitle('From').notSortable(),
        DTColumnBuilder.newColumn('ToBranch').withTitle('To').notSortable(),
        DTColumnBuilder.newColumn('ConsignorName').withTitle('Consignor').notSortable(),
        DTColumnBuilder.newColumn('ConsigneeName').withTitle('Consignee').notSortable(),
        DTColumnBuilder.newColumn('BillingParty').withTitle('Billing Party').notSortable(),
        DTColumnBuilder.newColumn('ConsignorGSTNo').withTitle('Consignor GST No').notSortable(),
        DTColumnBuilder.newColumn('PostalCode').withTitle('Billing Party GST No').notSortable(),
        DTColumnBuilder.newColumn('TotalArticles').withTitle('No Of Article').notSortable(),
        DTColumnBuilder.newColumn('ActualWeight').withTitle('Actual Weight(kg.)').notSortable(),
        DTColumnBuilder.newColumn('InvoiceValue').withTitle('Invoice Value').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentAmount').withTitle('Consignment Amount').notSortable(),
        DTColumnBuilder.newColumn('GSTAmount').withTitle('GST Amount').notSortable()
    ];

    $scope.InitializeDatePicker = function () {
        $('#txtFromDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#txtToDate').datepicker('setStartDate', startDate);
        });
        $('#txtToDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#txtFromDate').datepicker('setEndDate', FromEndDate);
        });
    }
    $scope.GetGSTReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $('#ddlBranch').val();
        if (branchId == "") {
            Notification.error({ message: "Please select all criteria.", delay: 5000 });
        }
        else {
            var dtinstance = $scope.dtInstance.DataTable;
            $scope.dtOptions.ajax.data = function (d) {
                d.fromDate = fromdate;
                d.toDate = todate;
                d.branchId = branchId;
            };
            dtinstance.ajax.reload(function (json) { }, true);
        }
    }
    $scope.ExportGSTReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $("#ddlBranch").val();
        if (branchId != "") {
            $timeout(function () {
                var link = document.createElement('a');
                link.download = "GSTReport.xls";
                link.href = "/Report/ExportGSTReport?Fromdate=" + fromdate + "&Todate=" + todate + "&BranchId=" + branchId;
                link.click();
            }, 100);
        }
        else {
            Notification.error({ message: "Please select branch.", delay: 5000 });
        }
    }
}]);

App.controller('VendorSalesReport_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, $timeout) {
  
    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Report/GetPaymentTypeVendorSalesData",
            type: 'GET',
            data: function (d) {
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
                d.vendorId = $("#ddlVendors").val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('searching', false)
        .withOption('lengthChange', false)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('PaymentTypeID').withTitle('PaymentTypeID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('PaymentType').withTitle('Payment Type').notSortable(),
        DTColumnBuilder.newColumn('TotalLR').withTitle('Total LR').notSortable(),
        DTColumnBuilder.newColumn('TotalArticles').withTitle('Total No. Of Articles').notSortable(),
        DTColumnBuilder.newColumn('ActualWeight').withTitle('Actual Weight(kg.)').notSortable(),
        DTColumnBuilder.newColumn('ChargedWeight').withTitle('Charged Weight(kg.)').notSortable(),
        DTColumnBuilder.newColumn('InvoiceValue').withTitle('Invoice Value').notSortable(),
        DTColumnBuilder.newColumn('GrantTotal').withTitle('Grand Total').notSortable(),
        DTColumnBuilder.newColumn('Action').notSortable().renderWith(function (data, type, row, meta) {
            if (row.PaymentTypeID != 0) {
                return '<a style="font-size:18px" data-ng-click="GetDetailVendorSalesOfPaymentType(' + row.PaymentTypeID + ',' + $("#ddlVendors").val() + ')" class="pull-left font20 marleft15">Details</a>'
            }
            else
                return ''
        })

    ];
    $scope.GetVendors = function () {

        var vendor = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVendors',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    vendor.push(response.data[i]);
                }
                $scope.Vendors = vendor;
              
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetVendors();
    $scope.GetDetailVendorSalesOfPaymentType = function (PaymentTypeID, vendorId) {
        window.location.href = "/Report/VendorSalesDetail?PaymentTypeID=" + PaymentTypeID + "&VendorId=" + vendorId + "&FromDate=" + $('#txtFromDate').val() + "&ToDate=" + $('#txtToDate').val();
    }
    $scope.InitializeDatePicker = function () {
        $('#txtFromDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#txtToDate').datepicker('setStartDate', startDate);
        });
        $('#txtToDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#txtFromDate').datepicker('setEndDate', FromEndDate);
        });
    }
    $scope.GetPaymentTypeWiseVendorSalesReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var vendorId = $('#ddlVendors').val();
        if (vendorId == "") {
            Notification.error({ message: "Please select vendor.", delay: 5000 });
        }
        else {
            var dtinstance = $scope.dtInstance.DataTable;
            $scope.dtOptions.ajax.data = function (d) {
                d.fromDate = fromdate;
                d.toDate = todate;
                d.vendorId = vendorId;
            };
            dtinstance.ajax.reload(function (json) { }, true);
        }
    }

    $scope.ResetPaymentTypeWiseVendorSalesReport = function () {
        $('#txtFromDate').val("");
        $('#txtToDate').val("");
        $("#ddlVendors").val("");
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var vendorId = $("#ddlVendors").val();
        var dtinstance = $scope.dtInstance.DataTable;
        $scope.dtOptions.ajax.data = function (d) {
            d.fromDate = fromdate;
            d.toDate = todate;
            d.vendorId = vendorId;
        };
        dtinstance.ajax.reload(function (json) { }, true);
    }
}]);

App.controller('VendorSalesDetail_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "BranchData", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, BranchData, $timeout) {

    $scope.dtOptions1 = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Report/GetVendorSalesDetails",
            type: 'GET',
            data: function (d) {
                d.fromDate = $('#hdnFromDate').val();
                d.toDate = $('#hdnToDate').val();
                d.vendorId = $("#hdnVendorID").val();
                d.paymentTypeId = $("#hdnPaymentTypeID").val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('searching', false)
        .withOption('lengthChange', false)
        .withOption('info', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance1 = {};

    $scope.dtColumns1 = [
        DTColumnBuilder.newColumn('ConsignmentNo').withTitle('Lr No').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentDate').withTitle('Lr Date').notSortable(),
        DTColumnBuilder.newColumn('FromBranch').withTitle('From').notSortable(),
        DTColumnBuilder.newColumn('ToBranch').withTitle('To').notSortable(),
        DTColumnBuilder.newColumn('ConsignorName').withTitle('Consignor').notSortable(),
        DTColumnBuilder.newColumn('ConsigneeName').withTitle('Consignee').notSortable(),
        DTColumnBuilder.newColumn('TotalArticles').withTitle('No. Of Articles').notSortable(),
        DTColumnBuilder.newColumn('ActualWeight').withTitle('Actual Weight').notSortable(),
        DTColumnBuilder.newColumn('ChargedWeight').withTitle('Charged Weight').notSortable(),
        DTColumnBuilder.newColumn('InvoiceValue').withTitle('Invoice Value').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentAmount').withTitle('Consignment Amount').notSortable()
    ];
    $scope.ExportPaymentTypeSalesDetails = function () {
        var fromdate = $("#hdnFromDate").val();
        var todate = $("#hdnToDate").val();
        var vendorId = $("#hdnVendorID").val();
        var paymentTypeId = $("#hdnPaymentTypeID").val();
        if (fromdate == undefined) fromdate = '';
        if (todate == undefined) todate = '';
        if (vendorId == "") {
            Notification.error({ message: "Please select vendor.", delay: 5000 });
        }
        else {
            $timeout(function () {
                var link = document.createElement('a');
                link.download = "VendorSalesDetail.xls";
                link.href = "/Report/ExportPaymentTypeVendorSalesDetail?Fromdate=" + fromdate + "&Todate=" + todate + "&VendorId=" + vendorId + "&PaymentTypeId=" + paymentTypeId;
                link.click();
            }, 100);
        }
    }
}]);
App.controller('MHELVendorSalesReport_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, $timeout) {

    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Report/GetPaymentTypeMHELVendorSalesData",
            type: 'GET',
            data: function (d) {
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
                d.vendorId = $("#ddlVendors").val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('searching', false)
        .withOption('lengthChange', false)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('PaymentTypeID').withTitle('PaymentTypeID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('PaymentType').withTitle('Payment Type').notSortable(),
        DTColumnBuilder.newColumn('TotalLR').withTitle('Total LR').notSortable(),
        DTColumnBuilder.newColumn('TotalArticles').withTitle('Total No. Of Articles').notSortable(),
        DTColumnBuilder.newColumn('ActualWeight').withTitle('Actual Weight(kg.)').notSortable(),
        DTColumnBuilder.newColumn('ChargedWeight').withTitle('Charged Weight(kg.)').notSortable(),
        DTColumnBuilder.newColumn('InvoiceValue').withTitle('Invoice Value').notSortable(),
        DTColumnBuilder.newColumn('GrantTotal').withTitle('Grand Total').notSortable(),
        DTColumnBuilder.newColumn('Action').notSortable().renderWith(function (data, type, row, meta) {
            if (row.PaymentTypeID != 0) {
                return '<a style="font-size:18px" data-ng-click="GetDetailVendorSalesOfPaymentType(' + row.PaymentTypeID + ',' + $("#ddlVendors").val() + ')" class="pull-left font20 marleft15">Details</a>'
            }
            else
                return ''
        })

    ];
    $scope.GetVendors = function () {

        var vendor = [];
        $http({
            method: 'GET',
            url: '/Operations/GetMHELVendors',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    vendor.push(response.data[i]);
                }
                $scope.Vendors = vendor;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetVendors();
    $scope.GetDetailVendorSalesOfPaymentType = function (PaymentTypeID, vendorId) {
        window.location.href = "/Report/MHELVendorSalesDetail?PaymentTypeID=" + PaymentTypeID + "&VendorId=" + vendorId + "&FromDate=" + $('#txtFromDate').val() + "&ToDate=" + $('#txtToDate').val();
    }
    $scope.InitializeDatePicker = function () {
        $('#txtFromDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#txtToDate').datepicker('setStartDate', startDate);
        });
        $('#txtToDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#txtFromDate').datepicker('setEndDate', FromEndDate);
        });
    }
    $scope.GetPaymentTypeWiseVendorSalesReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var vendorId = $('#ddlVendors').val();
        if (vendorId == "") {
            Notification.error({ message: "Please select vendor.", delay: 5000 });
        }
        else {
            var dtinstance = $scope.dtInstance.DataTable;
            $scope.dtOptions.ajax.data = function (d) {
                d.fromDate = fromdate;
                d.toDate = todate;
                d.vendorId = vendorId;
            };
            dtinstance.ajax.reload(function (json) { }, true);
        }
    }

    $scope.ResetPaymentTypeWiseVendorSalesReport = function () {
        $('#txtFromDate').val("");
        $('#txtToDate').val("");
        $("#ddlVendors").val("");
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var vendorId = $("#ddlVendors").val();
        var dtinstance = $scope.dtInstance.DataTable;
        $scope.dtOptions.ajax.data = function (d) {
            d.fromDate = fromdate;
            d.toDate = todate;
            d.vendorId = vendorId;
        };
        dtinstance.ajax.reload(function (json) { }, true);
    }
}]);

App.controller('MHELVendorSalesDetail_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "BranchData", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, BranchData, $timeout) {

    $scope.dtOptions1 = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Report/GetMHELVendorSalesDetails",
            type: 'GET',
            data: function (d) {
                d.fromDate = $('#hdnFromDate').val();
                d.toDate = $('#hdnToDate').val();
                d.vendorId = $("#hdnVendorID").val();
                d.paymentTypeId = $("#hdnPaymentTypeID").val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('searching', false)
        .withOption('lengthChange', false)
        .withOption('info', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance1 = {};

    $scope.dtColumns1 = [
        DTColumnBuilder.newColumn('ConsignmentNo').withTitle('Lr No').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentDate').withTitle('Lr Date').notSortable(),
        DTColumnBuilder.newColumn('FromBranch').withTitle('From').notSortable(),
        DTColumnBuilder.newColumn('ToBranch').withTitle('To').notSortable(),
        DTColumnBuilder.newColumn('ConsignorName').withTitle('Consignor').notSortable(),
        DTColumnBuilder.newColumn('ConsigneeName').withTitle('Consignee').notSortable(),
        DTColumnBuilder.newColumn('TotalArticles').withTitle('No. Of Articles').notSortable(),
        DTColumnBuilder.newColumn('ActualWeight').withTitle('Actual Weight').notSortable(),
        DTColumnBuilder.newColumn('ChargedWeight').withTitle('Charged Weight').notSortable(),
        DTColumnBuilder.newColumn('InvoiceValue').withTitle('Invoice Value').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentAmount').withTitle('Consignment Amount').notSortable()
    ];
    $scope.ExportPaymentTypeSalesDetails = function () {
        var fromdate = $("#hdnFromDate").val();
        var todate = $("#hdnToDate").val();
        var vendorId = $("#hdnVendorID").val();
        var paymentTypeId = $("#hdnPaymentTypeID").val();
        if (fromdate == undefined) fromdate = '';
        if (todate == undefined) todate = '';
        if (vendorId == "") {
            Notification.error({ message: "Please select vendor.", delay: 5000 });
        }
        else {
            $timeout(function () {
                var link = document.createElement('a');
                link.download = "MHELVendorSalesDetail.xls";
                link.href = "/Report/ExportPaymentTypeMHELVendorSalesDetail?Fromdate=" + fromdate + "&Todate=" + todate + "&VendorId=" + vendorId + "&PaymentTypeId=" + paymentTypeId;
                link.click();
            }, 100);
        }
    }
}]);

App.controller('BilledConsignmentBookingReport_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "BranchData", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, BranchData, $timeout) {
    $scope.Branches = BranchData.GetData();
    $scope.consignmentBookingCollection = [];

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: "/Report/GetBilledConsignmentBookingData",
            type: 'GET',
            data: function (d) {
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
                d.branchId = $('#ddlBranch').val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)

        .withOption('destroy', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [

        DTColumnBuilder.newColumn('LrNo').withTitle('Lr No').notSortable(),
        DTColumnBuilder.newColumn('LrDate').withTitle('Lr Date').notSortable(),
        DTColumnBuilder.newColumn('LrTime').withTitle('Time Of Consignment Note').notSortable(),
        DTColumnBuilder.newColumn('PaymentTerm').withTitle('Payment Term').notSortable(),
        DTColumnBuilder.newColumn('BookingBranch').withTitle('BookingBranch').notSortable(),
        DTColumnBuilder.newColumn('PickupFrom').withTitle('Pickup From').notSortable(),
        DTColumnBuilder.newColumn('DeliveryBranch').withTitle('DeliveryBranch').notSortable(),
        DTColumnBuilder.newColumn('Destination').withTitle('Destination').notSortable(),
        DTColumnBuilder.newColumn('ConsignorName').withTitle('Consignor Name').notSortable(),
        DTColumnBuilder.newColumn('ConsignorGSTNo').withTitle('GST No Of Consignor').notSortable(),
        DTColumnBuilder.newColumn('ConsigneeName').withTitle('Consignee Name').notSortable(),
        DTColumnBuilder.newColumn('ConsigneeGSTNo').withTitle('GST No Of Consignee').notSortable(),
        DTColumnBuilder.newColumn('BillingParty').withTitle('Billing Party').notSortable(),
        DTColumnBuilder.newColumn('InvoiceNo').withTitle('Invoice No').notSortable(),
        DTColumnBuilder.newColumn('InvoiceDate').withTitle('Invoice Date').notSortable(),
        DTColumnBuilder.newColumn('GoodsDescription').withTitle('Goods Description').notSortable(),
        DTColumnBuilder.newColumn('InvoiceValue').withTitle('Invoice Value').notSortable(),
        DTColumnBuilder.newColumn('DeliveryType').withTitle('Delivery Type').notSortable(),
        DTColumnBuilder.newColumn('NoOfArticles').withTitle('No Of Articles').notSortable(),
        DTColumnBuilder.newColumn('TypeOfPacking').withTitle('Type Of Packing').notSortable(),
        DTColumnBuilder.newColumn('EWayBillNo').withTitle('E-Way Bill No').notSortable(),
        DTColumnBuilder.newColumn('ActualWeight').withTitle('ActualWeight(Kg.)').notSortable(),
        DTColumnBuilder.newColumn('ChargeWeight').withTitle('Charge Weight(Kg.)').notSortable(),
        DTColumnBuilder.newColumn('CFT').withTitle('CFT(L x W x H)').notSortable(),
        DTColumnBuilder.newColumn('FreightRate').withTitle('Freight Rate per bag/per kg').notSortable(),
        DTColumnBuilder.newColumn('BasicFreight').withTitle('Basic Freight').notSortable(),
        DTColumnBuilder.newColumn('DocketCharges').withTitle('Docket Charges').notSortable(),
        DTColumnBuilder.newColumn('FOV').withTitle('FOV').notSortable(),
        DTColumnBuilder.newColumn('PickupCharges').withTitle('Pickup Charges').notSortable(),
        DTColumnBuilder.newColumn('DeliveryCharges').withTitle('Delivery Charges').notSortable(),
        DTColumnBuilder.newColumn('HamaliCharges').withTitle('Hamali Charges').notSortable(),
        DTColumnBuilder.newColumn('CODCharges').withTitle('COD Charges').notSortable(),
        DTColumnBuilder.newColumn('OtherCharges').withTitle('Other Charges').notSortable(),
        DTColumnBuilder.newColumn('SubTotal').withTitle('Sub Total').notSortable(),
        DTColumnBuilder.newColumn('GrandTotal').withTitle('Grand Total').notSortable(),
        DTColumnBuilder.newColumn('VendorCode').withTitle('BA/Vendor Code').notSortable(),
        DTColumnBuilder.newColumn('PONo').withTitle('PO No/ASN No').notSortable(),
        DTColumnBuilder.newColumn('PartNo').withTitle('Part No').notSortable(),
        DTColumnBuilder.newColumn('VehicleNo').withTitle('Vehicle No').notSortable(),
        DTColumnBuilder.newColumn('InvoiceQuantity').withTitle('Invoice Quantity').notSortable(),
        DTColumnBuilder.newColumn('BookingType').withTitle('Booking Type').notSortable(),
        DTColumnBuilder.newColumn('Remarks').withTitle('Remarks').notSortable(),
        DTColumnBuilder.newColumn('ContactPersonName').withTitle('Contact Person').notSortable(),
        DTColumnBuilder.newColumn('MobileNumber').withTitle('Contact No').notSortable()
    ];

    $scope.InitializeDatePicker = function () {
        $('#txtFromDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#txtToDate').datepicker('setStartDate', startDate);
        });
        $('#txtToDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#txtFromDate').datepicker('setEndDate', FromEndDate);
        });
    }
    $scope.GetConsignmentBookingReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $('#ddlBranch').val();
        if (fromdate == "" || todate == "") {
            Notification.error({ message: "Please select all criteria.", delay: 5000 });
        }
        else {
            var dtinstance = $scope.dtInstance.DataTable;
            $scope.dtOptions.ajax.data = function (d) {
                d.fromDate = fromdate;
                d.toDate = todate;
                d.branchId = branchId;
            };
            dtinstance.ajax.reload(function (json) { }, true);
        }
    }
    $scope.ExportConsignmentBookingReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $("#ddlBranch").val();
        if (branchId != "") {
            $timeout(function () {
                var link = document.createElement('a');
                link.download = "BilledConsignmentReport.xls";
                link.href = "/Report/ExportBilledConsignmentBookingReport?Fromdate=" + fromdate + "&Todate=" + todate + "&BranchId=" + branchId;
                link.click();
            }, 100);
        }
        else {
            Notification.error({ message: "Please select criteria.", delay: 5000 });
        }
    }
}]);

App.controller('UnbilledConsignmentBookingReport_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "BranchData", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, BranchData, $timeout) {
    $scope.Branches = BranchData.GetData();
    $scope.consignmentBookingCollection = [];

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: "/Report/GetUnBilledConsignmentBookingData",
            type: 'GET',
            data: function (d) {
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
                d.branchId = $('#ddlBranch').val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)

        .withOption('destroy', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [

        DTColumnBuilder.newColumn('LrNo').withTitle('Lr No').notSortable(),
        DTColumnBuilder.newColumn('LrDate').withTitle('Lr Date').notSortable(),
        DTColumnBuilder.newColumn('LrTime').withTitle('Time Of Consignment Note').notSortable(),
        DTColumnBuilder.newColumn('PaymentTerm').withTitle('Payment Term').notSortable(),
        DTColumnBuilder.newColumn('BookingBranch').withTitle('BookingBranch').notSortable(),
        DTColumnBuilder.newColumn('PickupFrom').withTitle('Pickup From').notSortable(),
        DTColumnBuilder.newColumn('DeliveryBranch').withTitle('DeliveryBranch').notSortable(),
        DTColumnBuilder.newColumn('Destination').withTitle('Destination').notSortable(),
        DTColumnBuilder.newColumn('ConsignorName').withTitle('Consignor Name').notSortable(),
        DTColumnBuilder.newColumn('ConsignorGSTNo').withTitle('GST No Of Consignor').notSortable(),
        DTColumnBuilder.newColumn('ConsigneeName').withTitle('Consignee Name').notSortable(),
        DTColumnBuilder.newColumn('ConsigneeGSTNo').withTitle('GST No Of Consignee').notSortable(),
        DTColumnBuilder.newColumn('BillingParty').withTitle('Billing Party').notSortable(),
        DTColumnBuilder.newColumn('InvoiceNo').withTitle('Invoice No').notSortable(),
        DTColumnBuilder.newColumn('InvoiceDate').withTitle('Invoice Date').notSortable(),
        DTColumnBuilder.newColumn('GoodsDescription').withTitle('Goods Description').notSortable(),
        DTColumnBuilder.newColumn('InvoiceValue').withTitle('Invoice Value').notSortable(),
        DTColumnBuilder.newColumn('DeliveryType').withTitle('Delivery Type').notSortable(),
        DTColumnBuilder.newColumn('NoOfArticles').withTitle('No Of Articles').notSortable(),
        DTColumnBuilder.newColumn('TypeOfPacking').withTitle('Type Of Packing').notSortable(),
        DTColumnBuilder.newColumn('EWayBillNo').withTitle('E-Way Bill No').notSortable(),
        DTColumnBuilder.newColumn('ActualWeight').withTitle('ActualWeight(Kg.)').notSortable(),
        DTColumnBuilder.newColumn('ChargeWeight').withTitle('Charge Weight(Kg.)').notSortable(),
        DTColumnBuilder.newColumn('CFT').withTitle('CFT(L x W x H)').notSortable(),
        DTColumnBuilder.newColumn('FreightRate').withTitle('Freight Rate per bag/per kg').notSortable(),
        DTColumnBuilder.newColumn('BasicFreight').withTitle('Basic Freight').notSortable(),
        DTColumnBuilder.newColumn('DocketCharges').withTitle('Docket Charges').notSortable(),
        DTColumnBuilder.newColumn('FOV').withTitle('FOV').notSortable(),
        DTColumnBuilder.newColumn('PickupCharges').withTitle('Pickup Charges').notSortable(),
        DTColumnBuilder.newColumn('DeliveryCharges').withTitle('Delivery Charges').notSortable(),
        DTColumnBuilder.newColumn('HamaliCharges').withTitle('Hamali Charges').notSortable(),
        DTColumnBuilder.newColumn('CODCharges').withTitle('COD Charges').notSortable(),
        DTColumnBuilder.newColumn('OtherCharges').withTitle('Other Charges').notSortable(),
        DTColumnBuilder.newColumn('SubTotal').withTitle('Sub Total').notSortable(),
        DTColumnBuilder.newColumn('GrandTotal').withTitle('Grand Total').notSortable(),
        DTColumnBuilder.newColumn('VendorCode').withTitle('BA/Vendor Code').notSortable(),
        DTColumnBuilder.newColumn('PONo').withTitle('PO No/ASN No').notSortable(),
        DTColumnBuilder.newColumn('PartNo').withTitle('Part No').notSortable(),
        DTColumnBuilder.newColumn('VehicleNo').withTitle('Vehicle No').notSortable(),
        DTColumnBuilder.newColumn('InvoiceQuantity').withTitle('Invoice Quantity').notSortable(),
        DTColumnBuilder.newColumn('BookingType').withTitle('Booking Type').notSortable(),
        DTColumnBuilder.newColumn('Remarks').withTitle('Remarks').notSortable(),
        DTColumnBuilder.newColumn('ContactPersonName').withTitle('Contact Person').notSortable(),
        DTColumnBuilder.newColumn('MobileNumber').withTitle('Contact No').notSortable()
    ];

    $scope.InitializeDatePicker = function () {
        $('#txtFromDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#txtToDate').datepicker('setStartDate', startDate);
        });
        $('#txtToDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#txtFromDate').datepicker('setEndDate', FromEndDate);
        });
    }
    $scope.GetConsignmentBookingReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $('#ddlBranch').val();
        if (fromdate == "" || todate == "") {
            Notification.error({ message: "Please select all criteria.", delay: 5000 });
        }
        else {
            var dtinstance = $scope.dtInstance.DataTable;
            $scope.dtOptions.ajax.data = function (d) {
                d.fromDate = fromdate;
                d.toDate = todate;
                d.branchId = branchId;
            };
            dtinstance.ajax.reload(function (json) { }, true);
        }
    }
    $scope.ExportConsignmentBookingReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $("#ddlBranch").val();
        if (branchId != "") {
            $timeout(function () {
                var link = document.createElement('a');
                link.download = "UnbilledConsignmentReport.xls";
                link.href = "/Report/ExportUnBilledConsignmentBookingReport?Fromdate=" + fromdate + "&Todate=" + todate + "&BranchId=" + branchId;
                link.click();
            }, 100);
        }
        else {
            Notification.error({ message: "Please select criteria.", delay: 5000 });
        }
    }
}]);

App.controller('CancelledConsignmentBookingReport_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "BranchData", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, BranchData, $timeout) {
    $scope.Branches = BranchData.GetData();
    $scope.consignmentBookingCollection = [];

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: "/Report/GetCancelledConsignmentBookingData",
            type: 'GET',
            data: function (d) {
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
                d.branchId = $('#ddlBranch').val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)

        .withOption('destroy', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [

        DTColumnBuilder.newColumn('LrNo').withTitle('Lr No').notSortable(),
        DTColumnBuilder.newColumn('LrDate').withTitle('Lr Date').notSortable(),
        DTColumnBuilder.newColumn('LrTime').withTitle('Time Of Consignment Note').notSortable(),
        DTColumnBuilder.newColumn('PaymentTerm').withTitle('Payment Term').notSortable(),
        DTColumnBuilder.newColumn('BookingBranch').withTitle('BookingBranch').notSortable(),
        DTColumnBuilder.newColumn('PickupFrom').withTitle('Pickup From').notSortable(),
        DTColumnBuilder.newColumn('DeliveryBranch').withTitle('DeliveryBranch').notSortable(),
        DTColumnBuilder.newColumn('Destination').withTitle('Destination').notSortable(),
        DTColumnBuilder.newColumn('ConsignorName').withTitle('Consignor Name').notSortable(),
        DTColumnBuilder.newColumn('ConsignorGSTNo').withTitle('GST No Of Consignor').notSortable(),
        DTColumnBuilder.newColumn('ConsigneeName').withTitle('Consignee Name').notSortable(),
        DTColumnBuilder.newColumn('ConsigneeGSTNo').withTitle('GST No Of Consignee').notSortable(),
        DTColumnBuilder.newColumn('BillingParty').withTitle('Billing Party').notSortable(),
        DTColumnBuilder.newColumn('InvoiceNo').withTitle('Invoice No').notSortable(),
        DTColumnBuilder.newColumn('InvoiceDate').withTitle('Invoice Date').notSortable(),
        DTColumnBuilder.newColumn('GoodsDescription').withTitle('Goods Description').notSortable(),
        DTColumnBuilder.newColumn('InvoiceValue').withTitle('Invoice Value').notSortable(),
        DTColumnBuilder.newColumn('DeliveryType').withTitle('Delivery Type').notSortable(),
        DTColumnBuilder.newColumn('NoOfArticles').withTitle('No Of Articles').notSortable(),
        DTColumnBuilder.newColumn('TypeOfPacking').withTitle('Type Of Packing').notSortable(),
        DTColumnBuilder.newColumn('EWayBillNo').withTitle('E-Way Bill No').notSortable(),
        DTColumnBuilder.newColumn('ActualWeight').withTitle('ActualWeight(Kg.)').notSortable(),
        DTColumnBuilder.newColumn('ChargeWeight').withTitle('Charge Weight(Kg.)').notSortable(),
        DTColumnBuilder.newColumn('CFT').withTitle('CFT(L x W x H)').notSortable(),
        DTColumnBuilder.newColumn('FreightRate').withTitle('Freight Rate per bag/per kg').notSortable(),
        DTColumnBuilder.newColumn('BasicFreight').withTitle('Basic Freight').notSortable(),
        DTColumnBuilder.newColumn('DocketCharges').withTitle('Docket Charges').notSortable(),
        DTColumnBuilder.newColumn('FOV').withTitle('FOV').notSortable(),
        DTColumnBuilder.newColumn('PickupCharges').withTitle('Pickup Charges').notSortable(),
        DTColumnBuilder.newColumn('DeliveryCharges').withTitle('Delivery Charges').notSortable(),
        DTColumnBuilder.newColumn('HamaliCharges').withTitle('Hamali Charges').notSortable(),
        DTColumnBuilder.newColumn('CODCharges').withTitle('COD Charges').notSortable(),
        DTColumnBuilder.newColumn('OtherCharges').withTitle('Other Charges').notSortable(),
        DTColumnBuilder.newColumn('SubTotal').withTitle('Sub Total').notSortable(),
        DTColumnBuilder.newColumn('GrandTotal').withTitle('Grand Total').notSortable(),
        DTColumnBuilder.newColumn('VendorCode').withTitle('BA/Vendor Code').notSortable(),
        DTColumnBuilder.newColumn('PONo').withTitle('PO No/ASN No').notSortable(),
        DTColumnBuilder.newColumn('PartNo').withTitle('Part No').notSortable(),
        DTColumnBuilder.newColumn('VehicleNo').withTitle('Vehicle No').notSortable(),
        DTColumnBuilder.newColumn('InvoiceQuantity').withTitle('Invoice Quantity').notSortable(),
        DTColumnBuilder.newColumn('BookingType').withTitle('Booking Type').notSortable(),
        DTColumnBuilder.newColumn('Remarks').withTitle('Remarks').notSortable(),
        DTColumnBuilder.newColumn('ContactPersonName').withTitle('Contact Person').notSortable(),
        DTColumnBuilder.newColumn('MobileNumber').withTitle('Contact No').notSortable()
    ];

    $scope.InitializeDatePicker = function () {
        $('#txtFromDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#txtToDate').datepicker('setStartDate', startDate);
        });
        $('#txtToDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#txtFromDate').datepicker('setEndDate', FromEndDate);
        });
    }
    $scope.GetConsignmentBookingReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $('#ddlBranch').val();
        if (fromdate == "" || todate == "") {
            Notification.error({ message: "Please select all criteria.", delay: 5000 });
        }
        else {
            var dtinstance = $scope.dtInstance.DataTable;
            $scope.dtOptions.ajax.data = function (d) {
                d.fromDate = fromdate;
                d.toDate = todate;
                d.branchId = branchId;
            };
            dtinstance.ajax.reload(function (json) { }, true);
        }
    }
    $scope.ExportConsignmentBookingReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $("#ddlBranch").val();
        if (branchId != "") {
            $timeout(function () {
                var link = document.createElement('a');
                link.download = "CancelledConsignmentReport.xls";
                link.href = "/Report/ExportCancelledConsignmentBookingReport?Fromdate=" + fromdate + "&Todate=" + todate + "&BranchId=" + branchId;
                link.click();
            }, 100);
        }
        else {
            Notification.error({ message: "Please select criteria.", delay: 5000 });
        }
    }
}]);

App.controller('LRExperienceReport_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "BranchData", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, BranchData, $timeout) {
    $scope.Branches = BranchData.GetData();
    $scope.consignmentBookingCollection = [];
    $scope.InitializeDatePicker = function () {
        $('#txtFromDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#txtToDate').datepicker('setStartDate', startDate);
        });
        $('#txtToDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#txtFromDate').datepicker('setEndDate', FromEndDate);
        });
    }
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: "/Report/GetLRExperiencegData",
            type: 'GET',
            data: function (d) {
                d.branchId = $('#ddlBranch').val();
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)

        .withOption('destroy', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [

        DTColumnBuilder.newColumn('VoucherType').withTitle('Voucher Type').notSortable(),
        DTColumnBuilder.newColumn('Branch').withTitle('Branch').notSortable(),
        DTColumnBuilder.newColumn('ManifestNo').withTitle('THC Number').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentNo').withTitle('LR No').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentDate').withTitle('Manifest Date').notSortable(),
        DTColumnBuilder.newColumn('FromBranch').withTitle('From').notSortable(),
        DTColumnBuilder.newColumn('ToBranch').withTitle('To').notSortable(),
        DTColumnBuilder.newColumn('ConsigneeName').withTitle('Consignee').notSortable(),
        DTColumnBuilder.newColumn('NoOfArticles').withTitle('No Of Articles').notSortable(),
        DTColumnBuilder.newColumn('ActualWeight').withTitle('Actual Weight').notSortable(),
        DTColumnBuilder.newColumn('VendorName').withTitle('Vendor Name').notSortable(),
        DTColumnBuilder.newColumn('VehicleNo').withTitle('Vehicle No').notSortable(),
        DTColumnBuilder.newColumn('Transit').withTitle('Transit').notSortable(),
        DTColumnBuilder.newColumn('TotalAmount').withTitle('LR Expense').notSortable(),
        DTColumnBuilder.newColumn('DebitLedger').withTitle('Debit Ledger').notSortable()
     
    ];

    $scope.GetLRExperienceReport = function () {
      
        var branchId = $('#ddlBranch').val();
        if (branchId == "0" ) {
            Notification.error({ message: "Please select branch.", delay: 5000 });
        }
        else {
            var dtinstance = $scope.dtInstance.DataTable;
            $scope.dtOptions.ajax.data = function (d) {
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
                d.branchId = branchId;
            };
            dtinstance.ajax.reload(function (json) { }, true);
        }
    }
    $scope.ExportLRExperienceReport = function () {
        var fromdate = $('#txtFromDate').val();
        var todate = $('#txtToDate').val();
        var branchId = $("#ddlBranch").val();
        if (branchId != "0") {
            $timeout(function () {
                var link = document.createElement('a');
                link.download = "LRExperienceReport.xls";
                link.href = "/Report/ExportLRExperiencegReport?Fromdate=" + fromdate + "&Todate=" + todate+"&BranchId=" + branchId;
                link.click();
            }, 100);
        }
        else {
            Notification.error({ message: "Please select branch.", delay: 5000 });
        }
    }
}]);

App.controller('VendorVehicleReport_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "BranchData", "$timeout","Excel", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, BranchData, $timeout,Excel) {
    $scope.Branches = BranchData.GetData();
    $scope.GetVendors = function () {

        var vendor = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVehicleVendors',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    vendor.push(response.data[i]);
                }
                $scope.Vendors = vendor;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetVendors();
    $scope.consignmentBookingCollection = [];
    $scope.InitializeDatePicker = function () {
        $('#txtFromDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#txtToDate').datepicker('setStartDate', startDate);
        });
        $('#txtToDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#txtFromDate').datepicker('setEndDate', FromEndDate);
        });
    }
    var Expense = 0;
    var Received = 0;
    var Balance = 0;
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: "/Report/GetVendorVehicleData",
            type: 'GET',
            data: function (d) {
                d.branchId = $('#ddlBranch').val();
                d.vendorId = $("#ddlVendor").val();
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', false)
        .withOption('paging', false)
        .withOption('searching', false)
        .withOption('destroy', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [

        DTColumnBuilder.newColumn('VendorName').withTitle('Vendor Name').notSortable(),
        DTColumnBuilder.newColumn('HireDate').withTitle('Hire Date').notSortable(),
        DTColumnBuilder.newColumn('TimeStr').withTitle('Time').notSortable(),
        DTColumnBuilder.newColumn('VehicleNo').withTitle('Vehicle No').notSortable(),
        DTColumnBuilder.newColumn('BranchName').withTitle('Branch').notSortable(),
        DTColumnBuilder.newColumn('Amount').withTitle('Amount').notSortable(),
        DTColumnBuilder.newColumn('PaidAmount').withTitle('Paid Amount').notSortable(),
        DTColumnBuilder.newColumn('FromBranch').withTitle('From').notSortable(),
        DTColumnBuilder.newColumn('ToBranch').withTitle('To').notSortable(),
         DTColumnBuilder.newColumn('').withTitle('Balance Flow').notSortable().renderWith(function (data, type, row, meta) {
             if (row.Amount != 0 && row.PaidAmount != 0) {
                 Balance = Balance + (parseFloat(row.Amount) - parseFloat(row.PaidAmount));
             }
             else if (row.PaidAmount != 0) {
                 Balance = Balance - parseFloat(row.PaidAmount);
             }
             else {
                 Balance = Balance + parseFloat(row.Amount);
             }
            return Balance;
        })

    ];
    setTimeout(function () {
        $("#lblBalance").text(Balance)
    }, 2000);
    $scope.GetVendorVehicleReport = function () {
        var vendorId = $('#ddlVendor').val(); 
        if (vendorId == "") {
            Notification.error({ message: "Please select vendor.", delay: 5000 });
        }
        else {
            var dtinstance = $scope.dtInstance.DataTable;
            $scope.dtOptions.ajax.data = function (d) {
                d.branchId = $('#ddlBranch').val();
                d.vendorId = vendorId;
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
               
            };
            dtinstance.ajax.reload(function (json) { }, true);
        }
    }
    $scope.exportToExcel = function (divId) {
        $scope.exportHref = Excel.tableToExcel(divId, 'VendorExpenseReport');
        $timeout(function () {
            var link = document.createElement('a');
            link.download = "VendorExpenseReport.xls";
            link.href = $scope.exportHref;
            link.click();
        }, 100);
    }
}]);

App.controller('TransactionReport_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "BranchData", "Excel", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, BranchData, Excel,$timeout) {
    $scope.Branches = BranchData.GetData();
    $scope.consignmentBookingCollection = [];
    var Expense = 0;
    var Received = 0;
    var Balance = 0;
    $scope.InitializeDatePicker = function () {
        $('#txtFromDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#txtToDate').datepicker('setStartDate', startDate);
        });
        $('#txtToDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#txtFromDate').datepicker('setEndDate', FromEndDate);
        });
    }
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: "/Report/GetTransactionData",
            type: 'GET',
            data: function (d) {
                d.branchId = $('#ddlBranch').val();
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', false)
        .withOption('searching', false)
        .withOption('paging', false)
        .withOption('destroy', true)
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('BalanceFlow').withTitle('BalanceFlow').notVisible(),
        DTColumnBuilder.newColumn('DateStr').withTitle('Date').notSortable(),
        DTColumnBuilder.newColumn('TimeStr').withTitle('Time').notSortable(),
        DTColumnBuilder.newColumn('VoucherNo').withTitle('Voucher No').notSortable(),
        DTColumnBuilder.newColumn('AccountHead').withTitle('Account Head').notSortable(),
        DTColumnBuilder.newColumn('Particular').withTitle('Particulars').notSortable(),
        DTColumnBuilder.newColumn('Location').withTitle('Pickup/Delivery Location').notSortable(),
        DTColumnBuilder.newColumn('THCNo').withTitle('THC No').notSortable(),
        DTColumnBuilder.newColumn('LRNo').withTitle('LR No').notSortable(),
        DTColumnBuilder.newColumn('NoOfArticles').withTitle('No Of Articles').notSortable(),
        DTColumnBuilder.newColumn('Weight').withTitle('Weight').notSortable(),
        DTColumnBuilder.newColumn('VehicleNo').withTitle('Vehicle No').notSortable(),
        DTColumnBuilder.newColumn('PersonName').withTitle('Paid To').notSortable(),
        DTColumnBuilder.newColumn('ReceivedAmount').withTitle('Received Amount').notSortable(),
        DTColumnBuilder.newColumn('ExpenseAmount').withTitle('Expense Amount').notSortable(),
        DTColumnBuilder.newColumn('').withTitle('Balance Flow').notSortable().renderWith(function (data, type, row, meta) {
            if (Balance == 0) { Balance = row.BalanceFlow}
            if (row.ReceivedAmount != 0) { Received = row.ReceivedAmount; Balance = Balance + Received }
            if (row.ExpenseAmount != 0) { Expense = row.ExpenseAmount; Balance = Balance - Expense }
            return Balance;
        })

    ];
    setTimeout(function () {
        $("#lblBalance").text(Balance/2)
    }, 2000);

    $scope.GetTransactionReport = function () {
        var branchId = $('#ddlBranch').val();
        if (branchId == "") {
            Notification.error({ message: "Please select branch.", delay: 5000 });
        }
        else {
            var dtinstance = $scope.dtInstance.DataTable;
            $scope.dtOptions.ajax.data = function (d) {
                d.branchId = branchId;
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();

            };
            dtinstance.ajax.reload(function (json) { }, true);
        }
    }
    $scope.exportToExcel = function (divId) {
        $scope.exportHref = Excel.tableToExcel(divId, 'TransactionReport');
        $timeout(function () {
            var link = document.createElement('a');
            link.download = "TransactionReport.xls";
            link.href = $scope.exportHref;
            link.click();
        }, 100);
    }
}]);


App.controller('OtherVendorExpenseReport_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "BranchData", "$timeout","Excel", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, BranchData, $timeout, Excel) {
    $scope.Branches = BranchData.GetData();
    $scope.GetVendors = function () {

        var vendor = [];
        $http({
            method: 'GET',
            url: '/Operations/GetOtherVendors',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    vendor.push(response.data[i]);
                }
                $scope.Vendors = vendor;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetVendors();
    $scope.consignmentBookingCollection = [];
    $scope.InitializeDatePicker = function () {
        $('#txtFromDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#txtToDate').datepicker('setStartDate', startDate);
        });
        $('#txtToDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false,
            clearBtn: true
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#txtFromDate').datepicker('setEndDate', FromEndDate);
        });
    }
    var Expense = 0;
    var Balance = 0;
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: "/Report/GetOtherVendorData",
            type: 'GET',
            data: function (d) {
                d.branchId = $('#ddlBranch').val();
                d.vendorId = $("#ddlVendor").val();
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();
            }
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', false)
        .withOption('paging', false)
        .withOption('searching', false)
        .withOption('destroy', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [

        DTColumnBuilder.newColumn('ExpenseDateStr').withTitle('Date').notSortable(),
        DTColumnBuilder.newColumn('TimeStr').withTitle('Time').notSortable(),
        DTColumnBuilder.newColumn('InvoiceNo').withTitle('Invoice No').notSortable(),
        DTColumnBuilder.newColumn('BranchName').withTitle('Branch').notSortable(),
        DTColumnBuilder.newColumn('Amount').withTitle('Amount').notSortable(),
        DTColumnBuilder.newColumn('PaidAmount').withTitle('PaidAmount').notSortable(),
       
        DTColumnBuilder.newColumn('').withTitle('Balance Flow').notSortable().renderWith(function (data, type, row, meta) {
            if (row.Amount != 0 && row.PaidAmount != 0) { var amount = parseFloat(row.Amount) - parseFloat(row.PaidAmount); Balance = parseFloat(Balance) + parseFloat(amount) }
            else if (row.PaidAmount != 0 && row.Amount == 0) { Expense = row.PaidAmount; Balance = parseFloat(Balance) - parseFloat(Expense) }
            else { Balance = parseFloat(Balance) + parseFloat(row.Amount)}
            return Balance;
        })

    ];
    
    $scope.GetOtherVendorReport = function () {
        var vendorId = $('#ddlVendor').val();
        if (vendorId == "") {
            Notification.error({ message: "Please select vendor.", delay: 5000 });
        }
        else {
            var dtinstance = $scope.dtInstance.DataTable;
            $scope.dtOptions.ajax.data = function (d) {
                d.branchId = $('#ddlBranch').val();
                d.vendorId = vendorId;
                d.fromDate = $('#txtFromDate').val();
                d.toDate = $('#txtToDate').val();

            };
            dtinstance.ajax.reload(function (json) { }, true);

            setTimeout(function () {
                $("#lblBalance").text(Balance)
            }, 1000);

        }
    }
    $scope.exportToExcel = function (divId) {
        $scope.exportHref = Excel.tableToExcel(divId, 'OtherVendorExpenseReport');
        $timeout(function () {
            var link = document.createElement('a');
            link.download = "OtherVendorExpenseReport.xls";
            link.href = $scope.exportHref;
            link.click();
        }, 100);
    }
}]);




