﻿
var Login = angular.module("LoginModule", ['ui-notification']);
Login.controller('LoginController', ["$scope", "$http", "Notification", function ($scope, $http, Notification) {

    $scope.userData = { EmailID: '', Password: '' };
    $scope.SELECTEDROLE = '';

    $scope.ValidateUser = function () {
     
        if ($scope.userData.EmailID == "") {
            Notification.error({
                message: 'Please enter valid Email Id !!!', delay: 5000
            });
            return;
        }

        if ($scope.userData.Password == "") {
            Notification.error({
                message: 'Please enter valid password !!!', delay: 5000
            });
            return;
        }

        $http({
            method: 'POST',
            url: '/Login/ValidateUser',
            params: {
                emailID: $scope.userData.EmailID, userPword: $scope.userData.Password
            },
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }
        })
        .then(function successCallback(response) {
            if (response.data == true) {
                $scope.LoadHome();
            }
            else
                Notification.error({
                    message: 'Please enter valid username or password !!!', delay: 5000
                });
        },
        function errorCallback(response) {
            Notification.error({
                message: response.status + '  ' + response.statusText, delay: 5000
            });

            Notification.error({
                message: response.data , delay: 5000
            });
        });
    }

    $scope.LoadHome = function () {
        $http({
            method: 'POST',
            url: '/Login/LoadHome',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
        .then(function successCallback(response) {
            window.location.href = response.data;
        },
        function errorCallback(response) {
            Notification.error({ message: response.status + '  ' + response.statusText, delay: 5000 });
        });
    }

    $scope.RoleList = [];
    $scope.getRoles = function () {


        $http({
            method: 'GET',
            url: '/Login/getRoles',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         if (response.data.length > 0) {

             var roles = response.data.split(",");
             for (i = 0; i < roles.length ; i++) {
                 $scope.RoleList.push({ Name: '' });
                 $scope.RoleList[i].Name = roles[i];
             }


             if ($scope.RoleList.length > 1) {
                 $('#SelectRolesModel').modal("show");
                 //  $scope.LoadHome();
             }
             else {
                 $scope.LoadHome();
             }
         }

     },
     function errorCallback(response) {
         Notification.error({ message: response.status + '  ' + response.statusText, delay: 5000 });
     });
    }
    $scope.SaveRole = function () {

        $('#SelectRolesModel').modal("hide");

        $scope.LoadHome();

        $http({
            method: 'POST',
            url: '/Login/SaveRole',
            params: { nRole: $scope.SELECTEDROLE },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
   .then(function successCallback(response) {


   },
   function errorCallback(response) {
       Notification.error({ message: response.status + '  ' + response.statusText, delay: 5000 });
   });

    }

    $scope.SelectRole = function (item) {
        $scope.SELECTEDROLE = item;
    }

}]);


