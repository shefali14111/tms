﻿App.controller('LocalManifest_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder) {

    $("#select_all").change(function () {  //"select all" change 
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function () { //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
        for (i = 0; i < $scope.activeConsignmentBooking.length; i++) {
            $scope.activeConsignmentBooking[i].checkSelected = true;
        }

    });

    $('.checkbox').change(function () { //".checkbox" change 
        if (this.checked == false) { //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

        if ($('.checkbox:checked').length == $('.checkbox').length) {
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });
    /**ADD HIRED VEHICLES **/
    $scope.LocalVehicleHire = {
        LocalVehicleHireID: '',
        BranchID: '',
        HireDate: $scope.currentDate,
        TruckBookingType: '',
        VehicleID: '',
        VendorName: '',
        VendorType: '',
        BookingAmount: '',
        AdvancePayment: '',
        BalanceAmount: '',
        PaymentTypeID: '',
        BankName: '',
        BranchName: '',
        IFSCCode: '',
        PanNo: '',
        Remarks: '',
        CreatedBy: '',
        Created: '',
        ModifiedBy: '',
        Modified: '',
        DeliveryVehicleHireID: '',
    };


    $scope.VehicleMaster = {
        VehicleName: '',
        VehicleNo: '',
        VehicleType: '',
        RegistrationDate: '',
        FitnessExpDate: '',
        InsuranceExpDate: '',
        PUCExpDate: '',
        EngineNo: '',
        ChasisNo: '',
        OwnerType: '',
        BrokerName: '',
        VendorName: '',
        loadingCapacity: '',
        MaxloadingCapacity: '',
        isHiredVehicle: '',
        CreatedBy: '',
        Created: '',
        ModifiedBy: '',
        Modified: '',
    }
    $scope.closeVehicelModal = function () {
        $scope.VehicleMaster = {
            VehicleName: '',
            VehicleNo: '',
            VehicleType: '',
            RegistrationDate: '',
            FitnessExpDate: '',
            InsuranceExpDate: '',
            PUCExpDate: '',
            EngineNo: '',
            ChasisNo: '',
            OwnerType: '',
            BrokerName: '',
            VendorName: '',
            loadingCapacity: '',
            MaxloadingCapacity: '',
            isHiredVehicle: '',
            CreatedBy: '',
            Created: '',
            ModifiedBy: '',
            Modified: '',
        }
        $('#AddHiredVehicleModal').modal('hide');
    }
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: "/Operations/GetLocalVehicleHireData",
            type: 'GET'
        })
        .withOption('order', [3, 'desc'])
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { search: "", searchPlaceholder: "Search..." })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('LocalVehicleHireID').withTitle('LocalVehicleHireID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('VehicleID').withTitle('VehicleID').notVisible().notSortable(),

        DTColumnBuilder.newColumn('VehicleNo').withTitle('Vehicle No').notSortable(),
        DTColumnBuilder.newColumn('BranchName').withTitle('Booking Branch').notSortable(),
        DTColumnBuilder.newColumn('HireDate').withTitle('Hire Date'),
        DTColumnBuilder.newColumn('VendorName').withTitle('Vendor Name').notSortable(),
        DTColumnBuilder.newColumn('BookingAmount').withTitle('Booking Amount').notSortable(),
        DTColumnBuilder.newColumn('BookingType').withTitle('Booking Type').notSortable(),
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            return '<a data-ng-click="EditVehicleHirepayment(' + row.LocalVehicleHireID + ')" class="pull-left font20 marleft15"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a class="pull-left marleft15 font20 delete" ng-click="DeleteVehicleHirePayment(' + row.LocalVehicleHireID + ')"><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
        }),
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            if($("#hdnBranchID").val() != '') {
                return '<a data-ng-click="AddManifest(' + row.LocalVehicleHireID + ',' + row.VehicleID + ')" class="pull-left marleft15" style="font-size:16px;"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;&nbsp;Add Manifest</a>'
            }
            else
                return '';
            })
    ];
    $scope.dtDelOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: "/Operations/GetDeliveryVehicleHireData",
            type: 'GET'
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { search: "", searchPlaceholder: "Search..." })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtDelInstance = {};

    $scope.dtDelColumns = [
        DTColumnBuilder.newColumn('DeliveryVehicleHireID').withTitle('DeliveryVehicleHireID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('VehicleID').withTitle('VehicleID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('VehicleNo').withTitle('Vehicle No').notSortable(),
        DTColumnBuilder.newColumn('BranchName').withTitle('Booking Branch').notSortable(),
        DTColumnBuilder.newColumn('HireDate').withTitle('Hire Date'),
        DTColumnBuilder.newColumn('VendorName').withTitle('Vendor Name').notSortable(),
        DTColumnBuilder.newColumn('BookingAmount').withTitle('Booking Amount').notSortable(),
        DTColumnBuilder.newColumn('BookingType').withTitle('Booking Type').notSortable(),
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            return '<a data-ng-click="EditDeliveryVehicleHirepayment(' + row.DeliveryVehicleHireID + ')" class="pull-left font20 marleft15"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a class="pull-left marleft15 font20 delete" ng-click="DeleteDeliveryVehicleHire(' + row.DeliveryVehicleHireID + ')"><i class="fa fa-trash-o" aria-hidden="true"></i></a>'
        }),
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            if ($("#hdnBranchID").val() != '0') {
                return '<a data-ng-click="AddDeliveryManifest(' + row.DeliveryVehicleHireID + ',' + row.VehicleID + ')" class="pull-left marleft15" style="font-size:16px;"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;&nbsp;Add Manifest</a>'
            }
            else
                return '';
            })
    ];
    $scope.ChangePaymentType = function () {
        if ($("#ddlPaymentType :selected").text().toLowerCase() == 'cheque') {
            $scope.IsDisabledCheque = false;
        }
        else { $scope.IsDisabledCheque = true; }
    }
    $scope.showVehicleModal = function () { $('#AddHiredVehicleModal').modal('show'); }
    $scope.AddVehicle = function () {

        if ($scope.frmVehicle.$valid) {
            var isHired = true;
            $scope.VehicleMaster.isHiredVehicle = isHired;
            $http({
                method: 'POST',
                url: '/Operations/AddVehicle',
                data: JSON.stringify($scope.VehicleMaster),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);
                    $scope.closeVehicelModal();

                    Notification.success({ message: "Hired Vehicle saved successfully.", delay: 5000 });

                    $scope.GetVehicles(isHired);

                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }



        else {

            Notification.error({ message: "Please fill all the required fields on the form.", delay: 5000 });
            return;
        }
    }

    /**END ADD HIRED VEHICLES **/
    $scope.currentDate = $filter('date')(new Date(), 'dd/MM/yyyy');
    var date = new Date();
    var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
    var am_pm = date.getHours() >= 12 ? "PM" : "AM";
    hours = hours < 10 ? "0" + hours : hours;
    var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
    time = hours + ":" + minutes + ":" + seconds + " " + am_pm;
    $scope.currentTime = time;
    $('#txtRegistrationDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        forceParse: false
    });

    jQuery(function ($) {
        $("#txtRegistrationDate").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
    });

    $('#txtEstDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        forceParse: false
    });

    jQuery(function ($) {
        $("#txtEstDate").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
    });
    $('#txtFitnessExpDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        forceParse: false
    });

    jQuery(function ($) {
        $("#txtFitnessExpDate").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
    });

    $('#txtInsuranceExpDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        forceParse: false
    });

    jQuery(function ($) {
        $("#txtInsuranceExpDate").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
    });

    $('#txtPUCExpDate').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '-1d',
        // endDate: '0d',
        autoclose: true,
        forceParse: false
    });

    jQuery(function ($) {
        $("#txtPUCExpDate").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
    });

    $scope.FormatDate = function (jsonDate) {
        if (jsonDate == null) {
            return "";
        }
        else {
            var date = new Date(parseInt(jsonDate.substr(6)));//$filter('date')(parseInt(jsonDate.substr(6)), 'dd/MM/yyyy'); //parseInt(jsonDate.substr(6)));
            return date;
        }
    }



    $scope.activeVehicleHires = [];
    $scope.activeVehicleHireCollection = [];
    $scope.PaymentTypes = [];
    $scope.Branches = [];
    $scope.GetBranches = function () {

        //$scope.activetrainingOpportunities = '';
        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    brnches.push(response.data[i]);
                }
                $scope.Branches = brnches;
                // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }



    $scope.GetPaymentTypes = function () {

        //$scope.activetrainingOpportunities = '';
        var pmtTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVehicleHirePaymentTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    pmtTypes.push(response.data[i]);
                }
                $scope.PaymentTypes = pmtTypes;
                // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.BookingTypes = [];
    $scope.GetTruckBookingTypes = function () {

        //$scope.activetrainingOpportunities = '';
        var bkTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetTruckBookingTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    bkTypes.push(response.data[i]);
                }
                $scope.BookingTypes = bkTypes;
                // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }


    $scope.Vehicles = [];
    $scope.GetVehicles = function (isHired) {


        var vehList = [];
        $http({
            method: 'GET',
            params: { 'isHired': isHired, },
            url: '/Operations/GetVehicles',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    vehList.push(response.data[i]);
                }
                $scope.Vehicles = vehList;
                // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }

    //$scope.showVehicleDropdown = false;
    //$scope.showVehicleTextInput = true;
    $scope.EnableVendor = false;
    $scope.HiredandNew = false;
    $scope.EnableVehicle = function () {
        var booktypeSelected = $scope.LocalVehicleHire.TruckBookingType;
        var bookType = filterFilter($scope.BookingTypes, { BookingTypeID: booktypeSelected });
        var isHired = false;

        if (bookType.length > 0) {
            if (bookType[0].BookingType == 'Own') {
                isHired = false;

            }
            else {

                isHired = true;

            }

            $scope.GetVehicles(isHired);
        }
    }
    $scope.loadVehicleHirePayments = function () {

        var dtinstance = $scope.dtInstance.DataTable;
        dtinstance.ajax.reload(function (json) { }, true);
    }

    $scope.selectConsignment = function () {

        var totalArticle = 0;
        var totalActualWeight = 0;
        for (i = 0; i < $scope.activeConsignmentBooking.length; i++) {
            if ($scope.activeConsignmentBooking[i].checkSelected == true) {
                totalArticle = totalArticle + parseInt($scope.activeConsignmentBooking[i].Article);
                totalActualWeight = totalActualWeight + parseFloat($scope.activeConsignmentBooking[i].ActualWeight);
            }
        }
        $scope.TotalArticles = totalArticle;
        $scope.TotalActualWeight = totalActualWeight;
    }
    $scope.selectDeliveryConsignment = function () {

        var totalArticle = 0;
        var totalActualWeight = 0;
        for (i = 0; i < $scope.activeConsignmentBooking.length; i++) {
            if ($scope.activeConsignmentBooking[i].checkSelected == true) {
                totalArticle = totalArticle + parseInt($scope.activeConsignmentBooking[i].Article);
                totalActualWeight = totalActualWeight + parseFloat($scope.activeConsignmentBooking[i].ActualWeight);
            }
        }
        $scope.DeliveryTotalArticles = totalArticle;
        $scope.DeliveryTotalActualWeight = totalActualWeight;
    }
    $scope.UpdateLocalManifestArticleAndWeight = function () {
        $http({
            method: 'POST',
            url: '/Operations/UpdateManifestArticleAndWeight',
            dataType: 'json',
            traditional: true,
            contentType: 'application/json; charset=utf-8',
            params: { 'totalArticle': $scope.TotalArticles, 'totalActualWeight': $scope.TotalActualWeight, 'manifestId': $scope.VehicleManifest.ManifestID, 'type': 'L' }

        })
            .then(function successCallback(response) {

            }, function errorCallback(response) {
                Notification.error({ message: response.message, delay: 5000 });
            });
    }
    $scope.UpdateDeliveryManifestArticleAndWeight = function () {
        $http({
            method: 'POST',
            url: '/Operations/UpdateManifestArticleAndWeight',
            dataType: 'json',
            traditional: true,
            contentType: 'application/json; charset=utf-8',
            params: { 'totalArticle': $scope.DeliveryTotalArticles, 'totalActualWeight': $scope.DeliveryTotalActualWeight, 'manifestId': $scope.VehicleManifest.DeliveryManifestID, 'type': 'D' }

        })
            .then(function successCallback(response) {

            }, function errorCallback(response) {
                Notification.error({ message: response.message, delay: 5000 });
            });
    }
    $scope.clearVehicleHire = function () {
        $scope.LocalVehicleHire = {
            LocalVehicleHireID: '',
            BranchID: '',
            HireDate: $scope.currentDate,
            TruckBookingType: '',
            VehicleID: '',
            VendorName: '',
            VendorType: '',
            BookingAmount: '',
            AdvancePayment: '',
            BalanceAmount: '',
            PaymentTypeID: '',
            BankName: '',
            BranchName: '',
            IFSCCode: '',
            PanNo: '',
            Remarks: '',
            CreatedBy: '',
            Created: '',
            ModifiedBy: '',
            Modified: '',
            DeliveryVehicleHireID: ''
        };


    }

    $scope.GetVehicleHirepaymentByID = function (VehicleHireID) {

        var hirepayments = [];
        $http({
            method: 'GET',
            params: { 'vehicleHireID': VehicleHireID },
            url: '/Operations/GetLocalVehicleHirePaymentByID',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    hirepayments.push(response.data[i]);
                }


                $scope.LocalVehicleHire.LocalVehicleHireID = hirepayments[0].LocalVehicleHireID;
                $scope.LocalVehicleHire.BranchID = hirepayments[0].BranchID;
                $scope.LocalVehicleHire.TruckBookingType = hirepayments[0].TruckBookingType;
                $scope.LocalVehicleHire.VendorName = hirepayments[0].VendorName;
                $scope.LocalVehicleHire.VendorType = hirepayments[0].VendorType;
                $scope.LocalVehicleHire.BookingAmount = hirepayments[0].BookingAmount;
                $scope.LocalVehicleHire.AdvancePayment = hirepayments[0].AdvancePayment;
                $scope.LocalVehicleHire.BalanceAmount = hirepayments[0].BalanceAmount;
                $scope.LocalVehicleHire.PaymentTypeID = hirepayments[0].PaymentTypeID;
                $scope.LocalVehicleHire.BankName = hirepayments[0].BankName;
                $scope.LocalVehicleHire.BranchName = hirepayments[0].BranchName;
                $scope.LocalVehicleHire.IFSCCode = hirepayments[0].IFSCCode;
                $scope.LocalVehicleHire.PanNo = hirepayments[0].PanNo;
                $scope.LocalVehicleHire.Remarks = hirepayments[0].Remarks;
                $scope.LocalVehicleHire.CreatedBy = hirepayments[0].CreatedBy;
                $scope.LocalVehicleHire.Created = hirepayments[0].Created;
                $scope.LocalVehicleHire.ModifiedBy = hirepayments[0].ModifiedBy;
                $scope.LocalVehicleHire.Modified = hirepayments[0].Modified;

                $scope.EnableVehicle();
                $scope.LocalVehicleHire.VehicleID = hirepayments[0].VehicleID;
                setTimeout(function () { $scope.GetVehicleDetails(); }, 3000);
                $scope.LocalVehicleHire.HireDate = hirepayments[0].HireDateStr;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.closeVehicleHire = function () {
        $scope.clearVehicleHire();
        $('#LocalVehicleHireModal').modal('hide');
    }
    $scope.AddVehicleHirepayment = function () {
        $.get("/Operations/CheckDeliveryStatus")
            .done(function (response) {

                if (response == "False") {
                    Notification.error({ message: "Request is not allowed.", delay: 5000 });
                }
                else {
                    $scope.clearVehicleHire();
                    $scope.GetBranches();
                    $scope.GetTruckBookingTypes();
                    $scope.GetPaymentTypes();
                    $('#LocalVehicleHireModal').modal('show');
                }
            });

    }


    $scope.EditVehicleHirepayment = function (LocalVehicleHireID) {
        $scope.clearVehicleHire();
        $scope.GetBranches();
        $scope.GetTruckBookingTypes();
        $scope.GetPaymentTypes();

        $scope.GetVehicleHirepaymentByID(LocalVehicleHireID);
        $('#LocalVehicleHireModal').modal('show');

    }
    $scope.RegistrationDate = "";
    $scope.FitnessExpDate = "";
    $scope.InsuranceExpDate = "";
    $scope.PUCExpDate = "";
    $scope.GetVehicleDetails = function () {
        var vehID = $scope.LocalVehicleHire.VehicleID;
        var vehDetails = filterFilter($scope.Vehicles, { VehicleID: vehID },true);
        if (vehDetails.length > 0) {
            $scope.RegistrationDate = vehDetails[0].RegistrationDate;
            $scope.FitnessExpDate = vehDetails[0].FitnessExpDate;
            $scope.InsuranceExpDate = vehDetails[0].InsuranceExpDate;
            $scope.PUCExpDate = vehDetails[0].PUCExpDate;
            $scope.LocalVehicleHire.VendorName = vehDetails[0].VendorName;
        }
    }
    $scope.calculateBalanceAmount = function () {
        var bookingAmt = 0;
        if ($scope.LocalVehicleHire.BookingAmount != '')
            bookingAmt = parseFloat($scope.LocalVehicleHire.BookingAmount);
        var advPayment = 0;
        if ($scope.LocalVehicleHire.AdvancePayment != '')
            advPayment = parseFloat($scope.LocalVehicleHire.AdvancePayment);
        if (advPayment > bookingAmt) {
            Notification.error({ message: 'Advance payment can not be greater than booking amount !', delay: 5000 });
        }
        var balanceAmt = eval(bookingAmt - advPayment);
        $scope.LocalVehicleHire.BalanceAmount = balanceAmt;

    }
    $scope.DeleteVehicleHirePayment = function (vehicleHireID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Operations/DeleteLocalVehicleHire',
                params: { 'vehicleHireID': vehicleHireID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Local Vehicle Hire payment deleted successfully.", delay: 5000 });
                    $scope.loadVehicleHirePayments();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
    $scope.SaveVehicleHire = function () {

        if ($scope.frmVehHire.$valid) {
            if ($scope.LocalVehicleHire.LocalVehicleHireID == '') {
                $http({
                    method: 'POST',
                    url: '/Operations/AddLocalVehicleHirePayment',
                    data: JSON.stringify($scope.LocalVehicleHire),
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
                    .then(function successCallback(response) {
                        console.log(response);
                        $scope.clearVehicleHire();
                        $('#LocalVehicleHireModal').modal('hide');
                        Notification.success({ message: "Local Vehicle Hire payment saved successfully.", delay: 5000 });

                        $scope.loadVehicleHirePayments();

                    }, function errorCallback(response) {
                        Notification.error({ message: response.message, delay: 5000 });
                    });
            }
            else {
                $http({
                    method: 'POST',
                    url: '/Operations/UpdateLocalVehicleHirePayment',
                    //  data: JSON.stringify($scope.AUDITS),
                    data: JSON.stringify($scope.LocalVehicleHire),
                    //  params:{audit: $scope.AUDITS, application: $scope.APPLICATION },
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
                    .then(function successCallback(response) {
                        console.log(response);
                        $scope.clearVehicleHire();
                        $('#LocalVehicleHireModal').modal('hide');

                        Notification.success({ message: "Local Vehicle Hire payment updated successfully.", delay: 5000 });
                        $scope.loadVehicleHirePayments();
                        // $(#).('hide')

                    }, function errorCallback(response) {
                        Notification.error({ message: response.message, delay: 5000 });
                    });
            }



        }
        else {

            Notification.error({ message: "Please fill all the required fields on the form.", delay: 5000 });
            return;
        }
    }

    /* BEGIN Manifeast */
    $scope.ManifestSaved = false;
    $scope.VehicleManifest = {
        ManifestID: '',
        VehicleHireID: '',
        ManifestNo: '',
        VehicleID: '',
        DriverName: '',
        DriverMobileNo: '',
        LicenseNo: '',
        EstimatedDeliveryDate: '',
        ArrivalTime: '',
        DepartureTime: $scope.currentTime,
        FromLoc: '',
        ToLoc: '',
        CreatedBy: '',
        Created: '',
        ModifiedBy: '',
        Modified: '',
        DeliveryManifestID: '',
        DeliveryVehicleHireID: ''
    };

    $scope.GetNextManifestNo = function () {

        var branchID = $('#hdnBranchID').attr("value");;

        $http({
            method: 'GET',
            params: { 'branchID': branchID },
            url: '/Operations/GetNextLocalManifestNo',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                $scope.VehicleManifest.ManifestNo = response.data;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }

    $scope.activeConsignmentBooking = [];
    $scope.activeConsignmentBookingCollection = [];
    $scope.GetConsignmentsForManifest = function () {

        $scope.activeConsignmentBooking = '';
        var bookings = [];
        $http({
            method: 'GET',
            params: { 'manifestId': $scope.VehicleManifest.ManifestID, },
            url: '/Operations/GetConsignmentsForLocalManifest',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    bookings.push(response.data[i]);
                    bookings[i].checkSelected = false;
                }
                $scope.activeConsignmentBooking = bookings;
                $scope.activeConsignmentBookingCollection = bookings;
                $scope.GetManifestConsignments();
            },
                function errorCallback(response) {
                    alert(response);
                });
    }


    $scope.loadVehicleManifest = function (localVehicleHireId) {


        $http({
            method: 'GET',
            params: { 'vehicleHireID': localVehicleHireId, },
            url: '/Operations/GetLocalVehicleHireManifestByVehicleHireID',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                if (response.data.length > 0) {
                    $scope.VehicleManifest = {
                        ManifestID: response.data[0].ManifestID,
                        VehicleHireID: response.data[0].VehicleHireID,
                        ManifestNo: response.data[0].ManifestNo,
                        VehicleID: response.data[0].VehicleID,
                        DriverName: response.data[0].DriverName,
                        DriverMobileNo: response.data[0].DriverMobileNo,
                        LicenseNo: response.data[0].LicenseNo,
                        EstimatedDeliveryDate: response.data[0].EstimatedDeliveryDate,
                        ArrivalTime: response.data[0].ArrivalTime,
                        DepartureTime: response.data[0].DepartureTime,
                        FromLoc: response.data[0].FromLoc,
                        ToLoc: response.data[0].ToLoc,
                        CreatedBy: response.data[0].CreatedBy,
                        Created: $filter('date')($scope.FormatDate(response.data[0].Created), 'dd/MM/yyyy'),
                        ModifiedBy: response.data[0].ModifiedBy,
                        Modified: response.data[0].Modified,
                    };
                    $scope.ManifestSaved = true;
                }
            },
                function errorCallback(response) {
                    alert(response);
                });
    }


    $scope.clearManifest = function () {
        $scope.showManifestConsignments = false;
        $scope.ManifestSaved = false;
        $scope.ManifestVehicleNo = '';
        $scope.ManifestVehicleType = '';
        $scope.VehicleManifest = {
            ManifestID: '',
            VehicleHireID: '',
            ManifestNo: '',
            VehicleID: '',
            DriverName: '',
            DriverMobileNo: '',
            LicenseNo: '',
            EstimatedDeliveryDate: '',
            ArrivalTime: '',
            DepartureTime: '',
            FromLoc: '',
            ToLoc: '',
            CreatedBy: '',
            Created: '',
            ModifiedBy: '',
            Modified: '',
            DeliveryManifestID: '',
            DeliveryVehicleHireID: ''
        };
    }

    $scope.ManifestVehicleNo = '';
    $scope.ManifestVehicleType = '';
    $scope.closeManifest = function () {

        $scope.clearManifest();
        $('#ManifestModal').modal('hide');
    }
    $scope.GetOtherBranches = function () {

        var otherBranches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetOtherBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    otherBranches.push(response.data[i]);
                }
                $scope.OtherBranches = otherBranches;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.AddManifest = function (localVehicleHireId, vehicleId) {
        $.get("/Operations/CheckDeliveryStatus")
            .done(function (response) {

                if (response == "False") {
                    Notification.error({ message: "Request is not allowed.", delay: 5000 });
                }
                else {
                    $scope.clearManifest();
                    $scope.GetOtherBranches();

                    $scope.loadVehicleManifest(localVehicleHireId);
                    $scope.VehicleManifest.DepartureTime = $scope.currentTime;
                    $scope.GetSelectedVehicleForHire(vehicleId);
                    $scope.VehicleManifest.VehicleHireID = localVehicleHireId;
                    $scope.VehicleManifest.Created = $scope.currentDate;
                    $scope.VehicleManifest.ArrivalTime = $scope.currentTime;
                    $('#ManifestModal').modal('show');
                }
            });


    }
    $scope.GetSelectedVehicleForHire = function (vehicleID) {

        $http({
            method: 'GET',
            params: { 'vehicleID': vehicleID, },
            url: '/Operations/GetVehicleByID',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                if (response.data.length > 0) {
                    $scope.ManifestVehicleNo = response.data[0].VehicleNo;
                }


            },
                function errorCallback(response) {
                    alert(response);
                });

    }

    $scope.DeleteManifest = function (manifestID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Operations/DeleteLocalVehicleHireManifest',
                params: { 'manifestID': manifestID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Local Vehicle Manifest deleted successfully.", delay: 5000 });
                    $scope.loadVehicleHirePayments();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
    $scope.SaveManifest = function () {

        if ($scope.frmManifest.$valid) {
            if ($scope.VehicleManifest.ManifestID == '') {
                $http({
                    method: 'POST',
                    url: '/Operations/AddLocalVehicleHireManifest',
                    data: JSON.stringify($scope.VehicleManifest),
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
                    .then(function successCallback(response) {
                        console.log(response);

                        // $('#ManifestModal').modal('hide');
                        Notification.success({ message: "Local Vehicle Manifest saved successfully.", delay: 5000 });
                        var manifestID = response.data;

                        $scope.VehicleManifest.ManifestID = manifestID;

                        $scope.ManifestSaved = true;

                    }, function errorCallback(response) {
                        Notification.error({ message: response.message, delay: 5000 });
                    });
            }
            else {
                $http({
                    method: 'POST',
                    url: '/Operations/UpdateLocalVehicleHireManifest',
                    //  data: JSON.stringify($scope.AUDITS),
                    data: JSON.stringify($scope.VehicleManifest),
                    //  params:{audit: $scope.AUDITS, application: $scope.APPLICATION },
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
                    .then(function successCallback(response) {
                        console.log(response);
                        $scope.clearManifest();
                        $('#ManifestModal').modal('hide');
                        $scope.ManifestSaved = true;
                        Notification.success({ message: "Local Vehicle Manifest updated successfully.", delay: 5000 });
                        $scope.loadConsignmentBookings();
                        // $(#).('hide')

                    }, function errorCallback(response) {
                        Notification.error({ message: response.message, delay: 5000 });
                    });
            }



        }
        else {

            Notification.error({ message: "Please fill all the required fields on the form.", delay: 5000 });
            return;
        }
    }
    //delivery vehicle hire - start
    $scope.activeDeliveryVehicleHireCollection = '';
    $scope.loadDeliveryVehicle = function () {

        var dtDelInstance = $scope.dtDelInstance.DataTable;
        dtDelInstance.ajax.reload(function (json) { }, true);
    }

    $scope.SaveDeliveryVehicleHire = function () {

        if ($scope.frmDelVehHire.$valid) {

            if ($scope.LocalVehicleHire.DeliveryVehicleHireID == '') {
                $http({
                    method: 'POST',
                    url: '/Operations/AddDeliveryVehicleHire',
                    data: JSON.stringify($scope.LocalVehicleHire),
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
                    .then(function successCallback(response) {
                        $scope.clearVehicleHire();
                        $('#LocalVehicleHireModal').modal('hide');
                        Notification.success({ message: "Delivery Vehicle Hire saved successfully.", delay: 5000 });

                        $scope.loadDeliveryVehicle();

                    }, function errorCallback(response) {
                        alert(JSON.stringify(response.statusText));

                    });
            }
            else {
                $http({
                    method: 'POST',
                    url: '/Operations/UpdateDeliveryVehicleHire',
                    data: JSON.stringify($scope.LocalVehicleHire),
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
                    .then(function successCallback(response) {
                        console.log(response);
                        $('#VehicleHireModal').modal('hide');

                        Notification.success({ message: "Delivery Vehicle Hire updated successfully.", delay: 5000 });
                        $scope.loadDeliveryVehicle();

                    }, function errorCallback(response) {
                        Notification.error({ message: response.data, delay: 5000 });
                    });
            }



        }
        else {

            Notification.error({ message: "Please fill all the required fields on the form.", delay: 5000 });
            return;
        }
    }
    $scope.EditDeliveryVehicleHirepayment = function (DeliveryVehicleHireID) {
        $scope.clearVehicleHire();
        $scope.GetBranches();
        $scope.GetTruckBookingTypes();
        $scope.GetPaymentTypes();
        setTimeout(function () { $scope.GetDeliveryVehicleHireByID(DeliveryVehicleHireID); }, 3000);

        $('#LocalVehicleHireModal').modal('show');
    }
    $scope.GetDeliveryVehicleHireByID = function (DeliveryVehicleHireID) {

        var hirepayments = [];
        $http({
            method: 'GET',
            params: { 'deliveryVehicleHireID': DeliveryVehicleHireID },
            url: '/Operations/GetDeliveryVehicleHireByID',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    hirepayments.push(response.data[i]);
                }

                $scope.LocalVehicleHire.DeliveryVehicleHireID = hirepayments[0].DeliveryVehicleHireID;
                $scope.LocalVehicleHire.BranchID = hirepayments[0].BranchID;
                $scope.LocalVehicleHire.TruckBookingType = hirepayments[0].TruckBookingType;
                $scope.LocalVehicleHire.VendorName = hirepayments[0].VendorName;
                $scope.LocalVehicleHire.VendorType = hirepayments[0].VendorType;
                $scope.LocalVehicleHire.BookingAmount = hirepayments[0].BookingAmount;
                $scope.LocalVehicleHire.AdvancePayment = hirepayments[0].AdvancePayment;
                $scope.LocalVehicleHire.BalanceAmount = hirepayments[0].BalanceAmount;
                $scope.LocalVehicleHire.PaymentTypeID = hirepayments[0].PaymentTypeID;
                $scope.LocalVehicleHire.BankName = hirepayments[0].BankName;
                $scope.LocalVehicleHire.BranchName = hirepayments[0].BranchName;
                $scope.LocalVehicleHire.IFSCCode = hirepayments[0].IFSCCode;
                $scope.LocalVehicleHire.PanNo = hirepayments[0].PanNo;
                $scope.LocalVehicleHire.Remarks = hirepayments[0].Remarks;
                $scope.LocalVehicleHire.CreatedBy = hirepayments[0].CreatedBy;
                $scope.LocalVehicleHire.Created = hirepayments[0].Created;
                $scope.LocalVehicleHire.ModifiedBy = hirepayments[0].ModifiedBy;
                $scope.LocalVehicleHire.Modified = hirepayments[0].Modified;

                $scope.LocalVehicleHire.VehicleID = hirepayments[0].VehicleID;
                $scope.LocalVehicleHire.HireDate = hirepayments[0].HireDateStr;
                $scope.EnableVehicle();
                setTimeout(function () { $scope.GetVehicleDetails(); }, 2000);
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.DeleteDeliveryVehicleHire = function (DeliveryVehicleHireID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Operations/DeleteDeliveryVehicleHire',
                params: { 'deliveryVehicleHireID': DeliveryVehicleHireID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Delivery Vehicle Hire deleted successfully.", delay: 5000 });
                    $scope.loadDeliveryVehicle();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
    $scope.deliveryStatusTypes = [];
    $scope.GetDeliveryStatusMaster = function () {

        //$scope.activetrainingOpportunities = '';
        var statusTypes = [];
        $http({
            method: 'GET',
            async: true,
            url: '/Operations/GetDeliveryStatusMaster',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    statusTypes.push(response.data[i]);
                }
                $scope.deliveryStatusTypes = statusTypes;
                // $scope.activetrainingOpportunitiesCollection = trainingOpportunities;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.AddDeliveryManifest = function (hireid, vehicleid) {
        $.get("/Operations/CheckDeliveryStatus")
            .done(function (response) {

                if (response == "False") {
                    Notification.error({ message: "Request is not allowed.", delay: 5000 });
                }
                else {
                    $scope.clearManifest();
                    $scope.GetOtherBranches();
                    $scope.loadDeliveryVehicleManifest(hireid);
                    $scope.VehicleManifest.DepartureTime = $scope.currentTime;
                    $scope.GetSelectedVehicleForHire(vehicleid);
                    $scope.VehicleManifest.ArrivalTime = $scope.currentTime;
                    $scope.VehicleManifest.DeliveryVehicleHireID = hireid;
                    $scope.VehicleManifest.Created = $scope.currentDate;
                    $scope.GetDeliveryStatusMaster();
                    $scope.ManifestDeliveryStatus = '';
                    $('#ManifestModal').modal('show');
                }
            });

    }
    $scope.loadDeliveryVehicleManifest = function (hireid) {
        $http({
            method: 'GET',
            params: { 'deliveryVehicleHireID': hireid, },
            url: '/Operations/GetDeliveryVehicleHireManifestByVehicleHireID',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                if (response.data.length > 0) {
                    $scope.VehicleManifest = {
                        DeliveryManifestID: response.data[0].DeliveryManifestID,
                        DeliveryVehicleHireID: response.data[0].DeliveryVehicleHireID,
                        ManifestNo: response.data[0].ManifestNo,
                        VehicleID: response.data[0].VehicleID,
                        DriverName: response.data[0].DriverName,
                        DriverMobileNo: response.data[0].DriverMobileNo,
                        LicenseNo: response.data[0].LicenseNo,
                        EstimatedDeliveryDate: response.data[0].EstimatedDeliveryDate,
                        ArrivalTime: response.data[0].ArrivalTime,
                        DepartureTime: response.data[0].DepartureTime,
                        FromLoc: response.data[0].FromLoc,
                        ToLoc: response.data[0].ToLoc,
                        CreatedBy: response.data[0].CreatedBy,
                        Created: $filter('date')($scope.FormatDate(response.data[0].Created), 'dd/MM/yyyy'),
                        ModifiedBy: response.data[0].ModifiedBy,
                        Modified: response.data[0].Modified,
                    };
                    $scope.ManifestSaved = true;
                }
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.SaveDeliveryManifest = function () {

        if ($scope.frmDeliveryManifest.$valid) {
            if ($scope.VehicleManifest.DeliveryManifestID == '') {
                $http({
                    method: 'POST',
                    url: '/Operations/AddDeliveryVehicleHireManifest',
                    data: JSON.stringify($scope.VehicleManifest),
                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
                    .then(function successCallback(response) {
                        console.log(response);

                        Notification.success({ message: "Delivery Vehicle Manifest saved successfully.", delay: 5000 });
                        var manifestID = response.data;

                        $scope.VehicleManifest.DeliveryManifestID = manifestID;

                        $scope.ManifestSaved = true;

                    }, function errorCallback(response) {
                        Notification.error({ message: response.message, delay: 5000 });
                    });
            }
            else {
                $http({
                    method: 'POST',
                    url: '/Operations/UpdateDeliveryVehicleHireManifest',
                    //  data: JSON.stringify($scope.AUDITS),
                    data: JSON.stringify($scope.VehicleManifest),

                    headers: { 'Content-Type': 'application/json; charset=utf-8' }
                })
                    .then(function successCallback(response) {
                        console.log(response);

                        $('#ManifestModal').modal('hide');
                        $scope.ManifestSaved = true;
                        Notification.success({ message: "Delivery Vehicle Manifest updated successfully.", delay: 5000 });

                        $scope.loadDeliveryVehicleManifest($scope.VehicleManifest.DeliveryVehicleHireID);
                        $scope.clearManifest();
                    }, function errorCallback(response) {
                        Notification.error({ message: response.message, delay: 5000 });
                    });
            }



        }
        else {

            Notification.error({ message: "Please fill all the required fields on the form.", delay: 5000 });
            return;
        }
    }
    $scope.ShowDeliveryConsignment = function () {
        $scope.GetConsignmentsForDeliveryManifest();
        $scope.showManifestConsignments = true;
    }
    $scope.GetConsignmentsForDeliveryManifest = function () {

        $scope.activeConsignmentBooking = '';
        var bookings = [];
        $http({
            method: 'GET',
            params: { 'manifestId': $scope.VehicleManifest.DeliveryManifestID, },
            url: '/Operations/GetConsignmentsForDeliveryManifest',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    bookings.push(response.data[i]);
                    bookings[i].checkSelected = false;
                }
                $scope.activeConsignmentBooking = bookings;
                $scope.activeConsignmentBookingCollection = bookings;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.AddDeliveryManifestConsignments = function () {
        var newManifestConsignments = [];
        for (i = 0; i < $scope.activeConsignmentBooking.length; i++) {
            //var traingOpp  = [];
            if ($scope.activeConsignmentBooking[i].checkSelected == true) {

                var mfConsignment = {
                    DeliveryManifestConsignmentID: '',
                    DeliveryManifestID: $scope.VehicleManifest.DeliveryManifestID,
                    ConsignmentID: $scope.activeConsignmentBooking[i].ConsignmentID,
                    CreatedBy: '',
                    Created: '',
                    DeliveryStatus: $scope.ManifestDeliveryStatus
                }
                newManifestConsignments.push(mfConsignment);

            }


        }
        if (newManifestConsignments.length > 0) {
            $http({
                method: 'POST',
                url: '/Operations/AddDeliveryManifestConsignment',
                dataType: 'json',
                traditional: true,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(newManifestConsignments)

            })
                .then(function successCallback(response) {
                    console.log(response);
                    $scope.UpdateDeliveryManifestArticleAndWeight();
                    Notification.success({ message: "Delivery Manifest consignments saved successfully.", delay: 5000 });

                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });

        }
        else {
            Notification.error({ message: "Please select consignments.", delay: 5000 });
        }
    }
    
    //delivery vehicle hire - end
    //Manifest Consignments

    $scope.ManifestConsignment = {
        ManifestConsignmentID: '',
        ManifestID: '',
        ConsignmentID: '',
        CreatedBy: '',
        Created: ''
    }
    $scope.ManifestConsignment = [];
    $scope.ShowConsignment = function () {
        $scope.GetConsignmentsForManifest();
        //$scope.GetManifestConsignments();
        $scope.showManifestConsignments = true;
    }
    $scope.CancelConsignment = function () {

        $scope.activeConsignmentBooking = [];
        $scope.showManifestConsignments = false;
    }

    $scope.GetManifestConsignments = function () {

        $http({
            method: 'GET',
            params: { 'manifestID': $scope.VehicleManifest.ManifestID },
            url: '/Operations/GetLocalManifestConsignments',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
     .then(function successCallback(response) {
         for (i = 0; i < response.data.length; i++) {
             $scope.ManifestConsignment.push(response.data[i]);

             for (j = 0; j < $scope.activeConsignmentBooking.length; j++) {
                 if ($scope.activeConsignmentBooking[j].ConsignmentID == response.data[i].ConsignmentID) {
                     $scope.activeConsignmentBooking[j].checkSelected = true;
                     break;
                 }
             }
         }
       
     },
     function errorCallback(response) {
         alert(response);
     });
    }

    $scope.AddManifestConsignments = function () {

        var newManifestConsignments = [];
        for (i = 0; i < $scope.activeConsignmentBooking.length; i++) {
            //var traingOpp  = [];
            if ($scope.activeConsignmentBooking[i].checkSelected == true) {

                var mfConsignment = {
                    ManifestConsignmentID: '',
                    ManifestID: $scope.VehicleManifest.ManifestID,
                    ConsignmentID: $scope.activeConsignmentBooking[i].ConsignmentID,
                    CreatedBy: '',
                    Created: ''
                }
                newManifestConsignments.push(mfConsignment);
                // traingOpp.push(activetrainingPlanOpps[i]);
            }


        }

        $http({
            method: 'POST',
            url: '/Operations/AddLocalManifestConsignment',
            dataType: 'json',
            traditional: true,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(newManifestConsignments)

        })
         .then(function successCallback(response) {
             console.log(response);
             $scope.UpdateLocalManifestArticleAndWeight();

             Notification.success({ message: "Local Manifest consignments saved successfully.", delay: 5000 });

         }, function errorCallback(response) {
             Notification.error({ message: response.message, delay: 5000 });
         });
        //}
    }

    /*End Manifest*/


}]);