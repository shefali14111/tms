﻿App.controller('UserMaster_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder) {

    $scope.InitUser = function () {
        $scope.UserDetails = {
            UserID: '',
            UserName: '',
            Address: '',
            Pincode: '',
            city: '',
            state: '',
            Nationalty: '',
            EmailID: '',
            Password: '',
            ContactNo: '',
            ZoneID: '',
            BranchID: '',
            RoleID: '',
            ReportingManagerID: '',
            DepartmentID: '',
            HO: ''
        };
    }
    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Administration/GetUserData",
            type: 'GET'

        })
        .withOption('aaSorting', [[2, "desc"]])
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { search: "", searchPlaceholder: "Search By Name..." })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('UserID').withTitle('UserID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('UserName').withTitle('User Name').notSortable(),
        DTColumnBuilder.newColumn('EmailID').withTitle('Email ID'),
        DTColumnBuilder.newColumn('Role').withTitle('Role').notSortable(),
        DTColumnBuilder.newColumn('Branch').withTitle('Branch').notSortable(),
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            var strLinks = '<a ng-click="EditUserData(' + row.UserID + ')" class="pull-left font20 marleft15" > <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
            strLinks = strLinks + '<a class="pull-left marleft15 font20 delete" ng-click="DeleteUser(' + row.UserID + ')" > <i class="fa fa-trash-o" aria-hidden="true"></i></a>';
            return strLinks;

        })
    ];

    $scope.Branches = [];
    $scope.GetBranches = function () {
        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    brnches.push(response.data[i]);
                }
                $scope.Branches = brnches;

            },
                function errorCallback(response) {
                    alert(response);
                });

    }
    $scope.Zones = [];
    $scope.GetZones = function () {
        var zone = [];
        $http({
            method: 'GET',
            url: '/Administration/GetZones',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    zone.push(response.data[i]);
                }
                $scope.Zones = zone;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.Roles = [];
    $scope.GetRoles = function () {
        var role = [];
        $http({
            method: 'GET',
            url: '/Administration/GetRoles',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    role.push(response.data[i]);
                }
                $scope.Roles = role;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.Departments = [];
    $scope.GetDepartments = function () {
        var dep = [];
        $http({
            method: 'GET',
            url: '/Administration/GetDepartments',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    dep.push(response.data[i]);
                }
                $scope.Departments = dep;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.ReportingManagers = [];
    $scope.GetManagers = function () {
        var user = [];
        $http({
            method: 'GET',
            url: '/Administration/GetAdminUsers',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    user.push(response.data[i]);
                }
                $scope.ReportingManagers = user;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetBranches();
    $scope.GetZones();
    $scope.GetRoles();
    $scope.GetDepartments();
    $scope.GetManagers();
    $scope.AddUser = function () {
        $scope.InitUser();
        $scope.isDisableBranchDropdown = true;
        $("#UserModal").modal("show");
    }
    $scope.closeUser = function () {
        $("#UserModal").modal("hide");
    }
    $scope.ChangeRole = function () {

        if ($("#ddlRole option:selected").text().toLowerCase() != 'operation head') {
            $scope.isDisableBranchDropdown = true;
            $scope.UserDetails.BranchID = '';
        }
        else {
            $scope.isDisableBranchDropdown = false;
        }
    }
    $scope.SaveUser = function () {
        if ($scope.frmUser.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/AddUser',
                data: JSON.stringify($scope.UserDetails),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);
                    Notification.success({ message: "User saved successfully.", delay: 5000 });
                    $scope.closeUser();
                    var dtinstance = $scope.dtInstance.DataTable;
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeleteUser = function (UserID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeleteUser',
                params: { 'UserID': UserID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "User deleted successfully.", delay: 5000 });
                    var dtinstance = $scope.dtInstance.DataTable;
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
    $scope.GetUserByID = function (UserId) {
        $scope.EditUser = {
            UserID: '',
            UserName: '',
            Address: '',
            Pincode: '',
            city: '',
            state: '',
            Nationalty: '',
            EmailID: '',
            Password: '',
            ContactNo: '',
            ZoneID: '',
            BranchID: '',
            RoleID: '',
            ReportingManagerID: '',
            DepartmentID: '',
            HO: ''
        };
        var user = [];
        $http({
            method: 'GET',
            url: '/Administration/GetUserByID',
            params: { 'UserID': UserId },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    user.push(response.data[i]);
                }

                $scope.EditUser.UserID = user[0].UserID;
                $scope.EditUser.UserName = user[0].UserName;
                $scope.EditUser.Address = user[0].Address;
                $scope.EditUser.Pincode = user[0].Pincode;
                $scope.EditUser.city = user[0].city;
                $scope.EditUser.state = user[0].state;
                $scope.EditUser.Nationalty = user[0].Nationalty;
                $scope.EditUser.EmailID = user[0].EmailID;
                $scope.EditUser.Password = user[0].Password;
                $scope.EditUser.ContactNo = user[0].ContactNo;
                $scope.EditUser.ZoneID = user[0].ZoneID;
                $scope.EditUser.BranchID = user[0].BranchID;
                $scope.EditUser.RoleID = user[0].RoleID;
                $scope.EditUser.ReportingManagerID = user[0].ReportingManagerID;
                $scope.EditUser.DepartmentID = user[0].DepartmentID;
                $scope.EditUser.HO = user[0].HO;
                if ($scope.EditUser.BranchID != '' && $scope.EditUser.BranchID != '0')
                    $scope.isDisableEditBranchDropdown = false;
                else
                    $scope.isDisableEditBranchDropdown = true;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.closeEditUser = function () {
        $("#EditUserModal").modal("hide");
    }
    $scope.EditUserData = function (UserID) {
        $scope.GetUserByID(UserID);
        $("#EditUserModal").modal("show");
       
    }
    $scope.ChangeEditRole = function () {
        if ($("#ddlEditRole option:selected").text().toLowerCase() != 'operation head') {
            $scope.isDisableEditBranchDropdown = true;
            $scope.EditUser.BranchID = '';
        }
        else {
            $scope.isDisableEditBranchDropdown = false;
        }
    }
    $scope.UpdateUser = function () {

        if ($scope.frmEditUser.$valid) {
            $http({
                method: 'POST',
                url: '/Administration/UpdateUser',
                data: JSON.stringify($scope.EditUser),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeEditUser();
                    Notification.success({ message: "User updated successfully.", delay: 5000 });
                    var dtinstance = $scope.dtInstance.DataTable;
                   
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);

App.controller('UserRoleMaster_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder) {
 
    $scope.InitRole = function () {
        $scope.Role = {
            RoleID: '',
            RoleName: ''
        };
    }

    $scope.Roles = [];
    $scope.GetRoles = function () {
        var role = [];
        $http({
            method: 'GET',
            url: '/Administration/GetRoles',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    role.push(response.data[i]);
                }
                $scope.Roles = role;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
  
    $scope.AddRole = function () {
        $scope.InitRole();
        $("#RoleModal").modal("show");
    }
    $scope.closeRole = function () {
        $("#RoleModal").modal("hide");
    }
  
    $scope.SaveRole = function () {
        if ($scope.frmRole.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/AddRole',
                data: JSON.stringify($scope.Role),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);
                    Notification.success({ message: "Role saved successfully.", delay: 5000 });
                    $scope.closeRole();
                    $scope.GetRoles();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeleteRole = function (RoleID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeleteRole',
                params: { 'RoleID': RoleID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Role deleted successfully.", delay: 5000 });
                    $scope.GetRoles();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
   
    $scope.closeEditRole = function () {
        $("#EditRoleModal").modal("hide");
    }
    $scope.EditRoleData = function (RoleID,RoleName) {
        $scope.EditRole = {
            RoleID: RoleID,
            RoleName: RoleName
        };
        $("#EditRoleModal").modal("show");

    }
  
    $scope.UpdateRole = function () {

        if ($scope.frmEditRole.$valid) {
            $http({
                method: 'POST',
                url: '/Administration/UpdateRole',
                data: JSON.stringify($scope.EditRole),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeEditRole();
                    Notification.success({ message: "Role updated successfully.", delay: 5000 });
                    $scope.GetRoles();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);

App.controller('PackagingMaster_ngController', ["$scope", "$http", "Notification", function ($scope, $http, Notification) {

    $scope.InitPackaging = function () {
        $scope.PackagingData = {
            PackagingID:'',
            Packaging: ''
        };
    }

    $scope.Packagings = [];
    $scope.GetPackaging = function () {
        var packagings = [];
        $http({
            method: 'GET',
            url: '/Administration/GetPackaging',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    packagings.push(response.data[i]);
                }
                $scope.Packagings = packagings;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }

    $scope.AddPackaging = function () {
        $scope.InitPackaging();
        $("#PackagingModal").modal("show");
    }
    $scope.closePackaging = function () {
        $("#PackagingModal").modal("hide");
    }

    $scope.SavePackaging = function () {
        if ($scope.frmPackaging.$valid) {
          
            $http({
                method: 'POST',
                url: '/Administration/AddPackaging',
                data: JSON.stringify($scope.PackagingData),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);
                    Notification.success({ message: "Packaging saved successfully.", delay: 5000 });
                    $scope.closePackaging();
                    $scope.GetPackaging();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeletePackaging = function (PackagingID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeletePackaging',
                params: { 'PackagingID': PackagingID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Packaging deleted successfully.", delay: 5000 });
                    $scope.GetPackaging();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }

    $scope.closeEditPackaging = function () {
        $("#EditPackagingModal").modal("hide");
    }
    $scope.EditPackagingData = function (PackagingID, Packaging) {
      
        $scope.EditPackaging = {
            PackagingID: PackagingID,
            Packaging: Packaging
        };
        $("#EditPackagingModal").modal("show");

    }

    $scope.UpdatePackaging = function () {

        if ($scope.frmEditPackaging.$valid) {
           
            $http({
                method: 'POST',
                url: '/Administration/UpdatePackaging',
                data: JSON.stringify($scope.EditPackaging),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeEditPackaging();
                    Notification.success({ message: "Packaging updated successfully.", delay: 5000 });
                    $scope.GetPackaging();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);

App.controller('ODAMaster_ngController', ["$scope", "$http", "Notification", function ($scope, $http, Notification) {

    $scope.InitODA = function () {
        $scope.ODA = {
            ODAID: '',
            PinCode: '',
            ODAAmount: ''
        };
    }

    $scope.ODAs = [];
    $scope.GetODA = function () {
        var odas = [];
        $http({
            method: 'GET',
            url: '/Administration/GetODA',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    odas.push(response.data[i]);
                }
                $scope.ODAs = odas;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }

    $scope.AddODA = function () {
        $scope.InitODA();
        $("#ODAModal").modal("show");
    }
    $scope.closeODA = function () {
        $("#ODAModal").modal("hide");
    }

    $scope.SaveODA = function () {
        if ($scope.frmODA.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/AddODA',
                data: JSON.stringify($scope.ODA),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);
                    Notification.success({ message: "ODA saved successfully.", delay: 5000 });
                    $scope.closeODA();
                    $scope.GetODA();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeleteODA = function (ODAID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeleteODA',
                params: { 'ODAID': ODAID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "ODA deleted successfully.", delay: 5000 });
                    $scope.GetODA();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }

    $scope.closeEditODA = function () {
        $("#EditODAModal").modal("hide");
    }
    $scope.EditODAData = function (ODAID, Pincode, Amount) {

        $scope.EditODA = {
            ODAID: ODAID,
            PinCode: Pincode,
            ODAAmount: Amount
        };
        $("#EditODAModal").modal("show");

    }

    $scope.UpdateODA = function () {

        if ($scope.frmEditRole.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/UpdateODA',
                data: JSON.stringify($scope.EditODA),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeEditODA();
                    Notification.success({ message: "ODA updated successfully.", delay: 5000 });
                    $scope.GetODA();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);

App.controller('PaymentTypeMaster_ngController', ["$scope", "$http", "Notification", function ($scope, $http, Notification) {

    $scope.InitPaymentType = function () {
        $scope.PaymentType = {
            PaymentTypeID: '',
            PaymentType: '',
            Description: ''
        };
    }

    $scope.PaymentTypes = [];
    $scope.GetPaymentType = function () {
        var paymenttypes = [];
        $http({
            method: 'GET',
            url: '/Administration/GetPaymentType',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    paymenttypes.push(response.data[i]);
                }
                $scope.PaymentTypes = paymenttypes;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }

    $scope.AddPaymentType = function () {
        $scope.InitPaymentType();
        $("#PaymentTypeModal").modal("show");
    }
    $scope.closePaymentType = function () {
        $("#PaymentTypeModal").modal("hide");
    }

    $scope.SavePaymentType = function () {
        if ($scope.frmPaymentType.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/AddPaymentType',
                data: JSON.stringify($scope.PaymentType),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);
                    Notification.success({ message: "Payment Type saved successfully.", delay: 5000 });
                    $scope.closePaymentType();
                    $scope.GetPaymentType();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeletePaymentType = function (PaymentTypeID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeletePaymentType',
                params: { 'PaymentTypeID': PaymentTypeID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Payment Type deleted successfully.", delay: 5000 });
                    $scope.GetPaymentType();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }

    $scope.closeEditPaymentType = function () {
        $("#EditPaymentTypeModal").modal("hide");
    }
    $scope.EditPaymentTypeData = function (PaymentTypeID, PaymentType, Description) {

        $scope.EditPaymentType = {
            PaymentTypeID: PaymentTypeID,
            PaymentType: PaymentType,
            Description: Description
        };
        $("#EditPaymentTypeModal").modal("show");

    }

    $scope.UpdatePaymentType = function () {

        if ($scope.frmEditPaymentType.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/UpdatePaymentType',
                data: JSON.stringify($scope.EditPaymentType),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeEditPaymentType();
                    Notification.success({ message: "Payment Type updated successfully.", delay: 5000 });
                    $scope.GetPaymentType();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);

App.controller('VehicleHirePaymentTypeMaster_ngController', ["$scope", "$http", "Notification", function ($scope, $http, Notification) {

    $scope.InitPaymentType = function () {
        $scope.PaymentType = {
            PaymentTypeID: '',
            PaymentType: '',
            Description: ''
        };
    }

    $scope.PaymentTypes = [];
    $scope.GetPaymentType = function () {
        var paymenttypes = [];
        $http({
            method: 'GET',
            url: '/Administration/GetVehicleHirePaymentType',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    paymenttypes.push(response.data[i]);
                }
                $scope.PaymentTypes = paymenttypes;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }

    $scope.AddPaymentType = function () {
        $scope.InitPaymentType();
        $("#PaymentTypeModal").modal("show");
    }
    $scope.closePaymentType = function () {
        $("#PaymentTypeModal").modal("hide");
    }

    $scope.SavePaymentType = function () {
        if ($scope.frmPaymentType.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/AddVehicleHirePaymentType',
                data: JSON.stringify($scope.PaymentType),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);
                    Notification.success({ message: "Payment Type saved successfully.", delay: 5000 });
                    $scope.closePaymentType();
                    $scope.GetPaymentType();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeletePaymentType = function (PaymentTypeID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeleteVehicleHirePaymentType',
                params: { 'PaymentTypeID': PaymentTypeID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Payment Type deleted successfully.", delay: 5000 });
                    $scope.GetPaymentType();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }

    $scope.closeEditPaymentType = function () {
        $("#EditPaymentTypeModal").modal("hide");
    }
    $scope.EditPaymentTypeData = function (PaymentTypeID, PaymentType, Description) {

        $scope.EditPaymentType = {
            PaymentTypeID: PaymentTypeID,
            PaymentType: PaymentType,
            Description: Description
        };
        $("#EditPaymentTypeModal").modal("show");

    }

    $scope.UpdatePaymentType = function () {

        if ($scope.frmEditPaymentType.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/UpdateVehicleHirePaymentType',
                data: JSON.stringify($scope.EditPaymentType),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeEditPaymentType();
                    Notification.success({ message: "Payment Type updated successfully.", delay: 5000 });
                    $scope.GetPaymentType();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);

App.controller('CargoTypeMaster_ngController', ["$scope", "$http", "Notification", function ($scope, $http, Notification) {

    $scope.InitCargoType = function () {
        $scope.CargoType = {
            CargoTypeID: '',
            CargoType: '',
            Description: ''
        };
    }

    $scope.CargoTypes = [];
    $scope.GetCargoType = function () {
        var cargotypes = [];
        $http({
            method: 'GET',
            url: '/Administration/GetCargoType',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    cargotypes.push(response.data[i]);
                }
                $scope.CargoTypes = cargotypes;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }

    $scope.AddCargoType = function () {
        $scope.InitCargoType();
        $("#CargoTypeModal").modal("show");
    }
    $scope.closeCargoType = function () {
        $("#CargoTypeModal").modal("hide");
    }

    $scope.SaveCargoType = function () {
        if ($scope.frmCargoType.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/AddCargoType',
                data: JSON.stringify($scope.CargoType),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);
                    Notification.success({ message: "Cargo Type saved successfully.", delay: 5000 });
                    $scope.closeCargoType();
                    $scope.GetCargoType();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeleteCargoType = function (CargoTypeID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeleteCargoType',
                params: { 'CargoTypeID': CargoTypeID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Cargo Type deleted successfully.", delay: 5000 });
                    $scope.GetCargoType();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }

    $scope.closeEditCargoType = function () {
        $("#EditCargoTypeModal").modal("hide");
    }
    $scope.EditCargoTypeData = function (CargoTypeID, CargoType, Description) {

        $scope.EditCargoType = {
            CargoTypeID: CargoTypeID,
            CargoType: CargoType,
            Description: Description
        };
        $("#EditCargoTypeModal").modal("show");

    }

    $scope.UpdateCargoType = function () {

        if ($scope.frmEditCargoType.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/UpdateCargoType',
                data: JSON.stringify($scope.EditCargoType),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeEditCargoType();
                    Notification.success({ message: "Cargo Type updated successfully.", delay: 5000 });
                    $scope.GetCargoType();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);

App.controller('ChargeTypeMaster_ngController', ["$scope", "$http", "Notification", function ($scope, $http, Notification) {

    $scope.InitChargeType = function () {
        $scope.ChargeType = {
            ChargeTypeID: '',
            ChargeType: ''
        };
    }

    $scope.ChargeTypes = [];
    $scope.GetChargeType = function () {
        var chargetypes = [];
        $http({
            method: 'GET',
            url: '/Administration/GetChargeType',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    chargetypes.push(response.data[i]);
                }
                $scope.ChargeTypes = chargetypes;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }

    $scope.AddChargeType = function () {
        $scope.InitChargeType();
        $("#ChargeTypeModal").modal("show");
    }
    $scope.closeChargeType = function () {
        $("#ChargeTypeModal").modal("hide");
    }

    $scope.SaveChargeType = function () {
        if ($scope.frmChargeType.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/AddChargeType',
                data: JSON.stringify($scope.ChargeType),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);
                    Notification.success({ message: "Charge Type saved successfully.", delay: 5000 });
                    $scope.closeChargeType();
                    $scope.GetChargeType();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeleteChargeType = function (ChargeTypeID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeleteChargeType',
                params: { 'ChargeTypeID': ChargeTypeID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Charge Type deleted successfully.", delay: 5000 });
                    $scope.GetChargeType();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }

    $scope.closeEditChargeType = function () {
        $("#EditChargeTypeModal").modal("hide");
    }
    $scope.EditChargeTypeData = function (ChargeTypeID, ChargeType) {

        $scope.EditChargeType = {
            ChargeTypeID: ChargeTypeID,
            ChargeType: ChargeType
        };
        $("#EditChargeTypeModal").modal("show");

    }

    $scope.UpdateChargeType = function () {

        if ($scope.frmEditChargeType.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/UpdateChargeType',
                data: JSON.stringify($scope.EditChargeType),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeEditChargeType();
                    Notification.success({ message: "Charge Type updated successfully.", delay: 5000 });
                    $scope.GetChargeType();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);

App.controller('AccountMaster_ngController', ["$scope", "$http", "Notification", function ($scope, $http, Notification) {

    $scope.InitAccount = function () {
        $scope.Account = {
            AccountID: '',
            AccountHead: ''
        };
    }

    $scope.Accounts = [];
    $scope.GetAccounts = function () {
        var accounts = [];
        $http({
            method: 'GET',
            url: '/Administration/GetAccount',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    accounts.push(response.data[i]);
                }
                $scope.Accounts = accounts;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }

    $scope.AddAccount = function () {
        $scope.InitAccount();
        $("#AccountModal").modal("show");
    }
    $scope.closeAccount = function () {
        $("#AccountModal").modal("hide");
    }

    $scope.SaveAccount = function () {
        if ($scope.frmAccount.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/AddAccount',
                data: JSON.stringify($scope.Account),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);
                    Notification.success({ message: "Account saved successfully.", delay: 5000 });
                    $scope.closeAccount();
                    $scope.GetAccounts();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeleteAccount = function (AccountID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeleteAccount',
                params: { 'AccountID': AccountID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Account deleted successfully.", delay: 5000 });
                    $scope.GetAccounts();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }

    $scope.closeEditAccount = function () {
        $("#EditAccountModal").modal("hide");
    }
    $scope.EditAccountData = function (AccountID, AccountHead) {
        $scope.EditAccount = {
            AccountID: AccountID,
            AccountHead: AccountHead
        };
        $("#EditAccountModal").modal("show");

    }

    $scope.UpdateAccount = function () {

        if ($scope.frmEditAccount.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/UpdateAccount',
                data: JSON.stringify($scope.EditAccount),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeEditAccount();
                    Notification.success({ message: "Account updated successfully.", delay: 5000 });
                    $scope.GetAccounts();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);

App.controller('AttributeMaster_ngController', ["$scope", "$http", "Notification", function ($scope, $http, Notification) {

    $scope.InitAttribute = function () {
        $scope.Attribute = {
            AttributeId: '',
            Name: '',
            Value:''
        };
    }

    $scope.Attributes = [];
    $scope.GetAttributes = function () {
        var attributes = [];
        $http({
            method: 'GET',
            url: '/Administration/GetAttribute',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    attributes.push(response.data[i]);
                }
                $scope.Attributes = attributes;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }

    $scope.AddAttribute = function () {
        $scope.InitAttribute();
        $("#AttributeModal").modal("show");
    }
    $scope.closeAttribute = function () {
        $("#AttributeModal").modal("hide");
    }

    $scope.SaveAttribute = function () {
        if ($scope.frmAttribute.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/AddAttribute',
                data: JSON.stringify($scope.Attribute),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);
                    Notification.success({ message: "Attribute saved successfully.", delay: 5000 });
                    $scope.closeAttribute();
                    $scope.GetAttributes();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeleteAttribute = function (AttributeID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeleteAttribute',
                params: { 'AttributeID': AttributeID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Attribute deleted successfully.", delay: 5000 });
                    $scope.GetAttributes();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }

    $scope.closeEditAttribute= function () {
        $("#EditAttributeModal").modal("hide");
    }
    $scope.EditAttributeData = function (AttributeID, Name, Value) {

        $scope.EditAttribute = {
            AttributeId: AttributeID,
            Name: Name,
            Value: Value
        };
        $("#EditAttributeModal").modal("show");

    }

    $scope.UpdateAttribute = function () {

        if ($scope.frmEditAttribute.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/UpdateAttribute',
                data: JSON.stringify($scope.EditAttribute),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeEditAttribute();
                    Notification.success({ message: "Attribute updated successfully.", delay: 5000 });
                    $scope.GetAttributes();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);

App.controller('GoodsMaster_ngController', ["$scope", "$http", "Notification", function ($scope, $http, Notification) {

    $scope.InitGoods = function () {
        $scope.GoodsData = {
            GoodsID: '',
            GoodsDescription: '',
            GoodsCode: ''
        };
    }

    $scope.Goods = [];
    $scope.GetGoods = function () {
        var goods = [];
        $http({
            method: 'GET',
            url: '/Administration/GetGoods',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    goods.push(response.data[i]);
                }
                $scope.Goods = goods;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }

    $scope.AddGoods = function () {
        $scope.InitGoods();
        $("#GoodsModal").modal("show");
    }
    $scope.closeGoods = function () {
        $("#GoodsModal").modal("hide");
    }

    $scope.SaveGoods= function () {
        if ($scope.frmGoods.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/AddGoods',
                data: JSON.stringify($scope.GoodsData),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);
                    Notification.success({ message: "Goods saved successfully.", delay: 5000 });
                    $scope.closeGoods();
                    $scope.GetGoods();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeleteGoods = function (GoodsID) {
        var state = confirm("Do you want to delete?");
      
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeleteGoods',
                params: { 'GoodsID': GoodsID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Goods deleted successfully.", delay: 5000 });
                    $scope.GetGoods();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }

    $scope.closeEditGoods = function () {
        $("#EditGoodsModal").modal("hide");
    }
    $scope.EditGoodsData = function (GoodsID, Code, Description) {

        $scope.EditGoods = {
            GoodsID: GoodsID,
            GoodsDescription: Description,
            GoodsCode: Code
        };
        $("#EditGoodsModal").modal("show");

    }

    $scope.UpdateGoods = function () {

        if ($scope.frmEditGoods.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/UpdateGoods',
                data: JSON.stringify($scope.EditGoods),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeEditGoods();
                    Notification.success({ message: "Goods updated successfully.", delay: 5000 });
                    $scope.GetGoods();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);

App.controller('BranchMaster_ngController', ["$scope", "$http", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, Notification,$compile,DTOptionsBuilder, DTColumnBuilder) {

    $scope.InitBranch = function () {
        $scope.Branch = {
            BranchID: '',
            BranchName: '',
            ZoneID: '',
            BranchCode: '',
            Address: '',
            Pincode: '',
            city: '',
            state: '',
            Nationalty: '',
            ContactPerson: '',
            MobileNo: '',
            Email: '',
            Website: '',
            CashLimit: ''
        };
    }
    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Administration/GetBranchData",
            type: 'GET'

        })
        .withOption('aaSorting', [[2, "desc"]])
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { search: "", searchPlaceholder: "Search By Name..." })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('BranchID').withTitle('BranchID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('BranchName').withTitle('Branch Name').notSortable(),
        DTColumnBuilder.newColumn('BranchCode').withTitle('Branch Code').notSortable(),
        DTColumnBuilder.newColumn('city').withTitle('city').notSortable(),
        DTColumnBuilder.newColumn('state').withTitle('state').notSortable(),
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            var strLinks = '<a ng-click="EditBranchData(' + row.BranchID + ')" class="pull-left font20 marleft15" > <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
            strLinks = strLinks + '<a class="pull-left marleft15 font20 delete" ng-click="DeleteBranch(' + row.BranchID + ')" > <i class="fa fa-trash-o" aria-hidden="true"></i></a>';
            return strLinks;

        })
    ];
   
    $scope.BrancheNames = [];
    $scope.GetBrancheNames = function () {
        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    if (brnches.indexOf(response.data[i]["BranchName"]) == -1) {
                        brnches.push(response.data[i]["BranchName"]);
                    }
                }
                $scope.BrancheNames = brnches;
               
            },
                function errorCallback(response) {
                    alert(response);
                });

    }
   
    $scope.GetBrancheNames();
    $scope.complete = function (string) {
        if (string == '' || string == undefined) {
            $scope.filterBranch = null;
        }
        else {
            var output = [];
            angular.forEach($scope.BrancheNames, function (branch) {
                if (branch.toLowerCase().startsWith(string.toLowerCase())) {
                    output.push(branch);
                }
            });
            $scope.filterBranch = output;
        }
    }
    $scope.fillTextbox = function (string) {
        $scope.branchdata = string;
        $scope.filterBranch = null;
    }
    $scope.Zones = [];
    $scope.GetZones = function () {
        var zone = [];
        $http({
            method: 'GET',
            url: '/Administration/GetZones',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    zone.push(response.data[i]);
                }
                $scope.Zones = zone;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetZones();
    $scope.GetBranches = function () {
        var dtinstance = $scope.dtInstance.DataTable;
        dtinstance.ajax.reload(function (json) { }, true);
    }

    $scope.AddBranch = function () {
        $scope.InitBranch();
        $("#BranchModal").modal("show");
    }
    $scope.closeBranch = function () {
        $("#BranchModal").modal("hide");
    }

    $scope.SaveBranch = function () {
        if ($scope.frmBranch.$valid) {
            $scope.Branch.BranchName = $scope.branchdata;
            $http({
                method: 'POST',
                url: '/Administration/AddBranch',
                data: JSON.stringify($scope.Branch),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    if (response.data == "Succeess") {
                        Notification.success({ message: "Branch saved successfully.", delay: 5000 });
                        $scope.closeBranch();
                        $scope.GetBranches();
                    }
                    if (response.data == "AlreadyExist") {
                        Notification.warning({ message: "Branch alredy exist.", delay: 5000 });
                    }
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeleteBranch = function (BranchID) {
        var state = confirm("Do you want to delete?");

        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeleteBranch',
                params: { 'BranchID': BranchID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Branch deleted successfully.", delay: 5000 });
                    $scope.GetBranches();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
    $scope.GetBranchByID = function (BranchID) {
        $scope.EditBranch = {
            BranchID: '',
            BranchName: '',
            ZoneID: '',
            BranchCode: '',
            Address: '',
            Pincode: '',
            city: '',
            state: '',
            Nationalty: '',
            ContactPerson: '',
            MobileNo: '',
            Email: '',
            Website: '',
            CashLimit: ''  
        };
        var branch = [];
        $http({
            method: 'GET',
            url: '/Administration/GetBranchByID',
            params: { 'BranchID': BranchID },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    branch.push(response.data[i]);
                }

                $scope.EditBranch.BranchID = branch[0].BranchID;
                $scope.EditBranch.BranchName = branch[0].BranchName;
                $scope.EditBranch.ZoneID = branch[0].ZoneID;
                $scope.EditBranch.BranchCode = branch[0].BranchCode;
                $scope.EditBranch.Address = branch[0].Address;
                $scope.EditBranch.Pincode = branch[0].Pincode;
                $scope.EditBranch.city = branch[0].city;
                $scope.EditBranch.state = branch[0].state;
                $scope.EditBranch.Nationalty = branch[0].Nationalty;
                $scope.EditBranch.ContactPerson = branch[0].ContactPerson;
                $scope.EditBranch.MobileNo = branch[0].MobileNo;
                $scope.EditBranch.Email = branch[0].Email;
                $scope.EditBranch.Website = branch[0].Website;
                $scope.EditBranch.CashLimit = branch[0].CashLimit;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.closeEditBranch = function () {
        $("#EditBranchModal").modal("hide");
    }
    $scope.EditBranchData = function (BranchID) {

        $scope.GetBranchByID(BranchID);
        $("#EditBranchModal").modal("show");

    }

    $scope.UpdateBranch = function () {

        if ($scope.frmEditBranch.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/UpdateBranch',
                data: JSON.stringify($scope.EditBranch),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeEditBranch();
                    Notification.success({ message: "Branch updated successfully.", delay: 5000 });
                    $scope.GetBranches();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
   
}]);

App.controller('OtherBranchMaster_ngController', ["$scope", "$http", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, Notification,$compile, DTOptionsBuilder, DTColumnBuilder) {

    $scope.InitBranch = function () {
        $scope.Branch = {
            OtherBranchID: '',
            BranchName: '',
            ZoneID: '',
            BranchCode: '',
            Address: '',
            Pincode: '',
            city: '',
            state: '',
            Nationalty: '',
            ContactPerson: '',
            MobileNo: '',
            Email: '',
            Website: '',
            CashLimit: ''
        };
    }
    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Administration/GetOtherBranchData",
            type: 'GET'

        })
        .withOption('aaSorting', [[2, "desc"]])
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { search: "", searchPlaceholder: "Search By Name..." })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('OtherBranchID').withTitle('OtherBranchID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('BranchName').withTitle('Branch Name').notSortable(),
        DTColumnBuilder.newColumn('BranchCode').withTitle('Branch Code').notSortable(),
        DTColumnBuilder.newColumn('city').withTitle('city').notSortable(),
        DTColumnBuilder.newColumn('state').withTitle('state').notSortable(),
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            var strLinks = '<a ng-click="EditBranchData(' + row.OtherBranchID + ')" class="pull-left font20 marleft15" > <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
            strLinks = strLinks + '<a class="pull-left marleft15 font20 delete" ng-click="DeleteBranch(' + row.OtherBranchID + ')" > <i class="fa fa-trash-o" aria-hidden="true"></i></a>';
            return strLinks;

        })
    ];
   
    $scope.Zones = [];
    $scope.GetZones = function () {
        var zone = [];
        $http({
            method: 'GET',
            url: '/Administration/GetZones',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    zone.push(response.data[i]);
                }
                $scope.Zones = zone;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetZones();
    $scope.BrancheNames = [];
    $scope.GetBrancheNames = function () {
        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    if (brnches.indexOf(response.data[i]["BranchName"]) == -1) {
                        brnches.push(response.data[i]["BranchName"]);
                    }
                }
                $scope.BrancheNames = brnches;

            },
                function errorCallback(response) {
                    alert(response);
                });

    }

    $scope.GetBrancheNames();
    $scope.complete = function (string) {
        if (string == '' || string == undefined) {
            $scope.filterBranch = null;
        }
        else {
            var output = [];
            angular.forEach($scope.BrancheNames, function (branch) {
                if (branch.toLowerCase().startsWith(string.toLowerCase())) {
                    output.push(branch);
                }
            });
            $scope.filterBranch = output;
        }
    }
    $scope.fillTextbox = function (string) {
        $scope.branchdata = string;
        $scope.filterBranch = null;
    }
    $scope.GetBranches = function () {
        var dtinstance = $scope.dtInstance.DataTable;
        dtinstance.ajax.reload(function (json) { }, true);
    }

    $scope.AddBranch = function () {
        $scope.InitBranch();
        $("#BranchModal").modal("show");
    }
    $scope.closeBranch = function () {
        $("#BranchModal").modal("hide");
    }

    $scope.SaveBranch = function () {
        if ($scope.frmBranch.$valid) {
            $scope.Branch.BranchName = $scope.branchdata;
            $http({
                method: 'POST',
                url: '/Administration/AddOtherBranch',
                data: JSON.stringify($scope.Branch),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    if (response.data == "Succeess") {
                        Notification.success({ message: "Branch saved successfully.", delay: 5000 });
                        $scope.closeBranch();
                        $scope.GetBranches();
                    }
                    if (response.data == "AlreadyExist") {
                        Notification.warning({ message: "Branch alredy exist.", delay: 5000 });
                    }
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeleteBranch = function (OtherBranchID) {
        var state = confirm("Do you want to delete?");

        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeleteOtherBranch',
                params: { 'OtherBranchID': OtherBranchID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Branch deleted successfully.", delay: 5000 });
                    $scope.GetBranches();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
    $scope.GetBranchByID = function (OtherBranchID) {
        $scope.EditBranch = {
            OtherBranchID: '',
            BranchName: '',
            ZoneID: '',
            BranchCode: '',
            Address: '',
            Pincode: '',
            city: '',
            state: '',
            Nationalty: '',
            ContactPerson: '',
            MobileNo: '',
            Email: '',
            Website: '',
            CashLimit: ''
        };
        var branch = [];
        $http({
            method: 'GET',
            url: '/Administration/GetOtherBranchByID',
            params: { 'OtherBranchID': OtherBranchID },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    branch.push(response.data[i]);
                }

                $scope.EditBranch.OtherBranchID = branch[0].OtherBranchID;
                $scope.EditBranch.BranchName = branch[0].BranchName;
                $scope.EditBranch.ZoneID = branch[0].ZoneID;
                $scope.EditBranch.BranchCode = branch[0].BranchCode;
                $scope.EditBranch.Address = branch[0].Address;
                $scope.EditBranch.Pincode = branch[0].Pincode;
                $scope.EditBranch.city = branch[0].city;
                $scope.EditBranch.state = branch[0].state;
                $scope.EditBranch.Nationalty = branch[0].Nationalty;
                $scope.EditBranch.ContactPerson = branch[0].ContactPerson;
                $scope.EditBranch.MobileNo = branch[0].MobileNo;
                $scope.EditBranch.Email = branch[0].Email;
                $scope.EditBranch.Website = branch[0].Website;
                $scope.EditBranch.CashLimit = branch[0].CashLimit;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.closeEditBranch = function () {
        $("#EditBranchModal").modal("hide");
    }
    $scope.EditBranchData = function (BranchID) {

        $scope.GetBranchByID(BranchID);
        $("#EditBranchModal").modal("show");

    }

    $scope.UpdateBranch = function () {

        if ($scope.frmEditBranch.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/UpdateOtherBranch',
                data: JSON.stringify($scope.EditBranch),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeEditBranch();
                    Notification.success({ message: "Branch updated successfully.", delay: 5000 });
                    $scope.GetBranches();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }

}]);

App.controller('OtherVendorMaster_ngController', ["$scope", "$http", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, Notification, $compile, DTOptionsBuilder, DTColumnBuilder) {

    $scope.InitVendor = function () {
        $scope.Vendordata = '';
        $scope.Vendor = {
            VendorID: '',
            VendorName: '',
            VendorCode: '',
            City: '',
            State: '',
            Country: '',
            Address: '',
            PostalCode: '',
            ContactNo: ''
        };
    }
    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Administration/GetOtherVendorData",
            type: 'GET'

        })
        .withOption('aaSorting', [[2, "desc"]])
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { search: "", searchPlaceholder: "Search By Name..." })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('VendorID').withTitle('VendorID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('VendorName').withTitle('Vendor Name').notSortable(),
        DTColumnBuilder.newColumn('VendorCode').withTitle('Vendor Code').notSortable(),
        DTColumnBuilder.newColumn('City').withTitle('city').notSortable(),
        DTColumnBuilder.newColumn('State').withTitle('state').notSortable(),
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            var strLinks = '<a ng-click="EditVendorData(' + row.VendorID + ')" class="pull-left font20 marleft15" > <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
            strLinks = strLinks + '<a class="pull-left marleft15 font20 delete" ng-click="DeleteVendor(' + row.VendorID + ')" > <i class="fa fa-trash-o" aria-hidden="true"></i></a>';
            return strLinks;

        })
    ];

    $scope.VendorNames = [];
    $scope.GetVendorNames = function () {
        var vendors = [];
        $http({
            method: 'GET',
            url: '/Operations/GetOtherVendors',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    if (vendors.indexOf(response.data[i]["VendorName"]) == -1) {
                        vendors.push(response.data[i]["VendorName"]);
                    }
                }
                $scope.VendorNames = vendors;

            },
                function errorCallback(response) {
                    alert(response);
                });

    }

    $scope.GetVendorNames();
    $scope.complete = function (string) {
        if (string == '' || string == undefined) {
            $scope.filterVendor = null;
        }
        else {
            var output = [];
            angular.forEach($scope.VendorNames, function (Vendor) {
                if (Vendor.toLowerCase().startsWith(string.toLowerCase())) {
                    output.push(Vendor);
                }
            });
            $scope.filterVendor = output;
        }
    }
    $scope.fillTextbox = function (string) {
        $scope.Vendordata = string;
        $scope.filterVendor = null;
    }
    $scope.completeEdit = function (string) {
        if (string == '' || string == undefined) {
            $scope.filterEditVendor = null;
        }
        else {
            var output = [];
            angular.forEach($scope.VendorNames, function (Vendor) {
                if (Vendor.toLowerCase().startsWith(string.toLowerCase())) {
                    output.push(Vendor);
                }
            });
            $scope.filterEditVendor = output;
        }
    }
    $scope.fillEditTextbox = function (string) {
        $scope.editVendordata = string;
        $scope.filterEditVendor = null;
    }
    $scope.GetVendors = function () {
        var dtinstance = $scope.dtInstance.DataTable;
        dtinstance.ajax.reload(function (json) { }, true);
    }

    $scope.AddVendor = function () {
        $scope.InitVendor();
        $("#VendorModal").modal("show");
    }
    $scope.closeVendor = function () {
        $("#VendorModal").modal("hide");
    }

    $scope.SaveVendor = function () {
        if ($scope.frmVendor.$valid) {
            $scope.Vendor.VendorName = $scope.Vendordata;
            $http({
                method: 'POST',
                url: '/Administration/AddOtherVendor',
                data: JSON.stringify($scope.Vendor),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    if (response.data == "Succeess") {
                        Notification.success({ message: "Vendor saved successfully.", delay: 5000 });
                        $scope.closeVendor();
                        $scope.GetVendors();
                    }
                    if (response.data == "AlreadyExist") {
                        Notification.warning({ message: "Vendor alredy exist.", delay: 5000 });
                    }
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeleteVendor = function (VendorID) {
        var state = confirm("Do you want to delete?");

        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeleteOtherVendor',
                params: { 'VendorID': VendorID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Vendor deleted successfully.", delay: 5000 });
                    $scope.GetVendors();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
    $scope.GetVendorByID = function (VendorID) {
        $scope.EditVendor = {
            VendorID: '',
            VendorName: '',
            VendorCode: '',
            City: '',
            State: '',
            Country: '',
            Address: '',
            PostalCode: '',
            ContactNo: ''
        };
        var Vendor = [];
        $http({
            method: 'GET',
            url: '/Administration/GetOtherVendorByID',
            params: { 'VendorID': VendorID },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    Vendor.push(response.data[i]);
                }
                $scope.editVendordata = Vendor[0].VendorName;
                $scope.EditVendor.VendorID = Vendor[0].VendorID;
                $scope.EditVendor.VendorName = Vendor[0].VendorName;
                $scope.EditVendor.City = Vendor[0].City;
                $scope.EditVendor.VendorCode = Vendor[0].VendorCode;
                $scope.EditVendor.State = Vendor[0].State;
                $scope.EditVendor.Country = Vendor[0].Country;
                $scope.EditVendor.Address = Vendor[0].Address;
                $scope.EditVendor.PostalCode = Vendor[0].PostalCode;
                $scope.EditVendor.ContactNo = Vendor[0].ContactNo;
               
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.closeEditVendor = function () {
        $("#EditVendorModal").modal("hide");
    }
    $scope.EditVendorData = function (VendorID) {

        $scope.GetVendorByID(VendorID);
        $("#EditVendorModal").modal("show");

    }

    $scope.UpdateVendor = function () {

        if ($scope.frmEditVendor.$valid) {
            $scope.EditVendor.VendorName = $scope.editVendordata;
            $http({
                method: 'POST',
                url: '/Administration/UpdateOtherVendor',
                data: JSON.stringify($scope.EditVendor),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    if (response.data == "Succeess") {
                        $scope.closeEditVendor();
                        Notification.success({ message: "Vendor updated successfully.", delay: 5000 });
                        $scope.GetVendors();
                    }
                    if (response.data == "AlreadyExist") {
                        Notification.warning({ message: "Vendor alredy exist.", delay: 5000 });
                    }
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
  
}]);

App.controller('VendorMaster_ngController', ["$scope", "$http", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, Notification, $compile, DTOptionsBuilder, DTColumnBuilder) {

    $scope.InitVendor = function () {
        $scope.Vendordata = '';
        $scope.Vendor = {
            VendorID: '',
            VendorName: '',
            VendorCode: '',
            City: '',
            State: '',
            Country: '',
            Address: '',
            PostalCode: '',
            ContactNo: ''
        };
    }
    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Administration/GetVendorData",
            type: 'GET'

        })
        .withOption('aaSorting', [[2, "desc"]])
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { search: "", searchPlaceholder: "Search By Name..." })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('VendorID').withTitle('VendorID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('VendorName').withTitle('Vendor Name').notSortable(),
        DTColumnBuilder.newColumn('VendorCode').withTitle('Vendor Code').notSortable(),
        DTColumnBuilder.newColumn('City').withTitle('city').notSortable(),
        DTColumnBuilder.newColumn('State').withTitle('state').notSortable(),
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            var strLinks = '<a ng-click="EditVendorData(' + row.VendorID + ')" class="pull-left font20 marleft15" > <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
            strLinks = strLinks + '<a class="pull-left marleft15 font20 delete" ng-click="DeleteVendor(' + row.VendorID + ')" > <i class="fa fa-trash-o" aria-hidden="true"></i></a>';
            return strLinks;

        })
    ];

    $scope.VendorNames = [];
    $scope.GetVendorNames = function () {
        var vendors = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVendors',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    if (vendors.indexOf(response.data[i]["VendorName"]) == -1) {
                        vendors.push(response.data[i]["VendorName"]);
                    }
                }
                $scope.VendorNames = vendors;

            },
                function errorCallback(response) {
                    alert(response);
                });

    }

    $scope.GetVendorNames();
    $scope.complete = function (string) {
        if (string == '' || string == undefined) {
            $scope.filterVendor = null;
        }
        else {
            var output = [];
            angular.forEach($scope.VendorNames, function (Vendor) {
                if (Vendor.toLowerCase().startsWith(string.toLowerCase())) {
                    output.push(Vendor);
                }
            });
            $scope.filterVendor = output;
        }
    }
    $scope.fillTextbox = function (string) {
        $scope.Vendordata = string;
        $scope.filterVendor = null;
    }
    $scope.completeEdit = function (string) {
        if (string == '' || string == undefined) {
            $scope.filterEditVendor = null;
        }
        else {
            var output = [];
            angular.forEach($scope.VendorNames, function (Vendor) {
                if (Vendor.toLowerCase().startsWith(string.toLowerCase())) {
                    output.push(Vendor);
                }
            });
            $scope.filterEditVendor = output;
        }
    }
    $scope.fillEditTextbox = function (string) {
        $scope.editVendordata = string;
        $scope.filterEditVendor = null;
    }
    $scope.GetVendors = function () {
        var dtinstance = $scope.dtInstance.DataTable;
        dtinstance.ajax.reload(function (json) { }, true);
    }

    $scope.AddVendor = function () {
        $scope.InitVendor();
        $("#VendorModal").modal("show");
    }
    $scope.closeVendor = function () {
        $("#VendorModal").modal("hide");
    }

    $scope.SaveVendor = function () {
        if ($scope.frmVendor.$valid) {
            $scope.Vendor.VendorName = $scope.Vendordata;
            $http({
                method: 'POST',
                url: '/Administration/AddVendor',
                data: JSON.stringify($scope.Vendor),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    if (response.data == "Succeess") {
                        Notification.success({ message: "Vendor saved successfully.", delay: 5000 });
                        $scope.closeVendor();
                        $scope.GetVendors();
                    }
                    if (response.data == "AlreadyExist") {
                        Notification.warning({ message: "Vendor alredy exist.", delay: 5000 });
                    }
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeleteVendor = function (VendorID) {
        var state = confirm("Do you want to delete?");

        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeleteVendor',
                params: { 'VendorID': VendorID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Vendor deleted successfully.", delay: 5000 });
                    $scope.GetVendors();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
    $scope.GetVendorByID = function (VendorID) {
        $scope.EditVendor = {
            VendorID: '',
            VendorName: '',
            VendorCode: '',
            City: '',
            State: '',
            Country: '',
            Address: '',
            PostalCode: '',
            ContactNo: ''
        };
        var Vendor = [];
        $http({
            method: 'GET',
            url: '/Administration/GetVendorByID',
            params: { 'VendorID': VendorID },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    Vendor.push(response.data[i]);
                }
                $scope.editVendordata = Vendor[0].VendorName;
                $scope.EditVendor.VendorID = Vendor[0].VendorID;
                $scope.EditVendor.VendorName = Vendor[0].VendorName;
                $scope.EditVendor.City = Vendor[0].City;
                $scope.EditVendor.VendorCode = Vendor[0].VendorCode;
                $scope.EditVendor.State = Vendor[0].State;
                $scope.EditVendor.Country = Vendor[0].Country;
                $scope.EditVendor.Address = Vendor[0].Address;
                $scope.EditVendor.PostalCode = Vendor[0].PostalCode;
                $scope.EditVendor.ContactNo = Vendor[0].ContactNo;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.closeEditVendor = function () {
        $("#EditVendorModal").modal("hide");
    }
    $scope.EditVendorData = function (VendorID) {

        $scope.GetVendorByID(VendorID);
        $("#EditVendorModal").modal("show");

    }

    $scope.UpdateVendor = function () {

        if ($scope.frmEditVendor.$valid) {
            $scope.EditVendor.VendorName = $scope.editVendordata;
            $http({
                method: 'POST',
                url: '/Administration/UpdateVendor',
                data: JSON.stringify($scope.EditVendor),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    if (response.data == "Succeess") {
                        $scope.closeEditVendor();
                        Notification.success({ message: "Vendor updated successfully.", delay: 5000 });
                        $scope.GetVendors();
                    }
                    if (response.data == "AlreadyExist") {
                        Notification.warning({ message: "Vendor alredy exist.", delay: 5000 });
                    }
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }

}]);

App.controller('VehicleVendorMaster_ngController', ["$scope", "$http", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, Notification, $compile, DTOptionsBuilder, DTColumnBuilder) {

    $scope.InitVendor = function () {
        $scope.Vendordata = '';
        $scope.Vendor = {
            VehicleVendorID: '',
            VendorName: '',
            VendorCode: '',
            City: '',
            State: '',
            Country: '',
            Address: '',
            PostalCode: '',
            ContactNo: ''
        };
    }
    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Administration/GetVehicleVendorData",
            type: 'GET'

        })
        .withOption('aaSorting', [[2, "desc"]])
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { search: "", searchPlaceholder: "Search By Name..." })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('VehicleVendorID').withTitle('VehicleVendorID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('VendorName').withTitle('Vendor Name').notSortable(),
        DTColumnBuilder.newColumn('VendorCode').withTitle('Vendor Code').notSortable(),
        DTColumnBuilder.newColumn('City').withTitle('city').notSortable(),
        DTColumnBuilder.newColumn('State').withTitle('state').notSortable(),
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            var strLinks = '<a ng-click="EditVendorData(' + row.VehicleVendorID + ')" class="pull-left font20 marleft15" > <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
            strLinks = strLinks + '<a class="pull-left marleft15 font20 delete" ng-click="DeleteVendor(' + row.VehicleVendorID + ')" > <i class="fa fa-trash-o" aria-hidden="true"></i></a>';
            return strLinks;

        })
    ];

    $scope.VendorNames = [];
    $scope.GetVendorNames = function () {
        var vendors = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVehicleVendors',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    if (vendors.indexOf(response.data[i]["VendorName"]) == -1) {
                        vendors.push(response.data[i]["VendorName"]);
                    }
                }
                $scope.VendorNames = vendors;

            },
                function errorCallback(response) {
                    alert(response);
                });

    }

    $scope.GetVendorNames();
    $scope.complete = function (string) {
        if (string == '' || string == undefined) {
            $scope.filterVendor = null;
        }
        else {
            var output = [];
            angular.forEach($scope.VendorNames, function (Vendor) {
                if (Vendor.toLowerCase().startsWith(string.toLowerCase())) {
                    output.push(Vendor);
                }
            });
            $scope.filterVendor = output;
        }
    }
    $scope.fillTextbox = function (string) {
        $scope.Vendordata = string;
        $scope.filterVendor = null;
    }
    $scope.completeEdit = function (string) {
        if (string == '' || string == undefined) {
            $scope.filterEditVendor = null;
        }
        else {
            var output = [];
            angular.forEach($scope.VendorNames, function (Vendor) {
                if (Vendor.toLowerCase().startsWith(string.toLowerCase())) {
                    output.push(Vendor);
                }
            });
            $scope.filterEditVendor = output;
        }
    }
    $scope.fillEditTextbox = function (string) {
        $scope.editVendordata = string;
        $scope.filterEditVendor = null;
    }
    $scope.GetVendors = function () {
        var dtinstance = $scope.dtInstance.DataTable;
        dtinstance.ajax.reload(function (json) { }, true);
    }

    $scope.AddVendor = function () {
        $scope.InitVendor();
        $("#VendorModal").modal("show");
    }
    $scope.closeVendor = function () {
        $("#VendorModal").modal("hide");
    }

    $scope.SaveVendor = function () {
        if ($scope.frmVendor.$valid) {
            $scope.Vendor.VendorName = $scope.Vendordata;
            $http({
                method: 'POST',
                url: '/Administration/AddVehicleVendor',
                data: JSON.stringify($scope.Vendor),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    if (response.data == "Succeess") {
                        Notification.success({ message: "Vendor saved successfully.", delay: 5000 });
                        $scope.closeVendor();
                        $scope.GetVendors();
                    }
                    if (response.data == "AlreadyExist") {
                        Notification.warning({ message: "Vendor alredy exist.", delay: 5000 });
                    }
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeleteVendor = function (VendorID) {
        var state = confirm("Do you want to delete?");

        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeleteVehicleVendor',
                params: { 'VehicleVendorID': VendorID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Vendor deleted successfully.", delay: 5000 });
                    $scope.GetVendors();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
    $scope.GetVendorByID = function (VendorID) {
        $scope.EditVendor = {
            VendorID: '',
            VendorName: '',
            VendorCode: '',
            City: '',
            State: '',
            Country: '',
            Address: '',
            PostalCode: '',
            ContactNo: ''
        };
        var Vendor = [];
        $http({
            method: 'GET',
            url: '/Administration/GetVehicleVendorByID',
            params: { 'VehicleVendorID': VendorID },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    Vendor.push(response.data[i]);
                }
                $scope.editVendordata = Vendor[0].VendorName;
                $scope.EditVendor.VehicleVendorID = Vendor[0].VehicleVendorID;
                $scope.EditVendor.VendorName = Vendor[0].VendorName;
                $scope.EditVendor.City = Vendor[0].City;
                $scope.EditVendor.VendorCode = Vendor[0].VendorCode;
                $scope.EditVendor.State = Vendor[0].State;
                $scope.EditVendor.Country = Vendor[0].Country;
                $scope.EditVendor.Address = Vendor[0].Address;
                $scope.EditVendor.PostalCode = Vendor[0].PostalCode;
                $scope.EditVendor.ContactNo = Vendor[0].ContactNo;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.closeEditVendor = function () {
        $("#EditVendorModal").modal("hide");
    }
    $scope.EditVendorData = function (VendorID) {

        $scope.GetVendorByID(VendorID);
        $("#EditVendorModal").modal("show");

    }

    $scope.UpdateVendor = function () {

        if ($scope.frmEditVendor.$valid) {
            $scope.EditVendor.VendorName = $scope.editVendordata;
            $http({
                method: 'POST',
                url: '/Administration/UpdateVehicleVendor',
                data: JSON.stringify($scope.EditVendor),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    if (response.data == "Succeess") {
                        $scope.closeEditVendor();
                        Notification.success({ message: "Vendor updated successfully.", delay: 5000 });
                        $scope.GetVendors();
                    }
                    if (response.data == "AlreadyExist") {
                        Notification.warning({ message: "Vendor alredy exist.", delay: 5000 });
                    }
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }

}]);

App.controller('VehicleMaster_ngController', ["$scope", "$http", "Notification", "$filter", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, Notification, $filter, $compile, DTOptionsBuilder, DTColumnBuilder) {

    $scope.InitVehicle = function () {
        $scope.Vehicle = {
            VehicleID: '',
            VehicleName: '',
            VehicleNo: '',
            VehicleType: '',
            RegistrationDate: '',
            FitnessExpDate: '',
            InsuranceExpDate: '',
            PUCExpDate: '',
            EngineNo: '',
            ChasisNo: '',
            OwnerType: '',
            BrokerName: '',
            loadingCapacity: '',
            MaxloadingCapacity: '',
            isHiredVehicle: '',
            VehicleVendorID: '',
            VendorName:''
        };
    }
    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Administration/GetVehicleData",
            type: 'GET'

        })
        .withOption('aaSorting', [[2, "desc"]])
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { search: "", searchPlaceholder: "Search By Name..." })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('VehicleID').withTitle('VehicleID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('VehicleName').withTitle('Vehicle Name').notSortable(),
        DTColumnBuilder.newColumn('VehicleNo').withTitle('Vehicle No').notSortable(),
        DTColumnBuilder.newColumn('VehicleVendor').withTitle('Vendor Name').notSortable(),
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            var strLinks = '<a ng-click="EditVehicleData(' + row.VehicleID + ')" class="pull-left font20 marleft15" > <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
            strLinks = strLinks + '<a class="pull-left marleft15 font20 delete" ng-click="DeleteVehicle(' + row.VehicleID + ')" > <i class="fa fa-trash-o" aria-hidden="true"></i></a>';
            return strLinks;

        })
    ];
    $scope.GetVendors = function () {

        var vendor = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVehicleVendors',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    vendor.push(response.data[i]);
                }
                $scope.Vendors = vendor;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetVendors();
    $scope.GetVehicles = function () {
        var dtinstance = $scope.dtInstance.DataTable;
        dtinstance.ajax.reload(function (json) { }, true);
    }

    $scope.AddVehicle = function () {
        $scope.InitVehicle();
        $('#txtRegistrationDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false
        });
        $('#txtFitnessExpDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false
        });
        $('#txtInsuranceExpDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false
        });
        $('#txtPUCExpDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false
        });
        $("#VehicleModal").modal("show");
    }
    $scope.closeVehicle = function () {
        $("#VehicleModal").modal("hide");
    }

    $scope.SaveVehicle = function () {
        if ($scope.frmVehicle.$valid) {
           
            $http({
                method: 'POST',
                url: '/Administration/AddVehicle',
                data: JSON.stringify($scope.Vehicle),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    if (response.data == "Succeess") {
                        Notification.success({ message: "Vehicle saved successfully.", delay: 5000 });
                        $scope.closeVehicle();
                        $scope.GetVehicles();
                    }
                   
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeleteVehicle = function (VehicleID) {
        var state = confirm("Do you want to delete?");

        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeleteVehicle',
                params: { 'VehicleID': VehicleID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Vehicle deleted successfully.", delay: 5000 });
                    $scope.GetVehicles();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
    $scope.GetVehicleByID = function (VehicleID) {
        $scope.EditVehicle = {
            VehicleID: '',
            VehicleName: '',
            VehicleNo: '',
            VehicleType: '',
            RegistrationDate: '',
            FitnessExpDate: '',
            InsuranceExpDate: '',
            PUCExpDate: '',
            EngineNo: '',
            ChasisNo: '',
            OwnerType: '',
            BrokerName: '',
            loadingCapacity: '',
            MaxloadingCapacity: '',
            isHiredVehicle: '',
            VehicleVendorID: '',
            VendorName: ''
        };
        var Vehicle = [];
        $http({
            method: 'GET',
            url: '/Administration/GetVehicleByID',
            params: { 'VehicleID': VehicleID },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    Vehicle.push(response.data[i]);
                }
                $scope.EditVehicle.VehicleID = Vehicle[0].VehicleID;
                $scope.EditVehicle.VehicleName = Vehicle[0].VehicleName;
                $scope.EditVehicle.VehicleNo = Vehicle[0].VehicleNo;
                $scope.EditVehicle.VehicleType = Vehicle[0].VehicleType;
                $scope.EditVehicle.RegistrationDate = $filter('date')(Vehicle[0].RegistrationDate, 'dd/MM/yyyy');
                $scope.EditVehicle.FitnessExpDate = $filter('date')(Vehicle[0].FitnessExpDate, 'dd/MM/yyyy');
                $scope.EditVehicle.InsuranceExpDate = $filter('date')(Vehicle[0].InsuranceExpDate, 'dd/MM/yyyy');
                $scope.EditVehicle.PUCExpDate = $filter('date')(Vehicle[0].PUCExpDate, 'dd/MM/yyyy');
                $scope.EditVehicle.EngineNo = Vehicle[0].EngineNo;
                $scope.EditVehicle.ChasisNo = Vehicle[0].ChasisNo;
                $scope.EditVehicle.OwnerType = Vehicle[0].OwnerType;
                $scope.EditVehicle.BrokerName = Vehicle[0].BrokerName;
                $scope.EditVehicle.loadingCapacity = Vehicle[0].loadingCapacity;
                $scope.EditVehicle.MaxloadingCapacity = Vehicle[0].MaxloadingCapacity;
                $scope.EditVehicle.isHiredVehicle = Vehicle[0].isHiredVehicle;
                $scope.EditVehicle.VehicleVendorID = Vehicle[0].VehicleVendorID;
             
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.closeEditVehicle = function () {
        $("#EditVehicleModal").modal("hide");
    }
    $scope.EditVehicleData = function (VehicleID) {
        $('#txtEditRegistrationDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false
        });
        $('#txtEditFitnessExpDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false
        });
        $('#txtEditInsuranceExpDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false
        });
        $('#txtEditPUCExpDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            forceParse: false
        });
        $scope.GetVehicleByID(VehicleID);
        $("#EditVehicleModal").modal("show");

    }

    $scope.UpdateVehicle = function () {

        if ($scope.frmEditVehicle.$valid) {
            $http({
                method: 'POST',
                url: '/Administration/UpdateVehicle',
                data: JSON.stringify($scope.EditVehicle),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    if (response.data == "Succeess") {
                        $scope.closeEditVehicle();
                        Notification.success({ message: "Vehicle updated successfully.", delay: 5000 });
                        $scope.GetVehicles();
                    }
                   
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }

}]);

App.controller('PinCodeMaster_ngController', ["$scope", "$http", "Notification", function ($scope, $http, Notification) {

    $scope.InitPinCode = function () {
        $scope.PinCode = {
            PinCodeId: '',
            PinCode: ''
        };
    }

    $scope.PinCodes = [];
    $scope.GetPinCodes = function () {
        var pincodes = [];
        $http({
            method: 'GET',
            url: '/Administration/GetPinCodes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    pincodes.push(response.data[i]);
                }
                $scope.PinCodes = pincodes;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }

    $scope.AddPinCode = function () {
        $scope.InitPinCode();
        $("#PinCodeModal").modal("show");
    }
    $scope.closePinCode = function () {
        $("#PinCodeModal").modal("hide");
    }

    $scope.SavePinCode = function () {
        if ($scope.frmPinCode.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/AddPinCode',
                data: JSON.stringify($scope.PinCode),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);
                    Notification.success({ message: "PinCode saved successfully.", delay: 5000 });
                    $scope.closePinCode();
                    $scope.GetPinCodes();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeletePinCode = function (PinCodeId) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeletePinCode',
                params: { 'PinCodeId': PinCodeId },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "PinCode deleted successfully.", delay: 5000 });
                    $scope.GetPinCodes();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }

    $scope.closeEditPinCode = function () {
        $("#EditPinCodeModal").modal("hide");
    }
    $scope.EditPinCodeData = function (PinCodeID, PinCode) {

        $scope.EditPinCode = {
            PinCodeId: PinCodeID,
            PinCode: PinCode
        };
        $("#EditPinCodeModal").modal("show");

    }

    $scope.UpdatePinCode = function () {

        if ($scope.frmEditPinCode.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/UpdatePinCode',
                data: JSON.stringify($scope.EditPinCode),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeEditPinCode();
                    Notification.success({ message: "PinCode updated successfully.", delay: 5000 });
                    $scope.GetPinCodes();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);

App.controller('FOVMaster_ngController', ["$scope", "$http", "Notification", function ($scope, $http, Notification) {

    $scope.InitFOV = function () {
        $scope.FOV = {
            FOVId: '',
            FOVPercent: ''
        };
    }

    $scope.FOVs = [];
    $scope.GetFOVs = function () {
        var FOVs = [];
        $http({
            method: 'GET',
            url: '/Administration/GetFOV',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    FOVs.push(response.data[i]);
                }
                $scope.FOVs = FOVs;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }

    $scope.AddFOV = function () {
        $scope.InitFOV();
        $("#FOVModal").modal("show");
    }
    $scope.closeFOV = function () {
        $("#FOVModal").modal("hide");
    }

    $scope.SaveFOV = function () {
        if ($scope.frmFOV.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/AddFOV',
                data: JSON.stringify($scope.FOV),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);
                    Notification.success({ message: "FOV saved successfully.", delay: 5000 });
                    $scope.closeFOV();
                    $scope.GetFOVs();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeleteFOV = function (FOVId) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeleteFOV',
                params: { 'FOVId': FOVId },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "FOV deleted successfully.", delay: 5000 });
                    $scope.GetFOVs();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }

    $scope.closeEditFOV = function () {
        $("#EditFOVModal").modal("hide");
    }
    $scope.EditFOVData = function (FOVID, FOV) {

        $scope.EditFOV = {
            FOVId: FOVID,
            FOVPercent: FOV
        };
        $("#EditFOVModal").modal("show");

    }

    $scope.UpdateFOV = function () {

        if ($scope.frmEditFOV.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/UpdateFOV',
                data: JSON.stringify($scope.EditFOV),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeEditFOV();
                    Notification.success({ message: "FOV updated successfully.", delay: 5000 });
                    $scope.GetFOVs();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);

App.controller('BranchRunningMaster_ngController', ["$scope", "$http", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, Notification,$compile, DTOptionsBuilder, DTColumnBuilder) {

    $scope.InitBranchRunning = function () {
        $scope.BranchRunning = {
            RunningNoID: '',
            BranchID: '',
            LastRunningNo: '',
            TransactionType: ''
        };
    }
    $scope.Branches = [];
    $scope.GetBranches = function () {
        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                        brnches.push(response.data[i]);
                }
                $scope.Branches = brnches;

            },
                function errorCallback(response) {
                    alert(response);
                });

    }
    $scope.GetBranches();
    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Administration/GetBranchRunningData",
            type: 'GET'

        })
        .withOption('aaSorting', [[2, "desc"]])
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { search: "", searchPlaceholder: "Search By Branch Name..." })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('RunningNoID').withTitle('RunningNoID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('BranchID').withTitle('BranchID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('BranchName').withTitle('Branch Name').notSortable(),
        DTColumnBuilder.newColumn('TransactionType').withTitle('Transaction Type').notSortable(),
        DTColumnBuilder.newColumn('LastRunningNo').withTitle('Last Running No').notSortable(),
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            var strLinks = '<a ng-click="EditBranchRunningData(' + row.RunningNoID + ',' + row.BranchID + ',\'' + row.TransactionType + '\',\'' + row.LastRunningNo+'\')" class="pull-left font20 marleft15" > <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
            strLinks = strLinks + '<a class="pull-left marleft15 font20 delete" ng-click="DeleteBranchRunning(' + row.RunningNoID + ')" > <i class="fa fa-trash-o" aria-hidden="true"></i></a>';
            return strLinks;

        })
    ];
    $scope.BranchRunnings = [];
    $scope.GetBranchRunning = function () {
        var dtinstance = $scope.dtInstance.DataTable;
        dtinstance.ajax.reload(function (json) { }, true);
    }

    $scope.AddBranchRunning = function () {
        $scope.InitBranchRunning();
        $("#BranchRunningModal").modal("show");
    }
    $scope.closeBranchRunning = function () {
        $("#BranchRunningModal").modal("hide");
    }

    $scope.SaveBranchRunning = function () {
        console.log($scope.frmBranchRunning.$valid)
        if ($scope.frmBranchRunning.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/AddBranchRunning',
                data: JSON.stringify($scope.BranchRunning),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    if (response.data == "Succeess") {
                        Notification.success({ message: "Branch Running No saved successfully.", delay: 5000 });
                        $scope.closeBranchRunning();
                        $scope.GetBranchRunning();
                    }
                    if (response.data == "AlreadyExist") {
                        Notification.warning({ message: "Branch Running No alredy configured.", delay: 5000 });
                    }
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeleteBranchRunning = function (BranchRunningID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeleteBranchRunning',
                params: { 'BranchRunningID': BranchRunningID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Branch Running No deleted successfully.", delay: 5000 });
                    $scope.GetBranchRunning();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }

    $scope.closeEditBranchRunning = function () {
        $("#EditBranchRunningModal").modal("hide");
    }
    $scope.EditBranchRunningData = function (RunningNoID, BranchID, TransactionType, LastRunningNo) {
        $scope.EditBranchRunning = {
            RunningNoID: RunningNoID,
            BranchID: BranchID,
            LastRunningNo: LastRunningNo,
            TransactionType: TransactionType
        };
        $("#EditBranchRunningModal").modal("show");

    }

    $scope.UpdateBranchRunning = function () {

        if ($scope.frmEditBranchRunningModal.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/UpdateBranchRunning',
                data: JSON.stringify($scope.EditBranchRunning),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    if (response.data == "Succeess") {
                        $scope.closeEditBranchRunning();
                        Notification.success({ message: "Branch Running No updated successfully.", delay: 5000 });
                        $scope.GetBranchRunning();
                    }
                    if (response.data == "AlreadyExist") {
                        Notification.warning({ message: "Branch Running No alredy configured.", delay: 5000 });
                    }
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);

App.controller('CompanyMaster_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder) {

    $scope.InitCompany = function () {
        $scope.CompanyDetails = {
            CompanyID: '',
            CompanyName: '',
            CompanyCode: '',
            ContactNo: '',
            Address: '',
            ContactPerson: '',
            EmailID: '',
            Website: '',
            CTSNo: '',
            TINo: '',
            PanNo: '',
            ServiceTaxNo: '',
            GSTNo: '',
            TDS: ''
        };
    }
    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Administration/GetCompanyData",
            type: 'GET'

        })
        .withOption('aaSorting', [[2, "desc"]])
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { search: "", searchPlaceholder: "Search By Name..." })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('CompanyID').withTitle('CompanyID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('CompanyName').withTitle('Company Name').notSortable(),
        DTColumnBuilder.newColumn('CompanyCode').withTitle('Code'),
        DTColumnBuilder.newColumn('ContactPerson').withTitle('Contact Person').notSortable(),
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            var strLinks = '<a ng-click="EditCompanyData(' + row.CompanyID + ')" class="pull-left font20 marleft15" > <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
            strLinks = strLinks + '<a class="pull-left marleft15 font20 delete" ng-click="DeleteCompany(' + row.CompanyID + ')" > <i class="fa fa-trash-o" aria-hidden="true"></i></a>';
            return strLinks;

        })
    ];

    $scope.AddCompany = function () {
        $scope.InitCompany();
        $("#CompanyModal").modal("show");
    }
    $scope.closeCompany = function () {
        $("#CompanyModal").modal("hide");
    }
   
    $scope.SaveCompany = function () {
        if ($scope.frmCompany.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/AddCompany',
                data: JSON.stringify($scope.CompanyDetails),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);
                    Notification.success({ message: "Company saved successfully.", delay: 5000 });
                    $scope.closeCompany();
                    var dtinstance = $scope.dtInstance.DataTable;
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeleteCompany = function (CompanyID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeleteCompany',
                params: { 'CompanyID': CompanyID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Company deleted successfully.", delay: 5000 });
                    var dtinstance = $scope.dtInstance.DataTable;
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
    $scope.GetCompanyByID = function (CompanyId) {
        $scope.EditCompany = {
            CompanyID: '',
            CompanyName: '',
            CompanyCode: '',
            ContactNo: '',
            Address: '',
            ContactPerson: '',
            EmailID: '',
            Website: '',
            CTSNo: '',
            TINo: '',
            PanNo: '',
            ServiceTaxNo: '',
            GSTNo: '',
            TDS: ''
        };
        var Company = [];
        $http({
            method: 'GET',
            url: '/Administration/GetCompanyByID',
            params: { 'CompanyID': CompanyId },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    Company.push(response.data[i]);
                }

                $scope.EditCompany.CompanyID = Company[0].CompanyID;
                $scope.EditCompany.CompanyName = Company[0].CompanyName;
                $scope.EditCompany.CompanyCode = Company[0].CompanyCode;
                $scope.EditCompany.ContactNo = Company[0].ContactNo;
                $scope.EditCompany.Address = Company[0].Address;
                $scope.EditCompany.ContactPerson = Company[0].ContactPerson;
                $scope.EditCompany.EmailID = Company[0].EmailID;
                $scope.EditCompany.Website = Company[0].Website;
                $scope.EditCompany.CTSNo = Company[0].CTSNo;
                $scope.EditCompany.TINo = Company[0].TINo;
                $scope.EditCompany.PanNo = Company[0].PanNo;
                $scope.EditCompany.ServiceTaxNo = Company[0].ServiceTaxNo;
                $scope.EditCompany.GSTNo = Company[0].GSTNo;
                $scope.EditCompany.TDS = Company[0].TDS;
               
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.closeEditCompany = function () {
        $("#EditCompanyModal").modal("hide");
    }
    $scope.EditCompanyData = function (CompanyID) {
        $scope.GetCompanyByID(CompanyID);
        $("#EditCompanyModal").modal("show");

    }
    
    $scope.UpdateCompany = function () {

        if ($scope.frmEditCompany.$valid) {
            $http({
                method: 'POST',
                url: '/Administration/UpdateCompany',
                data: JSON.stringify($scope.EditCompany),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeEditCompany();
                    Notification.success({ message: "Company updated successfully.", delay: 5000 });
                    var dtinstance = $scope.dtInstance.DataTable;

                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);

App.controller('TaxMaster_ngController', ["$scope", "$http", "Notification", function ($scope, $http, Notification) {

    $scope.InitTax = function () {
        $scope.Tax = {
            TaxID: '',
            TaxType: '',
            TaxPercent: '',
            Description: '',
            Status:''

        };
    }

    $scope.Taxes = [];
    $scope.GetTaxes = function () {
        var Taxs = [];
        $http({
            method: 'GET',
            url: '/Administration/GetTaxes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    Taxs.push(response.data[i]);
                }
                $scope.Taxes = Taxs;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }

    $scope.AddTax = function () {
        $scope.InitTax();
        $("#TaxModal").modal("show");
    }
    $scope.closeTax = function () {
        $("#TaxModal").modal("hide");
    }

    $scope.SaveTax = function () {
        if ($scope.frmTax.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/AddTax',
                data: JSON.stringify($scope.Tax),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);
                    Notification.success({ message: "Tax saved successfully.", delay: 5000 });
                    $scope.closeTax();
                    $scope.GetTaxes();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeleteTax = function (TaxId) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeleteTax',
                params: { 'TaxId': TaxId },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Tax deleted successfully.", delay: 5000 });
                    $scope.GetTaxes();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }

    $scope.closeEditTax = function () {
        $("#EditTaxModal").modal("hide");
    }
    $scope.EditTaxData = function (TaxID, TaxType, TaxPercent, Description, Status) {

        $scope.EditTax = {
            TaxID: TaxID,
            TaxType: TaxType,
            TaxPercent: TaxPercent,
            Description: Description,
            Status: Status
        };
        $("#EditTaxModal").modal("show");

    }

    $scope.UpdateTax = function () {

        if ($scope.frmEditTax.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/UpdateTax',
                data: JSON.stringify($scope.EditTax),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeEditTax();
                    Notification.success({ message: "Tax updated successfully.", delay: 5000 });
                    $scope.GetTaxes();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);

App.controller('ZoneMaster_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder) {

    $scope.InitZone = function () {
        $scope.ZoneDetails = {
            ZoneID: '',
            Zone: '',
            Address: '',
            Pincode: '',
            city: '',
            state: '',
            Nationalty: '',
            ContactPerson: '',
            MobileNo: '',
            Email: '',
            Website: '',
            CashLimit: '',
            CompanyID: ''
        };
    }
    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Administration/GetZoneData",
            type: 'GET'

        })
        .withOption('aaSorting', [[2, "desc"]])
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { search: "", searchPlaceholder: "Search By Name..." })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('ZoneID').withTitle('ZoneID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('Zone').withTitle('Zone').notSortable(),
        DTColumnBuilder.newColumn('ContactPerson').withTitle('Contact Person'),
        DTColumnBuilder.newColumn('Email').withTitle('Email').notSortable(),
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            var strLinks = '<a ng-click="EditZoneData(' + row.ZoneID + ')" class="pull-left font20 marleft15" > <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
            strLinks = strLinks + '<a class="pull-left marleft15 font20 delete" ng-click="DeleteZone(' + row.ZoneID + ')" > <i class="fa fa-trash-o" aria-hidden="true"></i></a>';
            return strLinks;

        })
    ];

    $scope.AddZone = function () {
        $scope.InitZone();
        $("#ZoneModal").modal("show");
    }
    $scope.closeZone = function () {
        $("#ZoneModal").modal("hide");
    }

    $scope.SaveZone = function () {
        if ($scope.frmZone.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/AddZone',
                data: JSON.stringify($scope.ZoneDetails),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);
                    Notification.success({ message: "Zone saved successfully.", delay: 5000 });
                    $scope.closeZone();
                    var dtinstance = $scope.dtInstance.DataTable;
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeleteZone = function (ZoneID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeleteZone',
                params: { 'ZoneID': ZoneID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Zone deleted successfully.", delay: 5000 });
                    var dtinstance = $scope.dtInstance.DataTable;
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
    $scope.GetZoneByID = function (ZoneId) {
        $scope.EditZone = {
            ZoneID: '',
            Zone: '',
            Address: '',
            Pincode: '',
            city: '',
            state: '',
            Nationalty: '',
            ContactPerson: '',
            MobileNo: '',
            Email: '',
            Website: '',
            CashLimit: '',
            CompanyID: ''
        };
        var Zone = [];
        $http({
            method: 'GET',
            url: '/Administration/GetZoneByID',
            params: { 'ZoneID': ZoneId },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    Zone.push(response.data[i]);
                }

                $scope.EditZone.ZoneID = Zone[0].ZoneID;
                $scope.EditZone.Zone = Zone[0].Zone;
                $scope.EditZone.Address = Zone[0].Address;
                $scope.EditZone.Pincode = Zone[0].Pincode;
                $scope.EditZone.city = Zone[0].city;
                $scope.EditZone.state = Zone[0].state;
                $scope.EditZone.Nationalty = Zone[0].Nationalty;
                $scope.EditZone.ContactPerson = Zone[0].ContactPerson;
                $scope.EditZone.MobileNo = Zone[0].MobileNo;
                $scope.EditZone.Email = Zone[0].Email;
                $scope.EditZone.Website = Zone[0].Website;
                $scope.EditZone.CashLimit = Zone[0].CashLimit;
                $scope.EditZone.CompanyID = Zone[0].CompanyID;

            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.closeEditZone = function () {
        $("#EditZoneModal").modal("hide");
    }
    $scope.EditZoneData = function (ZoneID) {
        $scope.GetZoneByID(ZoneID);
        $("#EditZoneModal").modal("show");

    }

    $scope.UpdateZone = function () {

        if ($scope.frmEditZone.$valid) {
            $http({
                method: 'POST',
                url: '/Administration/UpdateZone',
                data: JSON.stringify($scope.EditZone),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeEditZone();
                    Notification.success({ message: "Zone updated successfully.", delay: 5000 });
                    var dtinstance = $scope.dtInstance.DataTable;

                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);


App.controller('PartNoMaster_ngController', ["$scope", "$http", "Notification", function ($scope, $http, Notification) {

    $scope.InitPartNo = function () {
        $scope.PartNo = {
            PartNoID: '',
            PartNo: '',
            PerPriceWeight: '',
            VendorID: '',
            VendorName:''
        };
    }

    $scope.PartNos = [];
    $scope.GetPartNos = function () {
        var PartNos = [];
        $http({
            method: 'GET',
            url: '/Administration/GetPartNos',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    PartNos.push(response.data[i]);
                }
                $scope.PartNos = PartNos;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetVendors = function () {

        var vendor = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVendors',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    vendor.push(response.data[i]);
                }
                $scope.Vendors = vendor;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.AddPartNo = function () {
        $scope.InitPartNo();
        $scope.GetVendors();
        $("#PartNoModal").modal("show");
    }
    $scope.closePartNo = function () {
        $("#PartNoModal").modal("hide");
    }

    $scope.SavePartNo = function () {
        if ($scope.frmPartNo.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/AddPartNo',
                data: JSON.stringify($scope.PartNo),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    console.log(response);
                    Notification.success({ message: "Part No saved successfully.", delay: 5000 });
                    $scope.closePartNo();
                    $scope.GetPartNos();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.DeletePartNo = function (PartNoId) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Administration/DeletePartNo',
                params: { 'PartNoId': PartNoId },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Part No deleted successfully.", delay: 5000 });
                    $scope.GetPartNos();
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }

    $scope.closeEditPartNo = function () {
        $("#EditPartNoModal").modal("hide");
    }
    $scope.EditPartNoData = function (PartNoID, PartNo, PerPriceWeight, VendorId) {
        $scope.GetVendors();
        $scope.EditPartNo = {
            PartNoID: PartNoID,
            PartNo: PartNo,
            PerPriceWeight: PerPriceWeight,
            VendorID: VendorId
        };
        $("#EditPartNoModal").modal("show");

    }

    $scope.UpdatePartNo = function () {

        if ($scope.frmEditPartNo.$valid) {

            $http({
                method: 'POST',
                url: '/Administration/UpdatePartNo',
                data: JSON.stringify($scope.EditPartNo),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    $scope.closeEditPartNo();
                    Notification.success({ message: "Part No updated successfully.", delay: 5000 });
                    $scope.GetPartNos();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }
        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);

App.controller('BranchBalance_ngController', ["$scope", "$http", "Notification", function ($scope, $http, Notification) {
    

    $scope.BranchBalance = [];
    $scope.GetBranchBalance = function () {
        var branchbalance = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranchBalanceData',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    branchbalance.push(response.data[i]);
                }
                $scope.BranchBalance = branchbalance;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }

}]);