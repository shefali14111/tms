﻿
App.controller('VendorRates_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder) {

    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Setting/GetVendorRates",
            type: 'GET'
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('VendorRateID').withTitle('VendorRateID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('BranchName').withTitle('Branch').notSortable(),
        DTColumnBuilder.newColumn('BillingParty').withTitle('Billing Party').notSortable(),
        DTColumnBuilder.newColumn('Consignor').withTitle('Consignor').notSortable(),
        DTColumnBuilder.newColumn('Consignee').withTitle('Consignee').notSortable(),
        DTColumnBuilder.newColumn('FromBranch').withTitle('From').notSortable(),
        DTColumnBuilder.newColumn('ToBranch').withTitle('To').notSortable(),
        DTColumnBuilder.newColumn('Action').notSortable().renderWith(function (data, type, row, meta) {
            return '<a class="pull-left marleft15 font20 delete" ng-click="DeleteVendorRate(' + row.VendorRateID + ')" > <i class="fa fa-trash-o" aria-hidden="true"></i></a>';
        })

    ];
    $scope.DeleteVendorRate = function (VendorRateID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Setting/DeleteVendorRate',
                params: { 'VendorRateID': VendorRateID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Vendor rate deleted successfully.", delay: 5000 });
                    var dtinstance = $scope.dtInstance.DataTable;
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
    $scope.ConfigureVendorRates = function () {
        $scope.VendorRate = {
            VendorRateID: '',
            BranchId: '',
            BillingPartyId: '',
            ConsignorId: '',
            ConsigneeId: '',
            FROM: '',
            TO: '',
            ChargeTypeId: '',
            FRIEGHTRATE: '',
            DocketCharge: '',
            DoorCollection: '',
            DeliveryCharge: '',
            FOV: '',
            HamaliCharge: '',
            OtherCharges: '',
            MinKG: ''
        };
        $scope.GetBranches();
        $scope.GetOtherBranches();
        $scope.GetVendors();
        $scope.GetChargeTypes();
        $("#ConfigureRateModal").modal("show");
    }


    $scope.clearVendorRate = function () {
        $scope.VendorRate = {
            VendorRateID: '',
            BranchId: '',
            BillingPartyId: '',
            ConsignorId: '',
            ConsigneeId: '',
            FROM: '',
            TO: '',
            ChargeTypeId: '',
            FRIEGHTRATE: '',
            DocketCharge: '',
            DoorCollection: '',
            DeliveryCharge: '',
            FOV: '',
            HamaliCharge: '',
            OtherCharges: '',
            MinKG: ''
        };
    }
    $scope.closeRateModal = function () {
        $scope.clearVendorRate();
        $("#ConfigureRateModal").modal("hide");
    }
    $scope.GetBranches = function () {

        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    brnches.push(response.data[i]);
                }
                $scope.Branches = brnches;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetOtherBranches = function () {

        var otherBranches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetOtherBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    otherBranches.push(response.data[i]);
                }
                $scope.OtherBranches = otherBranches;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetVendors = function () {

        var vendor = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVendors',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    vendor.push(response.data[i]);
                }
                $scope.Vendors = vendor;
                $scope.consignorVendors = vendor;
                $scope.BillingVendors = vendor;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetChargeTypes = function () {

        var chrgTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetChargeTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    chrgTypes.push(response.data[i]);
                }
                $scope.ChargeTypes = chrgTypes;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.AddVendorRate = function () {
        if ($scope.frmRates.$valid) {
            $http({
                method: 'POST',
                url: '/Setting/AddVendorRate',
                data: JSON.stringify($scope.VendorRate),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    if (response.data == 1) {
                        Notification.success({ message: "Vendor Rates configured successfully.", delay: 5000 });
                        $scope.closeRateModal();
                        var dtinstance = $scope.dtInstance.DataTable;
                        dtinstance.ajax.reload(function (json) { }, true);
                    }
                    else {
                        Notification.error({ message: "Vendor Rates are already configured for selected criteria.", delay: 5000 });
                    }
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }

        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);

App.controller('MHELVendorRates_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder) {

    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Setting/GetMHELVendorRates",
            type: 'GET'
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('MHELVendorRateID').withTitle('MHELVendorRateID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('BillingParty').withTitle('Billing Party').notSortable(),
        DTColumnBuilder.newColumn('Consignor').withTitle('Consignor').notSortable(),
        DTColumnBuilder.newColumn('Consignee').withTitle('Consignee').notSortable(),
        DTColumnBuilder.newColumn('FromBranch').withTitle('From').notSortable(),
        DTColumnBuilder.newColumn('ToBranch').withTitle('To').notSortable(),
        DTColumnBuilder.newColumn('PartNo').withTitle('Part No').notSortable(),
        DTColumnBuilder.newColumn('Action').notSortable().renderWith(function (data, type, row, meta) {
            return '<a class="pull-left marleft15 font20 delete" ng-click="EditVendorRateData(' + row.MHELVendorRateID + ')" > <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a class="pull-left marleft15 font20 delete" ng-click="DeleteVendorRate(' + row.MHELVendorRateID + ')" > <i class="fa fa-trash-o" aria-hidden="true"></i></a>';
        })

    ];
    $scope.DeleteVendorRate = function (VendorRateID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Setting/DeleteMHELVendorRate',
                params: { 'MHELVendorRateID': VendorRateID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "MHEL Vendor rate deleted successfully.", delay: 5000 });
                    var dtinstance = $scope.dtInstance.DataTable;
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
    $scope.ConfigureVendorRates = function () {
        $scope.VendorRate = {
            MHELVendorRateID: '',
            BillingPartyId: '',
            ConsignorId: '',
            ConsigneeId: '',
            FROM: '',
            TO: '',
            ChargeTypeId: '',
            PartNo: '',
            PerPieceWeight: '',
            FRIEGHTRATE: '',
            DocketCharge: '',
            DoorCollection: '',
            DeliveryCharge: '',
            FOV: '',
            HamaliCharge: '',
            OtherCharges: '',
            MinKG: ''
        };
        $scope.GetOtherBranches();
        $scope.GetPartNos();
        $scope.GetVendors();
        $scope.GetMHELVendors();
        $scope.GetChargeTypes();
        $("#ConfigureRateModal").modal("show");
    }

    $scope.GetPartNos = function () {

        var partnos = [];
        $http({
            method: 'GET',
            url: '/Operations/GetPartNos',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    partnos.push(response.data[i]);
                }
                $scope.PartNos = partnos;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.clearVendorRate = function () {
        $scope.VendorRate = {
            MHELVendorRateID: '',
            BillingPartyId: '',
            ConsignorId: '',
            ConsigneeId: '',
            FROM: '',
            TO: '',
            ChargeTypeId: '',
            PartNo: '',
            PerPieceWeight: '',
            FRIEGHTRATE: '',
            DocketCharge: '',
            DoorCollection: '',
            DeliveryCharge: '',
            FOV: '',
            HamaliCharge: '',
            OtherCharges: '',
            MinKG: ''
        };
    }
    $scope.closeRateModal = function () {
        $scope.clearVendorRate();
        $("#ConfigureRateModal").modal("hide");
    }

    $scope.GetOtherBranches = function () {

        var otherBranches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetOtherBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    otherBranches.push(response.data[i]);
                }
                $scope.OtherBranches = otherBranches;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetVendors = function () {

        var vendor = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVendors',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    vendor.push(response.data[i]);
                }
                $scope.Vendors = vendor;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetMHELVendors = function () {

        var vendor = [];
        $http({
            method: 'GET',
            url: '/Operations/GetMHELVendors',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    vendor.push(response.data[i]);
                }
                $scope.BillingVendors = vendor;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetChargeTypes = function () {

        var chrgTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetChargeTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    chrgTypes.push(response.data[i]);
                }
                $scope.ChargeTypes = chrgTypes;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }

    $scope.AddVendorRate = function () {
        if ($scope.VendorRate.PartNo != "") {
            var partCon = filterFilter($scope.PartNos, { PartNoID: $scope.VendorRate.PartNo });
            if (partCon.length > 0) {
                $scope.VendorRate.PartNo = partCon[0].PartNo;
            }
        }
        if ($scope.frmRates.$valid) {
            $http({
                method: 'POST',
                url: '/Setting/AddMHELVendorRate',
                data: JSON.stringify($scope.VendorRate),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    if (response.data == 1) {
                        Notification.success({ message: "MHEL Vendor Rates configured successfully.", delay: 5000 });
                        $scope.closeRateModal();
                        var dtinstance = $scope.dtInstance.DataTable;
                        dtinstance.ajax.reload(function (json) { }, true);
                    }
                    else {
                        Notification.error({ message: "Vendor Rates are already configured for selected criteria.", delay: 5000 });
                    }
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }

        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.EditVendorRateData = function (VendorRateID) {
        $scope.GetOtherBranches();
        $scope.GetPartNos();
        $scope.GetVendors();
        $scope.GetMHELVendors();
        $scope.GetChargeTypes();
        $scope.EditVendorRate = {
            MHELVendorRateID: VendorRateID,
            BillingPartyId: '',
            ConsignorId: '',
            ConsigneeId: '',
            FROM: '',
            TO: '',
            ChargeTypeId: '',
            PartNo: '',
            PerPieceWeight: '',
            FRIEGHTRATE: '',
            DocketCharge: '',
            DoorCollection: '',
            DeliveryCharge: '',
            FOV: '',
            HamaliCharge: '',
            OtherCharges: '',
            MinKG: ''
        };
        var vendorrate = [];

        $http({
            method: 'POST',
            url: '/Setting/GetMHELVendorRateById',
            params: { 'MHELVendorRateID': VendorRateID },  // pass in data as strings
            headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    vendorrate.push(response.data[i]);
                }
                $scope.EditVendorRate.MHELVendorRateID = vendorrate[0].MHELVendorRateID;
                $scope.EditVendorRate.BillingPartyId = vendorrate[0].BillingPartyId;
                $scope.EditVendorRate.ConsignorId = vendorrate[0].ConsignorId;
                $scope.EditVendorRate.ConsigneeId = vendorrate[0].ConsigneeId;
                $scope.EditVendorRate.FROM = vendorrate[0].FROM;
                $scope.EditVendorRate.TO = vendorrate[0].TO;
                $scope.EditVendorRate.ChargeTypeId = vendorrate[0].ChargeTypeId;
                $scope.EditVendorRate.PartNo = vendorrate[0].PartNo;
                $scope.EditVendorRate.PerPieceWeight = vendorrate[0].PerPieceWeight;
                $scope.EditVendorRate.DocketCharge = vendorrate[0].DocketCharge;
                $scope.EditVendorRate.FRIEGHTRATE = vendorrate[0].FRIEGHTRATE;
                $scope.EditVendorRate.DoorCollection = vendorrate[0].DoorCollection;
                $scope.EditVendorRate.DeliveryCharge = vendorrate[0].DeliveryCharge;
                $scope.EditVendorRate.FOV = vendorrate[0].FOV;
                $scope.EditVendorRate.HamaliCharge = vendorrate[0].HamaliCharge;
                $scope.EditVendorRate.OtherCharges = vendorrate[0].OtherCharges;
                $scope.EditVendorRate.MinKG = vendorrate[0].MinKG;
              
                $("#EditConfigureRateModal").modal("show");
            }, function errorCallback(response) {
                var e = response.message;
            });

    }
    $scope.closeEditRateModal = function () {
        $("#EditConfigureRateModal").modal("hide");
    }
    $scope.UpdateVendorRate = function () {

        if ($scope.frmEditRates.$valid) {
            $http({
                method: 'POST',
                url: '/Setting/UpdateMHELVendorRate',
                data: JSON.stringify($scope.EditVendorRate),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    if (response.data == 1) {
                        Notification.success({ message: "MHEL Vendor Rates updated successfully.", delay: 5000 });
                        $scope.closeEditRateModal();
                        var dtinstance = $scope.dtInstance.DataTable;
                        dtinstance.ajax.reload(function (json) { }, true);
                    }
                    else {
                        Notification.error({ message: "Vendor Rates are already configured for selected criteria.", delay: 5000 });
                    }
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }

        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);
App.controller('MahindraVendorRates_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder) {

    $scope.dtOptions = DTOptionsBuilder.newOptions()

        .withOption('ajax', {
            url: "/Setting/GetMahindraVendorRates",
            type: 'GET'
        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)
        .withOption('stateSave', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('MahindraVendorRateID').withTitle('MahindraVendorRateID').notVisible().notSortable(),
        DTColumnBuilder.newColumn('BillingParty').withTitle('Billing Party').notSortable(),
        DTColumnBuilder.newColumn('Consignor').withTitle('Consignor').notSortable(),
        DTColumnBuilder.newColumn('Consignee').withTitle('Consignee').notSortable(),
        DTColumnBuilder.newColumn('FromBranch').withTitle('From').notSortable(),
        DTColumnBuilder.newColumn('ToBranch').withTitle('To').notSortable(),
        DTColumnBuilder.newColumn('Boxes').withTitle('Boxes').notSortable(),
        DTColumnBuilder.newColumn('Action').notSortable().renderWith(function (data, type, row, meta) {
            return '<a class="pull-left marleft15 font20 delete" ng-click="EditVendorRateData(' + row.MahindraVendorRateID + ')" > <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a class="pull-left marleft15 font20 delete" ng-click="DeleteVendorRate(' + row.MahindraVendorRateID + ')" > <i class="fa fa-trash-o" aria-hidden="true"></i></a>';
        })

    ];
   
    $scope.DeleteVendorRate = function (VendorRateID) {
        var state = confirm("Do you want to delete?");
        if (state == true) {
            $http({
                method: 'POST',
                url: '/Setting/DeleteMahindraVendorRate',
                params: { 'MahindraVendorRateID': VendorRateID },  // pass in data as strings
                headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
            })
                .then(function successCallback(response) {
                    Notification.success({ message: "Mahindra Vendor rate deleted successfully.", delay: 5000 });
                    var dtinstance = $scope.dtInstance.DataTable;
                    dtinstance.ajax.reload(function (json) { }, true);
                }, function errorCallback(response) {
                    var e = response.message;
                });
        }
    }
   
    $scope.ConfigureVendorRates = function () {
        $scope.VendorRate = {
            MahindraVendorRateID: '',
            BillingPartyId: '',
            ConsignorId: '',
            ConsigneeId: '',
            FROM: '',
            TO: '',
            ChargeTypeId: '',
            Boxes: '',
            L: '',
            W: '',
            H: '',
            ChargedWeight: '',
            FRIEGHTRATE: '',
            DocketCharge: '',
            DoorCollection: '',
            DeliveryCharge: '',
            FOV: '',
            HamaliCharge: '',
            OtherCharges: '',
            MinKG: ''
        };
        $scope.GetOtherBranches();
        $scope.GetVendors();
        $scope.GetChargeTypes();
        $scope.GetMHELVendors();
        $("#ConfigureRateModal").modal("show");
    }


    $scope.clearVendorRate = function () {
        $scope.VendorRate = {
            MahindraVendorRateID: '',
            BillingPartyId: '',
            ConsignorId: '',
            ConsigneeId: '',
            FROM: '',
            TO: '',
            ChargeTypeId: '',
            Boxes: '',
            L: '',
            W: '',
            H: '',
            ChargedWeight: '',
            FRIEGHTRATE: '',
            DocketCharge: '',
            DoorCollection: '',
            DeliveryCharge: '',
            FOV: '',
            HamaliCharge: '',
            OtherCharges: '',
            MinKG: ''
        };
    }
    $scope.closeRateModal = function () {
        $scope.clearVendorRate();
        $("#ConfigureRateModal").modal("hide");
    }

    $scope.GetOtherBranches = function () {
   
        var otherBranches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetOtherBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    otherBranches.push(response.data[i]);
                }
                $scope.OtherBranches = otherBranches;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetVendors = function () {

        var vendor = [];
        $http({
            method: 'GET',
            url: '/Operations/GetVendors',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    vendor.push(response.data[i]);
                }
                $scope.Vendors = vendor;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetMHELVendors = function () {

        var vendor = [];
        $http({
            method: 'GET',
            url: '/Operations/GetMHELVendors',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    vendor.push(response.data[i]);
                }
                $scope.BillingVendors = vendor;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.GetChargeTypes = function () {

        var chrgTypes = [];
        $http({
            method: 'GET',
            url: '/Operations/GetChargeTypes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    chrgTypes.push(response.data[i]);
                }
                $scope.ChargeTypes = chrgTypes;
            },
                function errorCallback(response) {
                    alert(response);
                });
    }
    $scope.AddVendorRate = function () {

        if ($scope.frmRates.$valid) {
            $http({
                method: 'POST',
                url: '/Setting/AddMahindraVendorRate',
                data: JSON.stringify($scope.VendorRate),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    if (response.data == 1) {
                        Notification.success({ message: "Mahindra Vendor Rates configured successfully.", delay: 5000 });
                        $scope.closeRateModal();
                        var dtinstance = $scope.dtInstance.DataTable;
                        dtinstance.ajax.reload(function (json) { }, true);
                    }
                    else {
                        Notification.error({ message: "Vendor Rates are already configured for selected criteria.", delay: 5000 });
                    }
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }

        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
    $scope.EditVendorRateData = function (VendorRateID) {
        $scope.GetOtherBranches();
        $scope.GetVendors();
        $scope.GetChargeTypes();
        $scope.GetMHELVendors();
        $scope.EditVendorRate = {
            MahindraVendorRateID: VendorRateID,
            BillingPartyId: '',
            ConsignorId: '',
            ConsigneeId: '',
            FROM: '',
            TO: '',
            ChargeTypeId: '',
            Boxes: '',
            L: '',
            W: '',
            H: '',
            ChargedWeight: '',
            FRIEGHTRATE: '',
            DocketCharge: '',
            DoorCollection: '',
            DeliveryCharge: '',
            FOV: '',
            HamaliCharge: '',
            OtherCharges: '',
            MinKG: ''
        };
        var vendorrate = [];

        $http({
            method: 'POST',
            url: '/Setting/GetMahindraVendorRateById',
            params: { 'MahindraVendorRateID': VendorRateID },  // pass in data as strings
            headers: { 'Content-Type': 'application/json; charset=utf-8' }  // set the headers so angular passing info as form data (not request payload)
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    vendorrate.push(response.data[i]);
                }
                $scope.EditVendorRate.MahindraVendorRateID = vendorrate[0].MahindraVendorRateID;
                $scope.EditVendorRate.BillingPartyId = vendorrate[0].BillingPartyId;
                $scope.EditVendorRate.ConsignorId = vendorrate[0].ConsignorId;
                $scope.EditVendorRate.ConsigneeId = vendorrate[0].ConsigneeId;
                $scope.EditVendorRate.FROM = vendorrate[0].FROM;
                $scope.EditVendorRate.TO = vendorrate[0].TO;
                $scope.EditVendorRate.ChargeTypeId = vendorrate[0].ChargeTypeId;
                $scope.EditVendorRate.Boxes = vendorrate[0].Boxes;
                $scope.EditVendorRate.L = vendorrate[0].L;
                $scope.EditVendorRate.W = vendorrate[0].W;
                $scope.EditVendorRate.H = vendorrate[0].H;
                $scope.EditVendorRate.ChargedWeight = vendorrate[0].ChargedWeight;
                $scope.EditVendorRate.DocketCharge = vendorrate[0].DocketCharge;
                $scope.EditVendorRate.FRIEGHTRATE = vendorrate[0].FRIEGHTRATE;
                $scope.EditVendorRate.DoorCollection = vendorrate[0].DoorCollection;
                $scope.EditVendorRate.DeliveryCharge = vendorrate[0].DeliveryCharge;
                $scope.EditVendorRate.FOV = vendorrate[0].FOV;
                $scope.EditVendorRate.HamaliCharge = vendorrate[0].HamaliCharge;
                $scope.EditVendorRate.OtherCharges = vendorrate[0].OtherCharges;
                $scope.EditVendorRate.MinKG = vendorrate[0].MinKG;
                $("#EditConfigureRateModal").modal("show");
            }, function errorCallback(response) {
                var e = response.message;
            });

    }
    $scope.closeEditRateModal = function () {
        $("#EditConfigureRateModal").modal("hide");
    }
    $scope.UpdateVendorRate = function () {

        if ($scope.frmEditRates.$valid) {
            $http({
                method: 'POST',
                url: '/Setting/UpdateMahindraVendorRate',
                data: JSON.stringify($scope.EditVendorRate),
                headers: { 'Content-Type': 'application/json; charset=utf-8' }
            })
                .then(function successCallback(response) {
                    if (response.data == 1) {
                        Notification.success({ message: "Mahindra Vendor Rates updated successfully.", delay: 5000 });
                        $scope.closeEditRateModal();
                        var dtinstance = $scope.dtInstance.DataTable;
                        dtinstance.ajax.reload(function (json) { }, true);
                    }
                    else {
                        Notification.error({ message: "Vendor Rates are already configured for selected criteria.", delay: 5000 });
                    }
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });
        }

        else {
            Notification.error({ message: "Please fill all the required fields of the form", delay: 5000 });
            return;
        }
    }
}]);
App.controller('POD_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "Excel", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, Excel, $timeout) {
    $scope.Branches = [];
    $scope.GetBranches = function () {
        var brnches = [];
        $http({
            method: 'GET',
            url: '/Operations/GetBranches',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    brnches.push(response.data[i]);
                }
                $scope.Branches = brnches;

            },
                function errorCallback(response) {
                    alert(response);
                });

    }
    $scope.GetBranches();

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            url: "/Operations/GetPODData",
            type: 'GET',
            data: function (d) {
                d.branchId = $("#ddlBranch").val();
            }

        })
        .withDataProp('data')
        .withOption('serverSide', true)
        .withOption('processing', true)
        .withOption('info', true)

        .withOption('destroy', true)
        .withPaginationType('full_numbers')
        .withOption('language', { processing: "<img src='/Images/loading.gif' width='50' height='50' style='margin-top:80px;' />" })
        .withOption('drawCallback', function (settings) {
            $compile(angular.element('#' + settings.sTableId).contents())($scope);
        });
    $scope.dtInstance = {};
    $scope.dtColumns = [
        DTColumnBuilder.newColumn('ManifestConsignmentID').withTitle('ManifestConsignmentID').notVisible(),
        DTColumnBuilder.newColumn('ConsignmentNo').withTitle('Lr No').notSortable(),
        DTColumnBuilder.newColumn('ConsignmentDate').withTitle('Lr Date').notSortable(),
        DTColumnBuilder.newColumn('BillingType').withTitle('Billing Type').notSortable(),
        DTColumnBuilder.newColumn('FromBranch').withTitle('From').notSortable(),
        DTColumnBuilder.newColumn('ToBranch').withTitle('To').notSortable(),
        DTColumnBuilder.newColumn('ConsignorName').withTitle('Consignor').notSortable(),
        DTColumnBuilder.newColumn('DeliveryStatus').withTitle('Delivery Status').notSortable(),
        DTColumnBuilder.newColumn('DeliveredBy').withTitle('Delivered By').notSortable(),
        DTColumnBuilder.newColumn('DeliveredBy').withTitle('Delivered By').notSortable(),
        DTColumnBuilder.newColumn('HOReceivedDate').withTitle('Received Date').notSortable(),
        DTColumnBuilder.newColumn('HOReceivedBy').withTitle('Received By').notSortable(),
        DTColumnBuilder.newColumn('HOReceivedStatus').withTitle('HOReceivedStatus').notVisible(),
        DTColumnBuilder.newColumn('').withTitle('HO Received Status').notSortable().renderWith(function (data, type, row, meta) {
            return '<div><span id="span_' + row.ManifestConsignmentID + '" class="pull-left">' + row.HOReceivedStatus + '</span><select id="drp_' + row.ManifestConsignmentID + '" class="form-control pull-left"  style="display:none"><option value="">-- select --</option><option value="1">Yes</option><option value="0">No</option></select></div>';

        }),
        DTColumnBuilder.newColumn('').notSortable().renderWith(function (data, type, row, meta) {
            return '<div style="width:100%"><a style="width:15%" id="edit_' + row.ManifestConsignmentID + '" data-ng-click="ShowEditMode(' + row.ManifestConsignmentID + ')" class="pull-left font20 marleft15"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>'
                + '<a style="visibility:hidden;width:15%" id="save_' + row.ManifestConsignmentID + '" class="pull-left marleft15 font20 delete" ng-click="updateReceivedStatus(' + row.ManifestConsignmentID + ')" > <i class="fa fa-floppy-o" aria-hidden="true"></i></a> '
                + '<a style="visibility:hidden;width:15%" id="cancel_' + row.ManifestConsignmentID + '" class="pull-left marleft15 font20 delete" ng-click="CancelReceivedStatus(' + row.ManifestConsignmentID + ')" > <i class="fa fa-times" aria-hidden="true"></i></a><div> '
        })

    ];
    $scope.ShowEditMode = function (ManifestConsignmentID) {
        $("#drp_" + ManifestConsignmentID).show();
        $("#span_" + ManifestConsignmentID).hide();
        $("#edit_" + ManifestConsignmentID).css('visibility', 'hidden');
        $("#save_" + ManifestConsignmentID).css('visibility', 'visible');
        $("#cancel_" + ManifestConsignmentID).css('visibility', 'visible');
    }
    $scope.CancelReceivedStatus = function (ManifestConsignmentID) {
        $("#drp_" + ManifestConsignmentID).hide();
        $("#span_" + ManifestConsignmentID).show();
        $("#edit_" + ManifestConsignmentID).css('visibility', 'visible');
        $("#save_" + ManifestConsignmentID).css('visibility', 'hidden');
        $("#cancel_" + ManifestConsignmentID).css('visibility', 'hidden');
    }
    $scope.updateReceivedStatus = function (ManifestConsignmentID) {
        var status = $("#drp_" + ManifestConsignmentID).val();
        if (status != '') {

            $http({
                method: 'POST',
                url: '/Operations/UpdatePODStatus',
                params: { 'status': status, 'ManifestConsignmentID': ManifestConsignmentID }

            })
                .then(function successCallback(response) {
                    console.log(response);
                    Notification.success({ message: "POD Status updated successfully.", delay: 5000 });
                    $scope.GetPOD();
                }, function errorCallback(response) {
                    Notification.error({ message: response.message, delay: 5000 });
                });

        }
    }

    $scope.GetPOD = function () {

        var branchId = $('#ddlBranch').val();

        var dtinstance = $scope.dtInstance.DataTable;
        $scope.dtOptions.ajax.data = function (d) {

            d.branchId = branchId;
        };
        dtinstance.ajax.reload(function (json) { }, true);

    }

}]);

App.controller('MasterSetting_ngController', ["$scope", "$http", "$filter", "filterFilter", "Notification", "$compile", "DTOptionsBuilder", "DTColumnBuilder", "Excel", "$timeout", function ($scope, $http, $filter, filterFilter, Notification, $compile, DTOptionsBuilder, DTColumnBuilder, Excel, $timeout) {

    $('#txtTimeFrom').timepicker({
        template: false,
        showInputs: false,
        minuteStep: 5
    });
    $('#txtTimeTo').timepicker({
        template: false,
        showInputs: false,
        minuteStep: 5
    });
    $('#txtFromDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        forceParse: false,
        clearBtn: true
    }).on('changeDate', function (selected) {
        startDate = new Date(selected.date.valueOf());
        startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
        $('#txtToDate').datepicker('setStartDate', startDate);
    });
    $('#txtToDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        forceParse: false,
        clearBtn: true
    }).on('changeDate', function (selected) {
        FromEndDate = new Date(selected.date.valueOf());
        FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
        $('#txtFromDate').datepicker('setEndDate', FromEndDate);
        });
    $('#txtREXDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        forceParse: false,
        clearBtn: true
    })
    $('#txtVendorStartDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        forceParse: false,
        clearBtn: true
    })
    $scope.GetAttributes = function () {
        $http({
            method: 'GET',
            url: '/Setting/GetAttributes',
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function successCallback(response) {
                for (i = 0; i < response.data.length; i++) {
                    if (response.data[i].Key == "INVOICE_COUNT") {
                        $scope.InvoiceCount = response.data[i].Value;
                    }
                    else if (response.data[i].Key == "TAX_MAX_AMOUNT") {
                        $scope.TaxAmount = response.data[i].Value;
                    }
                    else if (response.data[i].Key == "DOCKET_CHARGE") {
                        $scope.DocketCharge = response.data[i].Value;
                    }
                    else if (response.data[i].Key == "CONSIGNMENT_TIME") {
                        $scope.FromTime = response.data[i].Value.toString().split('-')[0];
                        $scope.ToTime = response.data[i].Value.toString().split('-')[1];
                    }
                    else if (response.data[i].Key == "FINANCIAL_YEAR") {
                        $scope.FromDate = response.data[i].Value.toString().split('-')[0]; 
                        $scope.ToDate = response.data[i].Value.toString().split('-')[1]; 
                    }
                    else if (response.data[i].Key == "REX_DATE") {
                        $scope.REXDate = response.data[i].Value;
                    }
                    else if (response.data[i].Key == "VENDOR_VEHICLE_DATE") {
                        $scope.VendorVehicleDate = response.data[i].Value;
                    }
                    else {
                        if (response.data[i].Value.toString().toLowerCase() == "true")
                            $scope.MahindraPrint = true;
                        else
                            $scope.MahindraPrint = false;
                    }
                }
            },
                function errorCallback(response) {
                    alert(response);
                });

    }
    $scope.GetAttributes();
    $scope.changeAtrributeValue = function (keyname) {
        if (keyname == "CONSIGNMENT_TIME") {
            valueData = $("#txtTimeFrom").val()+" - "+$("#txtTimeTo").val();
        }
        else if (keyname == "FINANCIAL_YEAR") {
            valueData = $("#txtFromDate").val() + " - " + $("#txtToDate").val();
        }
        else if (keyname == "MAHINDRA_PRINT") {
            valueData = $("#chkMahindra").is(":checked");
        }
        else if (keyname == "REX_DATE") {
            valueData = $("#txtREXDate").val();
        }
        else if (keyname == "VENDOR_VEHICLE_DATE") {
            valueData = $("#txtVendorStartDate").val();
        }
        else {
            valueData = $("input[name='" + keyname + "']").val();
        }
        $http({
            method: 'POST',
            url: '/Setting/UpdateMasterAttribute',
            dataType: 'json',
            traditional: true,
            contentType: 'application/json; charset=utf-8',
            params: { 'KeyName': keyname, 'ValueData': valueData }

        })
            .then(function successCallback(response) {
              
            }, function errorCallback(response) {
                Notification.error({ message: response.message, delay: 5000 });
            });
    }
  
}]);

