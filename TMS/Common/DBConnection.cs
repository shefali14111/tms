﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TMS.Common
{
    public class DBConnection : IDisposable
    {

        public SqlConnection connection = new SqlConnection();
        public SqlCommand command = new SqlCommand();
        public void ConnectoDB()
        {
            string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);

            connection.ConnectionString = connectionStr;
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            ConnectionOpen();
        }

        public void ConnectionOpen()
        {
            if (connection.State == ConnectionState.Closed)
                connection.Open();
        }

        public void ConnectionClose()
        {
            if (command.Connection.State == ConnectionState.Open)
                connection.Close();
        }

        public DataTable GetDataFromDB(string commandText)
        {
            try
            {
                ConnectoDB();
                command.CommandText = commandText;
                command.CommandType = CommandType.StoredProcedure;
             
                SqlDataAdapter da = new SqlDataAdapter(command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                ConnectionClose();

                if (ds.Tables.Count != 0)
                    return (ds.Tables[0]);
                else
                    return null;

            }
            catch (Exception x)
            {
                ConnectionClose();
                throw x;
            }
        }

        public object GetCount(string commandText)
        {
            try
            {
                ConnectoDB();
                command.CommandText = commandText;
                command.CommandType = CommandType.StoredProcedure;
            //    command.Parameters.Add("cv_1", OracleDbType.RefCursor).Direction = ParameterDirection.InputOutput;
                object count = command.ExecuteScalar();
                ConnectionClose();
                return count;
            }
            catch (Exception x)
            {
                ConnectionClose();
                throw x;
            }
        }

        public void ExecuteQuery(string commandText)
        {
            try
            {
                ConnectoDB();
                command.CommandText = commandText;
                command.CommandType = CommandType.StoredProcedure;
                command.ExecuteNonQuery();
                ConnectionClose();
                // return (ds.Tables[0]);
            }
            catch (Exception x)
            {
                ConnectionClose();
                throw x;
            }
        }

        public void Dispose()
        {
            try
            {
                command.Dispose();
                connection.Close();
            }
            catch
            {
            }

            // Dispose();
        }
    }

}