﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TMS.EFModel;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace TMS.Common
{
    public class Utility
    {
        internal static void AddErrorlog( ErrorLogging  errLog)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
               

                ;
                db.command.CommandText = "AddErrorlog";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@FunctionName", SqlDbType.VarChar).Value = errLog.FunctionName;
                db.command.Parameters.Add("@UserID", SqlDbType.Int).Value = errLog.UserID;
                db.command.Parameters.Add("@Message", SqlDbType.VarChar).Value = errLog.Message;
                db.command.Parameters.Add("@ErrorStackTrace", SqlDbType.VarChar).Value = errLog.ErrorStackTrace;


                db.command.Parameters.Add("@ErrorInnerException", SqlDbType.VarChar).Value = errLog.ErrorInnerException;
               
                db.command.ExecuteNonQuery();
                db.ConnectionClose();



            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        public static void LogError(Exception ex, string function,int  loginID)
        {
           
                try
                {
                ErrorLogging eLog = new ErrorLogging();
                    if (ex.InnerException != null)
                        eLog.ErrorInnerException = ex.InnerException.ToString();
                    if (ex.StackTrace != null)
                        eLog.ErrorStackTrace = ex.StackTrace;
                    eLog.FunctionName = function;
                    if (ex.Message != null)
                        eLog.Message = ex.Message;
                    eLog.UserID = loginID;
                    AddErrorlog(eLog);
                }
                catch (Exception e)
                {
                }
           
        }
        public static DateTime getCurrentTime()
        {
            DateTime utcTime = DateTime.UtcNow; // gives you current Time in server timeZone
            //DateTime utcTime = serverTime.ToUniversalTime(); // convert it to Utc using timezone setting of server computer
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime localTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, tzi); // convert from utc to local
            return localTime;
        }
    }
}