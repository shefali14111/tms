﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace TMS
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public sealed class AuthorizeAttribute : System.Web.Mvc.AuthorizeAttribute, IAuthorizationFilter
    {
        public bool isAceessDenied = false;
        public USERROLE[] AccessLevels { get; set; }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            string SessionToken = Convert.ToString(HttpContext.Current.Session["UserName"]);
            if (string.IsNullOrEmpty(SessionToken) || SessionToken == " ")
            {
                return false;
            }
            else
            {
                string RoleName = Convert.ToString(System.Web.HttpContext.Current.Session["RoleName"]);
                USERROLE SessionRole = EnumFunction.GetValueFromDescription<USERROLE>(RoleName);
                if (AccessLevels == null)
                    return true;
                else if (AccessLevels.Contains(SessionRole))
                    return true;
                else
                {
                    isAceessDenied = true;
                    return false;
                }
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.HttpContext.Items["AjaxPermissionDenied"] = true;
                if (filterContext.HttpContext.User.Identity.IsAuthenticated == false)
                    filterContext.HttpContext.Response.StatusCode = 401;
                else
                    filterContext.HttpContext.Response.StatusCode = 403;

                filterContext.HttpContext.Response.SuppressFormsAuthenticationRedirect = true;
                filterContext.HttpContext.Response.End();
            }
            if (isAceessDenied == false)
                filterContext.HttpContext.Response.Redirect("/Login/Login");
            else
                filterContext.HttpContext.Response.Redirect("/Login/AccessDenied");
            base.HandleUnauthorizedRequest(filterContext);
        }
    }
}