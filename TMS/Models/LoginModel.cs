﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TMS.Common;
using TMS.EFModel;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace TMS.Models
{
    public class LoginModel
    {
        public static UserMaster ValidateUser(string emailID, String userPword)
        {
            SqlConnection connection = new SqlConnection();
            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();

                connection.ConnectionString = connectionStr;
                cmd.CommandText = "ValidateUser";
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramEmail = new SqlParameter();
                paramEmail.ParameterName = "@UserEmail";
                paramEmail.Direction = ParameterDirection.Input;
                paramEmail.DbType = DbType.String;
                paramEmail.Value = emailID;

                SqlParameter paramPwd = new SqlParameter();
                paramPwd.ParameterName = "@Password";
                paramPwd.Direction = ParameterDirection.Input;
                paramPwd.DbType = DbType.String;
                paramPwd.Value = userPword;
                cmd.Parameters.Add(paramEmail);
                cmd.Parameters.Add(paramPwd);
                cmd.Connection = connection;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                connection.Open();
                da.Fill(ds, "table1");
                connection.Close();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    UserMaster loginUser = new UserMaster();
                    loginUser.UserID = Convert.ToInt32(ds.Tables[0].Rows[0]["UserID"]);
                    loginUser.UserName = ds.Tables[0].Rows[0]["UserName"].ToString();
                    loginUser.BranchID = Convert.ToInt32(ds.Tables[0].Rows[0]["BranchID"]);
                    loginUser.ZoneID = Convert.ToInt32(ds.Tables[0].Rows[0]["ZoneID"]);
                    loginUser.RoleID = Convert.ToInt32(ds.Tables[0].Rows[0]["RoleID"]);
                    if (ds.Tables[0].Rows[0]["ReportingManagerID"] != DBNull.Value)
                        loginUser.ReportingManagerID = Convert.ToInt32(ds.Tables[0].Rows[0]["ReportingManagerID"]);
                    loginUser.RoleName = ds.Tables[0].Rows[0]["RoleName"].ToString();
                    loginUser.BranchName = ds.Tables[0].Rows[0]["BranchName"].ToString();
                    return loginUser;
                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();

                throw ex;
            }



        }

        public static DataTable getUserProfile(string emailID)
        {
            using (DBConnection db = new DBConnection())
            {
                try
                {
                    string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["DbConnection"]);
                    SqlCommand cmd = new SqlCommand();
                    SqlConnection connection = new SqlConnection();
                    connection.ConnectionString = connectionStr;
                    cmd.CommandText = "GetUserProfile";
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter paramEmail = new SqlParameter();
                    paramEmail.ParameterName = "@UserEmail";
                    paramEmail.Direction = ParameterDirection.Input;
                    paramEmail.DbType = DbType.String;
                    paramEmail.Value = emailID;

                    cmd.Parameters.Add(paramEmail);

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds, "table1");
                    db.ConnectionClose();
                    db.Dispose();
                    return ds.Tables[0];
                }
                catch (Exception ex)
                {
                    return null;
                }
            }


        }



    }
}