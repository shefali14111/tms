﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using TMS.Common;
using TMS.CustomModels;
using TMS.EFModel;
using TMS.Helper;

namespace TMS.Models
{
    public class PaymentModel
    {
        #region To Pay Collection
        internal static Dictionary<int, List<ToPayAndPaidConsignment>> GetToPayConsignments(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, DateTime? fromDate, DateTime? toDate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetConsignmentsForToPayAndPaid";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = fromDate;
                db.command.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = toDate;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<ToPayAndPaidConsignment>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<ToPayAndPaidConsignment>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static List<ToPayAndPaidConsignment> GetConsignmentPaymentDetails(int consignmentID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandText = "GetConsignmentPaymentDetails";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pConsignmentID", SqlDbType.Int).Value = consignmentID;
                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<ToPayAndPaidConsignment>(ds.Tables[0]);
                return list;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }

        internal static int AddPaymentForConsignment(PaymentMaster PaymentMaster, int loginID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                DateTime datePayment = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                DateTime CurrentDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                TimeSpan timePayment = CurrentDate.TimeOfDay;

                db.command.CommandText = "AddPaymentForConsignment";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pConsignmentId", SqlDbType.Int).Value = PaymentMaster.ConsignmentId;
                db.command.Parameters.Add("@pReceivedAmount", SqlDbType.Float).Value = PaymentMaster.ReceivedAmount;
                db.command.Parameters.Add("@pPaymentTypeID", SqlDbType.Int).Value = PaymentMaster.PaymentTypeID;
                db.command.Parameters.Add("@pBankName", SqlDbType.VarChar).Value = PaymentMaster.BankName;
                db.command.Parameters.Add("@pBranchName", SqlDbType.VarChar).Value = PaymentMaster.BranchName;
                db.command.Parameters.Add("@pChequeNo", SqlDbType.VarChar).Value = PaymentMaster.ChequeNo;
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = PaymentMaster.BranchID;
                DateTime? Chequedate = null;
                if (!String.IsNullOrEmpty(PaymentMaster.ChequeDate))
                {
                    Chequedate = DateTime.ParseExact(PaymentMaster.ChequeDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                }
                db.command.Parameters.Add("@pChequeDate", SqlDbType.DateTime).Value = Chequedate;
                db.command.Parameters.Add("@pRemarks", SqlDbType.VarChar).Value = PaymentMaster.Remarks;
                db.command.Parameters.Add("@pReceivedBy", SqlDbType.Int).Value = loginID;
                db.command.Parameters.Add("@pReceivedDate", SqlDbType.DateTime).Value = datePayment;
                db.command.Parameters.Add("@pReceivedTime", SqlDbType.Time).Value = timePayment;

                SqlParameter paramID = new SqlParameter();
                paramID.ParameterName = "@PaymentMasterID";
                paramID.Direction = ParameterDirection.Output;
                paramID.DbType = DbType.Int32;
                db.command.Parameters.Add(paramID);


                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int paymentID = Convert.ToInt32(paramID.Value);
                return paymentID;

            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static DataTable GetToPayConsignmentsReport(int? branchId, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentsForToPayAndPaid";
                cmd.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                SqlParameter paramFromDate = new SqlParameter();
                paramFromDate.ParameterName = "@FromDate";
                paramFromDate.Direction = ParameterDirection.Input;
                paramFromDate.DbType = DbType.DateTime;
                paramFromDate.Value = fromDate;
                cmd.Parameters.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter();
                paramToDate.ParameterName = "@ToDate";
                paramToDate.Direction = ParameterDirection.Input;
                paramToDate.DbType = DbType.DateTime;
                paramToDate.Value = toDate;
                cmd.Parameters.Add(paramToDate);


                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static Dictionary<int, List<ToPayAndPaidConsignment>> GetPaymentForToPayAndPaid(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, DateTime? fromDate, DateTime? toDate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetPaymentForToPayAndPaid";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = fromDate;
                db.command.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = toDate;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<ToPayAndPaidConsignment>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<ToPayAndPaidConsignment>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static DataTable GetPaymentConsignmentReport(int? branchId, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetPaymentForToPayAndPaid";
                cmd.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                SqlParameter paramFromDate = new SqlParameter();
                paramFromDate.ParameterName = "@FromDate";
                paramFromDate.Direction = ParameterDirection.Input;
                paramFromDate.DbType = DbType.DateTime;
                paramFromDate.Value = fromDate;
                cmd.Parameters.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter();
                paramToDate.ParameterName = "@ToDate";
                paramToDate.Direction = ParameterDirection.Input;
                paramToDate.DbType = DbType.DateTime;
                paramToDate.Value = toDate;
                cmd.Parameters.Add(paramToDate);


                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Vehicle Hire Balance Payment
        internal static Dictionary<int, List<VehicleHireBalancePayment>> GetVehicleHireBalancePayment(int? branchId, int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetManifestForBalancePayment";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<VehicleHireBalancePayment>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<VehicleHireBalancePayment>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static List<VehicleHireBalancePayment> GetManifestPaymentDetails(int VehicleHireID, string HireType)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandText = "GetManifestPaymentDetails";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pVehicleHireID", SqlDbType.Int).Value = VehicleHireID;
                db.command.Parameters.Add("@pHireType", SqlDbType.VarChar).Value = HireType;
                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<VehicleHireBalancePayment>(ds.Tables[0]);
                return list;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static int AddPaymentForManifest(BalancePaymentMaster PaymentMaster, int loginID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                DateTime datePayment = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                TimeSpan timePayment = datePayment.TimeOfDay;

                db.command.CommandText = "AddPaymentForVehicleHire";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pVehicleHirePaymentId", SqlDbType.Int).Value = PaymentMaster.VehicleHirePaymentId;
                db.command.Parameters.Add("@pBalanceAmount", SqlDbType.Float).Value = PaymentMaster.BalanceAmount;
                db.command.Parameters.Add("@pRemarks", SqlDbType.VarChar).Value = PaymentMaster.Remarks;
                db.command.Parameters.Add("@pPaidBy", SqlDbType.Int).Value = loginID;
                db.command.Parameters.Add("@pPaymentDate", SqlDbType.DateTime).Value = datePayment;
                db.command.Parameters.Add("@pPaymentTime", SqlDbType.Time).Value = timePayment;
                db.command.Parameters.Add("@pPaymentTypeID", SqlDbType.Int).Value = PaymentMaster.PaymentTypeID;
                db.command.Parameters.Add("@pBankName", SqlDbType.VarChar).Value = PaymentMaster.BankName;
                db.command.Parameters.Add("@pBranchName", SqlDbType.VarChar).Value = PaymentMaster.BranchName;
                db.command.Parameters.Add("@pChequeNo", SqlDbType.VarChar).Value = PaymentMaster.ChequeNo;
                db.command.Parameters.Add("@pHireType", SqlDbType.VarChar).Value = PaymentMaster.HireType;
                DateTime? Chequedate = null;
                if (!String.IsNullOrEmpty(PaymentMaster.ChequeDate))
                {
                    Chequedate = DateTime.ParseExact(PaymentMaster.ChequeDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                }
                db.command.Parameters.Add("@pChequeDate", SqlDbType.DateTime).Value = Chequedate;
                SqlParameter paramID = new SqlParameter();
                paramID.ParameterName = "@BalancePaymentMasterID";
                paramID.Direction = ParameterDirection.Output;
                paramID.DbType = DbType.Int32;
                db.command.Parameters.Add(paramID);


                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int paymentID = Convert.ToInt32(paramID.Value);
                return paymentID;

            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }

        internal static Dictionary<int, List<PaymentTypaSales>> GetPaymentTypeSalesData(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, DateTime? dtfrom, DateTime? dtto)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetPaymentTypeSalesReport";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = dtfrom;
                db.command.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = dtto;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                DataTable dtPaymentSales = ds.Tables[0];
                if (dtPaymentSales.Rows.Count >= 1)
                {
                    DataRow dtrow = dtPaymentSales.NewRow();
                    dtrow["PaymentTypeID"] = 0;
                    dtrow["PaymentType"] = "Total";
                    dtrow["TotalLR"] = Convert.ToInt32(dtPaymentSales.Compute("Sum(TotalLR)", string.Empty));
                    dtrow["TotalArticles"] = Convert.ToInt32(dtPaymentSales.Compute("Sum(TotalArticles)", string.Empty));
                    dtrow["ActualWeight"] = Convert.ToDouble(dtPaymentSales.Compute("Sum(ActualWeight)", string.Empty));
                    dtrow["ChargedWeight"] = Convert.ToDouble(dtPaymentSales.Compute("Sum(ChargedWeight)", string.Empty));
                    dtrow["InvoiceValue"] = Convert.ToDouble(dtPaymentSales.Compute("Sum(InvoiceValue)", string.Empty));
                    dtrow["GrantTotal"] = Convert.ToDouble(dtPaymentSales.Compute("Sum(GrantTotal)", string.Empty));
                    dtPaymentSales.Rows.Add(dtrow);
                }
                var list = DatatableHelper.ToList<PaymentTypaSales>(dtPaymentSales);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<PaymentTypaSales>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Vendor Slaes Report
        internal static Dictionary<int, List<PaymentTypaSales>> GetPaymentTypeVendorSalesData(int? vendorId, int start, int length, string search, int sortColumn, string sortDirection, DateTime? dtfrom, DateTime? dtto)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetPaymentTypeVendorSalesReport";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pVendorId", SqlDbType.Int).Value = vendorId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = dtfrom;
                db.command.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = dtto;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                DataTable dtPaymentSales = ds.Tables[0];
                if (dtPaymentSales.Rows.Count >= 1)
                {
                    DataRow dtrow = dtPaymentSales.NewRow();
                    dtrow["PaymentTypeID"] = 0;
                    dtrow["PaymentType"] = "Total";
                    dtrow["TotalLR"] = Convert.ToInt32(dtPaymentSales.Compute("Sum(TotalLR)", string.Empty));
                    dtrow["TotalArticles"] = Convert.ToInt32(dtPaymentSales.Compute("Sum(TotalArticles)", string.Empty));
                    dtrow["ActualWeight"] = Convert.ToDouble(dtPaymentSales.Compute("Sum(ActualWeight)", string.Empty));
                    dtrow["ChargedWeight"] = Convert.ToDouble(dtPaymentSales.Compute("Sum(ChargedWeight)", string.Empty));
                    dtrow["InvoiceValue"] = Convert.ToDouble(dtPaymentSales.Compute("Sum(InvoiceValue)", string.Empty));
                    dtrow["GrantTotal"] = Convert.ToDouble(dtPaymentSales.Compute("Sum(GrantTotal)", string.Empty));
                    dtPaymentSales.Rows.Add(dtrow);
                }
                var list = DatatableHelper.ToList<PaymentTypaSales>(dtPaymentSales);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<PaymentTypaSales>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region MHEL Vendor Slaes Report
        internal static Dictionary<int, List<PaymentTypaSales>> GetPaymentTypeMHELVendorSalesData(int? vendorId, int start, int length, string search, int sortColumn, string sortDirection, DateTime? dtfrom, DateTime? dtto)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetPaymentTypeMHELVendorSalesData";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pVendorId", SqlDbType.Int).Value = vendorId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = dtfrom;
                db.command.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = dtto;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                DataTable dtPaymentSales = ds.Tables[0];
                if (dtPaymentSales.Rows.Count >= 1)
                {
                    DataRow dtrow = dtPaymentSales.NewRow();
                    dtrow["PaymentTypeID"] = 0;
                    dtrow["PaymentType"] = "Total";
                    dtrow["TotalLR"] = Convert.ToInt32(dtPaymentSales.Compute("Sum(TotalLR)", string.Empty));
                    dtrow["TotalArticles"] = Convert.ToInt32(dtPaymentSales.Compute("Sum(TotalArticles)", string.Empty));
                    dtrow["ActualWeight"] = Convert.ToDouble(dtPaymentSales.Compute("Sum(ActualWeight)", string.Empty));
                    dtrow["ChargedWeight"] = Convert.ToDouble(dtPaymentSales.Compute("Sum(ChargedWeight)", string.Empty));
                    dtrow["InvoiceValue"] = Convert.ToDouble(dtPaymentSales.Compute("Sum(InvoiceValue)", string.Empty));
                    dtrow["GrantTotal"] = Convert.ToDouble(dtPaymentSales.Compute("Sum(GrantTotal)", string.Empty));
                    dtPaymentSales.Rows.Add(dtrow);
                }
                var list = DatatableHelper.ToList<PaymentTypaSales>(dtPaymentSales);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<PaymentTypaSales>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Deposite
        internal static Dictionary<int, List<DepositeMaster>> GetDeposites(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, DateTime? fromDate, DateTime? toDate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetDeposites";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<DepositeMaster>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<DepositeMaster>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }

        internal static void AddDeposite(DepositeMaster DepositeMaster)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                DateTime dateDeposite = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

                db.command.CommandText = "AddDeposite";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pDate", SqlDbType.DateTime).Value = dateDeposite;
                db.command.Parameters.Add("@pBranchID", SqlDbType.Int).Value = DepositeMaster.BranchID;
                db.command.Parameters.Add("@pDepositeAmount", SqlDbType.Float).Value = DepositeMaster.DepositeAmount;


                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }

        internal static void DeleteDeposite(int DepositeID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteDeposite";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pDepositeID", SqlDbType.Int).Value = DepositeID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Vehicle Vendor Deposite
        internal static Dictionary<int, List<VehicleVendorDepositeMaster>> GetVehicleVendorDeposites(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, DateTime? fromDate, DateTime? toDate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetVehicleVendorDeposites";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<VehicleVendorDepositeMaster>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<VehicleVendorDepositeMaster>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }

        internal static void AddVehicleVendorDeposite(VehicleVendorDepositeMaster DepositeMaster)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                DateTime dateDeposite = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

                db.command.CommandText = "AddVehicleVendorDeposite";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pDate", SqlDbType.DateTime).Value = dateDeposite;
                db.command.Parameters.Add("@pBranchID", SqlDbType.Int).Value = DepositeMaster.BranchID;
                db.command.Parameters.Add("@pDepositeAmount", SqlDbType.Float).Value = DepositeMaster.DepositeAmount;
                db.command.Parameters.Add("@pVehicleVendorID", SqlDbType.Int).Value = DepositeMaster.VehicleVendorID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }

        internal static void DeleteVehicleVendorDeposite(int DepositeID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteVehicleVendorDeposite";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pDepositeID", SqlDbType.Int).Value = DepositeID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region Transaction Report
        internal static DataTable GetTransactionReport(int? branchId, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetTransactionReport";
                cmd.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                SqlParameter paramFromDate = new SqlParameter();
                paramFromDate.ParameterName = "@FromDate";
                paramFromDate.Direction = ParameterDirection.Input;
                paramFromDate.DbType = DbType.DateTime;
                paramFromDate.Value = fromDate;
                cmd.Parameters.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter();
                paramToDate.ParameterName = "@ToDate";
                paramToDate.Direction = ParameterDirection.Input;
                paramToDate.DbType = DbType.DateTime;
                paramToDate.Value = toDate;
                cmd.Parameters.Add(paramToDate);
                cmd.CommandTimeout = 0;
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static Dictionary<int, List<TransactionModel>> GetTransactionReportIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandText = "GetTransactionReport";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;

                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
                db.command.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;

                db.command.CommandTimeout = 0;
                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                int count = 0;
                var data = new Dictionary<int, List<TransactionModel>>();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToString(ds.Tables[1].Rows[0][0]) != "")
                    {
                        count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                    }
                    var list = DatatableHelper.ToList<TransactionModel>(ds.Tables[0]);
                    if (list != null)
                    {
                        list = list.OrderBy(l => l.OrderDate).ToList();
                        var obj = list.FirstOrDefault();
                        obj.BalanceFlow = count;
                    }

                  
                    data.Add(count, list);
                }
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }
        #endregion

        #region Other Vendor Balance Payment
        internal static Dictionary<int, List<VendorExpenseMaster>> GetOtherVendorBalancePayment(int? branchId, int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetVendorExpenseForBalancePayment";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<VendorExpenseMaster>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<VendorExpenseMaster>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static List<VendorExpenseMaster> GetVendorExpenseDetails(int VendorExpenseID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandText = "GetOtherVendorPaymentDetails";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pVendorExpenseID", SqlDbType.Int).Value = VendorExpenseID;
                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<VendorExpenseMaster>(ds.Tables[0]);
                return list;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static int AddPaymentForOtherVendor(OtherVendorBalancePaymentMaster PaymentMaster, int loginID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                DateTime datePayment = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                TimeSpan timePayment = datePayment.TimeOfDay;

                db.command.CommandText = "AddPaymentForOtherVendor";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pVendorExpenseID", SqlDbType.Int).Value = PaymentMaster.VendorExpenseID;
                db.command.Parameters.Add("@pBalanceAmount", SqlDbType.Float).Value = PaymentMaster.BalanceAmount;
                db.command.Parameters.Add("@pRemarks", SqlDbType.VarChar).Value = PaymentMaster.Remarks;
                db.command.Parameters.Add("@pPaidBy", SqlDbType.Int).Value = loginID;
                db.command.Parameters.Add("@pPaymentDate", SqlDbType.DateTime).Value = datePayment;
                db.command.Parameters.Add("@pPaymentTime", SqlDbType.Time).Value = timePayment;
                db.command.Parameters.Add("@pPaymentTypeID", SqlDbType.Int).Value = PaymentMaster.PaymentTypeID;
                db.command.Parameters.Add("@pBankName", SqlDbType.VarChar).Value = PaymentMaster.BankName;
                db.command.Parameters.Add("@pBranchName", SqlDbType.VarChar).Value = PaymentMaster.BranchName;
                db.command.Parameters.Add("@pChequeNo", SqlDbType.VarChar).Value = PaymentMaster.ChequeNo;
                DateTime? Chequedate = null;
                if (!String.IsNullOrEmpty(PaymentMaster.ChequeDate))
                {
                    Chequedate = DateTime.ParseExact(PaymentMaster.ChequeDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                }
                db.command.Parameters.Add("@pChequeDate", SqlDbType.DateTime).Value = Chequedate;
                SqlParameter paramID = new SqlParameter();
                paramID.ParameterName = "@BalancePaymentMasterID";
                paramID.Direction = ParameterDirection.Output;
                paramID.DbType = DbType.Int32;
                db.command.Parameters.Add(paramID);


                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int paymentID = Convert.ToInt32(paramID.Value);
                return paymentID;

            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }

        #endregion
    }
}