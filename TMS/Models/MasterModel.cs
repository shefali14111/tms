﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TMS.Common;
using TMS.EFModel;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using TMS.Helper;
using TMS.CustomModels;
using System.Globalization;

namespace TMS.Models
{
    public class MasterModel
    {
        public static string GetAttributeValueByKey(string key)
        {
            try
            {
                DBConnection db = new DBConnection();
                db.ConnectoDB();

                db.command.CommandType = CommandType.StoredProcedure;
                db.command.CommandText = "GetAttributeValueByKey";
                SqlParameter paramKey = new SqlParameter();
                paramKey.ParameterName = "@pKey";
                paramKey.Direction = ParameterDirection.Input;
                paramKey.DbType = DbType.String;
                paramKey.Value = key;

                db.command.Parameters.Add("@AttributeValue", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                db.command.Parameters.Add(paramKey);
                db.ConnectionOpen();
                db.command.ExecuteNonQuery();
                var attributeval = Convert.ToString(db.command.Parameters["@AttributeValue"].Value);
                db.ConnectionClose();
                return attributeval;

            }
            catch (Exception x)
            {
                return null;
            }
        }

        public static string GetBranchNameByBranchId(int branchId)
        {
            try
            {
                DBConnection db = new DBConnection();
                db.ConnectoDB();

                db.command.CommandType = CommandType.StoredProcedure;
                db.command.CommandText = "GetBranchNameByBranchId";
                SqlParameter paramKey = new SqlParameter();
                paramKey.ParameterName = "@pBranchId";
                paramKey.Direction = ParameterDirection.Input;
                paramKey.DbType = DbType.Int32;
                paramKey.Value = branchId;

                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                db.command.Parameters.Add(paramKey);
                db.ConnectionOpen();
                db.command.ExecuteNonQuery();
                var attributeval = Convert.ToString(db.command.Parameters["@BranchName"].Value);
                db.ConnectionClose();
                return attributeval;

            }
            catch (Exception x)
            {
                return null;
            }
        }


        public static string GetPaymentTypeById(int paymentTypeId)
        {
            try
            {
                DBConnection db = new DBConnection();
                db.ConnectoDB();

                db.command.CommandType = CommandType.StoredProcedure;
                db.command.CommandText = "GetPaymentTypeById";
                SqlParameter paramKey = new SqlParameter();
                paramKey.ParameterName = "@pPaymentTypeId";
                paramKey.Direction = ParameterDirection.Input;
                paramKey.DbType = DbType.Int32;
                paramKey.Value = paymentTypeId;

                db.command.Parameters.Add("@PaymentType", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                db.command.Parameters.Add(paramKey);
                db.ConnectionOpen();
                db.command.ExecuteNonQuery();
                var attributeval = Convert.ToString(db.command.Parameters["@PaymentType"].Value);
                db.ConnectionClose();
                return attributeval;

            }
            catch (Exception x)
            {
                return null;
            }
        }

        public static List<VendorRateMaster> GetVendorRate(VendorRateMaster vendorrate)
        {
            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetVendorRates";

                cmd.Parameters.Add("@pBranchId", SqlDbType.Int).Value = vendorrate.BranchId;
                cmd.Parameters.Add("@pBillingPartyId", SqlDbType.Int).Value = vendorrate.BillingPartyId;
                cmd.Parameters.Add("@pConsignorId", SqlDbType.Int).Value = vendorrate.ConsignorId;
                cmd.Parameters.Add("@pConsigneeId", SqlDbType.Int).Value = vendorrate.ConsigneeId;
                cmd.Parameters.Add("@pFROM", SqlDbType.Int).Value = vendorrate.FROM;
                cmd.Parameters.Add("@pTO", SqlDbType.Int).Value = vendorrate.TO;
                cmd.Parameters.Add("@pChargeTypeId", SqlDbType.Int).Value = vendorrate.ChargeTypeId;
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                if (ds.Tables.Count > 0)
                {
                    return DatatableHelper.ToList<VendorRateMaster>(ds.Tables[0]);
                }
                return null;
            }
            catch (Exception x)
            {
                return null;
            }
        }
        public static List<MHELVendorRateMaster> GetMHELVendorRate(MHELVendorRateMaster vendorrate)
        {
            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetMHELVendorRates";
                cmd.Parameters.Add("@pBillingPartyId", SqlDbType.Int).Value = vendorrate.BillingPartyId;
                cmd.Parameters.Add("@pConsignorId", SqlDbType.Int).Value = vendorrate.ConsignorId;
                cmd.Parameters.Add("@pConsigneeId", SqlDbType.Int).Value = vendorrate.ConsigneeId;
                cmd.Parameters.Add("@pFROM", SqlDbType.Int).Value = vendorrate.FROM;
                cmd.Parameters.Add("@pTO", SqlDbType.Int).Value = vendorrate.TO;
                cmd.Parameters.Add("@pChargeTypeId", SqlDbType.Int).Value = vendorrate.ChargeTypeId;
                cmd.Parameters.Add("@pPartNo", SqlDbType.VarChar).Value = vendorrate.PartNo;
                cmd.Parameters.Add("@pPerPieceWeight", SqlDbType.Float).Value = vendorrate.PerPieceWeight;
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                if (ds.Tables.Count > 0)
                {
                    return DatatableHelper.ToList<MHELVendorRateMaster>(ds.Tables[0]);
                }
                return null;
            }
            catch (Exception x)
            {
                return null;
            }
        }
        public static List<MahindraVendorRateMaster> GetMahindraVendorRate(MahindraVendorRateMaster vendorrate)
        {
            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetMahindraVendorRate";
                cmd.Parameters.Add("@pBillingPartyId", SqlDbType.Int).Value = vendorrate.BillingPartyId;
                cmd.Parameters.Add("@pConsignorId", SqlDbType.Int).Value = vendorrate.ConsignorId;
                cmd.Parameters.Add("@pConsigneeId", SqlDbType.Int).Value = vendorrate.ConsigneeId;
                cmd.Parameters.Add("@pFROM", SqlDbType.Int).Value = vendorrate.FROM;
                cmd.Parameters.Add("@pTO", SqlDbType.Int).Value = vendorrate.TO;
                cmd.Parameters.Add("@pChargeTypeId", SqlDbType.Int).Value = vendorrate.ChargeTypeId;
                cmd.Parameters.Add("@pBoxes", SqlDbType.Int).Value = vendorrate.Boxes;
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                if (ds.Tables.Count > 0)
                {
                    return DatatableHelper.ToList<MahindraVendorRateMaster>(ds.Tables[0]);
                }
                return null;
            }
            catch (Exception x)
            {
                return null;
            }
        }

        public static void UpdateMasterAttribute(string keyName, string valueData)
        {
            string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = connectionStr;

            try
            {
                SqlCommand updateCmd = new SqlCommand();
                updateCmd.CommandText = "UpdateMasterAttribute";
                updateCmd.CommandType = CommandType.StoredProcedure;

                updateCmd.Parameters.Add("@pKey", SqlDbType.VarChar).Value = keyName;
                updateCmd.Parameters.Add("@pValue", SqlDbType.VarChar).Value = valueData;
                updateCmd.Connection = connection;
                connection.Open();
                updateCmd.ExecuteNonQuery();
                connection.Close();

            }///end try
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();

                throw new Exception(ex.Message);
            }

        }

        public static DataTable GetManifestsForBranch(int branchId)
        {
            SqlConnection connection = new SqlConnection();
            SqlCommand command = new SqlCommand();
            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);

                connection.ConnectionString = connectionStr;
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                connection.Open();
                command.CommandText = "GetManifestsForBranch";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@pBranchID", SqlDbType.Int).Value = branchId;
                SqlDataAdapter da = new SqlDataAdapter(command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                if (ds.Tables.Count != 0)
                    return (ds.Tables[0]);
                else
                    return null;

            }
            catch (Exception x)
            {
                connection.Close();
                throw x;
            }
        }
        public static DataTable GetAccounts(int branchId)
        {
            SqlConnection connection = new SqlConnection();
            SqlCommand command = new SqlCommand();
            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);

                connection.ConnectionString = connectionStr;
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                connection.Open();
                command.CommandText = "GetAccounts";
                command.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter(command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                if (ds.Tables.Count != 0)
                    return (ds.Tables[0]);
                else
                    return null;

            }
            catch (Exception x)
            {
                connection.Close();
                throw x;
            }
        }

        #region Users
        internal static Dictionary<int, List<UserModel>> GetUserIndex(int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetUserIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<UserModel>(ds.Tables[0]);
                int count = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<UserModel>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }
        internal static object GetAdminUsers()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<UserMaster> list = new List<UserMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetAdminUsers");
                return list = DatatableHelper.ToList<UserMaster>(dt);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        internal static void AddUser(UserMaster user, int loginID)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddUser";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@UserName", SqlDbType.VarChar).Value = user.UserName;
                db.command.Parameters.Add("@Address", SqlDbType.VarChar).Value = user.Address;
                db.command.Parameters.Add("@Pincode", SqlDbType.VarChar).Value = user.Pincode;
                db.command.Parameters.Add("@city", SqlDbType.VarChar).Value = user.city;
                db.command.Parameters.Add("@state", SqlDbType.VarChar).Value = user.state;
                db.command.Parameters.Add("@Nationalty", SqlDbType.VarChar).Value = user.Nationalty;
                db.command.Parameters.Add("@EmailID", SqlDbType.VarChar).Value = user.EmailID;
                db.command.Parameters.Add("@Password", SqlDbType.VarChar).Value = user.Password;
                db.command.Parameters.Add("@ContactNo", SqlDbType.VarChar).Value = user.ContactNo;
                db.command.Parameters.Add("@ZoneID", SqlDbType.Int).Value = user.ZoneID;
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = user.BranchID;
                db.command.Parameters.Add("@RoleID", SqlDbType.Int).Value = user.RoleID;
                db.command.Parameters.Add("@ReportingManagerID", SqlDbType.Int).Value = user.ReportingManagerID;
                db.command.Parameters.Add("@DepartmentID", SqlDbType.Int).Value = user.DepartmentID;
                db.command.Parameters.Add("@HO", SqlDbType.Int).Value = user.HO;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteUser(int userID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteUser";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@puserID", SqlDbType.Int).Value = userID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static List<UserMaster> GetUserByID(int UserID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetUsersByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@UserID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = UserID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<UserMaster> list = new List<UserMaster>();
                var listUser = DatatableHelper.ToList<UserMaster>(ds.Tables[0]);
                return listUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static void UpdateUser(UserMaster user)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateUser";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@UserID", SqlDbType.VarChar).Value = user.UserID;
                db.command.Parameters.Add("@UserName", SqlDbType.VarChar).Value = user.UserName;
                db.command.Parameters.Add("@Address", SqlDbType.VarChar).Value = user.Address;
                db.command.Parameters.Add("@Pincode", SqlDbType.VarChar).Value = user.Pincode;
                db.command.Parameters.Add("@city", SqlDbType.VarChar).Value = user.city;
                db.command.Parameters.Add("@state", SqlDbType.VarChar).Value = user.state;
                db.command.Parameters.Add("@Nationalty", SqlDbType.VarChar).Value = user.Nationalty;
                db.command.Parameters.Add("@EmailID", SqlDbType.VarChar).Value = user.EmailID;
                db.command.Parameters.Add("@Password", SqlDbType.VarChar).Value = user.Password;
                db.command.Parameters.Add("@ContactNo", SqlDbType.VarChar).Value = user.ContactNo;
                db.command.Parameters.Add("@ZoneID", SqlDbType.Int).Value = user.ZoneID;
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = user.BranchID;
                db.command.Parameters.Add("@RoleID", SqlDbType.Int).Value = user.RoleID;
                db.command.Parameters.Add("@ReportingManagerID", SqlDbType.Int).Value = user.ReportingManagerID;
                db.command.Parameters.Add("@DepartmentID", SqlDbType.Int).Value = user.DepartmentID;
                db.command.Parameters.Add("@HO", SqlDbType.Int).Value = user.HO;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        #endregion

        #region Zone
        internal static object GetZones()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<ZoneMaster> list = new List<ZoneMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetZones");
                return list = DatatableHelper.ToList<ZoneMaster>(dt);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        #endregion

        #region Role
        internal static object GetRoles()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<RoleMaster> list = new List<RoleMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetRoles");
                return list = DatatableHelper.ToList<RoleMaster>(dt);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        internal static void AddRole(RoleMaster role)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddRole";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@RoleName", SqlDbType.VarChar).Value = role.RoleName;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateRole(RoleMaster role)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateRole";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@RoleID", SqlDbType.Int).Value = role.RoleID;
                db.command.Parameters.Add("@RoleName", SqlDbType.VarChar).Value = role.RoleName;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteRole(int roleID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteRole";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pRoleID", SqlDbType.Int).Value = roleID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Packaging
        internal static object GetPackagings()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<PackagingMaster> list = new List<PackagingMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetPackagings");
                return list = DatatableHelper.ToList<PackagingMaster>(dt);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        internal static void AddPackaging(PackagingMaster packaging)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddPackaging";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@Packaging", SqlDbType.VarChar).Value = packaging.Packaging;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdatePackaging(PackagingMaster packaging)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdatePackaging";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@PackagingID", SqlDbType.Int).Value = packaging.PackagingID;
                db.command.Parameters.Add("@Packaging", SqlDbType.VarChar).Value = packaging.Packaging;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeletePackaging(int packagingID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeletePackaging";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@packagingID", SqlDbType.Int).Value = packagingID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Department
        internal static object GetDepartments()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<DepartmentMaster> list = new List<DepartmentMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetDepartments");
                return list = DatatableHelper.ToList<DepartmentMaster>(dt);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        #endregion

        #region ODA
        internal static object GetODA()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<ODAMaster> list = new List<ODAMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetODA");
                return list = DatatableHelper.ToList<ODAMaster>(dt);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        internal static void AddODA(ODAMaster oda)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddODA";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@Pincode", SqlDbType.NVarChar).Value = oda.PinCode;
                db.command.Parameters.Add("@Amount", SqlDbType.Decimal).Value = oda.ODAAmount;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateODA(ODAMaster oda)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateODA";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ODAId", SqlDbType.Int).Value = oda.ODAID;
                db.command.Parameters.Add("@Pincode", SqlDbType.NVarChar).Value = oda.PinCode;
                db.command.Parameters.Add("@Amount", SqlDbType.Decimal).Value = oda.ODAAmount;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteODA(int odaID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteODA";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pODAID", SqlDbType.Int).Value = odaID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Payment Type
        internal static object GetPaymentTypes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<PaymentTypeMaster> list = new List<PaymentTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetPaymentTypes");
                return list = DatatableHelper.ToList<PaymentTypeMaster>(dt);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        internal static void AddPaymentType(PaymentTypeMaster paymenttype)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddPaymentType";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@PaymentType", SqlDbType.VarChar).Value = paymenttype.PaymentType;
                db.command.Parameters.Add("@Description", SqlDbType.VarChar).Value = paymenttype.Description;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdatePaymentType(PaymentTypeMaster paymenttype)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdatePaymentType";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = paymenttype.PaymentTypeID;
                db.command.Parameters.Add("@PaymentType", SqlDbType.VarChar).Value = paymenttype.PaymentType;
                db.command.Parameters.Add("@Description", SqlDbType.VarChar).Value = paymenttype.Description;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeletePaymentType(int paymenttypeID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeletePaymentType";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@paymenttypeID", SqlDbType.Int).Value = paymenttypeID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Vehicle Hire Payment Type
        internal static object GetVehicleHirePaymentType()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<VehicleHirePaymentTypeMaster> list = new List<VehicleHirePaymentTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetVehicleHirePaymentTypes");
                return list = DatatableHelper.ToList<VehicleHirePaymentTypeMaster>(dt);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        internal static void AddVehicleHirePaymentType(VehicleHirePaymentTypeMaster paymenttype)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddVehicleHirePaymentType";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@PaymentType", SqlDbType.VarChar).Value = paymenttype.PaymentType;
                db.command.Parameters.Add("@Description", SqlDbType.VarChar).Value = paymenttype.Description;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateVehicleHirePaymentType(VehicleHirePaymentTypeMaster paymenttype)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateVehicleHirePaymentType";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = paymenttype.PaymentTypeID;
                db.command.Parameters.Add("@PaymentType", SqlDbType.VarChar).Value = paymenttype.PaymentType;
                db.command.Parameters.Add("@Description", SqlDbType.VarChar).Value = paymenttype.Description;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteVehicleHirePaymentType(int paymenttypeID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteVehicleHirePaymentType";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@paymenttypeID", SqlDbType.Int).Value = paymenttypeID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Cargo Type
        internal static object GetCargoType()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<CargoTypeMaster> list = new List<CargoTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetCargoTypes");
                return list = Helper.DatatableHelper.ToList<CargoTypeMaster>(dt);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        internal static void AddCargoType(CargoTypeMaster cargotype)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddCargoType";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@CargoType", SqlDbType.VarChar).Value = cargotype.CargoType;
                db.command.Parameters.Add("@Description", SqlDbType.VarChar).Value = cargotype.Description;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateCargoType(CargoTypeMaster cargotype)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateCargoType";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@CargoTypeID", SqlDbType.Int).Value = cargotype.CargoTypeID;
                db.command.Parameters.Add("@CargoType", SqlDbType.VarChar).Value = cargotype.CargoType;
                db.command.Parameters.Add("@Description", SqlDbType.VarChar).Value = cargotype.Description;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteCargoType(int cargotypeID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteCargoType";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@cargotypeID", SqlDbType.Int).Value = cargotypeID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Charge Type
        internal static object GetChargeType()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<ChargeTypeMaster> list = new List<ChargeTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetChargeTypes");
                return list = Helper.DatatableHelper.ToList<ChargeTypeMaster>(dt);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        internal static void AddChargeType(ChargeTypeMaster chargetype)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddChargeType";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ChargeType", SqlDbType.VarChar).Value = chargetype.ChargeType;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateChargeType(ChargeTypeMaster chargetype)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateChargeType";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ChargeTypeID", SqlDbType.Int).Value = chargetype.ChargeTypeID;
                db.command.Parameters.Add("@ChargeType", SqlDbType.VarChar).Value = chargetype.ChargeType;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteChargeType(int chargetypeID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteChargeType";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@chargetypeID", SqlDbType.Int).Value = chargetypeID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Account
        internal static object GetAccount()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<AccountMaster> list = new List<AccountMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetAccounts");
                return list = Helper.DatatableHelper.ToList<AccountMaster>(dt);
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        internal static void AddAccount(AccountMaster acct, int loginID)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddAccount";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@AccountHead", SqlDbType.VarChar).Value = acct.AccountHead;
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = loginID;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateAccount(AccountMaster account)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateAccount";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@AccountID", SqlDbType.Int).Value = account.AccountID;
                db.command.Parameters.Add("@AccountHead", SqlDbType.VarChar).Value = account.AccountHead;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteAccount(int AccountID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteAccount";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@AccountID", SqlDbType.Int).Value = AccountID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Attribute
        internal static object GetAttribute()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<AttributeMaster> list = new List<AttributeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetAttribute");
                list = Helper.DatatableHelper.ToList<AttributeMaster>(dt);
                var objYear = list.Where(l => l.Key == "FINANCIAL_YEAR").FirstOrDefault();
                var objREX = list.Where(l => l.Key == "REX_DATE").FirstOrDefault();
                var objVendorDate = list.Where(l => l.Key == "VENDOR_VEHICLE_DATE").FirstOrDefault();
                if (objYear != null)
                {
                    DateTime start = DateTime.ParseExact(objYear.Value.Split('-')[0].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime end = DateTime.ParseExact(objYear.Value.Split('-')[1].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    objYear.Value = start.ToString("dd/MM/yyyy") + "-" + end.ToString("dd/MM/yyyy");
                }
                if (objREX != null)
                {
                    DateTime rexdate = DateTime.ParseExact(objREX.Value.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    objREX.Value = rexdate.ToString("dd/MM/yyyy");
                }
                if (objVendorDate != null)
                {
                    DateTime vendordate = DateTime.ParseExact(objVendorDate.Value.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    objVendorDate.Value = vendordate.ToString("dd/MM/yyyy");
                }

                return list;
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        internal static void AddAttribute(AttributeMaster acct)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddAttribute";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@Name", SqlDbType.VarChar).Value = acct.Name;
                db.command.Parameters.Add("@Key", SqlDbType.VarChar).Value = acct.Key;
                db.command.Parameters.Add("@Value", SqlDbType.VarChar).Value = acct.Value;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateAttribute(AttributeMaster account)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateAttribute";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@AttributeID", SqlDbType.Int).Value = account.AttributeId;
                db.command.Parameters.Add("@Name", SqlDbType.VarChar).Value = account.Name;
                db.command.Parameters.Add("@Value", SqlDbType.VarChar).Value = account.Value;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteAttribute(int AttributeID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteAttribute";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@AttributeID", SqlDbType.Int).Value = AttributeID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Goods
        internal static List<GoodsMaster> GetGoods()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<GoodsMaster> list = new List<GoodsMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetGoods");
                return list = DatatableHelper.ToList<GoodsMaster>(dt);
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        internal static void AddGoods(GoodsMaster goods)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddGoods";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@GoodsDescription", SqlDbType.VarChar).Value = goods.GoodsDescription;
                db.command.Parameters.Add("@GoodsCode", SqlDbType.VarChar).Value = goods.GoodsCode;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateGoods(GoodsMaster goods)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateGoods";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@GoodsID", SqlDbType.Int).Value = goods.GoodsID;
                db.command.Parameters.Add("@GoodsDescription", SqlDbType.VarChar).Value = goods.GoodsDescription;
                db.command.Parameters.Add("@GoodsCode", SqlDbType.VarChar).Value = goods.GoodsCode;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteGoods(int GoodsID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteGoods";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@GoodsID", SqlDbType.Int).Value = GoodsID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Branch
        internal static Dictionary<int, List<BranchMaster>> GetBranchIndex(int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetBranchIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<BranchMaster>(ds.Tables[0]);
                int count = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<BranchMaster>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }

        internal static int AddBranch(BranchMaster branch, int loginID)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddBranch";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = branch.BranchName;
                db.command.Parameters.Add("@ZoneID", SqlDbType.Int).Value = branch.ZoneID;
                db.command.Parameters.Add("@BranchCode", SqlDbType.VarChar).Value = branch.BranchCode;
                db.command.Parameters.Add("@Address", SqlDbType.VarChar).Value = branch.Address;
                db.command.Parameters.Add("@Pincode", SqlDbType.VarChar).Value = branch.Pincode;
                db.command.Parameters.Add("@city", SqlDbType.VarChar).Value = branch.city;
                db.command.Parameters.Add("@state", SqlDbType.VarChar).Value = branch.state;
                db.command.Parameters.Add("@Nationalty", SqlDbType.VarChar).Value = branch.Nationalty;
                db.command.Parameters.Add("@ContactPerson", SqlDbType.VarChar).Value = branch.ContactPerson;
                db.command.Parameters.Add("@MobileNo", SqlDbType.VarChar).Value = branch.MobileNo;
                db.command.Parameters.Add("@Email", SqlDbType.VarChar).Value = branch.Email;
                db.command.Parameters.Add("@Website", SqlDbType.VarChar).Value = branch.Website;
                db.command.Parameters.Add("@CashLimit", SqlDbType.Float).Value = branch.CashLimit;
                db.command.Parameters.Add("@CraetedBy", SqlDbType.Int).Value = loginID;
                SqlParameter paramCount = new SqlParameter();
                paramCount.ParameterName = "@branchcount";
                paramCount.Direction = ParameterDirection.Output;
                paramCount.DbType = DbType.Int32;
                db.command.Parameters.Add(paramCount);
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int count = Convert.ToInt32(paramCount.Value);

                return count;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteBranch(int branchID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteBranch";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static List<BranchMaster> GetBranchByID(int BranchID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetBranchByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@BranchID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = BranchID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<BranchMaster> list = new List<BranchMaster>();
                var listUser = DatatableHelper.ToList<BranchMaster>(ds.Tables[0]);
                return listUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static void UpdateBranch(BranchMaster branch, int loginID)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateBranch";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branch.BranchID;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = branch.BranchName;
                db.command.Parameters.Add("@ZoneID", SqlDbType.Int).Value = branch.ZoneID;
                db.command.Parameters.Add("@BranchCode", SqlDbType.VarChar).Value = branch.BranchCode;
                db.command.Parameters.Add("@Address", SqlDbType.VarChar).Value = branch.Address;
                db.command.Parameters.Add("@Pincode", SqlDbType.VarChar).Value = branch.Pincode;
                db.command.Parameters.Add("@city", SqlDbType.VarChar).Value = branch.city;
                db.command.Parameters.Add("@state", SqlDbType.VarChar).Value = branch.state;
                db.command.Parameters.Add("@Nationalty", SqlDbType.VarChar).Value = branch.Nationalty;
                db.command.Parameters.Add("@ContactPerson", SqlDbType.VarChar).Value = branch.ContactPerson;
                db.command.Parameters.Add("@MobileNo", SqlDbType.VarChar).Value = branch.MobileNo;
                db.command.Parameters.Add("@Email", SqlDbType.VarChar).Value = branch.Email;
                db.command.Parameters.Add("@Website", SqlDbType.VarChar).Value = branch.Website;
                db.command.Parameters.Add("@CashLimit", SqlDbType.Float).Value = branch.CashLimit;
                db.command.Parameters.Add("@UpdatedBy", SqlDbType.Int).Value = loginID;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        #endregion

        #region Other Branch
        internal static Dictionary<int, List<OtherBranchMaster>> GetOtherBranchIndex(int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetOtherBranchIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<OtherBranchMaster>(ds.Tables[0]);
                int count = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<OtherBranchMaster>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }

        internal static int AddOtherBranch(OtherBranchMaster branch, int loginID)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddOtherBranch";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = branch.BranchName;
                db.command.Parameters.Add("@ZoneID", SqlDbType.Int).Value = branch.ZoneID;
                db.command.Parameters.Add("@BranchCode", SqlDbType.VarChar).Value = branch.BranchCode;
                db.command.Parameters.Add("@Address", SqlDbType.VarChar).Value = branch.Address;
                db.command.Parameters.Add("@Pincode", SqlDbType.VarChar).Value = branch.Pincode;
                db.command.Parameters.Add("@city", SqlDbType.VarChar).Value = branch.city;
                db.command.Parameters.Add("@state", SqlDbType.VarChar).Value = branch.state;
                db.command.Parameters.Add("@Nationalty", SqlDbType.VarChar).Value = branch.Nationalty;
                db.command.Parameters.Add("@ContactPerson", SqlDbType.VarChar).Value = branch.ContactPerson;
                db.command.Parameters.Add("@MobileNo", SqlDbType.VarChar).Value = branch.MobileNo;
                db.command.Parameters.Add("@Email", SqlDbType.VarChar).Value = branch.Email;
                db.command.Parameters.Add("@Website", SqlDbType.VarChar).Value = branch.Website;
                db.command.Parameters.Add("@CashLimit", SqlDbType.Float).Value = branch.CashLimit;
                db.command.Parameters.Add("@CraetedBy", SqlDbType.Int).Value = loginID;
                SqlParameter paramCount = new SqlParameter();
                paramCount.ParameterName = "@branchcount";
                paramCount.Direction = ParameterDirection.Output;
                paramCount.DbType = DbType.Int32;
                db.command.Parameters.Add(paramCount);
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int count = Convert.ToInt32(paramCount.Value);
                return count;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteOtherBranch(int branchID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteOtherBranch";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static List<OtherBranchMaster> GetOtherBranchByID(int BranchID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetOtherBranchByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@BranchID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = BranchID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<OtherBranchMaster> list = new List<OtherBranchMaster>();
                var listUser = DatatableHelper.ToList<OtherBranchMaster>(ds.Tables[0]);
                return listUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static void UpdateOtherBranch(OtherBranchMaster branch, int loginID)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateOtherBranch";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branch.OtherBranchID;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = branch.BranchName;
                db.command.Parameters.Add("@ZoneID", SqlDbType.Int).Value = branch.ZoneID;
                db.command.Parameters.Add("@BranchCode", SqlDbType.VarChar).Value = branch.BranchCode;
                db.command.Parameters.Add("@Address", SqlDbType.VarChar).Value = branch.Address;
                db.command.Parameters.Add("@Pincode", SqlDbType.VarChar).Value = branch.Pincode;
                db.command.Parameters.Add("@city", SqlDbType.VarChar).Value = branch.city;
                db.command.Parameters.Add("@state", SqlDbType.VarChar).Value = branch.state;
                db.command.Parameters.Add("@Nationalty", SqlDbType.VarChar).Value = branch.Nationalty;
                db.command.Parameters.Add("@ContactPerson", SqlDbType.VarChar).Value = branch.ContactPerson;
                db.command.Parameters.Add("@MobileNo", SqlDbType.VarChar).Value = branch.MobileNo;
                db.command.Parameters.Add("@Email", SqlDbType.VarChar).Value = branch.Email;
                db.command.Parameters.Add("@Website", SqlDbType.VarChar).Value = branch.Website;
                db.command.Parameters.Add("@CashLimit", SqlDbType.Float).Value = branch.CashLimit;
                db.command.Parameters.Add("@UpdatedBy", SqlDbType.Int).Value = loginID;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        #endregion

        #region Vendor
        internal static Dictionary<int, List<VendorMaster>> GetVendorIndex(int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetVendorIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<VendorMaster>(ds.Tables[0]);
                int count = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<VendorMaster>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }

        internal static int AddVendor(VendorMaster vendor, int loginID)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddVendor";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VendorName", SqlDbType.VarChar).Value = vendor.VendorName;
                db.command.Parameters.Add("@VendorCode", SqlDbType.VarChar).Value = vendor.VendorCode;
                db.command.Parameters.Add("@City", SqlDbType.VarChar).Value = vendor.City;
                db.command.Parameters.Add("@State", SqlDbType.VarChar).Value = vendor.State;
                db.command.Parameters.Add("@Country", SqlDbType.VarChar).Value = vendor.Country;
                db.command.Parameters.Add("@Address", SqlDbType.VarChar).Value = vendor.Address;
                db.command.Parameters.Add("@PostalCode", SqlDbType.VarChar).Value = vendor.PostalCode;
                db.command.Parameters.Add("@ContactNo", SqlDbType.VarChar).Value = vendor.ContactNo;
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = loginID;
                SqlParameter paramCount = new SqlParameter();
                paramCount.ParameterName = "@vendorcount";
                paramCount.Direction = ParameterDirection.Output;
                paramCount.DbType = DbType.Int32;
                db.command.Parameters.Add(paramCount);
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int count = Convert.ToInt32(paramCount.Value);
                return count;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteVendor(int vendorID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteVendor";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VendorID", SqlDbType.Int).Value = vendorID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static List<VendorMaster> GetVendorByID(int VendorID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetVendorByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@VendorID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = VendorID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<VendorMaster> list = new List<VendorMaster>();
                var listVendor = DatatableHelper.ToList<VendorMaster>(ds.Tables[0]);
                return listVendor;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static List<MHELVendorMaster> GetMHELVendorByID(int VendorID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetMHELVendorByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@VendorID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = VendorID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<MHELVendorMaster> list = new List<MHELVendorMaster>();
                var listVendor = DatatableHelper.ToList<MHELVendorMaster>(ds.Tables[0]);
                return listVendor;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static int UpdateVendor(VendorMaster vendor, int loginID)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateVendor";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VendorID", SqlDbType.Int).Value = vendor.VendorID;
                db.command.Parameters.Add("@VendorName", SqlDbType.VarChar).Value = vendor.VendorName;
                db.command.Parameters.Add("@VendorCode", SqlDbType.VarChar).Value = vendor.VendorCode;
                db.command.Parameters.Add("@City", SqlDbType.VarChar).Value = vendor.City;
                db.command.Parameters.Add("@State", SqlDbType.VarChar).Value = vendor.State;
                db.command.Parameters.Add("@Country", SqlDbType.VarChar).Value = vendor.Country;
                db.command.Parameters.Add("@Address", SqlDbType.VarChar).Value = vendor.Address;
                db.command.Parameters.Add("@PostalCode", SqlDbType.VarChar).Value = vendor.PostalCode;
                db.command.Parameters.Add("@ContactNo", SqlDbType.VarChar).Value = vendor.ContactNo;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = loginID;
                SqlParameter paramCount = new SqlParameter();
                paramCount.ParameterName = "@vendorcount";
                paramCount.Direction = ParameterDirection.Output;
                paramCount.DbType = DbType.Int32;
                db.command.Parameters.Add(paramCount);
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int count = Convert.ToInt32(paramCount.Value);
                return count;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        #endregion

        #region Vendor
        internal static Dictionary<int, List<VehicleVendorMaster>> GetVehicleVendorIndex(int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetVehicleVendorIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<VehicleVendorMaster>(ds.Tables[0]);
                int count = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<VehicleVendorMaster>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }

        internal static int AddVehicleVendor(VehicleVendorMaster vendor, int loginID)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddVehicleVendor";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VendorName", SqlDbType.VarChar).Value = vendor.VendorName;
                db.command.Parameters.Add("@VendorCode", SqlDbType.VarChar).Value = vendor.VendorCode;
                db.command.Parameters.Add("@City", SqlDbType.VarChar).Value = vendor.City;
                db.command.Parameters.Add("@State", SqlDbType.VarChar).Value = vendor.State;
                db.command.Parameters.Add("@Country", SqlDbType.VarChar).Value = vendor.Country;
                db.command.Parameters.Add("@Address", SqlDbType.VarChar).Value = vendor.Address;
                db.command.Parameters.Add("@PostalCode", SqlDbType.VarChar).Value = vendor.PostalCode;
                db.command.Parameters.Add("@ContactNo", SqlDbType.VarChar).Value = vendor.ContactNo;
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = loginID;
                SqlParameter paramCount = new SqlParameter();
                paramCount.ParameterName = "@vendorcount";
                paramCount.Direction = ParameterDirection.Output;
                paramCount.DbType = DbType.Int32;
                db.command.Parameters.Add(paramCount);
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int count = Convert.ToInt32(paramCount.Value);
                return count;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteVehicleVendor(int vendorID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteVehicleVendor";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VendorID", SqlDbType.Int).Value = vendorID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static List<VehicleVendorMaster> GetVehicleVendorByID(int VendorID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetVehicleVendorByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@VendorID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = VendorID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<VehicleVendorMaster> list = new List<VehicleVendorMaster>();
                var listVendor = DatatableHelper.ToList<VehicleVendorMaster>(ds.Tables[0]);
                return listVendor;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static int UpdateVehicleVendor(VehicleVendorMaster vendor, int loginID)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateVehicleVendor";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VendorID", SqlDbType.Int).Value = vendor.VehicleVendorID;
                db.command.Parameters.Add("@VendorName", SqlDbType.VarChar).Value = vendor.VendorName;
                db.command.Parameters.Add("@VendorCode", SqlDbType.VarChar).Value = vendor.VendorCode;
                db.command.Parameters.Add("@City", SqlDbType.VarChar).Value = vendor.City;
                db.command.Parameters.Add("@State", SqlDbType.VarChar).Value = vendor.State;
                db.command.Parameters.Add("@Country", SqlDbType.VarChar).Value = vendor.Country;
                db.command.Parameters.Add("@Address", SqlDbType.VarChar).Value = vendor.Address;
                db.command.Parameters.Add("@PostalCode", SqlDbType.VarChar).Value = vendor.PostalCode;
                db.command.Parameters.Add("@ContactNo", SqlDbType.VarChar).Value = vendor.ContactNo;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = loginID;
                SqlParameter paramCount = new SqlParameter();
                paramCount.ParameterName = "@vendorcount";
                paramCount.Direction = ParameterDirection.Output;
                paramCount.DbType = DbType.Int32;
                db.command.Parameters.Add(paramCount);
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int count = Convert.ToInt32(paramCount.Value);
                return count;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        #endregion

        #region Vehicle
        internal static Dictionary<int, List<VehicleMaster>> GetVehicleIndex(int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetVehicleIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<VehicleMaster>(ds.Tables[0]);
                int count = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<VehicleMaster>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }

        internal static void AddVehicle(VehicleMaster vehicle, int loginID)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddVehicle";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VehicleName", SqlDbType.VarChar).Value = vehicle.VehicleName;
                db.command.Parameters.Add("@VehicleNo", SqlDbType.VarChar).Value = vehicle.VehicleNo;
                db.command.Parameters.Add("@VehicleType", SqlDbType.VarChar).Value = vehicle.VehicleType;
                db.command.Parameters.Add("@RegistrationDate", SqlDbType.VarChar).Value = DateTime.ParseExact(vehicle.RegistrationDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                db.command.Parameters.Add("@FitnessExpDate", SqlDbType.VarChar).Value = DateTime.ParseExact(vehicle.FitnessExpDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                db.command.Parameters.Add("@InsuranceExpDate", SqlDbType.VarChar).Value = DateTime.ParseExact(vehicle.InsuranceExpDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                db.command.Parameters.Add("@PUCExpDate", SqlDbType.VarChar).Value = DateTime.ParseExact(vehicle.PUCExpDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                db.command.Parameters.Add("@EngineNo", SqlDbType.VarChar).Value = vehicle.EngineNo;
                db.command.Parameters.Add("@ChasisNo", SqlDbType.VarChar).Value = vehicle.ChasisNo;
                db.command.Parameters.Add("@OwnerType", SqlDbType.VarChar).Value = vehicle.OwnerType;
                db.command.Parameters.Add("@BrokerName", SqlDbType.VarChar).Value = vehicle.BrokerName;
                db.command.Parameters.Add("@loadingCapacity", SqlDbType.VarChar).Value = vehicle.loadingCapacity;
                db.command.Parameters.Add("@MaxloadingCapacity", SqlDbType.VarChar).Value = vehicle.MaxloadingCapacity;
                db.command.Parameters.Add("@isHiredVehicle", SqlDbType.Bit).Value = vehicle.isHiredVehicle;
                db.command.Parameters.Add("@VehicleVendorID", SqlDbType.VarChar).Value = vehicle.VehicleVendorID;
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = loginID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();

            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteVehicle(int vehicleID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteVehicle";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VehicleID", SqlDbType.Int).Value = vehicleID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static List<VehicleMaster> GetVehicleByID(int VehicleID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetVehicleByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@VehicleID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = VehicleID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<VehicleMaster> list = new List<VehicleMaster>();
                var listVendor = DatatableHelper.ToList<VehicleMaster>(ds.Tables[0]);
                return listVendor;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static void UpdateVehicle(VehicleMaster vehicle, int loginID)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateVehicle";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VehicleID", SqlDbType.Int).Value = vehicle.VehicleID;
                db.command.Parameters.Add("@VehicleName", SqlDbType.VarChar).Value = vehicle.VehicleName;
                db.command.Parameters.Add("@VehicleNo", SqlDbType.VarChar).Value = vehicle.VehicleNo;
                db.command.Parameters.Add("@VehicleType", SqlDbType.VarChar).Value = vehicle.VehicleType;
                db.command.Parameters.Add("@RegistrationDate", SqlDbType.VarChar).Value = DateTime.ParseExact(vehicle.RegistrationDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                db.command.Parameters.Add("@FitnessExpDate", SqlDbType.VarChar).Value = DateTime.ParseExact(vehicle.FitnessExpDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                db.command.Parameters.Add("@InsuranceExpDate", SqlDbType.VarChar).Value = DateTime.ParseExact(vehicle.InsuranceExpDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                db.command.Parameters.Add("@PUCExpDate", SqlDbType.VarChar).Value = DateTime.ParseExact(vehicle.PUCExpDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                db.command.Parameters.Add("@EngineNo", SqlDbType.VarChar).Value = vehicle.EngineNo;
                db.command.Parameters.Add("@ChasisNo", SqlDbType.VarChar).Value = vehicle.ChasisNo;
                db.command.Parameters.Add("@OwnerType", SqlDbType.VarChar).Value = vehicle.OwnerType;
                db.command.Parameters.Add("@BrokerName", SqlDbType.VarChar).Value = vehicle.BrokerName;
                db.command.Parameters.Add("@loadingCapacity", SqlDbType.VarChar).Value = vehicle.loadingCapacity;
                db.command.Parameters.Add("@MaxloadingCapacity", SqlDbType.VarChar).Value = vehicle.MaxloadingCapacity;
                db.command.Parameters.Add("@isHiredVehicle", SqlDbType.Bit).Value = vehicle.isHiredVehicle;
                db.command.Parameters.Add("@VehicleVendorID", SqlDbType.VarChar).Value = vehicle.VehicleVendorID;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = loginID;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();

            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }

        #region PinCode
        internal static object GetPinCodes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<PinCodeMaster> list = new List<PinCodeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetPinCodes");
                list = DatatableHelper.ToList<PinCodeMaster>(dt);
                list.RemoveAll(l => l.PinCode == "ODA");
                return list;
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        internal static void AddPinCode(PinCodeMaster pin)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddPinCode";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@PinCode", SqlDbType.VarChar).Value = pin.PinCode;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdatePinCode(PinCodeMaster pin)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdatePinCode";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@PinCodeId", SqlDbType.Int).Value = pin.PinCodeId;
                db.command.Parameters.Add("@PinCode", SqlDbType.VarChar).Value = pin.PinCode;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeletePinCode(int pincodeID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeletePinCode";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@PinCodeId", SqlDbType.Int).Value = pincodeID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region FOV
        internal static object GetFOV()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<FOVMaster> list = new List<FOVMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetFOV");
                list = DatatableHelper.ToList<FOVMaster>(dt);
                return list;
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        internal static void AddFOV(FOVMaster fov)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddFOV";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@FOVPercent", SqlDbType.Money).Value = fov.FOVPercent;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateFOV(FOVMaster fovdata)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateFOV";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@FOVId", SqlDbType.Int).Value = fovdata.FOVId;
                db.command.Parameters.Add("@FOVPercent", SqlDbType.Money).Value = fovdata.FOVPercent;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteFOV(int FOVId)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteFOV";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@FOVId", SqlDbType.Int).Value = FOVId;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Branch Running
        internal static Dictionary<int, List<BranchRunningNo>> GetBranchRunningIndex(int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetBranchRunningIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<BranchRunningNo>(ds.Tables[0]);
                int count = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<BranchRunningNo>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static int AddBranchRunning(BranchRunningNo runningno)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddBranchRunning";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = runningno.BranchID;
                db.command.Parameters.Add("@LastRunningNo", SqlDbType.Int).Value = runningno.LastRunningNo;
                db.command.Parameters.Add("@TransactionType", SqlDbType.VarChar).Value = runningno.TransactionType;
                SqlParameter paramCount = new SqlParameter();
                paramCount.ParameterName = "@runningcount";
                paramCount.Direction = ParameterDirection.Output;
                paramCount.DbType = DbType.Int32;
                db.command.Parameters.Add(paramCount);
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int count = Convert.ToInt32(paramCount.Value);
                return count;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static int UpdateBranchRunning(BranchRunningNo runningno)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateBranchRunning";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@RunningNoID", SqlDbType.Int).Value = runningno.RunningNoID;
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = runningno.BranchID;
                db.command.Parameters.Add("@LastRunningNo", SqlDbType.Int).Value = runningno.LastRunningNo;
                db.command.Parameters.Add("@TransactionType", SqlDbType.VarChar).Value = runningno.TransactionType;
                SqlParameter paramCount = new SqlParameter();
                paramCount.ParameterName = "@runningcount";
                paramCount.Direction = ParameterDirection.Output;
                paramCount.DbType = DbType.Int32;
                db.command.Parameters.Add(paramCount);
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int count = Convert.ToInt32(paramCount.Value);
                return count;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteBranchRunning(int runningID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteBranchRunning";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@RunningNoID", SqlDbType.Int).Value = runningID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Company
        internal static Dictionary<int, List<CompanyMaster>> GetCompanyIndex(int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetCompanyIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<CompanyMaster>(ds.Tables[0]);
                int count = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<CompanyMaster>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }

        internal static void AddCompany(CompanyMaster company)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddCompany";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@CompanyName", SqlDbType.VarChar).Value = company.CompanyName;
                db.command.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = company.CompanyCode;
                db.command.Parameters.Add("@ContactNo", SqlDbType.VarChar).Value = company.ContactNo;
                db.command.Parameters.Add("@Address", SqlDbType.VarChar).Value = company.Address;
                db.command.Parameters.Add("@ContactPerson", SqlDbType.VarChar).Value = company.ContactPerson;
                db.command.Parameters.Add("@EmailID", SqlDbType.VarChar).Value = company.EmailID;
                db.command.Parameters.Add("@Website", SqlDbType.VarChar).Value = company.Website;
                db.command.Parameters.Add("@CTSNo", SqlDbType.VarChar).Value = company.CTSNo;
                db.command.Parameters.Add("@TINo", SqlDbType.VarChar).Value = company.TINo;
                db.command.Parameters.Add("@PanNo", SqlDbType.VarChar).Value = company.PanNo;
                db.command.Parameters.Add("@ServiceTaxNo", SqlDbType.VarChar).Value = company.ServiceTaxNo;
                db.command.Parameters.Add("@GSTNo", SqlDbType.VarChar).Value = company.GSTNo;
                db.command.Parameters.Add("@TDS", SqlDbType.VarChar).Value = company.TDS;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteCompany(int companyID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteCompany";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@companyID", SqlDbType.Int).Value = companyID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static List<CompanyMaster> GetCompanyByID(int CompanyID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetCompanyByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@CompanyID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = CompanyID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<CompanyMaster> list = new List<CompanyMaster>();
                var listUser = DatatableHelper.ToList<CompanyMaster>(ds.Tables[0]);
                return listUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static void UpdateCompany(CompanyMaster company)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateCompany";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@CompanyID", SqlDbType.VarChar).Value = company.CompanyID;
                db.command.Parameters.Add("@CompanyName", SqlDbType.VarChar).Value = company.CompanyName;
                db.command.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = company.CompanyCode;
                db.command.Parameters.Add("@ContactNo", SqlDbType.VarChar).Value = company.ContactNo;
                db.command.Parameters.Add("@Address", SqlDbType.VarChar).Value = company.Address;
                db.command.Parameters.Add("@ContactPerson", SqlDbType.VarChar).Value = company.ContactPerson;
                db.command.Parameters.Add("@EmailID", SqlDbType.VarChar).Value = company.EmailID;
                db.command.Parameters.Add("@Website", SqlDbType.VarChar).Value = company.Website;
                db.command.Parameters.Add("@CTSNo", SqlDbType.VarChar).Value = company.CTSNo;
                db.command.Parameters.Add("@TINo", SqlDbType.VarChar).Value = company.TINo;
                db.command.Parameters.Add("@PanNo", SqlDbType.VarChar).Value = company.PanNo;
                db.command.Parameters.Add("@ServiceTaxNo", SqlDbType.VarChar).Value = company.ServiceTaxNo;
                db.command.Parameters.Add("@GSTNo", SqlDbType.VarChar).Value = company.GSTNo;
                db.command.Parameters.Add("@TDS", SqlDbType.VarChar).Value = company.TDS;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        #endregion

        #region Tax
        internal static object GetTaxes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<TaxMaster> list = new List<TaxMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetTaxData");
                list = DatatableHelper.ToList<TaxMaster>(dt);
                return list;
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        internal static void AddTax(TaxMaster tax)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddTax";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@TaxType", SqlDbType.VarChar).Value = tax.TaxType;
                db.command.Parameters.Add("@TaxPercent", SqlDbType.Float).Value = tax.TaxPercent;
                db.command.Parameters.Add("@Description", SqlDbType.VarChar).Value = tax.Description;
                db.command.Parameters.Add("@Status", SqlDbType.Bit).Value = tax.Status;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateTax(TaxMaster tax)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateTax";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@TaxID", SqlDbType.Int).Value = tax.TaxID;
                db.command.Parameters.Add("@TaxType", SqlDbType.VarChar).Value = tax.TaxType;
                db.command.Parameters.Add("@TaxPercent", SqlDbType.Float).Value = tax.TaxPercent;
                db.command.Parameters.Add("@Description", SqlDbType.VarChar).Value = tax.Description;
                db.command.Parameters.Add("@Status", SqlDbType.Bit).Value = tax.Status;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteTax(int TaxId)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteTax";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@TaxId", SqlDbType.Int).Value = TaxId;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Zone
        internal static Dictionary<int, List<ZoneMaster>> GetZoneIndex(int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetZoneIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<ZoneMaster>(ds.Tables[0]);
                int count = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<ZoneMaster>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }

        internal static void AddZone(ZoneMaster zonedata, int loginID)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddZone";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@Zone", SqlDbType.VarChar).Value = zonedata.Zone;
                db.command.Parameters.Add("@Address", SqlDbType.VarChar).Value = zonedata.Address;
                db.command.Parameters.Add("@Pincode", SqlDbType.VarChar).Value = zonedata.Pincode;
                db.command.Parameters.Add("@city", SqlDbType.VarChar).Value = zonedata.city;
                db.command.Parameters.Add("@state", SqlDbType.VarChar).Value = zonedata.state;
                db.command.Parameters.Add("@Nationalty", SqlDbType.VarChar).Value = zonedata.Nationalty;
                db.command.Parameters.Add("@ContactPerson", SqlDbType.VarChar).Value = zonedata.ContactPerson;
                db.command.Parameters.Add("@MobileNo", SqlDbType.VarChar).Value = zonedata.MobileNo;
                db.command.Parameters.Add("@Email", SqlDbType.VarChar).Value = zonedata.Email;
                db.command.Parameters.Add("@Website", SqlDbType.VarChar).Value = zonedata.Website;
                db.command.Parameters.Add("@CashLimit", SqlDbType.Float).Value = zonedata.CashLimit;
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = loginID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteZone(int ZoneID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteZone";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ZoneID", SqlDbType.Int).Value = ZoneID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static List<ZoneMaster> GetZoneByID(int ZoneID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetZoneyID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@ZoneID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = ZoneID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<ZoneMaster> list = new List<ZoneMaster>();
                var listUser = DatatableHelper.ToList<ZoneMaster>(ds.Tables[0]);
                return listUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static void UpdateZone(ZoneMaster zonedata, int loginID)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateZone";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ZoneID", SqlDbType.VarChar).Value = zonedata.ZoneID;
                db.command.Parameters.Add("@Zone", SqlDbType.VarChar).Value = zonedata.Zone;
                db.command.Parameters.Add("@Address", SqlDbType.VarChar).Value = zonedata.Address;
                db.command.Parameters.Add("@Pincode", SqlDbType.VarChar).Value = zonedata.Pincode;
                db.command.Parameters.Add("@city", SqlDbType.VarChar).Value = zonedata.city;
                db.command.Parameters.Add("@state", SqlDbType.VarChar).Value = zonedata.state;
                db.command.Parameters.Add("@Nationalty", SqlDbType.VarChar).Value = zonedata.Nationalty;
                db.command.Parameters.Add("@ContactPerson", SqlDbType.VarChar).Value = zonedata.ContactPerson;
                db.command.Parameters.Add("@MobileNo", SqlDbType.VarChar).Value = zonedata.MobileNo;
                db.command.Parameters.Add("@Email", SqlDbType.VarChar).Value = zonedata.Email;
                db.command.Parameters.Add("@Website", SqlDbType.VarChar).Value = zonedata.Website;
                db.command.Parameters.Add("@CashLimit", SqlDbType.Float).Value = zonedata.CashLimit;
                db.command.Parameters.Add("@ModifiedByBy", SqlDbType.Int).Value = loginID;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        #endregion

        #region Part No
        internal static object GetPartNos(int vendorID)
        {
            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetPartNos";
                SqlParameter paramVehID = new SqlParameter();
                paramVehID.ParameterName = "@VendorID";
                paramVehID.Direction = ParameterDirection.Input;
                paramVehID.DbType = DbType.Int32;
                paramVehID.Value = vendorID;

                cmd.Parameters.Add(paramVehID);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dsVehicle = new DataSet();
                da.Fill(dsVehicle, "table1");
                connection.Close();

                List<PartNoMaster> list = new List<PartNoMaster>();
                return list = DatatableHelper.ToList<PartNoMaster>(dsVehicle.Tables[0]);

            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetMasterPartNos()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<PartNoMaster> list = new List<PartNoMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetMasterPartNos");
                list = DatatableHelper.ToList<PartNoMaster>(dt);
                return list;
            }
            catch (Exception x)
            {
                throw x;
            }
        }
        internal static void AddPartNo(PartNoMaster partno)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddPartNo";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@PartNo", SqlDbType.VarChar).Value = partno.PartNo;
                db.command.Parameters.Add("@PerPriceWeight", SqlDbType.Float).Value = partno.PerPriceWeight;
                db.command.Parameters.Add("@VendorID", SqlDbType.Int).Value = partno.VendorID;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdatePartNo(PartNoMaster partno)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdatePartNo";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@PartNoID", SqlDbType.Int).Value = partno.PartNoID;
                db.command.Parameters.Add("@PartNo", SqlDbType.VarChar).Value = partno.PartNo;
                db.command.Parameters.Add("@PerPriceWeight", SqlDbType.Float).Value = partno.PerPriceWeight;
                db.command.Parameters.Add("@VendorID", SqlDbType.Int).Value = partno.VendorID;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeletePartNo(int PartNoId)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeletePartNo";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@PartNoId", SqlDbType.Int).Value = PartNoId;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #endregion

        public static double GetBalanceAmount(int BranchID)
        {
            try
            {
                DBConnection db = new DBConnection();
                db.ConnectoDB();

                db.command.CommandType = CommandType.StoredProcedure;
                db.command.CommandText = "GetBalanceAmount";
                SqlParameter paramKey = new SqlParameter();
                paramKey.ParameterName = "@pBranchID";
                paramKey.Direction = ParameterDirection.Input;
                paramKey.DbType = DbType.Int32;
                paramKey.Value = BranchID;

                db.command.Parameters.Add("@AttributeValue", SqlDbType.Float).Direction = ParameterDirection.Output;
                db.command.Parameters.Add(paramKey);
                db.ConnectionOpen();
                db.command.ExecuteNonQuery();
                var attributeval = Convert.ToDouble(db.command.Parameters["@AttributeValue"].Value);
                db.ConnectionClose();
                return attributeval;

            }
            catch (Exception x)
            {
                return 0;
            }
        }

        public static double GetVehicleVendorBalanceAmount(int BranchID,int VehicleVendorID)
        {
            try
            {
                DBConnection db = new DBConnection();
                db.ConnectoDB();

                db.command.CommandType = CommandType.StoredProcedure;
                db.command.CommandText = "GetVehicleVendorBalanceAmount";
                SqlParameter paramKey = new SqlParameter();
                paramKey.ParameterName = "@pBranchID";
                paramKey.Direction = ParameterDirection.Input;
                paramKey.DbType = DbType.Int32;
                paramKey.Value = BranchID;

                db.command.Parameters.Add("@AttributeValue", SqlDbType.Float).Direction = ParameterDirection.Output;
                db.command.Parameters.Add(paramKey);
                db.ConnectionOpen();
                db.command.ExecuteNonQuery();
                var attributeval = Convert.ToDouble(db.command.Parameters["@AttributeValue"].Value);
                db.ConnectionClose();
                return attributeval;

            }
            catch (Exception x)
            {
                return 0;
            }
        }
        internal static object GetExpenseTypes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<ExpenseTypeMaster> list = new List<ExpenseTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetExpenseTypes");
                return list = Helper.DatatableHelper.ToList<ExpenseTypeMaster>(dt);
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        internal static object GetBranchBalanceData()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<BranchBalance> list = new List<BranchBalance>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetBranchBalanceData");
                return list = Helper.DatatableHelper.ToList<BranchBalance>(dt);
            }
            catch (Exception x)
            {
                throw x;
            }
        }


        #region Other Vendor
        internal static Dictionary<int, List<VendorMaster>> GetOtherVendorIndex(int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetOtherVendorIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<VendorMaster>(ds.Tables[0]);
                int count = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<VendorMaster>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }

        internal static int AddOtherVendor(OtherVendorMaster vendor, int loginID)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddOtherVendor";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VendorName", SqlDbType.VarChar).Value = vendor.VendorName;
                db.command.Parameters.Add("@VendorCode", SqlDbType.VarChar).Value = vendor.VendorCode;
                db.command.Parameters.Add("@City", SqlDbType.VarChar).Value = vendor.City;
                db.command.Parameters.Add("@State", SqlDbType.VarChar).Value = vendor.State;
                db.command.Parameters.Add("@Country", SqlDbType.VarChar).Value = vendor.Country;
                db.command.Parameters.Add("@Address", SqlDbType.VarChar).Value = vendor.Address;
                db.command.Parameters.Add("@PostalCode", SqlDbType.VarChar).Value = vendor.PostalCode;
                db.command.Parameters.Add("@ContactNo", SqlDbType.VarChar).Value = vendor.ContactNo;
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = loginID;
                SqlParameter paramCount = new SqlParameter();
                paramCount.ParameterName = "@vendorcount";
                paramCount.Direction = ParameterDirection.Output;
                paramCount.DbType = DbType.Int32;
                db.command.Parameters.Add(paramCount);
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int count = Convert.ToInt32(paramCount.Value);
                return count;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteOtherVendor(int vendorID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteOtherVendor";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VendorID", SqlDbType.Int).Value = vendorID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static List<OtherVendorMaster> GetOtherVendorByID(int VendorID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetOtherVendorByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@VendorID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = VendorID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<OtherVendorMaster> list = new List<OtherVendorMaster>();
                var listVendor = DatatableHelper.ToList<OtherVendorMaster>(ds.Tables[0]);
                return listVendor;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        internal static int UpdateOtherVendor(OtherVendorMaster vendor, int loginID)
        {
            DBConnection db = new DBConnection();

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateOtherVendor";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VendorID", SqlDbType.Int).Value = vendor.VendorID;
                db.command.Parameters.Add("@VendorName", SqlDbType.VarChar).Value = vendor.VendorName;
                db.command.Parameters.Add("@VendorCode", SqlDbType.VarChar).Value = vendor.VendorCode;
                db.command.Parameters.Add("@City", SqlDbType.VarChar).Value = vendor.City;
                db.command.Parameters.Add("@State", SqlDbType.VarChar).Value = vendor.State;
                db.command.Parameters.Add("@Country", SqlDbType.VarChar).Value = vendor.Country;
                db.command.Parameters.Add("@Address", SqlDbType.VarChar).Value = vendor.Address;
                db.command.Parameters.Add("@PostalCode", SqlDbType.VarChar).Value = vendor.PostalCode;
                db.command.Parameters.Add("@ContactNo", SqlDbType.VarChar).Value = vendor.ContactNo;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = loginID;
                SqlParameter paramCount = new SqlParameter();
                paramCount.ParameterName = "@vendorcount";
                paramCount.Direction = ParameterDirection.Output;
                paramCount.DbType = DbType.Int32;
                db.command.Parameters.Add(paramCount);
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int count = Convert.ToInt32(paramCount.Value);
                return count;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        #endregion

    }
}