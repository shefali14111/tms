﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TMS.Models
{
    public static class TrackConsignmentModel
    {
        internal static TrackConsignmentData GetConsignmentDetails(string consignmentNo)
        {
            TrackConsignmentData objTrackConsignmentData = null;
            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "TrackConsignment";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@pconsignmentNo";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.String;
                paramCnB.Value = consignmentNo;
                cmd.Parameters.Add(paramCnB);
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                if (ds.Tables.Count > 0)
                {
                    objTrackConsignmentData = new TrackConsignmentData();
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        objTrackConsignmentData = ConvertToList<TrackConsignmentData>(ds.Tables[0])[0];
                    }
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        objTrackConsignmentData.LocalManifest = new ManifestData();
                        objTrackConsignmentData.LocalManifest = ConvertToList<ManifestData>(ds.Tables[1])[0];
                    }
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        objTrackConsignmentData.Manifest = new List<ManifestData>();
                        objTrackConsignmentData.Manifest = ConvertToList<ManifestData>(ds.Tables[2]);
                    }
                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        objTrackConsignmentData.DeliveryData = new DeliveryData();
                        objTrackConsignmentData.DeliveryData = ConvertToList<DeliveryData>(ds.Tables[3])[0];
                    }


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objTrackConsignmentData;
        }
        public static List<T> ConvertToList<T>(DataTable dt)
        {
            var columnNames = dt.Columns.Cast<DataColumn>().Select(c => c.ColumnName.ToLower()).ToList();
            var properties = typeof(T).GetProperties();
            return dt.AsEnumerable().Select(row =>
            {
                var objT = Activator.CreateInstance<T>();
                foreach (var prop in properties)
                {
                    if (columnNames.Contains(prop.Name.ToLower()))
                    {
                        try
                        {
                            if (row[prop.Name] == DBNull.Value && prop.PropertyType.IsValueType && Nullable.GetUnderlyingType(prop.PropertyType) != null)
                            {
                                prop.SetValue(objT,null);
                            }
                            else
                                prop.SetValue(objT, row[prop.Name]);
                        }
                        catch (Exception ex) { }
                    }
                }
                return objT;
            }).ToList();
        }



    }
    public class TrackConsignmentData
    {
        public string ConsignmentNo { get; set; }
        public string ConsignmentDate { get; set; }
        public string ConsignmentTime { get; set; }
        public string Consignee { get; set; }
        public string Consignor { get; set; }
        public string FromConsignment { get; set; }
        public string ToConsignment { get; set; }
        public ManifestData LocalManifest { get; set; }
        public List<ManifestData> Manifest { get; set; }
        public DeliveryData DeliveryData { get; set; }
    }
    public class ManifestData
    {
        public string ManifestNo { get; set; }
        public string BranchName { get; set; }
        public string ArrivalDate { get; set; }
        public string ArrivalTime { get; set; }
        public string Created { get; set; }
        public string DepartureTime { get; set; }
        public string FromLoc { get; set; }
        public string ToLoc { get; set; }

    }
    public class DeliveryData
    {
        public string DeliveryStatus { get; set; }
        public string BranchName { get; set; }
        public string DeliveryDate { get; set; }
        public string DeliveryTime { get; set; }
        public string DeliveryRemarks { get; set; }
        public string LicenseNo { get; set; }
    }
}