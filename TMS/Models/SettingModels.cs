﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using TMS.Common;
using TMS.CustomModels;
using TMS.EFModel;
using TMS.Helper;

namespace TMS.Models
{
    public class SettingModels
    {
        #region Vendor Rate
        internal static Dictionary<int, List<VendorRateModel>> GetVendorRatesIndex(int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetVendorRateIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<VendorRateModel>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<VendorRateModel>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static int AddVendorRate(VendorRateMaster vendorrate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
              
                db.command.CommandText = "AddVendorRate";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = vendorrate.BranchId;
                db.command.Parameters.Add("@pBillingPartyId", SqlDbType.Int).Value = vendorrate.BillingPartyId;
                db.command.Parameters.Add("@pConsignorId", SqlDbType.Int).Value = vendorrate.ConsignorId;
                db.command.Parameters.Add("@pConsigneeId", SqlDbType.Int).Value = vendorrate.ConsigneeId;
                db.command.Parameters.Add("@pFrom", SqlDbType.Int).Value = vendorrate.FROM;
                db.command.Parameters.Add("@pTo", SqlDbType.Int).Value = vendorrate.TO;
                db.command.Parameters.Add("@pChargeType", SqlDbType.Int).Value = vendorrate.ChargeTypeId;
                db.command.Parameters.Add("@pFreightRate", SqlDbType.Float).Value = vendorrate.FRIEGHTRATE;
                db.command.Parameters.Add("@pDocketCharge", SqlDbType.Float).Value = vendorrate.DocketCharge;
                db.command.Parameters.Add("@pDoorCollection", SqlDbType.Float).Value = vendorrate.DoorCollection;
                db.command.Parameters.Add("@pDeliveryCharge", SqlDbType.Float).Value = vendorrate.DeliveryCharge;
                db.command.Parameters.Add("@pFOV", SqlDbType.Float).Value = vendorrate.FOV;
                db.command.Parameters.Add("@pHamaliCharge", SqlDbType.Float).Value = vendorrate.HamaliCharge;
                db.command.Parameters.Add("@pOtherCharges", SqlDbType.Float).Value = vendorrate.OtherCharges;
                db.command.Parameters.Add("@pMinKG", SqlDbType.Int).Value = vendorrate.MinKG;

                SqlParameter paramStatus = new SqlParameter();
                paramStatus.ParameterName = "@pStatus";
                paramStatus.Direction = ParameterDirection.Output;
                paramStatus.DbType = DbType.Int16;
                db.command.Parameters.Add(paramStatus);

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int status = Convert.ToInt32(paramStatus.Value);
                return status;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteVendorRate(int VendorRateID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteVendorRate";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pVendorRateID", SqlDbType.Int).Value = VendorRateID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        #endregion

        #region Voucher
        internal static void DeleteVoucher(int voucherID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteVoucher";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pvoucherID", SqlDbType.Int).Value = voucherID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static void AddVoucher(VoucherMaster voucherMaster,int loginID)
        {
            DBConnection db = new DBConnection();
            DateTime VoucherDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddVoucher";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VoucherDate", SqlDbType.DateTime).Value = VoucherDate;
                db.command.Parameters.Add("@VoucherType", SqlDbType.Int).Value = voucherMaster.VoucherType;
                db.command.Parameters.Add("@DocumentType", SqlDbType.Int).Value = voucherMaster.DocumentType;
                db.command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = voucherMaster.BankName;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = voucherMaster.BranchName;
                db.command.Parameters.Add("@ChequeNo", SqlDbType.NVarChar).Value = voucherMaster.ChequeNo;
                DateTime? Chequedate = null;
                if (!String.IsNullOrEmpty(voucherMaster.ChequeDate))
                {
                    Chequedate = DateTime.ParseExact(voucherMaster.ChequeDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                }
                db.command.Parameters.Add("@ChequeDate", SqlDbType.DateTime).Value = Chequedate;
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = loginID;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = loginID;
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = voucherMaster.BranchID;
                db.command.Parameters.Add("@AccountHead", SqlDbType.Int).Value = voucherMaster.AccountID;
                db.command.Parameters.Add("@VendorName", SqlDbType.VarChar).Value = voucherMaster.VendorName;
                db.command.Parameters.Add("@VendorID", SqlDbType.Int).Value = voucherMaster.VendorID;
                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = voucherMaster.PaymentTypeID;
                db.command.Parameters.Add("@VoucherAmount", SqlDbType.Float).Value = voucherMaster.VoucherAmount;
                db.command.Parameters.Add("@UserID", SqlDbType.Int).Value = loginID;
                db.command.Parameters.Add("@ManifestID", SqlDbType.Int).Value = voucherMaster.ManifestID;
                db.command.Parameters.Add("@Particulars", SqlDbType.VarChar).Value = voucherMaster.Particulars;
                db.command.Parameters.Add("@HireType", SqlDbType.VarChar).Value = voucherMaster.HireType;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateVoucher(VoucherMaster voucherMaster, int loginID)
        {
            DBConnection db = new DBConnection();
           
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateVoucher";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pVoucherID", SqlDbType.Int).Value = voucherMaster.VoucherID;
                db.command.Parameters.Add("@VoucherType", SqlDbType.Int).Value = voucherMaster.VoucherType;
                db.command.Parameters.Add("@DocumentType", SqlDbType.Int).Value = voucherMaster.DocumentType;
                db.command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = voucherMaster.BankName;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = voucherMaster.BranchName;
                db.command.Parameters.Add("@ChequeNo", SqlDbType.NVarChar).Value = voucherMaster.ChequeNo;
                DateTime? Chequedate = null;
                if (!String.IsNullOrEmpty(voucherMaster.ChequeDate))
                {
                    Chequedate = DateTime.ParseExact(voucherMaster.ChequeDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                }
                db.command.Parameters.Add("@ChequeDate", SqlDbType.DateTime).Value = Chequedate;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = loginID;
                db.command.Parameters.Add("@AccountHead", SqlDbType.Int).Value = voucherMaster.AccountID;
                db.command.Parameters.Add("@VendorName", SqlDbType.VarChar).Value = voucherMaster.VendorName;
                db.command.Parameters.Add("@VendorID", SqlDbType.Int).Value = voucherMaster.VendorID;
                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = voucherMaster.PaymentTypeID;
                db.command.Parameters.Add("@VoucherAmount", SqlDbType.Float).Value = voucherMaster.VoucherAmount;
                db.command.Parameters.Add("@ManifestID", SqlDbType.Int).Value = voucherMaster.ManifestID;
                db.command.Parameters.Add("@Particulars", SqlDbType.VarChar).Value = voucherMaster.Particulars;
                db.command.Parameters.Add("@HireType", SqlDbType.VarChar).Value = voucherMaster.HireType;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static Dictionary<int, List<Voucher>> GetVoucherIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetVoucherIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<Voucher>(ds.Tables[0]);
                int count = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<Voucher>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static List<VoucherMaster> GetVoucherByID(int VoucherID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetVouchersByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@VoucherID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = VoucherID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<VoucherMaster> list = new List<VoucherMaster>();
                var listVoucher = DatatableHelper.ToList<VoucherMaster>(ds.Tables[0]);
                return listVoucher;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region MHEL Vendor Rate
        internal static Dictionary<int, List<MHELVendorRateMaster>> GetMHELVendorRatesIndex(int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetMHELVendorRateIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<MHELVendorRateMaster>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<MHELVendorRateMaster>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static int AddMHELVendorRate(MHELVendorRateMaster vendorrate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddMHELVendorRate";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBillingPartyId", SqlDbType.Int).Value = vendorrate.BillingPartyId;
                db.command.Parameters.Add("@pConsignorId", SqlDbType.Int).Value = vendorrate.ConsignorId;
                db.command.Parameters.Add("@pConsigneeId", SqlDbType.Int).Value = vendorrate.ConsigneeId;
                db.command.Parameters.Add("@pFrom", SqlDbType.Int).Value = vendorrate.FROM;
                db.command.Parameters.Add("@pTo", SqlDbType.Int).Value = vendorrate.TO;
                db.command.Parameters.Add("@pChargeType", SqlDbType.Int).Value = vendorrate.ChargeTypeId;
                db.command.Parameters.Add("@pPartNo", SqlDbType.VarChar).Value = vendorrate.PartNo;
                db.command.Parameters.Add("@pPerPieceWeight", SqlDbType.Float).Value = vendorrate.PerPieceWeight;
                db.command.Parameters.Add("@pFreightRate", SqlDbType.Float).Value = vendorrate.FRIEGHTRATE;
                db.command.Parameters.Add("@pDocketCharge", SqlDbType.Float).Value = vendorrate.DocketCharge;
                db.command.Parameters.Add("@pDoorCollection", SqlDbType.Float).Value = vendorrate.DoorCollection;
                db.command.Parameters.Add("@pDeliveryCharge", SqlDbType.Float).Value = vendorrate.DeliveryCharge;
                db.command.Parameters.Add("@pFOV", SqlDbType.Float).Value = vendorrate.FOV;
                db.command.Parameters.Add("@pHamaliCharge", SqlDbType.Float).Value = vendorrate.HamaliCharge;
                db.command.Parameters.Add("@pOtherCharges", SqlDbType.Float).Value = vendorrate.OtherCharges;
                db.command.Parameters.Add("@pMinKG", SqlDbType.Int).Value = vendorrate.MinKG;

                SqlParameter paramStatus = new SqlParameter();
                paramStatus.ParameterName = "@pStatus";
                paramStatus.Direction = ParameterDirection.Output;
                paramStatus.DbType = DbType.Int16;
                db.command.Parameters.Add(paramStatus);

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int status = Convert.ToInt32(paramStatus.Value);
                return status;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteMHELVendorRate(int VendorRateID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteMHELVendorRate";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pMHELVendorRateID", SqlDbType.Int).Value = VendorRateID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static List<MHELVendorRateMaster> GetMHELVendorRateById(int MHELRateID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetMHELVendorRateById";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@MHELVendorRateID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = MHELRateID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<MHELVendorRateMaster> list = new List<MHELVendorRateMaster>();
                var listMHEL = DatatableHelper.ToList<MHELVendorRateMaster>(ds.Tables[0]);
                return listMHEL;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static int UpdateMHELVendorRate(MHELVendorRateMaster vendorrate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandText = "UpdateMHELVendorRate";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pMHELVendorRateID", SqlDbType.Int).Value = vendorrate.MHELVendorRateID;
                db.command.Parameters.Add("@pBillingPartyId", SqlDbType.Int).Value = vendorrate.BillingPartyId;
                db.command.Parameters.Add("@pConsignorId", SqlDbType.Int).Value = vendorrate.ConsignorId;
                db.command.Parameters.Add("@pConsigneeId", SqlDbType.Int).Value = vendorrate.ConsigneeId;
                db.command.Parameters.Add("@pFrom", SqlDbType.Int).Value = vendorrate.FROM;
                db.command.Parameters.Add("@pTo", SqlDbType.Int).Value = vendorrate.TO;
                db.command.Parameters.Add("@pChargeType", SqlDbType.Int).Value = vendorrate.ChargeTypeId;
                db.command.Parameters.Add("@pPartNo", SqlDbType.VarChar).Value = vendorrate.PartNo;
                db.command.Parameters.Add("@pPerPieceWeight", SqlDbType.Float).Value = vendorrate.PerPieceWeight;
                db.command.Parameters.Add("@pFreightRate", SqlDbType.Float).Value = vendorrate.FRIEGHTRATE;
                db.command.Parameters.Add("@pDocketCharge", SqlDbType.Float).Value = vendorrate.DocketCharge;
                db.command.Parameters.Add("@pDoorCollection", SqlDbType.Float).Value = vendorrate.DoorCollection;
                db.command.Parameters.Add("@pDeliveryCharge", SqlDbType.Float).Value = vendorrate.DeliveryCharge;
                db.command.Parameters.Add("@pFOV", SqlDbType.Float).Value = vendorrate.FOV;
                db.command.Parameters.Add("@pHamaliCharge", SqlDbType.Float).Value = vendorrate.HamaliCharge;
                db.command.Parameters.Add("@pOtherCharges", SqlDbType.Float).Value = vendorrate.OtherCharges;
                db.command.Parameters.Add("@pMinKG", SqlDbType.Int).Value = vendorrate.MinKG;

                SqlParameter paramStatus = new SqlParameter();
                paramStatus.ParameterName = "@pStatus";
                paramStatus.Direction = ParameterDirection.Output;
                paramStatus.DbType = DbType.Int16;
                db.command.Parameters.Add(paramStatus);

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int status = Convert.ToInt32(paramStatus.Value);
                return status;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }

        #endregion

        #region Mahindra Vendor Rate
        internal static Dictionary<int, List<MahindraVendorRateMaster>> GetMahindraVendorRatesIndex(int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetMahindraVendorRateIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<MahindraVendorRateMaster>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<MahindraVendorRateMaster>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static int AddMahindraVendorRate(MahindraVendorRateMaster vendorrate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddMahindraVendorRate";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBillingPartyId", SqlDbType.Int).Value = vendorrate.BillingPartyId;
                db.command.Parameters.Add("@pConsignorId", SqlDbType.Int).Value = vendorrate.ConsignorId;
                db.command.Parameters.Add("@pConsigneeId", SqlDbType.Int).Value = vendorrate.ConsigneeId;
                db.command.Parameters.Add("@pFrom", SqlDbType.Int).Value = vendorrate.FROM;
                db.command.Parameters.Add("@pTo", SqlDbType.Int).Value = vendorrate.TO;
                db.command.Parameters.Add("@pChargeType", SqlDbType.Int).Value = vendorrate.ChargeTypeId;
                db.command.Parameters.Add("@pBoxes", SqlDbType.Int).Value = vendorrate.Boxes;
                db.command.Parameters.Add("@L", SqlDbType.Float).Value = vendorrate.L;
                db.command.Parameters.Add("@W", SqlDbType.Float).Value = vendorrate.W;
                db.command.Parameters.Add("@H", SqlDbType.Float).Value = vendorrate.H;
                db.command.Parameters.Add("@ChargedWeight", SqlDbType.Float).Value = vendorrate.ChargedWeight;
                db.command.Parameters.Add("@pFreightRate", SqlDbType.Float).Value = vendorrate.FRIEGHTRATE;
                db.command.Parameters.Add("@pDocketCharge", SqlDbType.Float).Value = vendorrate.DocketCharge;
                db.command.Parameters.Add("@pDoorCollection", SqlDbType.Float).Value = vendorrate.DoorCollection;
                db.command.Parameters.Add("@pDeliveryCharge", SqlDbType.Float).Value = vendorrate.DeliveryCharge;
                db.command.Parameters.Add("@pFOV", SqlDbType.Float).Value = vendorrate.FOV;
                db.command.Parameters.Add("@pHamaliCharge", SqlDbType.Float).Value = vendorrate.HamaliCharge;
                db.command.Parameters.Add("@pOtherCharges", SqlDbType.Float).Value = vendorrate.OtherCharges;
                db.command.Parameters.Add("@pMinKG", SqlDbType.Int).Value = vendorrate.MinKG;

                SqlParameter paramStatus = new SqlParameter();
                paramStatus.ParameterName = "@pStatus";
                paramStatus.Direction = ParameterDirection.Output;
                paramStatus.DbType = DbType.Int16;
                db.command.Parameters.Add(paramStatus);

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int status = Convert.ToInt32(paramStatus.Value);
                return status;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteMahindraVendorRate(int VendorRateID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteMahindraVendorRate";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pMahindraVendorRateID", SqlDbType.Int).Value = VendorRateID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }

        internal static List<MahindraVendorRateMaster> GetMahindraVendorRateById(int MahindraRateID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetMahindraVendorRateById";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@MahindraRateID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = MahindraRateID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<MahindraVendorRateMaster> list = new List<MahindraVendorRateMaster>();
                var listMHEL = DatatableHelper.ToList<MahindraVendorRateMaster>(ds.Tables[0]);
                return listMHEL;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static int UpdateMahindraVendorRate(MahindraVendorRateMaster vendorrate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandText = "UpdateMahindraVendorRate";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pMahindraVendorRateID", SqlDbType.Int).Value = vendorrate.MahindraVendorRateID;
                db.command.Parameters.Add("@pBillingPartyId", SqlDbType.Int).Value = vendorrate.BillingPartyId;
                db.command.Parameters.Add("@pConsignorId", SqlDbType.Int).Value = vendorrate.ConsignorId;
                db.command.Parameters.Add("@pConsigneeId", SqlDbType.Int).Value = vendorrate.ConsigneeId;
                db.command.Parameters.Add("@pFrom", SqlDbType.Int).Value = vendorrate.FROM;
                db.command.Parameters.Add("@pTo", SqlDbType.Int).Value = vendorrate.TO;
                db.command.Parameters.Add("@pChargeType", SqlDbType.Int).Value = vendorrate.ChargeTypeId;
                db.command.Parameters.Add("@pBoxes", SqlDbType.Int).Value = vendorrate.Boxes;
                db.command.Parameters.Add("@L", SqlDbType.Float).Value = vendorrate.L;
                db.command.Parameters.Add("@W", SqlDbType.Float).Value = vendorrate.W;
                db.command.Parameters.Add("@H", SqlDbType.Float).Value = vendorrate.H;
                db.command.Parameters.Add("@ChargedWeight", SqlDbType.Float).Value = vendorrate.ChargedWeight;
                db.command.Parameters.Add("@pFreightRate", SqlDbType.Float).Value = vendorrate.FRIEGHTRATE;
                db.command.Parameters.Add("@pDocketCharge", SqlDbType.Float).Value = vendorrate.DocketCharge;
                db.command.Parameters.Add("@pDoorCollection", SqlDbType.Float).Value = vendorrate.DoorCollection;
                db.command.Parameters.Add("@pDeliveryCharge", SqlDbType.Float).Value = vendorrate.DeliveryCharge;
                db.command.Parameters.Add("@pFOV", SqlDbType.Float).Value = vendorrate.FOV;
                db.command.Parameters.Add("@pHamaliCharge", SqlDbType.Float).Value = vendorrate.HamaliCharge;
                db.command.Parameters.Add("@pOtherCharges", SqlDbType.Float).Value = vendorrate.OtherCharges;
                db.command.Parameters.Add("@pMinKG", SqlDbType.Int).Value = vendorrate.MinKG;

                SqlParameter paramStatus = new SqlParameter();
                paramStatus.ParameterName = "@pStatus";
                paramStatus.Direction = ParameterDirection.Output;
                paramStatus.DbType = DbType.Int16;
                db.command.Parameters.Add(paramStatus);

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int status = Convert.ToInt32(paramStatus.Value);
                return status;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }
        #endregion

        #region Voucher
        internal static void DeleteConsignmentPayment(int consignmentPaymentID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteConsignmentVoucher";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pConsignmentVoucherID", SqlDbType.Int).Value = consignmentPaymentID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static void AddConsignmentPayment(ConsignmentVoucher voucherMaster, int loginID)
        {
            DBConnection db = new DBConnection();
            DateTime VoucherDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddConsignmentVoucher";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VoucherDate", SqlDbType.DateTime).Value = VoucherDate;
                db.command.Parameters.Add("@ConsignmentBillingID", SqlDbType.Int).Value = voucherMaster.ConsignmentBillingID;
                db.command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = voucherMaster.BankName;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = voucherMaster.BranchName;
                db.command.Parameters.Add("@ChequeNo", SqlDbType.NVarChar).Value = voucherMaster.ChequeNo;
                DateTime? Chequedate = null;
                if (!String.IsNullOrEmpty(voucherMaster.ChequeDate))
                {
                    Chequedate = DateTime.ParseExact(voucherMaster.ChequeDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                }
                db.command.Parameters.Add("@ChequeDate", SqlDbType.DateTime).Value = Chequedate;
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = loginID;
                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = voucherMaster.PaymentTypeID;
                db.command.Parameters.Add("@VoucherAmount", SqlDbType.Float).Value = voucherMaster.VoucherAmount;
                db.command.Parameters.Add("@Remarks", SqlDbType.VarChar).Value = voucherMaster.Remarks;
                db.command.Parameters.Add("@TDS", SqlDbType.Float).Value = voucherMaster.TDS;
                db.command.Parameters.Add("@Trade", SqlDbType.Float).Value = voucherMaster.Trade;
                db.command.Parameters.Add("@ShortAmount", SqlDbType.Float).Value = voucherMaster.ShortAmount;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
     
        internal static Dictionary<int, List<ConsignmentVoucher>> GetConsignmentPaymentIndex(int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetConsignmentPaymentIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<ConsignmentVoucher>(ds.Tables[0]);
                int count = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<ConsignmentVoucher>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }

        #endregion

        #region Expense
        internal static void DeleteExpense(int expenseID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteExpense";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pexpenseID", SqlDbType.Int).Value = expenseID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static void AddExpense(ExpenseMaster expenseMaster, int loginID)
        {
            DBConnection db = new DBConnection();
            DateTime ExpenseDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddExpense";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ExpenseDate", SqlDbType.DateTime).Value = ExpenseDate;
                db.command.Parameters.Add("@ExpenseType", SqlDbType.Int).Value = expenseMaster.ExpenseType;
                db.command.Parameters.Add("@PaidTo", SqlDbType.VarChar).Value = expenseMaster.PaidTo;
                db.command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = expenseMaster.BankName;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = expenseMaster.BranchName;
                db.command.Parameters.Add("@ChequeNo", SqlDbType.NVarChar).Value = expenseMaster.ChequeNo;
                DateTime? Chequedate = null;
                if (!String.IsNullOrEmpty(expenseMaster.ChequeDate))
                {
                    Chequedate = DateTime.ParseExact(expenseMaster.ChequeDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                }
                db.command.Parameters.Add("@ChequeDate", SqlDbType.DateTime).Value = Chequedate;
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = loginID;
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = expenseMaster.BranchID;
                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = expenseMaster.PaymentTypeID;
                db.command.Parameters.Add("@Amount", SqlDbType.Float).Value = expenseMaster.Amount;
                db.command.Parameters.Add("@UserID", SqlDbType.Int).Value = loginID;
                db.command.Parameters.Add("@Particulars", SqlDbType.VarChar).Value = expenseMaster.Particulars;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateExpense(ExpenseMaster expenseMaster, int loginID)
        {
            DBConnection db = new DBConnection();
            DateTime ExpenseDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateExpense";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ExpenseId", SqlDbType.Int).Value = expenseMaster.ExpenseID;
                db.command.Parameters.Add("@ExpenseType", SqlDbType.Int).Value = expenseMaster.ExpenseType;
                db.command.Parameters.Add("@PaidTo", SqlDbType.VarChar).Value = expenseMaster.PaidTo;
                db.command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = expenseMaster.BankName;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = expenseMaster.BranchName;
                db.command.Parameters.Add("@ChequeNo", SqlDbType.NVarChar).Value = expenseMaster.ChequeNo;
                DateTime? Chequedate = null;
                if (!String.IsNullOrEmpty(expenseMaster.ChequeDate))
                {
                    Chequedate = DateTime.ParseExact(expenseMaster.ChequeDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                }
                db.command.Parameters.Add("@ChequeDate", SqlDbType.DateTime).Value = Chequedate;
                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = expenseMaster.PaymentTypeID;
                db.command.Parameters.Add("@Amount", SqlDbType.Float).Value = expenseMaster.Amount;
                db.command.Parameters.Add("@Particulars", SqlDbType.VarChar).Value = expenseMaster.Particulars;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static Dictionary<int, List<ExpenseMaster>> GetExpenseIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetExpenseIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<ExpenseMaster>(ds.Tables[0]);
                int count = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<ExpenseMaster>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static List<ExpenseMaster> GetExpenseByID(int ExpenseID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetExpenseByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@ExpenseID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = ExpenseID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<ExpenseMaster> list = new List<ExpenseMaster>();
                var listVoucher = DatatableHelper.ToList<ExpenseMaster>(ds.Tables[0]);
                return listVoucher;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Vendor Expense
        internal static void DeleteVendorExpense(int expenseID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteVendorExpense";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pexpenseID", SqlDbType.Int).Value = expenseID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static void AddVendorExpense(VendorExpenseMaster expenseMaster, int loginID)
        {
            DBConnection db = new DBConnection();
            DateTime ExpenseDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

            db.ConnectoDB();
            try
            {

                db.command.CommandText = "AddVendorExpense";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ExpenseDate", SqlDbType.DateTime).Value = ExpenseDate;
                db.command.Parameters.Add("@VendorID", SqlDbType.Int).Value = expenseMaster.VendorID;
                db.command.Parameters.Add("@InvoiceNo", SqlDbType.VarChar).Value = expenseMaster.InvoiceNo;
                db.command.Parameters.Add("@BillAmount", SqlDbType.Float).Value = expenseMaster.BillAmount;
                db.command.Parameters.Add("@AdvanceAmount", SqlDbType.Float).Value = expenseMaster.AdvancePayment;
                db.command.Parameters.Add("@BalanceAmount", SqlDbType.Float).Value = expenseMaster.BalanceAmount;
                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = expenseMaster.PaymentTypeID;
                db.command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = expenseMaster.BankName;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = expenseMaster.BranchName;
                db.command.Parameters.Add("@ChequeNo", SqlDbType.NVarChar).Value = expenseMaster.ChequeNo;
                DateTime? Chequedate = null;
                if (!String.IsNullOrEmpty(expenseMaster.ChequeDate))
                {
                    Chequedate = DateTime.ParseExact(expenseMaster.ChequeDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                }
                db.command.Parameters.Add("@ChequeDate", SqlDbType.DateTime).Value = Chequedate;
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = loginID;
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = expenseMaster.BranchID;
                
                db.command.Parameters.Add("@UserID", SqlDbType.Int).Value = loginID;
                db.command.Parameters.Add("@Particulars", SqlDbType.VarChar).Value = expenseMaster.Particulars;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateVendorExpense(VendorExpenseMaster expenseMaster, int loginID)
        {
            DBConnection db = new DBConnection();
           
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateVendorExpense";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VendorExpenseID", SqlDbType.Int).Value = expenseMaster.VendorExpenseID;
                db.command.Parameters.Add("@VendorID", SqlDbType.Int).Value = expenseMaster.VendorID;
                db.command.Parameters.Add("@InvoiceNo", SqlDbType.VarChar).Value = expenseMaster.InvoiceNo;
                db.command.Parameters.Add("@BillAmount", SqlDbType.Float).Value = expenseMaster.BillAmount;
                db.command.Parameters.Add("@AdvanceAmount", SqlDbType.Float).Value = expenseMaster.AdvancePayment;
                db.command.Parameters.Add("@BalanceAmount", SqlDbType.Float).Value = expenseMaster.BalanceAmount;
                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = expenseMaster.PaymentTypeID;
                db.command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = expenseMaster.BankName;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = expenseMaster.BranchName;
                db.command.Parameters.Add("@ChequeNo", SqlDbType.NVarChar).Value = expenseMaster.ChequeNo;
                DateTime? Chequedate = null;
                if (!String.IsNullOrEmpty(expenseMaster.ChequeDate))
                {
                    Chequedate = DateTime.ParseExact(expenseMaster.ChequeDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                }
                db.command.Parameters.Add("@ChequeDate", SqlDbType.DateTime).Value = Chequedate;
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = loginID;
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = expenseMaster.BranchID;

                db.command.Parameters.Add("@UserID", SqlDbType.Int).Value = loginID;
                db.command.Parameters.Add("@Particulars", SqlDbType.VarChar).Value = expenseMaster.Particulars;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static Dictionary<int, List<VendorExpenseMaster>> GetVendorExpenseIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetVendorExpenseIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<VendorExpenseMaster>(ds.Tables[0]);
                int count = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<VendorExpenseMaster>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static List<VendorExpenseMaster> GetVendorExpenseByID(int ExpenseID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetVendorExpenseByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@ExpenseID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = ExpenseID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<VendorExpenseMaster> list = new List<VendorExpenseMaster>();
                var listVoucher = DatatableHelper.ToList<VendorExpenseMaster>(ds.Tables[0]);
                return listVoucher;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}