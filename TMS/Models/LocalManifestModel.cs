﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TMS.Common;
using TMS.Helper;
using TMS.EFModel;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Globalization;

namespace TMS.Models
{
    public class LocalManifestModel
    {


        #region Local Vehicle Hire Payment
        internal static Dictionary<int, DataTable> GetLocalVehicleHireIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetLocalVehicleHireIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();

                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, DataTable>();
                data.Add(count, ds.Tables[0]);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static Dictionary<int, DataTable> GetDeliveryVehicleHireIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetDeliveryVehicleHireIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();

                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, DataTable>();
                data.Add(count, ds.Tables[0]);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static DataTable GetLocalVehicleHirePayments(int branchID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetLocalVehicleHirePayment";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];
                //   List<VehicleHirePayment> list = new List<VehicleHirePayment>();
                // return list =  DatatableHelper.ToList<VehicleHirePayment>(ds.Tables[0]);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static void AddLocalVehicleHirePayment(int loginID, LocalVehicleHirePayment newVehicleHire)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newVehicleHire.CreatedBy = loginID;
                newVehicleHire.Created = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

                newVehicleHire.ModifiedBy = loginID;
                newVehicleHire.Modified =  TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                db.command.CommandText = "AddLocalVehicleHirePayment";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = newVehicleHire.BranchID;
                db.command.Parameters.Add("@HireDate", SqlDbType.DateTime).Value = newVehicleHire.HireDate;
                db.command.Parameters.Add("@TruckBookingType", SqlDbType.Int).Value = newVehicleHire.TruckBookingType;
                db.command.Parameters.Add("@VehicleID", SqlDbType.Int).Value = newVehicleHire.VehicleID;

                db.command.Parameters.Add("@VendorName", SqlDbType.VarChar).Value = newVehicleHire.VendorName;

                db.command.Parameters.Add("@VendorType ", SqlDbType.Int).Value = newVehicleHire.VendorType;

                db.command.Parameters.Add("@BookingAmount", SqlDbType.Float).Value = newVehicleHire.BookingAmount;
                db.command.Parameters.Add("@AdvancePayment", SqlDbType.Float).Value = newVehicleHire.AdvancePayment;
                db.command.Parameters.Add("@BalanceAmount", SqlDbType.Float).Value = newVehicleHire.BalanceAmount;

                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = newVehicleHire.PaymentTypeID;

                db.command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = newVehicleHire.BankName;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = newVehicleHire.BranchName;

                db.command.Parameters.Add("@IFSCCode", SqlDbType.VarChar).Value = newVehicleHire.IFSCCode;
                db.command.Parameters.Add("@PanNo", SqlDbType.VarChar).Value = newVehicleHire.PanNo;
                db.command.Parameters.Add("@Remarks", SqlDbType.VarChar).Value = newVehicleHire.Remarks;

                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = newVehicleHire.CreatedBy;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newVehicleHire.ModifiedBy;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();


            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }


        internal static void DeleteLocalVehicleHire(int loginID, int vehicleHireID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteLocalVehicleHire";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VehicleHireID", SqlDbType.Int).Value = vehicleHireID;
                //db.command.Parameters.Add("@DeletedBy", SqlDbType.Int).Value = loginID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateLocalVehicleHirePayment(int loginID, LocalVehicleHirePayment newVehicleHire)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newVehicleHire.CreatedBy = loginID;
                newVehicleHire.Created = System.DateTime.Now;

                newVehicleHire.ModifiedBy = loginID;
                newVehicleHire.Modified = System.DateTime.Now;

                db.command.CommandText = "UpdateLocalVehicleHirePayment";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();

                db.command.Parameters.Add("@TruckBookingType", SqlDbType.Int).Value = newVehicleHire.TruckBookingType;
                db.command.Parameters.Add("@VehicleID", SqlDbType.Int).Value = newVehicleHire.VehicleID;

                db.command.Parameters.Add("@VendorName", SqlDbType.VarChar).Value = newVehicleHire.VendorName;
                db.command.Parameters.Add("@VendorType ", SqlDbType.Int).Value = newVehicleHire.VendorType;
                db.command.Parameters.Add("@BookingAmount", SqlDbType.Float).Value = newVehicleHire.BookingAmount;
                db.command.Parameters.Add("@AdvancePayment", SqlDbType.Float).Value = newVehicleHire.AdvancePayment;
                db.command.Parameters.Add("@BalanceAmount", SqlDbType.Float).Value = newVehicleHire.BalanceAmount;

                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = newVehicleHire.PaymentTypeID;

                db.command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = newVehicleHire.BankName;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = newVehicleHire.BranchName;

                db.command.Parameters.Add("@IFSCCode", SqlDbType.VarChar).Value = newVehicleHire.IFSCCode;
                db.command.Parameters.Add("@PanNo", SqlDbType.VarChar).Value = newVehicleHire.PanNo;


                db.command.Parameters.Add("@VehicleHireID", SqlDbType.Int).Value = newVehicleHire.LocalVehicleHireID; 
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newVehicleHire.ModifiedBy;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();


            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }


        internal static List<LocalVehicleHirePayment> GetLocalVehicleHirePaymentByID(int vehicleHireID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetLocalVehicleHirePaymentByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@VehicleHireID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = vehicleHireID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<LocalVehicleHirePayment> list = new List<LocalVehicleHirePayment>();
                return list = DatatableHelper.ToList<LocalVehicleHirePayment>(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                // LogHelper.logException(ex, "GetConsignmentBookings", "");
                throw ex;
            }
        }

        #endregion

        #region Vehicle Hire Manifest
        internal static DataTable GetConsignmentsForLocalManifest(int branchID, int manifestID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentsForLocalManifest";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                SqlParameter paramManifestID = new SqlParameter();
                paramManifestID.ParameterName = "@ManifestID";
                paramManifestID.Direction = ParameterDirection.Input;
                paramManifestID.DbType = DbType.Int32;
                paramManifestID.Value = manifestID;

                cmd.Parameters.Add(paramBrnch);
                cmd.Parameters.Add(paramManifestID);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// M
        /// </summary>
        /// <param name="branchID"></param>
        /// <returns></returns>
        internal static List<LocalManifestConsignmentMapping> GetLocalManifestConsignments(int manifestID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetLocalManifestConsignments";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@ManifestID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = manifestID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                List<LocalManifestConsignmentMapping> list = new List<LocalManifestConsignmentMapping>();
                return list =  DatatableHelper.ToList<LocalManifestConsignmentMapping>(ds.Tables[0]);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static List<LocalVehicleHireManifest> GetLocalVehicleHireManifestByVehicleHireID(int vehicleHireID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetLocalVehicleHireManifestByVehicleHireID";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@VehicleHireID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = vehicleHireID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                List<LocalVehicleHireManifest> list = new List<LocalVehicleHireManifest>();
                return list =  DatatableHelper.ToList<LocalVehicleHireManifest>(ds.Tables[0]);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static int AddLocalVehicleHireManifest(int loginID, int branchID, LocalVehicleHireManifest newManifest)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newManifest.CreatedBy = loginID;
                newManifest.Created = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

                newManifest.ModifiedBy = loginID;
                newManifest.Modified = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

                db.command.CommandText = "AddLocalVehicleHireManifest";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VehicleHireID", SqlDbType.Int).Value = newManifest.VehicleHireID;

                db.command.Parameters.Add("@DriverName", SqlDbType.VarChar).Value = newManifest.DriverName;
                db.command.Parameters.Add("@DriverMobileNo", SqlDbType.VarChar).Value = newManifest.DriverMobileNo;
                db.command.Parameters.Add("@LicenseNo", SqlDbType.VarChar).Value = newManifest.LicenseNo;
                DateTime? estimated = null;
                if (!String.IsNullOrEmpty(newManifest.EstimatedDeliveryDate))
                    estimated = DateTime.ParseExact(newManifest.EstimatedDeliveryDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                db.command.Parameters.Add("@EstimatedDeliveryDate", SqlDbType.DateTime).Value = estimated;
                DateTime timespan = TimeZoneInfo.ConvertTime(DateTime.Parse(Convert.ToString(newManifest.ArrivalTime)), TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                string timeArrival = Convert.ToString(timespan.TimeOfDay);
                DateTime timespanDep = TimeZoneInfo.ConvertTime(DateTime.Parse(Convert.ToString(newManifest.DepartureTime)), TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                string timeDeparture = Convert.ToString(timespanDep.TimeOfDay);
                db.command.Parameters.Add("@ArrivalTime", SqlDbType.VarChar).Value = timeArrival;
                db.command.Parameters.Add("@DepartureTime", SqlDbType.VarChar).Value = timeDeparture;


                db.command.Parameters.Add("@FromLoc", SqlDbType.Int).Value = newManifest.FromLoc;

                db.command.Parameters.Add("@ToLoc", SqlDbType.Int).Value = newManifest.ToLoc;
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchID;
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = newManifest.CreatedBy;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newManifest.ModifiedBy;
                SqlParameter paramMFID = new SqlParameter();
                paramMFID.ParameterName = "@ManifestID";
                paramMFID.Direction = ParameterDirection.Output;
                paramMFID.DbType = DbType.Int32;
                db.command.Parameters.Add(paramMFID);


                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int manifestID = Convert.ToInt32(paramMFID.Value);
                return manifestID;

            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateLocalVehicleHireManifest(int loginID, LocalVehicleHireManifest  newManifest)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                newManifest.ModifiedBy = loginID;
                newManifest.Modified = System.DateTime.Now;

                db.command.CommandText = "UpdateLocalVehicleHireManifest";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ManifestNo", SqlDbType.VarChar).Value = newManifest.ManifestNo;


                db.command.Parameters.Add("@DriverName", SqlDbType.VarChar).Value = newManifest.DriverName;
                db.command.Parameters.Add("@DriverMobileNo", SqlDbType.VarChar).Value = newManifest.DriverMobileNo;
                db.command.Parameters.Add("@LicenseNo", SqlDbType.VarChar).Value = newManifest.LicenseNo;
                DateTime? estimated = null;
                if (!String.IsNullOrEmpty(newManifest.EstimatedDeliveryDate))
                    estimated = DateTime.ParseExact(newManifest.EstimatedDeliveryDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                db.command.Parameters.Add("@EstimatedDeliveryDate", SqlDbType.DateTime).Value = estimated;
                db.command.Parameters.Add("@ArrivalTime", SqlDbType.VarChar).Value = newManifest.ArrivalTime;
                db.command.Parameters.Add("@DepartureTime", SqlDbType.VarChar).Value = newManifest.DepartureTime;

                db.command.Parameters.Add("@FromLoc", SqlDbType.Int).Value = newManifest.FromLoc;

                db.command.Parameters.Add("@ToLoc", SqlDbType.Int).Value = newManifest.ToLoc;
                db.command.Parameters.Add("@ManifestID", SqlDbType.Int).Value = newManifest.ManifestID;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newManifest.ModifiedBy;


                db.command.ExecuteNonQuery();
                db.ConnectionClose();


            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }


        internal static void DeleteLocalVehicleHireManifest(int loginID, int manifestID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteLocalVehicleHireManifest";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ManifestID", SqlDbType.Int).Value = manifestID;
                //db.command.Parameters.Add("@DeletedBy", SqlDbType.Int).Value = loginID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static List<LocalVehicleHireManifest> GetLocalVehicleHireManifestByManifestID(int manifestID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetLocalVehicleHireManifestByManifestID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@ManifestID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = manifestID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<LocalVehicleHireManifest> list = new List<LocalVehicleHireManifest>();
                return list =  DatatableHelper.ToList<LocalVehicleHireManifest>(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                // LogHelper.logException(ex, "GetConsignmentBookings", "");
                throw ex;
            }
        }

        #endregion
        #region Delivery Vehicle Hire
        internal static DataTable GetDeliveryVehicleHire(int branchID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetDeliveryVehicleHire";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static void AddDeliveryVehicleHire(int loginID, DeliveryVehicleHire newVehicleHire)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newVehicleHire.CreatedBy = loginID;
                newVehicleHire.Created = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

                newVehicleHire.ModifiedBy = loginID;
                newVehicleHire.Modified = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                newVehicleHire.HireDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

                db.command.CommandText = "AddDeliveryVehicleHire";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = newVehicleHire.BranchID;
                db.command.Parameters.Add("@HireDate", SqlDbType.DateTime).Value = newVehicleHire.HireDate;
                db.command.Parameters.Add("@TruckBookingType", SqlDbType.Int).Value = newVehicleHire.TruckBookingType;
                db.command.Parameters.Add("@VehicleID", SqlDbType.Int).Value = newVehicleHire.VehicleID;


                db.command.Parameters.Add("@VendorName", SqlDbType.VarChar).Value = newVehicleHire.VendorName;
                db.command.Parameters.Add("@VendorType ", SqlDbType.Int).Value = newVehicleHire.VendorType;


                db.command.Parameters.Add("@BookingAmount", SqlDbType.Float).Value = newVehicleHire.BookingAmount;
                db.command.Parameters.Add("@AdvancePayment", SqlDbType.Float).Value = newVehicleHire.AdvancePayment;
                db.command.Parameters.Add("@BalanceAmount", SqlDbType.Float).Value = newVehicleHire.BalanceAmount;

                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = newVehicleHire.PaymentTypeID;

                db.command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = newVehicleHire.BankName;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = newVehicleHire.BranchName;

                db.command.Parameters.Add("@IFSCCode", SqlDbType.VarChar).Value = newVehicleHire.IFSCCode;
                db.command.Parameters.Add("@PanNo", SqlDbType.VarChar).Value = newVehicleHire.PanNo;
                db.command.Parameters.Add("@Remarks", SqlDbType.VarChar).Value = newVehicleHire.Remarks;

                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = newVehicleHire.CreatedBy;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newVehicleHire.ModifiedBy;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();


            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                Utility.LogError(ex, "AddDeliveryVehicleHire", loginID);
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static List<DeliveryVehicleHire> GetDeliveryVehicleHireByID(int deliveryVehicleHireID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetDelievryVehicleHireByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@DeliveryVehicleHireID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = deliveryVehicleHireID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                
                List<DeliveryVehicleHire> list = new List<DeliveryVehicleHire>();
                return list = DatatableHelper.ToList<DeliveryVehicleHire>(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                // LogHelper.logException(ex, "GetConsignmentBookings", "");
                throw ex;
            }
        }
        internal static void UpdateDeliveryVehicleHire(int loginID, DeliveryVehicleHire newVehicleHire)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newVehicleHire.CreatedBy = loginID;
                newVehicleHire.Created = System.DateTime.Now;

                newVehicleHire.ModifiedBy = loginID;
                newVehicleHire.Modified = System.DateTime.Now;

                db.command.CommandText = "UpdateDeliveryVehicleHire";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();

                db.command.Parameters.Add("@TruckBookingType", SqlDbType.Int).Value = newVehicleHire.TruckBookingType;
                db.command.Parameters.Add("@VehicleID", SqlDbType.Int).Value = newVehicleHire.VehicleID;

                db.command.Parameters.Add("@VendorName", SqlDbType.VarChar).Value = newVehicleHire.VendorName;
                db.command.Parameters.Add("@VendorType ", SqlDbType.Int).Value = newVehicleHire.VendorType;

                db.command.Parameters.Add("@BookingAmount", SqlDbType.Float).Value = newVehicleHire.BookingAmount;
                db.command.Parameters.Add("@AdvancePayment", SqlDbType.Float).Value = newVehicleHire.AdvancePayment;
                db.command.Parameters.Add("@BalanceAmount", SqlDbType.Float).Value = newVehicleHire.BalanceAmount;

                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = newVehicleHire.PaymentTypeID;

                db.command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = newVehicleHire.BankName;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = newVehicleHire.BranchName;

                db.command.Parameters.Add("@IFSCCode", SqlDbType.VarChar).Value = newVehicleHire.IFSCCode;
                db.command.Parameters.Add("@PanNo", SqlDbType.VarChar).Value = newVehicleHire.PanNo;

                db.command.Parameters.Add("@DeliveryVehicleHireID", SqlDbType.Int).Value = newVehicleHire.DeliveryVehicleHireID;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newVehicleHire.ModifiedBy;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();


            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteDeliveryVehicleHire(int loginID, int deliveryVehicleHireID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteDeliveryVehicleHire";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@DeliveryVehicleHireID", SqlDbType.Int).Value = deliveryVehicleHireID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static List<DeliveryVehicleHireManifest> GetDeliveryVehicleHireManifestByVehicleHireID(int deliveryVehicleHireID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetDeliveryVehicleHireManifestByDeliveryVehicleHireID";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@DeliveryVehicleHireID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = deliveryVehicleHireID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                List<DeliveryVehicleHireManifest> list = new List<DeliveryVehicleHireManifest>();
                return list = DatatableHelper.ToList<DeliveryVehicleHireManifest>(ds.Tables[0]);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static int AddDeliveryVehicleHireManifest(int loginID, int branchID, DeliveryVehicleHireManifest newManifest)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newManifest.CreatedBy = loginID;
                newManifest.Created = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

                newManifest.ModifiedBy = loginID;
                newManifest.Modified = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

                db.command.CommandText = "AddDeliveryVehicleHireManifest";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@DeliveryVehicleHireID", SqlDbType.Int).Value = newManifest.DeliveryVehicleHireID;

                db.command.Parameters.Add("@DriverName", SqlDbType.VarChar).Value = newManifest.DriverName;
                db.command.Parameters.Add("@DriverMobileNo", SqlDbType.VarChar).Value = newManifest.DriverMobileNo;
                db.command.Parameters.Add("@LicenseNo", SqlDbType.VarChar).Value = newManifest.LicenseNo;
                DateTime? estimated = null;
                if (!String.IsNullOrEmpty(newManifest.EstimatedDeliveryDate))
                    estimated = DateTime.ParseExact(newManifest.EstimatedDeliveryDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                db.command.Parameters.Add("@EstimatedDeliveryDate", SqlDbType.DateTime).Value = estimated;
                DateTime timespan = TimeZoneInfo.ConvertTime(DateTime.Parse(System.Convert.ToString(newManifest.ArrivalTime)), TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                string timeArrival = System.Convert.ToString(timespan.TimeOfDay);
                DateTime timespanDep = TimeZoneInfo.ConvertTime(DateTime.Parse(System.Convert.ToString(newManifest.DepartureTime)), TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                string timeDeparture = System.Convert.ToString(timespanDep.TimeOfDay);
                db.command.Parameters.Add("@ArrivalTime", SqlDbType.VarChar).Value = timeArrival;
                db.command.Parameters.Add("@DepartureTime", SqlDbType.VarChar).Value = timeDeparture;


                db.command.Parameters.Add("@FromLoc", SqlDbType.Int).Value = newManifest.FromLoc;

                db.command.Parameters.Add("@ToLoc", SqlDbType.Int).Value = newManifest.ToLoc;
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchID;
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = newManifest.CreatedBy;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newManifest.ModifiedBy;
                SqlParameter paramMFID = new SqlParameter();
                paramMFID.ParameterName = "@DeliveryManifestID";
                paramMFID.Direction = ParameterDirection.Output;
                paramMFID.DbType = DbType.Int32;
                db.command.Parameters.Add(paramMFID);


                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int manifestID = System.Convert.ToInt32(paramMFID.Value);
                return manifestID;

            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static DataTable GetConsignmentsForDeliveryManifest(int branchID, int manifestID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentsForDeliveryManifest";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                SqlParameter paramManifestID = new SqlParameter();
                paramManifestID.ParameterName = "@ManifestID";
                paramManifestID.Direction = ParameterDirection.Input;
                paramManifestID.DbType = DbType.Int32;
                paramManifestID.Value = manifestID;

                cmd.Parameters.Add(paramBrnch);
                cmd.Parameters.Add(paramManifestID);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static DataTable GetConsignmentsForManifestDelivery(int branchID, int manifestID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentsForManifestDelivery";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                SqlParameter paramManifestID = new SqlParameter();
                paramManifestID.ParameterName = "@ManifestID";
                paramManifestID.Direction = ParameterDirection.Input;
                paramManifestID.DbType = DbType.Int32;
                paramManifestID.Value = manifestID;

                cmd.Parameters.Add(paramBrnch);
                cmd.Parameters.Add(paramManifestID);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static void AddDeliveryManifestConsignment(int loginID, List<DeliveryManifestConsignmentMapping> ManifestConsignList)
        {
            string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = connectionStr;
            connection.Open();

            using (SqlTransaction trans = connection.BeginTransaction())
            {
                try
                {

                    SqlCommand delCmd = new SqlCommand();
                    delCmd.CommandText = "DeleteDeliveryManifestConsignments";
                    delCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter par = new SqlParameter();
                    delCmd.Parameters.Add("@ManifestID", SqlDbType.Int).Value = ManifestConsignList[0].DeliveryManifestID;
                    delCmd.Connection = connection;
                    delCmd.Transaction = trans;
                    delCmd.ExecuteNonQuery();

                    foreach (DeliveryManifestConsignmentMapping newManifestConsign in ManifestConsignList)
                    {

                        newManifestConsign.CreatedBy = loginID;
                        SqlCommand addCmd = new SqlCommand();
                        addCmd.CommandText = "AddDeliveryManifestConsignments";
                        addCmd.CommandType = CommandType.StoredProcedure;
                        addCmd.Parameters.Add("@ManifestID", SqlDbType.Int).Value = newManifestConsign.DeliveryManifestID;
                        addCmd.Parameters.Add("@ConsignmentID", SqlDbType.Int).Value = newManifestConsign.ConsignmentID;
                        addCmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = newManifestConsign.CreatedBy;
                        addCmd.Parameters.Add("@DeliveryStatus", SqlDbType.Int).Value = newManifestConsign.DeliveryStatus;
                        addCmd.Parameters.Add("@DeliveryDate", SqlDbType.DateTime).Value = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                        addCmd.Connection = connection;
                        addCmd.Transaction = trans;
                        addCmd.ExecuteNonQuery();
                    }

                    trans.Commit();
                    connection.Close();

                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    if (connection.State == ConnectionState.Open)
                        connection.Close();

                    throw new Exception(ex.Message);
                }
            }


        }
        internal static void UpdateDeliveryVehicleHireManifest(int loginID, DeliveryVehicleHireManifest newManifest)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                newManifest.ModifiedBy = loginID;
                newManifest.Modified = System.DateTime.Now;

                db.command.CommandText = "UpdateDeliveryVehicleHireManifest";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ManifestNo", SqlDbType.VarChar).Value = newManifest.ManifestNo;


                db.command.Parameters.Add("@DriverName", SqlDbType.VarChar).Value = newManifest.DriverName;
                db.command.Parameters.Add("@DriverMobileNo", SqlDbType.VarChar).Value = newManifest.DriverMobileNo;
                db.command.Parameters.Add("@LicenseNo", SqlDbType.VarChar).Value = newManifest.LicenseNo;
                DateTime? estimated = null;
                if (!String.IsNullOrEmpty(newManifest.EstimatedDeliveryDate))
                    estimated = DateTime.ParseExact(newManifest.EstimatedDeliveryDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                db.command.Parameters.Add("@EstimatedDeliveryDate", SqlDbType.DateTime).Value = estimated;
                db.command.Parameters.Add("@ArrivalTime", SqlDbType.VarChar).Value = newManifest.ArrivalTime;
                db.command.Parameters.Add("@DepartureTime", SqlDbType.VarChar).Value = newManifest.DepartureTime;

                db.command.Parameters.Add("@FromLoc", SqlDbType.Int).Value = newManifest.FromLoc;

                db.command.Parameters.Add("@ToLoc", SqlDbType.Int).Value = newManifest.ToLoc;
                db.command.Parameters.Add("@DeliveryManifestID", SqlDbType.Int).Value = newManifest.DeliveryManifestID;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newManifest.ModifiedBy;


                db.command.ExecuteNonQuery();
                db.ConnectionClose();


            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }

        #endregion
        #region manifest consignmets
        internal static void AddLocalManifestConsignment(int loginID, List<LocalManifestConsignmentMapping> ManifestConsignList)
        {
            string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = connectionStr;
            connection.Open();

            using (SqlTransaction trans = connection.BeginTransaction())
            {
                try
                {

                    SqlCommand delCmd = new SqlCommand();
                    delCmd.CommandText = "DeleteLocalManifestConsignments";
                    delCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter par = new SqlParameter();
                    delCmd.Parameters.Add("@ManifestID", SqlDbType.Int).Value = ManifestConsignList[0].ManifestID;
                    delCmd.Connection = connection;
                    delCmd.Transaction = trans;
                    delCmd.ExecuteNonQuery();

                    foreach (LocalManifestConsignmentMapping newManifestConsign in ManifestConsignList)
                    {

                        newManifestConsign.CreatedBy = loginID;
                        SqlCommand addCmd = new SqlCommand();
                        addCmd.CommandText = "AddLocalManifestConsignments";
                        addCmd.CommandType = CommandType.StoredProcedure;
                        addCmd.Parameters.Add("@ManifestID", SqlDbType.Int).Value = newManifestConsign.ManifestID;
                        addCmd.Parameters.Add("@ConsignmentID", SqlDbType.Int).Value = newManifestConsign.ConsignmentID;
                        addCmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = newManifestConsign.CreatedBy;
                        addCmd.Connection = connection;
                        addCmd.Transaction = trans;
                        addCmd.ExecuteNonQuery();
                    }

                    trans.Commit();
                    connection.Close();

                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    if (connection.State == ConnectionState.Open)
                        connection.Close();

                    throw new Exception(ex.Message);
                }
            }


        }



        #endregion
       
    }
}