﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TMS.Common;
using TMS.Helper;
using TMS.EFModel;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Data.SqlTypes;
using System.Globalization;
using TMS.CustomModels;

namespace TMS.Models
{
    public static class ManifestModel
    {
        #region VEHICLE MASTER
        internal static void AddVehicle(int loginID, VehicleMaster newVehicle)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newVehicle.CreatedBy = loginID;
                newVehicle.Created = System.DateTime.Now;

                newVehicle.ModifiedBy = loginID;
                newVehicle.Modified = System.DateTime.Now;
                // if (newVehicle.VehicleNo == null) newVehicle.VehicleNo = "";
                db.command.CommandText = "AddVehicle";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VehicleName", SqlDbType.VarChar).Value = newVehicle.VehicleName;
                db.command.Parameters.Add("@VehicleNo", SqlDbType.VarChar).Value = newVehicle.VehicleNo;
                db.command.Parameters.Add("@VehicleType", SqlDbType.VarChar).Value = newVehicle.VehicleType;

                db.command.Parameters.Add("@RegistrationDate", SqlDbType.Date).Value = DateTime.ParseExact(newVehicle.RegistrationDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                db.command.Parameters.Add("@FitnessExpDate ", SqlDbType.Date).Value = DateTime.ParseExact(newVehicle.FitnessExpDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);


                db.command.Parameters.Add("@InsuranceExpDate", SqlDbType.Date).Value = DateTime.ParseExact(newVehicle.InsuranceExpDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                db.command.Parameters.Add("@PUCExpDate", SqlDbType.Date).Value = DateTime.ParseExact(newVehicle.PUCExpDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                db.command.Parameters.Add("@EngineNo", SqlDbType.VarChar).Value = newVehicle.EngineNo;

                db.command.Parameters.Add("@ChasisNo", SqlDbType.VarChar).Value = newVehicle.ChasisNo;

                db.command.Parameters.Add("@OwnerType", SqlDbType.VarChar).Value = newVehicle.OwnerType;
                db.command.Parameters.Add("@BrokerName", SqlDbType.VarChar).Value = newVehicle.BrokerName;

                db.command.Parameters.Add("@VendorName", SqlDbType.VarChar).Value = newVehicle.VendorName;
                db.command.Parameters.Add("@loadingCapacity", SqlDbType.VarChar).Value = newVehicle.loadingCapacity;
                db.command.Parameters.Add("@MaxloadingCapacity", SqlDbType.VarChar).Value = newVehicle.MaxloadingCapacity;
                db.command.Parameters.Add("@isHiredVehicle", SqlDbType.Bit).Value = newVehicle.isHiredVehicle;
                db.command.Parameters.Add("@OPMODE", SqlDbType.Int).Value = 1;

                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = newVehicle.CreatedBy;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newVehicle.ModifiedBy;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();


            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        #endregion

        #region Vehicle Hire Payment
        internal static Dictionary<int, DataTable> GetVehicleHireIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetVehicleHireIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();

                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, DataTable>();
                data.Add(count, ds.Tables[0]);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static Dictionary<int, DataTable> GetDeliveryVehicleHireIndex(int branchId, int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetDeliveryVehicleHireIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();

                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, DataTable>();
                data.Add(count, ds.Tables[0]);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static Dictionary<int, DataTable> GetManifestUnloadIndex(int branchId, int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetManifestToUnloadIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchToId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                int count = 0;
                if (ds.Tables[1].Rows.Count > 0)
                    count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, DataTable>();
                data.Add(count, ds.Tables[0]);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }


        internal static Dictionary<int, DataTable> GetDeliveryManifestStatusData(int branchId, int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetDeliveryManifestStatusData";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();

                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, DataTable>();
                data.Add(count, ds.Tables[0]);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static Dictionary<int, DataTable> GetConsignmentDeliveryIndex(int branchId, int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetConsignmentDeliveryIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();

                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, DataTable>();
                data.Add(count, ds.Tables[0]);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static DataTable GetVehicleHirePayments(int branchID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetVehicleHirePayment";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];
                //   List<VehicleHirePayment> list = new List<VehicleHirePayment>();
                // return list =  DatatableHelper.ToList<VehicleHirePayment>(ds.Tables[0]);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static ManifestModels GetManifestById(int manifestId)
        {
            ManifestModels objManifest = new ManifestModels();
            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetManifestById";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@pManifestId";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.String;
                paramCnB.Value = manifestId;
                cmd.Parameters.Add(paramCnB);
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                var list = DatatableHelper.ToList<ManifestModels>(ds.Tables[0]);
                objManifest = list.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objManifest;
        }
        internal static ManifestConsignmentModels GetManifestConsignmentById(int manifestId)
        {
            ManifestConsignmentModels objManifest = new ManifestConsignmentModels();
            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetManifestConsignmentById";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@pManifestConsignmentId";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = manifestId;
                cmd.Parameters.Add(paramCnB);
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                var list = DatatableHelper.ToList<ManifestConsignmentModels>(ds.Tables[0]);
                objManifest = list.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objManifest;
        }

        internal static void AddVehicleHirePayment(int loginID, VehicleHirePayment newVehicleHire)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newVehicleHire.CreatedBy = loginID;
                newVehicleHire.Created = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

                newVehicleHire.ModifiedBy = loginID;
                newVehicleHire.Modified = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                db.command.CommandText = "AddVehicleHirePayment";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = newVehicleHire.BranchID;
                db.command.Parameters.Add("@HireDate", SqlDbType.DateTime).Value = newVehicleHire.HireDate;
                db.command.Parameters.Add("@TruckBookingType", SqlDbType.Int).Value = newVehicleHire.TruckBookingType;
                db.command.Parameters.Add("@VehicleID", SqlDbType.Int).Value = newVehicleHire.VehicleID;


                db.command.Parameters.Add("@VendorName", SqlDbType.VarChar).Value = newVehicleHire.VendorName;
                db.command.Parameters.Add("@VendorType ", SqlDbType.Int).Value = newVehicleHire.VendorType;


                db.command.Parameters.Add("@BookingAmount", SqlDbType.Float).Value = newVehicleHire.BookingAmount;
                db.command.Parameters.Add("@AdvancePayment", SqlDbType.Float).Value = newVehicleHire.AdvancePayment;
                db.command.Parameters.Add("@BalanceAmount", SqlDbType.Float).Value = newVehicleHire.BalanceAmount;

                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = newVehicleHire.PaymentTypeID;

                db.command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = newVehicleHire.BankName;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = newVehicleHire.BranchName;

                db.command.Parameters.Add("@IFSCCode", SqlDbType.VarChar).Value = newVehicleHire.IFSCCode;
                db.command.Parameters.Add("@PanNo", SqlDbType.VarChar).Value = newVehicleHire.PanNo;
                db.command.Parameters.Add("@Remarks", SqlDbType.VarChar).Value = newVehicleHire.Remarks;

                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = newVehicleHire.CreatedBy;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newVehicleHire.ModifiedBy;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();


            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                Utility.LogError(ex, "AddVehicleHirePayment", loginID);
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateVehicleHirePayment(int loginID, VehicleHirePayment newVehicleHire)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newVehicleHire.CreatedBy = loginID;
                newVehicleHire.Created = System.DateTime.Now;

                newVehicleHire.ModifiedBy = loginID;
                newVehicleHire.Modified = System.DateTime.Now;

                db.command.CommandText = "UpdateVehicleHirePayment";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();

                db.command.Parameters.Add("@TruckBookingType", SqlDbType.Int).Value = newVehicleHire.TruckBookingType;
                db.command.Parameters.Add("@VehicleID", SqlDbType.Int).Value = newVehicleHire.VehicleID;

                db.command.Parameters.Add("@VendorName", SqlDbType.VarChar).Value = newVehicleHire.VendorName;
                db.command.Parameters.Add("@VendorType ", SqlDbType.Int).Value = newVehicleHire.VendorType;

                db.command.Parameters.Add("@BookingAmount", SqlDbType.Float).Value = newVehicleHire.BookingAmount;
                db.command.Parameters.Add("@AdvancePayment", SqlDbType.Float).Value = newVehicleHire.AdvancePayment;
                db.command.Parameters.Add("@BalanceAmount", SqlDbType.Float).Value = newVehicleHire.BalanceAmount;

                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = newVehicleHire.PaymentTypeID;

                db.command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = newVehicleHire.BankName;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = newVehicleHire.BranchName;

                db.command.Parameters.Add("@IFSCCode", SqlDbType.VarChar).Value = newVehicleHire.IFSCCode;
                db.command.Parameters.Add("@PanNo", SqlDbType.VarChar).Value = newVehicleHire.PanNo;

                db.command.Parameters.Add("@VehicleHireID", SqlDbType.Int).Value = newVehicleHire.VehicleHireID;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newVehicleHire.ModifiedBy;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();


            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }


        internal static void DeleteVehicleHire(int loginID, int vehicleHireID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteVehicleHire";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VehicleHireID", SqlDbType.Int).Value = vehicleHireID;
                //db.command.Parameters.Add("@DeletedBy", SqlDbType.Int).Value = loginID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static List<VehicleHirePayment> GetVehicleHirePaymentByID(int vehicleHireID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetVehicleHirePaymentByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@VehicleHireID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = vehicleHireID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<VehicleHirePayment> list = new List<VehicleHirePayment>();
                return list = DatatableHelper.ToList<VehicleHirePayment>(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                // LogHelper.logException(ex, "GetConsignmentBookings", "");
                throw ex;
            }
        }

        #endregion



        #region Vehicle Hire Manifest
        internal static DataTable GetConsignmentsForManifest(int branchID, int manifestID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentsForManifest";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                SqlParameter paramManifestID = new SqlParameter();
                paramManifestID.ParameterName = "@ManifestID";
                paramManifestID.Direction = ParameterDirection.Input;
                paramManifestID.DbType = DbType.Int32;
                paramManifestID.Value = manifestID;

                SqlParameter paramVehicleNo = new SqlParameter();
                paramVehicleNo.ParameterName = "@VehicleNo";
                paramVehicleNo.Direction = ParameterDirection.Input;
                paramVehicleNo.DbType = DbType.String;
                paramVehicleNo.Value = "GF001";

                cmd.Parameters.Add(paramBrnch);
                cmd.Parameters.Add(paramManifestID);
                cmd.Parameters.Add(paramVehicleNo);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static DataTable GetConsignmentsForManifestReport(int manifestID, string Type)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentsForManifestReport";

                SqlParameter paramManifestID = new SqlParameter();
                paramManifestID.ParameterName = "@ManifestID";
                paramManifestID.Direction = ParameterDirection.Input;
                paramManifestID.DbType = DbType.Int32;
                paramManifestID.Value = manifestID;
                cmd.Parameters.Add(paramManifestID);

                SqlParameter paramType = new SqlParameter();
                paramType.ParameterName = "@Type";
                paramType.Direction = ParameterDirection.Input;
                paramType.DbType = DbType.String;
                paramType.Value = Type;
                cmd.Parameters.Add(paramType);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// M
        /// </summary>
        /// <param name="branchID"></param>
        /// <returns></returns>
        internal static List<ManifestConsignmentMapping> GetManifestConsignments(int manifestID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetManifestConsignments";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@ManifestID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = manifestID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                List<ManifestConsignmentMapping> list = new List<ManifestConsignmentMapping>();
                return list = DatatableHelper.ToList<ManifestConsignmentMapping>(ds.Tables[0]);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static List<VehicleHireManifest> GetVehicleHireManifestByVehicleHireID(int vehicleHireID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetVehicleHireManifestByVehicleHireID";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@VehicleHireID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = vehicleHireID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                List<VehicleHireManifest> list = new List<VehicleHireManifest>();
                return list = DatatableHelper.ToList<VehicleHireManifest>(ds.Tables[0]);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static int AddVehicleHireManifest(int loginID, int branchID, VehicleHireManifest newManifest)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newManifest.CreatedBy = loginID;
               
                newManifest.ModifiedBy = loginID;
                newManifest.Modified = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

                db.command.CommandText = "AddVehicleHireManifest";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@VehicleHireID", SqlDbType.Int).Value = newManifest.VehicleHireID;

                db.command.Parameters.Add("@DriverName", SqlDbType.VarChar).Value = newManifest.DriverName;
                db.command.Parameters.Add("@DriverMobileNo", SqlDbType.VarChar).Value = newManifest.DriverMobileNo;
                db.command.Parameters.Add("@LicenseNo", SqlDbType.VarChar).Value = newManifest.LicenseNo;
                DateTime? estimated = null;
                if (!String.IsNullOrEmpty(newManifest.EstimatedDeliveryDate))
                    estimated = DateTime.ParseExact(newManifest.EstimatedDeliveryDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                db.command.Parameters.Add("@EstimatedDeliveryDate", SqlDbType.DateTime).Value = estimated;
                DateTime timespan = TimeZoneInfo.ConvertTime(DateTime.Parse(Convert.ToString(newManifest.ArrivalTime)), TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                string timeArrival = timespan.TimeOfDay.ToString();
                DateTime timespanDep = TimeZoneInfo.ConvertTime(DateTime.Parse(Convert.ToString(newManifest.DepartureTime)), TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                string timeDeparture = timespanDep.TimeOfDay.ToString();
                db.command.Parameters.Add("@ArrivalTime", SqlDbType.VarChar).Value = timeArrival;
                db.command.Parameters.Add("@DepartureTime", SqlDbType.VarChar).Value = timeDeparture;
                db.command.Parameters.Add("@ArrivalDate", SqlDbType.DateTime).Value = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                db.command.Parameters.Add("@pCreated", SqlDbType.DateTime).Value = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                db.command.Parameters.Add("@FromLoc", SqlDbType.Int).Value = newManifest.FromLoc;

                db.command.Parameters.Add("@ToLoc", SqlDbType.Int).Value = newManifest.ToLoc;
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchID;
                db.command.Parameters.Add("@pDeliveryBranch", SqlDbType.Int).Value = newManifest.DeliveryBranch;
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = newManifest.CreatedBy;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newManifest.ModifiedBy;
                SqlParameter paramMFID = new SqlParameter();
                paramMFID.ParameterName = "@ManifestID";
                paramMFID.Direction = ParameterDirection.Output;
                paramMFID.DbType = DbType.Int32;
                db.command.Parameters.Add(paramMFID);


                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int manifestID = Convert.ToInt32(paramMFID.Value);
                return manifestID;

            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateVehicleHireManifest(int loginID, VehicleHireManifest newManifest)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                newManifest.ModifiedBy = loginID;
                newManifest.Modified = System.DateTime.Now;

                db.command.CommandText = "UpdateVehicleHireManifest";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();

                db.command.Parameters.Add("@DriverName", SqlDbType.VarChar).Value = newManifest.DriverName;
                db.command.Parameters.Add("@DriverMobileNo", SqlDbType.VarChar).Value = newManifest.DriverMobileNo;
                db.command.Parameters.Add("@LicenseNo", SqlDbType.VarChar).Value = newManifest.LicenseNo;
                DateTime? estimated = null;
                if (!String.IsNullOrEmpty(newManifest.EstimatedDeliveryDate))
                    estimated = DateTime.ParseExact(newManifest.EstimatedDeliveryDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                db.command.Parameters.Add("@EstimatedDeliveryDate", SqlDbType.DateTime).Value = estimated;
                db.command.Parameters.Add("@ArrivalTime", SqlDbType.VarChar).Value = newManifest.ArrivalTime;
                db.command.Parameters.Add("@DepartureTime", SqlDbType.VarChar).Value = newManifest.DepartureTime;

                db.command.Parameters.Add("@FromLoc", SqlDbType.Int).Value = newManifest.FromLoc;

                db.command.Parameters.Add("@ToLoc", SqlDbType.Int).Value = newManifest.ToLoc;

                db.command.Parameters.Add("@ManifestID", SqlDbType.Int).Value = newManifest.ManifestID;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = newManifest.ModifiedBy;
                db.command.Parameters.Add("@pDeliveryBranch", SqlDbType.Int).Value = newManifest.DeliveryBranch;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();


            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }

        internal static void UpdateManifestArticleAndWeight(int totalArticle, float totalActualWeight, int manifestId, char type)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateManifestArticleAndWeight";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();

                db.command.Parameters.Add("@pManifestID", SqlDbType.Int).Value = manifestId;
                db.command.Parameters.Add("@pTotalActualWeight", SqlDbType.Float).Value = totalActualWeight;
                db.command.Parameters.Add("@pTotalArticle", SqlDbType.Int).Value = totalArticle;
                db.command.Parameters.Add("@pType", SqlDbType.Char).Value = type;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();


            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }

        internal static void DeleteVehicleHireManifest(int loginID, int manifestID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteVehicleHireManifest";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ManifestID", SqlDbType.Int).Value = manifestID;
                //db.command.Parameters.Add("@DeletedBy", SqlDbType.Int).Value = loginID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static List<VehicleHireManifest> GetVehicleHireManifestByManifestID(int manifestID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetVehicleHireManifestByManifestID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@ManifestID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = manifestID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<VehicleHireManifest> list = new List<VehicleHireManifest>();
                return list = DatatableHelper.ToList<VehicleHireManifest>(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                // LogHelper.logException(ex, "GetConsignmentBookings", "");
                throw ex;
            }
        }

        #endregion

        #region MASTERS
        internal static object GetVehicleHirePaymentTypes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<VehicleHirePaymentTypeMaster> list = new List<VehicleHirePaymentTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetVehicleHirePaymentTypes");
                return list = DatatableHelper.ToList<VehicleHirePaymentTypeMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetTruckBookingTypes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<TruckBookingTypeMaster> list = new List<TruckBookingTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetTruckBookingTypes");
                return list = DatatableHelper.ToList<TruckBookingTypeMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetDeliveryStatusMaster()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<DeliveryStatusMaster> list = new List<DeliveryStatusMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetDeliveryStatusMaster");
                return list = DatatableHelper.ToList<DeliveryStatusMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        internal static object GetVendorTypes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<VendorTypeMaster> list = new List<VendorTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetVendorTypes");
                return list = DatatableHelper.ToList<VendorTypeMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        #endregion

        #region manifest consignmets
        internal static void AddManifestConsignment(int loginID, List<ManifestConsignmentMapping> ManifestConsignList)
        {
            string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = connectionStr;
            connection.Open();

            using (SqlTransaction trans = connection.BeginTransaction())
            {
                try
                {

                    SqlCommand delCmd = new SqlCommand();
                    delCmd.CommandText = "DeleteManifestConsignments";
                    delCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter par = new SqlParameter();
                    delCmd.Parameters.Add("@ManifestID", SqlDbType.Int).Value = ManifestConsignList[0].ManifestID;
                    delCmd.Connection = connection;
                    delCmd.Transaction = trans;
                    delCmd.ExecuteNonQuery();

                    foreach (ManifestConsignmentMapping newManifestConsign in ManifestConsignList)
                    {

                        newManifestConsign.CreatedBy = loginID;
                        SqlCommand addCmd = new SqlCommand();
                        addCmd.CommandText = "AddManifestConsignments";
                        addCmd.CommandType = CommandType.StoredProcedure;
                        addCmd.Parameters.Add("@ManifestID", SqlDbType.Int).Value = newManifestConsign.ManifestID;
                        addCmd.Parameters.Add("@ConsignmentID", SqlDbType.Int).Value = newManifestConsign.ConsignmentID;
                        addCmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = newManifestConsign.CreatedBy;
                        addCmd.Connection = connection;
                        addCmd.Transaction = trans;
                        addCmd.ExecuteNonQuery();
                    }

                    trans.Commit();
                    connection.Close();

                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    if (connection.State == ConnectionState.Open)
                        connection.Close();

                    throw new Exception(ex.Message);
                }
            }


        }



        #endregion

        #region Manifest Delivery
        internal static DataTable GetManifestConsignmentsForDelivery(int branchID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetManifestConsignmentsForDelivery";


                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID ";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;


                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static void UpdateManifestConsignmentDeliveryStatus(int loginID, ManifestConsignmentMapping manifestConsignment)
        {
            string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = connectionStr;


            try
            {
                SqlCommand updateCmd = new SqlCommand();
                updateCmd.CommandText = "UpdateManifestConsignmentDeliveryStatus";
                updateCmd.CommandType = CommandType.StoredProcedure;

                updateCmd.Parameters.Add("@ManifestConsignmentID", SqlDbType.Int).Value = manifestConsignment.ManifestConsignmentID;
                updateCmd.Parameters.Add("@loginID", SqlDbType.Int).Value = loginID;
                updateCmd.Parameters.Add("@ManifestID", SqlDbType.Int).Value = manifestConsignment.ManifestID;
                updateCmd.Parameters.Add("@DeliveryStatus", SqlDbType.Int).Value = manifestConsignment.DeliveryStatus;
                DateTime timespan = DateTime.Parse(Convert.ToString(manifestConsignment.DeliveryTime));
                TimeSpan timeDelivery = timespan.TimeOfDay;
                DateTime? deliverydate = null;
                if (!String.IsNullOrEmpty(manifestConsignment.DeliveryDate))
                {
                    deliverydate = DateTime.ParseExact(manifestConsignment.DeliveryDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                }
                updateCmd.Parameters.Add("@DeliveryDate", SqlDbType.DateTime).Value = deliverydate;
                updateCmd.Parameters.Add("@DeliveredArticles", SqlDbType.Int).Value = manifestConsignment.DeliveredArticles;
                updateCmd.Parameters.Add("@VehicleNo", SqlDbType.NVarChar).Value = manifestConsignment.VehicleNo;
                updateCmd.Parameters.Add("@DeliveryRemarks", SqlDbType.VarChar).Value = manifestConsignment.DeliveryRemarks;
                updateCmd.Parameters.Add("@DeliveryTime", SqlDbType.Time).Value = timeDelivery;
                updateCmd.Connection = connection;
                connection.Open();
                updateCmd.ExecuteNonQuery();



                connection.Close();

            }///end try
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();

                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateManifestConsignmentFile(long consignmentId, string filepath)
        {
            string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = connectionStr;


            try
            {
                SqlCommand updateCmd = new SqlCommand();
                updateCmd.CommandText = "UpdateManifestConsignmentFilePath";
                updateCmd.CommandType = CommandType.StoredProcedure;

                updateCmd.Parameters.Add("@ManifestConsignmentID", SqlDbType.Int).Value = consignmentId;
                updateCmd.Parameters.Add("@FilePath", SqlDbType.VarChar).Value = filepath;

                updateCmd.Connection = connection;
                connection.Open();
                updateCmd.ExecuteNonQuery();



                connection.Close();

            }///end try
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();

                throw new Exception(ex.Message);
            }


        }

        #endregion

        #region Manifest Unload
        internal static DataTable GetManifestsForUnloadByToBranchID(int branchID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetManifestsForUnloadByToBranchID";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@To_LocationID ";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        internal static object GetConsignmentReceivedStatus()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<ConsignmentReceivedStatu> list = new List<ConsignmentReceivedStatu>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetConsignmentReceivedStatus");
                return list = DatatableHelper.ToList<ConsignmentReceivedStatu>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }


        internal static DataTable GetUnloadManifestConsignments(int manifestID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetUnloadManifestConsignments";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@ManifestID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = manifestID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static DataTable GetUnloadManifestConsignmentsByVehicleID(int vehicleID, int BranchID, DateTime Created)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetUnloadManifestConsignmentsByVehicleID";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@VehicleID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = vehicleID;

                cmd.Parameters.Add(paramBrnch);
                SqlParameter paramBrnch1 = new SqlParameter();
                paramBrnch1.ParameterName = "@BranchID";
                paramBrnch1.Direction = ParameterDirection.Input;
                paramBrnch1.DbType = DbType.Int32;
                paramBrnch1.Value = BranchID;

                cmd.Parameters.Add(paramBrnch1);
                SqlParameter paramBrnch2 = new SqlParameter();
                paramBrnch2.ParameterName = "@Created";
                paramBrnch2.Direction = ParameterDirection.Input;
                paramBrnch2.DbType = DbType.DateTime;
                paramBrnch2.Value = Created;

                cmd.Parameters.Add(paramBrnch2);
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static void UpdateManifestConsignmentStatus(int loginID, List<ManifestConsignmentMapping> manifestConsignmentList)
        {
            string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = connectionStr;
            connection.Open();

            using (SqlTransaction trans = connection.BeginTransaction())
            {
                var manifestIDs = manifestConsignmentList.Select(m => m.ManifestID).Distinct();
                try
                {
                    foreach (int manifestID in manifestIDs)
                    {

                        SqlCommand mfCmd = new SqlCommand();
                        mfCmd.CommandText = "UnloadVehicleHireManifest";
                        mfCmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter par = new SqlParameter();

                        string time = DateTime.Now.ToString("hh:mm:ss tt");
                        mfCmd.Parameters.Add("@pArrivalTime", SqlDbType.NVarChar).Value = time;
                        mfCmd.Parameters.Add("@pArrivalDate", SqlDbType.DateTime).Value = DateTime.Now;
                        mfCmd.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = loginID;
                        mfCmd.Parameters.Add("@ManifestID", SqlDbType.Int).Value = manifestID;
                        mfCmd.Connection = connection;
                        mfCmd.Transaction = trans;
                        mfCmd.ExecuteNonQuery();
                    }
                    foreach (ManifestConsignmentMapping consignmentMapping in manifestConsignmentList)
                    {
                        SqlCommand updateCmd = new SqlCommand();
                        updateCmd.CommandText = "UpdateManifestConsignmentStatus";
                        updateCmd.CommandType = CommandType.StoredProcedure;

                        updateCmd.Parameters.Add("@ManifestConsignmentID", SqlDbType.Int).Value = consignmentMapping.ManifestConsignmentID;
                        updateCmd.Parameters.Add("@ReceivedArticles", SqlDbType.Int).Value = consignmentMapping.ReceivedArticles;
                        updateCmd.Parameters.Add("@ReceivedStatus", SqlDbType.VarChar).Value = consignmentMapping.ReceivedStatus;
                        updateCmd.Parameters.Add("@Remarks", SqlDbType.VarChar).Value = consignmentMapping.Remarks;
                        updateCmd.Connection = connection;
                        updateCmd.Transaction = trans;
                        updateCmd.ExecuteNonQuery();


                    }
                    trans.Commit();
                    connection.Close();

                }///end try
                catch (Exception ex)
                {
                    trans.Rollback();
                    if (connection.State == ConnectionState.Open)
                        connection.Close();

                    throw new Exception(ex.Message);
                }

            }
        }
        #endregion
        #region Manifest Report
        internal static DataTable GetManifestsForReport(System.Nullable<int> frombranchId, System.Nullable<int> tobranchID, System.Nullable<DateTime> createdDate)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetManifestsForReport";
                SqlParameter paramFromBrch = new SqlParameter();
                paramFromBrch.ParameterName = "@From_LocationID ";
                paramFromBrch.Direction = ParameterDirection.Input;
                paramFromBrch.DbType = DbType.Int32;
                paramFromBrch.Value = frombranchId;

                SqlParameter paramToBrnch = new SqlParameter();
                paramToBrnch.ParameterName = "@To_LocationID";
                paramToBrnch.Direction = ParameterDirection.Input;
                paramToBrnch.DbType = DbType.Int32;
                paramToBrnch.Value = tobranchID;

                SqlParameter paramCreatedDt = new SqlParameter();
                paramCreatedDt.ParameterName = "@Created";
                paramCreatedDt.Direction = ParameterDirection.Input;
                paramCreatedDt.DbType = DbType.DateTime;
                paramCreatedDt.Value = createdDate;

                cmd.Parameters.Add(paramFromBrch);
                cmd.Parameters.Add(paramToBrnch);
                cmd.Parameters.Add(paramCreatedDt);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static DataTable GetManifestReport(int manifestID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetManifestsReport";
                SqlParameter paramMFID = new SqlParameter();
                paramMFID.ParameterName = "@ManifestID ";
                paramMFID.Direction = ParameterDirection.Input;
                paramMFID.DbType = DbType.Int32;
                paramMFID.Value = manifestID;


                cmd.Parameters.Add(paramMFID);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static DataTable GetManifestConsignmentsForReports(int manifestID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetManifestConsignmentsForReports";
                SqlParameter paramMFID = new SqlParameter();
                paramMFID.ParameterName = "@ManifestID ";
                paramMFID.Direction = ParameterDirection.Input;
                paramMFID.DbType = DbType.Int32;
                paramMFID.Value = manifestID;


                cmd.Parameters.Add(paramMFID);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Reports
        internal static Dictionary<int, List<DeliveryStockModel>> GetDeliveryStockReportIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandText = "GetDeliveryStockReport";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();

                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = fromDate;
                db.command.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = toDate;
                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<DeliveryStockModel>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<DeliveryStockModel>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }
        internal static DataTable GetDeliveryStockReport(int? branchId, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetDeliveryStockReport";
                cmd.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                SqlParameter paramFromDate = new SqlParameter();
                paramFromDate.ParameterName = "@FromDate";
                paramFromDate.Direction = ParameterDirection.Input;
                paramFromDate.DbType = DbType.DateTime;
                paramFromDate.Value = fromDate;
                cmd.Parameters.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter();
                paramToDate.ParameterName = "@ToDate";
                paramToDate.Direction = ParameterDirection.Input;
                paramToDate.DbType = DbType.DateTime;
                paramToDate.Value = toDate;
                cmd.Parameters.Add(paramToDate);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static Dictionary<int, List<PendingStockModel>> GetPendingStockReportIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandTimeout = 0;
                db.command.CommandText = "GetPendingStockReport";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = fromDate;
                db.command.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = toDate;
                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<PendingStockModel>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<PendingStockModel>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }
        internal static DataTable GetPendingStockReport(int? branchId, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetPendingStockReport";
                cmd.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                SqlParameter paramFromDate = new SqlParameter();
                paramFromDate.ParameterName = "@FromDate";
                paramFromDate.Direction = ParameterDirection.Input;
                paramFromDate.DbType = DbType.DateTime;
                paramFromDate.Value = fromDate;
                cmd.Parameters.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter();
                paramToDate.ParameterName = "@ToDate";
                paramToDate.Direction = ParameterDirection.Input;
                paramToDate.DbType = DbType.DateTime;
                paramToDate.Value = toDate;
                cmd.Parameters.Add(paramToDate);


                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static Dictionary<int, List<DamageStockModel>> GetDamageStockReportIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandText = "GetDamageStockReport";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = fromDate;
                db.command.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = toDate;
                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<DamageStockModel>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<DamageStockModel>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }

        internal static DataTable GetDamageStockReport(int? branchId, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetDamageStockReport";
                cmd.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                SqlParameter paramFromDate = new SqlParameter();
                paramFromDate.ParameterName = "@FromDate";
                paramFromDate.Direction = ParameterDirection.Input;
                paramFromDate.DbType = DbType.DateTime;
                paramFromDate.Value = fromDate;
                cmd.Parameters.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter();
                paramToDate.ParameterName = "@ToDate";
                paramToDate.Direction = ParameterDirection.Input;
                paramToDate.DbType = DbType.DateTime;
                paramToDate.Value = toDate;
                cmd.Parameters.Add(paramToDate);


                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static Dictionary<int, List<VehicleHirePaymentModel>> GetVehicleHirePaymentReportIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandText = "GetVehicleHirePaymentReport";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
                db.command.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;
                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<VehicleHirePaymentModel>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<VehicleHirePaymentModel>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }

        internal static DataTable GetVehicleHirePaymentReport(int? branchId, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetVehicleHirePaymentReport";
                cmd.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                SqlParameter paramFromDate = new SqlParameter();
                paramFromDate.ParameterName = "@FromDate";
                paramFromDate.Direction = ParameterDirection.Input;
                paramFromDate.DbType = DbType.DateTime;
                paramFromDate.Value = fromDate;
                cmd.Parameters.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter();
                paramToDate.ParameterName = "@ToDate";
                paramToDate.Direction = ParameterDirection.Input;
                paramToDate.DbType = DbType.DateTime;
                paramToDate.Value = toDate;
                cmd.Parameters.Add(paramToDate);


                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static DataTable GetConsignmentBookingReport(int? branchId, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentBookingReport";
                cmd.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                SqlParameter paramFromDate = new SqlParameter();
                paramFromDate.ParameterName = "@FromDate";
                paramFromDate.Direction = ParameterDirection.Input;
                paramFromDate.DbType = DbType.DateTime;
                paramFromDate.Value = fromDate;
                cmd.Parameters.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter();
                paramToDate.ParameterName = "@ToDate";
                paramToDate.Direction = ParameterDirection.Input;
                paramToDate.DbType = DbType.DateTime;
                paramToDate.Value = toDate;
                cmd.Parameters.Add(paramToDate);

                cmd.CommandTimeout = 0;
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static Dictionary<int, List<ConsignmentBookingReportModel>> GetConsignmentBookingReportIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandText = "GetConsignmentBookingReport";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
                db.command.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;
                db.command.CommandTimeout = 0;
                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<ConsignmentBookingReportModel>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<ConsignmentBookingReportModel>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }

        internal static DataTable GetManifestReport(long branchId, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetManifestForReport";
                cmd.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                SqlParameter paramFromDate = new SqlParameter();
                paramFromDate.ParameterName = "@FromDate";
                paramFromDate.Direction = ParameterDirection.Input;
                paramFromDate.DbType = DbType.DateTime;
                paramFromDate.Value = fromDate;
                cmd.Parameters.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter();
                paramToDate.ParameterName = "@ToDate";
                paramToDate.Direction = ParameterDirection.Input;
                paramToDate.DbType = DbType.DateTime;
                paramToDate.Value = toDate;
                cmd.Parameters.Add(paramToDate);


                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static Dictionary<int, List<ManifestReportModel>> GetManifestReportIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, System.Nullable<DateTime> created, string from, string to, string type)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandText = "GetManifestForReport";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@Created", SqlDbType.DateTime).Value = created;
                int? fromBranch = null; int? toBranch = null;
                if (!string.IsNullOrEmpty(from))
                    fromBranch = Convert.ToInt32(from);
                if (!string.IsNullOrEmpty(to))
                    toBranch = Convert.ToInt32(to);
                db.command.Parameters.Add("@From_LocationID", SqlDbType.Int).Value = fromBranch;
                db.command.Parameters.Add("@To_LocationID", SqlDbType.Int).Value = toBranch;
                db.command.Parameters.Add("@ManifestType", SqlDbType.VarChar).Value = type;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<ManifestReportModel>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<ManifestReportModel>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }
        internal static List<ManifestDetailReportModel> ManifestDetailReportModel(int ManifestId, string Type)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandText = "GetManifestDetailReport";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ManifestId", SqlDbType.Int).Value = ManifestId;
                db.command.Parameters.Add("@ManifestType", SqlDbType.VarChar).Value = Type;
                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<ManifestDetailReportModel>(ds.Tables[0]);
                return list;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static Dictionary<int, List<DeliveryBranchModel>> GetDeliveryBranchReportIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandText = "GetDeliveryBranchReport";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();

                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = fromDate;
                db.command.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = toDate;
                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<DeliveryBranchModel>(ds.Tables[0]);
                int count = 1;
                var data = new Dictionary<int, List<DeliveryBranchModel>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }
        internal static DataTable GetManifestsByVehicleID(int VehicleID, int BranchID, DateTime Created)
        {
            ManifestModels objManifest = new ManifestModels();
            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetManifestsByVehicleID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@pVehicleID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.String;
                paramCnB.Value = VehicleID;
                cmd.Parameters.Add(paramCnB);
                SqlParameter paramCnA = new SqlParameter();
                paramCnA.ParameterName = "@pBranchID";
                paramCnA.Direction = ParameterDirection.Input;
                paramCnA.DbType = DbType.Int32;
                paramCnA.Value = BranchID;
                cmd.Parameters.Add(paramCnA);
                SqlParameter paramCn = new SqlParameter();
                paramCn.ParameterName = "@pCreated";
                paramCn.Direction = ParameterDirection.Input;
                paramCn.DbType = DbType.DateTime;
                paramCn.Value = Created;
                cmd.Parameters.Add(paramCn);
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        internal static DataTable GetLRExperienceReport(int? branchId, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetLRWiseExperienceData";
                cmd.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                SqlParameter paramFromDate = new SqlParameter();
                paramFromDate.ParameterName = "@FromDate";
                paramFromDate.Direction = ParameterDirection.Input;
                paramFromDate.DbType = DbType.DateTime;
                paramFromDate.Value = fromDate;
                cmd.Parameters.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter();
                paramToDate.ParameterName = "@ToDate";
                paramToDate.Direction = ParameterDirection.Input;
                paramToDate.DbType = DbType.DateTime;
                paramToDate.Value = toDate;
                cmd.Parameters.Add(paramToDate);
                cmd.CommandTimeout = 0;
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static Dictionary<int, List<LRExperienceReportModel>> GetLRExperienceReportIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandText = "GetLRWiseExperienceData";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
                db.command.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;

                db.command.CommandTimeout = 0;
                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<LRExperienceReportModel>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<LRExperienceReportModel>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }


        internal static DataTable GetVendorVehicleReport(int? branchId, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate, int VendorId)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetVendorVehicleData";
                cmd.Parameters.Add("@pBranchID", SqlDbType.Int).Value = branchId;
                cmd.Parameters.Add("@pVehicleVendorID", SqlDbType.Int).Value = VendorId;
                SqlParameter paramFromDate = new SqlParameter();
                paramFromDate.ParameterName = "@FromDate";
                paramFromDate.Direction = ParameterDirection.Input;
                paramFromDate.DbType = DbType.DateTime;
                paramFromDate.Value = fromDate;
                cmd.Parameters.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter();
                paramToDate.ParameterName = "@ToDate";
                paramToDate.Direction = ParameterDirection.Input;
                paramToDate.DbType = DbType.DateTime;
                paramToDate.Value = toDate;
                cmd.Parameters.Add(paramToDate);
                cmd.CommandTimeout = 0;
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static Dictionary<int, List<VendorVehicleModel>> GetVendorVehicleReportIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate, int vendorId)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandText = "GetVendorVehicleData";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchID", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pVehicleVendorID", SqlDbType.Int).Value = vendorId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
                db.command.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;

                db.command.CommandTimeout = 0;
                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<VendorVehicleModel>(ds.Tables[0]);
                int count = list.Count;
                var data = new Dictionary<int, List<VendorVehicleModel>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }

        internal static DataTable GetOtherVendorExpenseReport(int? branchId, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate, int VendorId)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetOtherVendorReport";
                cmd.Parameters.Add("@pBranchID", SqlDbType.Int).Value = branchId;
                cmd.Parameters.Add("@pOtherVendorID", SqlDbType.Int).Value = VendorId;
                SqlParameter paramFromDate = new SqlParameter();
                paramFromDate.ParameterName = "@FromDate";
                paramFromDate.Direction = ParameterDirection.Input;
                paramFromDate.DbType = DbType.DateTime;
                paramFromDate.Value = fromDate;
                cmd.Parameters.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter();
                paramToDate.ParameterName = "@ToDate";
                paramToDate.Direction = ParameterDirection.Input;
                paramToDate.DbType = DbType.DateTime;
                paramToDate.Value = toDate;
                cmd.Parameters.Add(paramToDate);
                cmd.CommandTimeout = 0;
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static Dictionary<int, List<OtherVendorReportModel>> GetOtherVendorReportIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate, int vendorId)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandText = "GetOtherVendorReport";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchID", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pOtherVendorID", SqlDbType.Int).Value = vendorId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
                db.command.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;

                db.command.CommandTimeout = 0;
                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<OtherVendorReportModel>(ds.Tables[0]);
                int count = list.Count;
                var data = new Dictionary<int, List<OtherVendorReportModel>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }



        #endregion

        internal static void UpdateManifestDeliveryStatus(int ManifestId, int Status)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateManifestDeliveryStatus";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                var deliverydate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
               
                TimeSpan timeDelivery = deliverydate.TimeOfDay;
                db.command.Parameters.Add("@pManifestID", SqlDbType.Int).Value = ManifestId;
                db.command.Parameters.Add("@pStatus", SqlDbType.Int).Value = Status;
                db.command.Parameters.Add("@pDeliveryDate", SqlDbType.DateTime).Value = deliverydate;
                db.command.Parameters.Add("@pDeliveryTime", SqlDbType.Time).Value = timeDelivery;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
               

            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
    }
}