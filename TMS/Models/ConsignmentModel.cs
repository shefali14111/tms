﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TMS.Common;
using TMS.EFModel;
using TMS.Helper;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Globalization;
using TMS.CustomModels;

namespace TMS.Models
{
    public static class ConsignmentModel
    {
        #region consignment booking
        internal static DataTable GetLocalVehiclesHired(int branchID)
        {
            try
            {
               
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetLocalVehiclesHired";
                cmd.Parameters.Add("@pCurrentDate", SqlDbType.DateTime).Value = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dsVehicles = new DataSet();
                da.Fill(dsVehicles, "table1");
                connection.Close();
                return dsVehicles.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        internal static DataTable GetConsignmentBookings(int branchID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentBookings";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        internal static int AddConsignmentBooking(int loginID, ConsignmentBooking newBooking)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newBooking.CreatedBy = loginID;
                newBooking.Created = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

                newBooking.ModifiedBy = loginID;
                newBooking.Modified = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                newBooking.ConsignmentNo = "";//AUTO GENERATED IN STORED PROC
                if (newBooking.ConsigneeID == null) newBooking.ConsigneeID = 0;
                if (newBooking.ConsignorID == null) newBooking.ConsignorID = 0;
                if (newBooking.ConsigneeAddress == null) newBooking.ConsigneeAddress = "";
                if (newBooking.ConsignorAddress == null) newBooking.ConsignorAddress = "";
                // if (newBooking.BillingPartyID == null) newBooking.BillingPartyID = 0;

                db.command.CommandText = "AddConsignmentBooking";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pConsignmentNo", SqlDbType.Char).Value = newBooking.ConsignmentNo;
                db.command.Parameters.Add("@pConsigneeName", SqlDbType.VarChar).Value = newBooking.ConsigneeName;
                db.command.Parameters.Add("@pConsigneeAddress", SqlDbType.VarChar).Value = newBooking.ConsigneeAddress;
                db.command.Parameters.Add("@pConsigneeID", SqlDbType.Int).Value = newBooking.ConsigneeID;


                db.command.Parameters.Add("@pConsignorName", SqlDbType.VarChar).Value = newBooking.ConsignorName;
                db.command.Parameters.Add("@pConsignorAddress", SqlDbType.VarChar).Value = newBooking.ConsignorAddress;

                db.command.Parameters.Add("@pCompanyID", SqlDbType.Int).Value = newBooking.CompanyID;
                db.command.Parameters.Add("@pBranchID", SqlDbType.Int).Value = newBooking.BranchID;
                db.command.Parameters.Add("@pZoneID", SqlDbType.Int).Value = newBooking.ZoneID;


                db.command.Parameters.Add("@pConsignorID", SqlDbType.Int).Value = newBooking.ConsignorID;

                db.command.Parameters.Add("@pConsignmentDate", SqlDbType.Date).Value = newBooking.ConsignmentDate;
                db.command.Parameters.Add("@pPaymentTypeID", SqlDbType.Int).Value = newBooking.PaymentTypeID;

                db.command.Parameters.Add("@pConsignmentFromID", SqlDbType.Int).Value = newBooking.ConsignmentFromID;
                db.command.Parameters.Add("@pConsignmentToID", SqlDbType.Int).Value = newBooking.ConsignmentToID;
                db.command.Parameters.Add("@pDeliveryTypeID", SqlDbType.Int).Value = newBooking.DeliveryTypeID;
                db.command.Parameters.Add("@pBillingPartyID", SqlDbType.Int).Value = newBooking.BillingPartyID;
                db.command.Parameters.Add("@pTransportTypeID", SqlDbType.Int).Value = newBooking.TransportTypeID;
                db.command.Parameters.Add("@pServiceTaxId", SqlDbType.Int).Value = newBooking.ServiceTaxId;
                db.command.Parameters.Add("@pDoorCollectionID", SqlDbType.Int).Value = newBooking.DoorCollectionID;
                db.command.Parameters.Add("@pTransportModeID", SqlDbType.Int).Value = newBooking.TransportModeID;
                db.command.Parameters.Add("@pCargotTypeID", SqlDbType.Int).Value = newBooking.CargotTypeID;
                db.command.Parameters.Add("@pCreatedBy", SqlDbType.Int).Value = newBooking.CreatedBy;
                db.command.Parameters.Add("@pModifiedBy", SqlDbType.Int).Value = newBooking.ModifiedBy;
                db.command.Parameters.Add("@pVehicleID", SqlDbType.Int).Value = newBooking.VehicleID;
                db.command.Parameters.Add("@pMobileNumber", SqlDbType.NVarChar).Value = newBooking.MobileNumber;
                db.command.Parameters.Add("@pContactPersonName", SqlDbType.VarChar).Value = newBooking.ContactPersonName;
                DateTime CurrentDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                TimeSpan timeConsignment = CurrentDate.TimeOfDay;
                db.command.Parameters.Add("@pConsignmentTime", SqlDbType.Time).Value = timeConsignment;
                db.command.Parameters.Add("@pDestinationId", SqlDbType.Int).Value = newBooking.DestinationID;
                db.command.Parameters.Add("@pPinCodeId", SqlDbType.Int).Value = newBooking.PinCodeID;
                db.command.Parameters.Add("@pODAId", SqlDbType.Int).Value = newBooking.ODAID;
                db.command.Parameters.Add("@pConsignorGSTNo", SqlDbType.VarChar).Value = newBooking.ConsignorGSTNo;
                db.command.Parameters.Add("@pConsigneeGSTNo", SqlDbType.VarChar).Value = newBooking.ConsigneeGSTNo;

                SqlParameter paramConsID = new SqlParameter();
                paramConsID.ParameterName = "@pConsignmentID";
                paramConsID.Direction = ParameterDirection.Output;
                paramConsID.DbType = DbType.Int32;
                db.command.Parameters.Add(paramConsID);
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int consignmentID = Convert.ToInt32(paramConsID.Value);
                return consignmentID;

            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static int AddConsignmentBookingWithoutConsignmentNo(int loginID, ConsignmentBooking newBooking)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newBooking.CreatedBy = loginID;
                newBooking.Created = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

                newBooking.ModifiedBy = loginID;
                newBooking.Modified = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                newBooking.ConsignmentNo = "";//AUTO GENERATED IN STORED PROC
                if (newBooking.ConsigneeID == null) newBooking.ConsigneeID = 0;
                if (newBooking.ConsignorID == null) newBooking.ConsignorID = 0;
                if (newBooking.ConsigneeAddress == null) newBooking.ConsigneeAddress = "";
                if (newBooking.ConsignorAddress == null) newBooking.ConsignorAddress = "";

                db.command.CommandText = "AddConsignmentBookingWithoutConsignmentNo";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchReservedID", SqlDbType.Int).Value = newBooking.BranchReservedID;
                db.command.Parameters.Add("@pConsigneeName", SqlDbType.VarChar).Value = newBooking.ConsigneeName;
                db.command.Parameters.Add("@pConsigneeAddress", SqlDbType.VarChar).Value = newBooking.ConsigneeAddress;
                db.command.Parameters.Add("@pConsigneeID", SqlDbType.Int).Value = newBooking.ConsigneeID;

                db.command.Parameters.Add("@pConsignorName", SqlDbType.VarChar).Value = newBooking.ConsignorName;
                db.command.Parameters.Add("@pConsignorAddress", SqlDbType.VarChar).Value = newBooking.ConsignorAddress;

                db.command.Parameters.Add("@pCompanyID", SqlDbType.Int).Value = newBooking.CompanyID;
                db.command.Parameters.Add("@pBranchID", SqlDbType.Int).Value = newBooking.BranchID;
                db.command.Parameters.Add("@pZoneID", SqlDbType.Int).Value = newBooking.ZoneID;

                db.command.Parameters.Add("@pConsignorID", SqlDbType.Int).Value = newBooking.ConsignorID;

                db.command.Parameters.Add("@pConsignmentDate", SqlDbType.Date).Value = newBooking.ConsignmentDate;
                db.command.Parameters.Add("@pPaymentTypeID", SqlDbType.Int).Value = newBooking.PaymentTypeID;

                db.command.Parameters.Add("@pConsignmentFromID", SqlDbType.Int).Value = newBooking.ConsignmentFromID;
                db.command.Parameters.Add("@pConsignmentToID", SqlDbType.Int).Value = newBooking.ConsignmentToID;
                db.command.Parameters.Add("@pDeliveryTypeID", SqlDbType.Int).Value = newBooking.DeliveryTypeID;
                db.command.Parameters.Add("@pBillingPartyID", SqlDbType.Int).Value = newBooking.BillingPartyID;
                db.command.Parameters.Add("@pTransportTypeID", SqlDbType.Int).Value = newBooking.TransportTypeID;
                db.command.Parameters.Add("@pServiceTaxId", SqlDbType.Int).Value = newBooking.ServiceTaxId;
                db.command.Parameters.Add("@pDoorCollectionID", SqlDbType.Int).Value = newBooking.DoorCollectionID;
                db.command.Parameters.Add("@pTransportModeID", SqlDbType.Int).Value = newBooking.TransportModeID;
                db.command.Parameters.Add("@pCargotTypeID", SqlDbType.Int).Value = newBooking.CargotTypeID;
                db.command.Parameters.Add("@pCreatedBy", SqlDbType.Int).Value = newBooking.CreatedBy;
                db.command.Parameters.Add("@pModifiedBy", SqlDbType.Int).Value = newBooking.ModifiedBy;
                db.command.Parameters.Add("@pVehicleID", SqlDbType.Int).Value = newBooking.VehicleID;
                db.command.Parameters.Add("@pMobileNumber", SqlDbType.Int).Value = newBooking.MobileNumber;
                db.command.Parameters.Add("@pContactPersonName", SqlDbType.VarChar).Value = newBooking.ContactPersonName;
                DateTime CurrentDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                TimeSpan timeConsignment = CurrentDate.TimeOfDay;
                db.command.Parameters.Add("@pConsignmentTime", SqlDbType.Time).Value = timeConsignment;
                db.command.Parameters.Add("@pDestinationId", SqlDbType.Int).Value = newBooking.DestinationID;
                db.command.Parameters.Add("@pPinCodeId", SqlDbType.Int).Value = newBooking.PinCodeID;
                db.command.Parameters.Add("@pODAId", SqlDbType.Int).Value = newBooking.ODAID;
                db.command.Parameters.Add("@pConsignorGSTNo", SqlDbType.VarChar).Value = newBooking.ConsignorGSTNo;
                db.command.Parameters.Add("@pConsigneeGSTNo", SqlDbType.VarChar).Value = newBooking.ConsigneeGSTNo;

                SqlParameter paramConsID = new SqlParameter();
                paramConsID.ParameterName = "@pConsignmentID";
                paramConsID.Direction = ParameterDirection.Output;
                paramConsID.DbType = DbType.Int32;
                db.command.Parameters.Add(paramConsID);
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int consignmentID = Convert.ToInt32(paramConsID.Value);
                return consignmentID;

            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static string ReserveConsignmentNo(int branchId)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "ReserveConsignmentNo";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pLRNo", SqlDbType.VarChar, 30).Direction = ParameterDirection.Output;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                string lrNo = Convert.ToString(db.command.Parameters["@pLRNo"].Value);
                return lrNo;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }

        internal static bool CheckConsignmentDeliveryStatus(int branchId)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetConsignmentDeliveryStatus";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
               
                int count = Convert.ToInt16(ds.Tables[0].Rows[0][0]);
                if (count > 0)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static Dictionary<int, List<ReservedConsignment>> GetReservedConsignmentIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetReservedConsignmentIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<ReservedConsignment>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<ReservedConsignment>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateConsignmentBooking(int loginID, ConsignmentBooking newBooking)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newBooking.CreatedBy = loginID;
                newBooking.Created = System.DateTime.Now;

                newBooking.ModifiedBy = loginID;
                newBooking.Modified = System.DateTime.Now;

                db.command.CommandText = "UpdateConsignmentBooking";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBillingPartyID", SqlDbType.Int).Value = newBooking.BillingPartyID;
                db.command.Parameters.Add("@pConsignmentFromID", SqlDbType.Int).Value = newBooking.ConsignmentFromID;
                db.command.Parameters.Add("@pConsignmentToID", SqlDbType.Int).Value = newBooking.ConsignmentToID;
                db.command.Parameters.Add("@pDestinationId", SqlDbType.Int).Value = newBooking.DestinationID;
                db.command.Parameters.Add("@pDeliveryTypeID", SqlDbType.Int).Value = newBooking.DeliveryTypeID;
                db.command.Parameters.Add("@pPaymentTypeID", SqlDbType.Int).Value = newBooking.PaymentTypeID;
                db.command.Parameters.Add("@pTransportTypeID", SqlDbType.Int).Value = newBooking.TransportTypeID;
                db.command.Parameters.Add("@pServiceTaxId", SqlDbType.Int).Value = newBooking.ServiceTaxId;
                db.command.Parameters.Add("@pCargotTypeID", SqlDbType.Int).Value = newBooking.CargotTypeID;
                db.command.Parameters.Add("@pTransportModeID", SqlDbType.Int).Value = newBooking.TransportModeID;
                db.command.Parameters.Add("@pDoorCollectionID", SqlDbType.Int).Value = newBooking.DoorCollectionID;
                db.command.Parameters.Add("@pVehicleID", SqlDbType.Int).Value = newBooking.VehicleID;
                db.command.Parameters.Add("@pPinCodeID", SqlDbType.Int).Value = newBooking.PinCodeID;
                db.command.Parameters.Add("@pODAID", SqlDbType.Int).Value = newBooking.ODAID;
                db.command.Parameters.Add("@pConsignorID", SqlDbType.Int).Value = newBooking.ConsignorID;
                db.command.Parameters.Add("@pConsignorName", SqlDbType.VarChar).Value = newBooking.ConsignorName;
                db.command.Parameters.Add("@pConsignorAddress", SqlDbType.VarChar).Value = newBooking.ConsignorAddress;
                db.command.Parameters.Add("@pConsignorGSTNo", SqlDbType.VarChar).Value = newBooking.ConsignorGSTNo;
                db.command.Parameters.Add("@pConsigneeID", SqlDbType.Int).Value = newBooking.ConsigneeID;
                db.command.Parameters.Add("@pConsigneeName", SqlDbType.VarChar).Value = newBooking.ConsigneeName;
                db.command.Parameters.Add("@pConsigneeAddress", SqlDbType.VarChar).Value = newBooking.ConsigneeAddress;
                db.command.Parameters.Add("@pConsigneeGSTNo", SqlDbType.VarChar).Value = newBooking.ConsigneeGSTNo;
                db.command.Parameters.Add("@pMobileNumber", SqlDbType.NVarChar).Value = newBooking.MobileNumber;
                db.command.Parameters.Add("@pContactPersonName", SqlDbType.VarChar).Value = newBooking.ContactPersonName;
                db.command.Parameters.Add("@pconsignmentID", SqlDbType.Int).Value = newBooking.ConsignmentID;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }


        internal static void DeleteConsignmentBooking(int loginID, int consignmentID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteConsignmentBooking";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ConsignmentID", SqlDbType.Int).Value = consignmentID;
                db.command.Parameters.Add("@DeletedBy", SqlDbType.Int).Value = loginID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static List<ConsignmentBooking> GetConsignmentBookingByID(int consignmentID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentBookingByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@ConsignmentID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = consignmentID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<ConsignmentBooking> list = new List<ConsignmentBooking>();
                var consignmentTime = Convert.ToString(ds.Tables[0].Rows[0]["ConsignmentTime"]);
                string formattedTime = "";
                if (consignmentTime != "")
                {
                    TimeSpan tm = TimeSpan.Parse(consignmentTime);
                    var dateTime = new DateTime(tm.Ticks);
                    formattedTime = dateTime.ToString("h:mm:ss tt", CultureInfo.InvariantCulture);
                }
                var listBooking = DatatableHelper.ToList<ConsignmentBooking>(ds.Tables[0]);
                listBooking.FirstOrDefault().ConsignmentTime = formattedTime;
                listBooking.FirstOrDefault().date = listBooking.FirstOrDefault().ConsignmentDate.Value.ToString("dd/MM/yyyy");
                return listBooking;
            }
            catch (Exception ex)
            {
                // LogHelper.logException(ex, "GetConsignmentBookings", "");
                throw ex;
            }
        }
      
        internal static List<ConsignmentDetailsModel> GetConsignmentDetailsByID(int consignmentID)
        {
            try
            {
                List<ConsignmentDetailsModel> consignmentList = new List<ConsignmentDetailsModel>();
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentDetailsByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@pConsignmentID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = consignmentID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                if (ds.Tables.Count > 0)
                {
                    ConsignmentDetailsModel consignment = new ConsignmentDetailsModel();
                    consignment = DatatableHelper.ToList<ConsignmentDetailsModel>(ds.Tables[0]).FirstOrDefault();
                    consignmentList.Add(consignment);
                }
                return consignmentList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        internal static bool CheckManifestForConsignment(int ConsignmentId)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            db.command.CommandText = "CheckManifestForConsignment";
            db.command.CommandType = CommandType.StoredProcedure;
            SqlParameter par = new SqlParameter();
            db.command.Parameters.Add("@pConsignmentId", SqlDbType.Int).Value = ConsignmentId;
            SqlParameter paramConsID = new SqlParameter();
            paramConsID.ParameterName = "@IsExist";
            paramConsID.Direction = ParameterDirection.Output;
            paramConsID.DbType = DbType.Boolean;
            db.command.Parameters.Add(paramConsID);
            db.command.ExecuteNonQuery();
            db.ConnectionClose();
            bool isExist = Convert.ToBoolean(paramConsID.Value);
            return isExist;
        }
       
        #endregion

        #region consignment Payment

        internal static double GetTotalBasicFreight(int consignmentID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetSumBasicFreight";
                SqlParameter paramConsID = new SqlParameter();
                paramConsID.ParameterName = "@ConsignmentID";
                paramConsID.Direction = ParameterDirection.Input;
                paramConsID.DbType = DbType.Int32;
                paramConsID.Value = consignmentID;

                cmd.Parameters.Add(paramConsID);

                connection.Open();
                var result = cmd.ExecuteScalar();

                double totalFreight = 0;
                if (result != System.DBNull.Value)
                    totalFreight = Convert.ToDouble(result);

                return totalFreight;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        internal static double GetInvoiceTotal(int consignmentID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetTotalInvoiceValue";
                SqlParameter paramConsID = new SqlParameter();
                paramConsID.ParameterName = "@ConsignmentID";
                paramConsID.Direction = ParameterDirection.Input;
                paramConsID.DbType = DbType.Int32;
                paramConsID.Value = consignmentID;

                cmd.Parameters.Add(paramConsID);

                connection.Open();
                var result = cmd.ExecuteScalar();

                double totalInvoice = 0;
                if (result != System.DBNull.Value)
                    totalInvoice = Convert.ToDouble(result);

                return totalInvoice;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        internal static void AddConsignmentPayment(int loginID, CustomPaymentModel customPaymentModel)
        {
            DateTime createdDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

            var paymentBooking = customPaymentModel.BookingPayment;
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                paymentBooking.CreatedBy = loginID;

                paymentBooking.ModifiedBy = loginID;

                if (paymentBooking.RoadPermitAttached == null)
                    paymentBooking.RoadPermitAttached = false;
                if (paymentBooking.TotalAmount == null)
                    paymentBooking.TotalAmount = 0;
                if (paymentBooking.GrandTotal == null)
                    paymentBooking.GrandTotal = 0;

                db.command.CommandText = "AddConsignmentPayment";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ConsignmentID", SqlDbType.Int).Value = paymentBooking.ConsignmentID;
                db.command.Parameters.Add("@GoodsDescription", SqlDbType.Int).Value = paymentBooking.GoodsDescription;
                db.command.Parameters.Add("@InsuranceFlag", SqlDbType.Bit).Value = paymentBooking.InsuranceFlag;
                db.command.Parameters.Add("@DocketCharges", SqlDbType.Float).Value = paymentBooking.DocketCharges;


                db.command.Parameters.Add("@FOV", SqlDbType.Float).Value = paymentBooking.FOV;

                db.command.Parameters.Add("@DoorCollection", SqlDbType.Float).Value = paymentBooking.DoorCollection;
                db.command.Parameters.Add("@Octroicharges", SqlDbType.Float).Value = paymentBooking.Octroicharges;
                db.command.Parameters.Add("@DoorDelivery", SqlDbType.Float).Value = paymentBooking.DoorDelivery;


                db.command.Parameters.Add("@OctroiSurcharges", SqlDbType.Float).Value = paymentBooking.OctroiSurcharges;

                db.command.Parameters.Add("@LabourCharges", SqlDbType.Float).Value = paymentBooking.LabourCharges;
                db.command.Parameters.Add("@OtherCharges", SqlDbType.Float).Value = paymentBooking.OtherCharges;

                db.command.Parameters.Add("@TotalAmount", SqlDbType.Float).Value = paymentBooking.TotalAmount;
                //db.command.Parameters.Add("@TaxFlag", SqlDbType.Bit ).Value = paymentBooking.TaxFlag;
                db.command.Parameters.Add("@RoadPermitAttached", SqlDbType.Bit).Value = paymentBooking.RoadPermitAttached;
                db.command.Parameters.Add("@RoadPermitType", SqlDbType.VarChar).Value = paymentBooking.RoadPermitType;
                db.command.Parameters.Add("@Remark", SqlDbType.VarChar).Value = paymentBooking.Remark;
                db.command.Parameters.Add("@GrandTotal", SqlDbType.Float).Value = paymentBooking.GrandTotal;
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = paymentBooking.CreatedBy;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = paymentBooking.ModifiedBy;

                db.command.Parameters.Add("@PaymentTypeID", SqlDbType.Int).Value = paymentBooking.PaymentTypeID;

                db.command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = paymentBooking.BankName;
                db.command.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = paymentBooking.BranchName;
                db.command.Parameters.Add("@ChequeNo", SqlDbType.VarChar).Value = paymentBooking.ChequeNo;
                DateTime? Chequedate = null;
                if (!String.IsNullOrEmpty(paymentBooking.ChequeDate))
                {
                    Chequedate = DateTime.ParseExact(paymentBooking.ChequeDate, "dd/MM/yyyy", DateTimeFormatInfo.InvariantInfo);
                }
                db.command.Parameters.Add("@ChequeDate", SqlDbType.DateTime).Value = Chequedate;

                db.command.Parameters.Add("@IFSCCode", SqlDbType.VarChar).Value = paymentBooking.IFSCCode;
                db.command.Parameters.Add("@PanNo", SqlDbType.VarChar).Value = paymentBooking.PanNo;
                db.command.Parameters.Add("@pEwayBillNo", SqlDbType.VarChar).Value = paymentBooking.EwayBillNo;
                db.command.Parameters.Add("@pVendorCode", SqlDbType.VarChar).Value = paymentBooking.VendorCode;
                db.command.Parameters.Add("@pDocuments", SqlDbType.VarChar).Value = paymentBooking.Documents;
                db.command.Parameters.Add("@pFOVId", SqlDbType.Int).Value = paymentBooking.FOVId;
                db.command.Parameters.Add("@pPaymentCollected", SqlDbType.Bit).Value = paymentBooking.PaymentCollected;
                db.command.Parameters.Add("@pStandardRate", SqlDbType.Decimal).Value = paymentBooking.StandardRate;
                db.command.Parameters.Add("@pBasicFreight", SqlDbType.Decimal).Value = paymentBooking.BasicFreight;
                db.command.Parameters.Add("@pPickup", SqlDbType.Decimal).Value = paymentBooking.Pickup;
                db.command.Parameters.Add("@pHamaliCharges", SqlDbType.Decimal).Value = paymentBooking.HamaliCharges;
                db.command.Parameters.Add("@pDDCharges", SqlDbType.Decimal).Value = paymentBooking.DDCharges;
                db.command.Parameters.Add("@pODACharges", SqlDbType.Decimal).Value = paymentBooking.ODACharges;

                db.command.Parameters.Add("@pOtherAOC", SqlDbType.Decimal).Value = paymentBooking.OtherAOC;
                db.command.Parameters.Add("@pCreated", SqlDbType.DateTime).Value = createdDate;
                var result = db.command.ExecuteScalar();
                db.ConnectionClose();

                //insert tax 
                if (customPaymentModel.PaymentTaxes != null)
                {
                    foreach (TaxMaster tax in customPaymentModel.PaymentTaxes)
                    {
                        DBConnection db1 = new DBConnection();
                        db1.ConnectoDB();
                        db1.command.CommandText = "AddPaymentTax";
                        db1.command.CommandType = CommandType.StoredProcedure;
                        db1.command.Parameters.Add("@pBookingPaymentId", SqlDbType.Int).Value = Convert.ToInt32(result);
                        db1.command.Parameters.Add("@pTaxType", SqlDbType.VarChar).Value = tax.TaxType;
                        db1.command.Parameters.Add("@pTaxValue", SqlDbType.Float).Value = tax.TaxPercent;
                        db1.command.ExecuteNonQuery();
                        db1.ConnectionClose();
                    }
                }
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }



        internal static Dictionary<int, List<ToPayAndPaidConsignment>> GetPaymentWiseConsignments(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, int? paymentTypeId, DateTime? fromDate, DateTime? toDate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetPaymentTypeWiseConsignments";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pPaymentTypeId", SqlDbType.Int).Value = paymentTypeId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
                db.command.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<ToPayAndPaidConsignment>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<ToPayAndPaidConsignment>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static DataTable GetPaymentWiseConsignmentsReport(int? branchId, DateTime? dtfrom, DateTime? dtto, int PaymentTypeId)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetPaymentTypeWiseConsignments";
                cmd.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                cmd.Parameters.Add("@pPaymentTypeId", SqlDbType.Int).Value = PaymentTypeId;
                SqlParameter paramFromDate = new SqlParameter();
                paramFromDate.ParameterName = "@FromDate";
                paramFromDate.Direction = ParameterDirection.Input;
                paramFromDate.DbType = DbType.DateTime;
                paramFromDate.Value = dtfrom;
                cmd.Parameters.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter();
                paramToDate.ParameterName = "@ToDate";
                paramToDate.Direction = ParameterDirection.Input;
                paramToDate.DbType = DbType.DateTime;
                paramToDate.Value = dtto;
                cmd.Parameters.Add(paramToDate);


                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        internal static void UpdateConsignmentPayment(int loginID, CustomPaymentModel customPaymentModel)
        {
            var paymentBooking = customPaymentModel.BookingPayment;
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {


                paymentBooking.ModifiedBy = loginID;


                db.command.CommandText = "UpdateConsignmentPayment";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();

                db.command.Parameters.Add("@pGoodsDescription", SqlDbType.Int).Value = paymentBooking.GoodsDescription;
                db.command.Parameters.Add("@pInsuranceFlag   ", SqlDbType.Bit).Value = paymentBooking.InsuranceFlag;
                db.command.Parameters.Add("@pEwayBillno", SqlDbType.VarChar).Value = paymentBooking.EwayBillNo;
                db.command.Parameters.Add("@pVendorCode", SqlDbType.VarChar).Value = paymentBooking.VendorCode;
                db.command.Parameters.Add("@pRoadPermitAttached", SqlDbType.Bit).Value = paymentBooking.RoadPermitAttached;
                db.command.Parameters.Add("@pDocuments", SqlDbType.VarChar).Value = paymentBooking.Documents;
                db.command.Parameters.Add("@pFOV", SqlDbType.Float).Value = paymentBooking.FOV;
                db.command.Parameters.Add("@pFOVId", SqlDbType.Int).Value = paymentBooking.FOVId;
                db.command.Parameters.Add("@pPaymentTypeID", SqlDbType.Int).Value = paymentBooking.PaymentTypeID;
                db.command.Parameters.Add("@pBankName", SqlDbType.VarChar).Value = paymentBooking.BankName;
                db.command.Parameters.Add("@pBranchName", SqlDbType.VarChar).Value = paymentBooking.BranchName;
                db.command.Parameters.Add("@pChequeNo", SqlDbType.VarChar).Value = paymentBooking.ChequeNo;
                DateTime? Chequedate = null;
                if (!String.IsNullOrEmpty(paymentBooking.ChequeDate))
                {
                    IFormatProvider provider = new System.Globalization.CultureInfo("en-GB", true);
                    Chequedate = Convert.ToDateTime(paymentBooking.ChequeDate, provider);
                }
                db.command.Parameters.Add("@pChequeDate", SqlDbType.DateTime).Value = Chequedate;
                db.command.Parameters.Add("@pPaymentCollected", SqlDbType.Bit).Value = paymentBooking.PaymentCollected;
                db.command.Parameters.Add("@pRemark", SqlDbType.VarChar).Value = paymentBooking.Remark;
                db.command.Parameters.Add("@pStandardRate", SqlDbType.Decimal).Value = paymentBooking.StandardRate;
                db.command.Parameters.Add("@pBasicFreight", SqlDbType.Decimal).Value = paymentBooking.BasicFreight;
                db.command.Parameters.Add("@pPickup", SqlDbType.Decimal).Value = paymentBooking.Pickup;
                db.command.Parameters.Add("@pHamaliCharges", SqlDbType.Decimal).Value = paymentBooking.HamaliCharges;
                db.command.Parameters.Add("@pDocketCharges", SqlDbType.Float).Value = paymentBooking.DocketCharges;
                db.command.Parameters.Add("@pDDCharges", SqlDbType.Decimal).Value = paymentBooking.DDCharges;
                db.command.Parameters.Add("@pODACharges", SqlDbType.Decimal).Value = paymentBooking.ODACharges;
                db.command.Parameters.Add("@pOtherAOC", SqlDbType.Decimal).Value = paymentBooking.OtherAOC;
                db.command.Parameters.Add("@pOtherCharges", SqlDbType.Float).Value = paymentBooking.OtherCharges;
                db.command.Parameters.Add("@pTotalAmount", SqlDbType.Float).Value = paymentBooking.TotalAmount;
                db.command.Parameters.Add("@pGrandTotal", SqlDbType.Float).Value = paymentBooking.GrandTotal;
                db.command.Parameters.Add("@pModifiedBy", SqlDbType.Int).Value = paymentBooking.ModifiedBy;
                db.command.Parameters.Add("@pBookingPaymentId", SqlDbType.Int).Value = paymentBooking.BookingPaymentID;

                db.ConnectionClose();
                DeleteConsignmentBookingPaymentTax(paymentBooking.BookingPaymentID);
                //insert tax 
                if (customPaymentModel.PaymentTaxes != null)
                {
                    foreach (TaxMaster tax in customPaymentModel.PaymentTaxes)
                    {
                        DBConnection db1 = new DBConnection();
                        db1.ConnectoDB();
                        db1.command.CommandText = "AddPaymentTax";
                        db1.command.CommandType = CommandType.StoredProcedure;
                        db1.command.Parameters.Add("@pBookingPaymentId", SqlDbType.Int).Value = paymentBooking.BookingPaymentID;
                        db1.command.Parameters.Add("@pTaxType", SqlDbType.VarChar).Value = tax.TaxType;
                        db1.command.Parameters.Add("@pTaxValue", SqlDbType.Float).Value = tax.TaxPercent;
                        db1.command.ExecuteNonQuery();
                        db1.ConnectionClose();
                    }
                }
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteConsignmentBookingPaymentTax(int bookingPaymentID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteBookingPaymentTaxes";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@bookingPaymentID", SqlDbType.Int).Value = bookingPaymentID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }

        internal static void DeleteConsignmentBookingPayment(int loginID, int bookingPaymentID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteConsignmentBookingPayment";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@bookingPaymentID", SqlDbType.Int).Value = bookingPaymentID;
                db.command.Parameters.Add("@DeletedBy", SqlDbType.Int).Value = loginID;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static List<ConsignmentBookingPayment> GetConsignmentBookingPaymentByID(int consignmentID)
        {

            try
            {
                var listPayment = new List<ConsignmentBookingPayment>();
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentBookingPaymentsByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@ConsignmentID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = consignmentID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<ConsignmentBookingPayment> list = new List<ConsignmentBookingPayment>();

                string formattedDate = "";
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string chequedate = Convert.ToString(ds.Tables[0].Rows[0]["ChequeDate"]);
                    if (chequedate != "")
                    {
                        DateTime tm = DateTime.Parse(chequedate);

                        formattedDate = tm.ToString("dd/MM/yyyy");
                    }
                    listPayment = DatatableHelper.ToList<ConsignmentBookingPayment>(ds.Tables[0]);
                    listPayment.FirstOrDefault().ChequeDate = formattedDate;
                }
                return listPayment;
            }
            catch (Exception ex)
            {
                // LogHelper.logException(ex, "GetConsignmentBookings", "");
                throw ex;
            }
        }
        internal static List<ConsignmentPartyInvoice> GetConsignmentPartyInvoiceByID(int consignmentPartyInvoiceID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentPartyInvoiceByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@consignmentPartyInvoiceID";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = consignmentPartyInvoiceID;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<ConsignmentPartyInvoice> list = new List<ConsignmentPartyInvoice>();
                var listPayment = DatatableHelper.ToList<ConsignmentPartyInvoice>(ds.Tables[0]);
                return listPayment;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        #endregion


        #region consignment Party Invoice
        internal static DataTable GetConsignmentPartyInvoices(int consignmentID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentPartyInvoices";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@consignmentID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = consignmentID;

                cmd.Parameters.Add(paramBrnch);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dsPartyInvoice = new DataSet();
                da.Fill(dsPartyInvoice, "table1");
                connection.Close();
                return dsPartyInvoice.Tables[0];


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static void AddConsignmentPartyInvoice(int loginID, ConsignmentPartyInvoice partyInvoice)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                partyInvoice.CreatedBy = loginID;
                partyInvoice.Created = System.DateTime.Now;

                partyInvoice.ModifiedBy = loginID;
                partyInvoice.Modified = System.DateTime.Now;

                ;
                db.command.CommandText = "AddConsignmentPartyInvoice";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ConsignmentID", SqlDbType.Int).Value = partyInvoice.ConsignmentID;
                db.command.Parameters.Add("@InvoiceNo", SqlDbType.VarChar).Value = partyInvoice.InvoiceNo;
                db.command.Parameters.Add("@InvoiceDate", SqlDbType.Date).Value = partyInvoice.InvoiceDate;
                db.command.Parameters.Add("@InvoiceQuantity", SqlDbType.Int).Value = partyInvoice.InvoiceQuantity;
                db.command.Parameters.Add("@InvoiceValue", SqlDbType.Float).Value = partyInvoice.InvoiceValue;
                db.command.Parameters.Add("@PartNo", SqlDbType.VarChar).Value = partyInvoice.PartNo;
                db.command.Parameters.Add("@ASNNo", SqlDbType.VarChar).Value = partyInvoice.ASNNo;
                db.command.Parameters.Add("@Article", SqlDbType.Int).Value = partyInvoice.Article;
                db.command.Parameters.Add("@PackagingID", SqlDbType.Int).Value = partyInvoice.PackagingID;
                db.command.Parameters.Add("@ActualWeight", SqlDbType.Float).Value = partyInvoice.ActualWeight;
                db.command.Parameters.Add("@ChargeWeight", SqlDbType.Float).Value = partyInvoice.ChargeWeight;
                db.command.Parameters.Add("@RatePerKG", SqlDbType.Float).Value = partyInvoice.RatePerKG;
                db.command.Parameters.Add("@BasicFreight", SqlDbType.Float).Value = partyInvoice.BasicFreight;
                db.command.Parameters.Add("@ChargeType", SqlDbType.Int).Value = partyInvoice.ChargeType;
                db.command.Parameters.Add("@LxWxH", SqlDbType.VarChar).Value = partyInvoice.LxWxH;
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = partyInvoice.CreatedBy;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = partyInvoice.ModifiedBy;
                db.command.Parameters.Add("@PONo", SqlDbType.VarChar).Value = partyInvoice.PONo;
                db.command.Parameters.Add("@StandardRate", SqlDbType.Decimal).Value = partyInvoice.StandardRate;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();



            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static void UpdateConsignmentPartyInvoice(int loginID, ConsignmentPartyInvoice partyInvoice)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                partyInvoice.CreatedBy = loginID;
                partyInvoice.Created = System.DateTime.Now;

                partyInvoice.ModifiedBy = loginID;
                partyInvoice.Modified = System.DateTime.Now;

                ;
                db.command.CommandText = "UpdateConsignmentPartyInvoice";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ConsignmentID", SqlDbType.Int).Value = partyInvoice.ConsignmentID;
                db.command.Parameters.Add("@InvoiceNo", SqlDbType.VarChar).Value = partyInvoice.InvoiceNo;
                db.command.Parameters.Add("@InvoiceDate", SqlDbType.Date).Value = partyInvoice.InvoiceDate;
                db.command.Parameters.Add("@InvoiceQuantity", SqlDbType.Int).Value = partyInvoice.InvoiceQuantity;
                db.command.Parameters.Add("@InvoiceValue", SqlDbType.Float).Value = partyInvoice.InvoiceValue;
                db.command.Parameters.Add("@PartNo", SqlDbType.VarChar).Value = partyInvoice.PartNo;
                db.command.Parameters.Add("@ASNNo", SqlDbType.VarChar).Value = partyInvoice.ASNNo;
                db.command.Parameters.Add("@Article", SqlDbType.Int).Value = partyInvoice.Article;
                db.command.Parameters.Add("@PackagingID", SqlDbType.Int).Value = partyInvoice.PackagingID;
                db.command.Parameters.Add("@ActualWeight", SqlDbType.Float).Value = partyInvoice.ActualWeight;
                db.command.Parameters.Add("@ChargeWeight", SqlDbType.Float).Value = partyInvoice.ChargeWeight;
                db.command.Parameters.Add("@RatePerKG", SqlDbType.Float).Value = partyInvoice.RatePerKG;
                db.command.Parameters.Add("@BasicFreight", SqlDbType.Float).Value = partyInvoice.BasicFreight;
                db.command.Parameters.Add("@ChargeType", SqlDbType.Int).Value = partyInvoice.ChargeType;
                db.command.Parameters.Add("@LxWxH", SqlDbType.VarChar).Value = partyInvoice.LxWxH;
                db.command.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = partyInvoice.CreatedBy;
                db.command.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = partyInvoice.ModifiedBy;
                db.command.Parameters.Add("@PONo", SqlDbType.VarChar).Value = partyInvoice.PONo;
                db.command.Parameters.Add("@StandardRate", SqlDbType.Decimal).Value = partyInvoice.StandardRate;
                db.command.Parameters.Add("@PartyInvoiceID", SqlDbType.Int).Value = partyInvoice.PartyInvoiceID;
                db.command.ExecuteNonQuery();
                db.ConnectionClose();



            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static void DeleteConsignmentPartyInvoice(int loginID, int partyInvoiceID)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "DeleteConsignmentPartyInvoice";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@PartyInvoiceID", SqlDbType.Int).Value = partyInvoiceID;


                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }


        #endregion


        #region MASTER DATA RETREIVAL
        internal static List<VehicleMaster> GetVehicleByID(int vehicleID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetVehicleByID";
                SqlParameter paramVehID = new SqlParameter();
                paramVehID.ParameterName = "@VehicleID";
                paramVehID.Direction = ParameterDirection.Input;
                paramVehID.DbType = DbType.Int32;
                paramVehID.Value = vehicleID;

                cmd.Parameters.Add(paramVehID);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet dsVehicle = new DataSet();
                da.Fill(dsVehicle, "table1");
                connection.Close();

                List<VehicleMaster> list = new List<VehicleMaster>();
                return list = DatatableHelper.ToList<VehicleMaster>(dsVehicle.Tables[0]);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        internal static object GetBranches()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<BranchMaster> list = new List<BranchMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetBranches");
                return list = DatatableHelper.ToList<BranchMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetOtherBranches()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<OtherBranchMaster> list = new List<OtherBranchMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetOtherBranches");
                return list = DatatableHelper.ToList<OtherBranchMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetCargoTypes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<CargoTypeMaster> list = new List<CargoTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetCargoTypes");
                return list = Helper.DatatableHelper.ToList<CargoTypeMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        internal static object GetChargeTypes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<ChargeTypeMaster> list = new List<ChargeTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetChargeTypes");
                return list = DatatableHelper.ToList<ChargeTypeMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetCompanies()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<CompanyMaster> list = new List<CompanyMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetCompanies");
                return list = DatatableHelper.ToList<CompanyMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        internal static String GetNextConsignmentNo(int branchID)
        {
            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetNextConsignmentNo";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;



                cmd.Parameters.Add(paramBrnch);
                connection.Open();
                object retVal = cmd.ExecuteScalar();

                String consignNo = cmd.ExecuteScalar().ToString();
                connection.Close();
                return consignNo;

            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        internal static void UpdatePODStatus(int loginID, string status, int manifestConsignmentID)
        {
            string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = connectionStr;
            var date = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));

            try
            {
                SqlCommand updateCmd = new SqlCommand();
                updateCmd.CommandText = "UpdatePODStatus";
                updateCmd.CommandType = CommandType.StoredProcedure;

                updateCmd.Parameters.Add("@pManifestConsignmentID", SqlDbType.Int).Value = manifestConsignmentID;
                updateCmd.Parameters.Add("@ploginID", SqlDbType.Int).Value = loginID;
                updateCmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = status;
                updateCmd.Parameters.Add("@pReceivedDate", SqlDbType.DateTime).Value = date;
                updateCmd.Connection = connection;
                connection.Open();
                updateCmd.ExecuteNonQuery();
                connection.Close();

            }///end try
            catch (Exception ex)
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();

                throw new Exception(ex.Message);
            }

        }

        internal static String GetNextRunningNo(int branchID, string TransactionType)
        {
            try
            {
                DBConnection db = new DBConnection();
                db.ConnectoDB();

                db.command.CommandType = CommandType.StoredProcedure;
                db.command.CommandText = "GetNextRunningNo";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                SqlParameter paramTrans = new SqlParameter();
                paramTrans.ParameterName = "@TransactionType";
                paramTrans.Direction = ParameterDirection.Input;
                paramTrans.DbType = DbType.String;
                paramTrans.Value = TransactionType;

                db.command.Parameters.Add("@ConsignMentNo", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                db.command.Parameters.Add(paramBrnch);
                db.command.Parameters.Add(paramTrans);
                db.ConnectionOpen();
                db.command.ExecuteNonQuery();
                String consignNo = Convert.ToString(db.command.Parameters["@ConsignMentNo"].Value);
                db.ConnectionClose();
                return consignNo;

            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static decimal GetStandardRate(int ChargeType, int FromBranch, int ToBranch)
        {
            try
            {
                DBConnection db = new DBConnection();
                db.ConnectoDB();

                db.command.CommandType = CommandType.StoredProcedure;
                db.command.CommandText = "GetStandardRate";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@ChargeType";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = ChargeType;

                SqlParameter paramFrom = new SqlParameter();
                paramFrom.ParameterName = "@ToBranch";
                paramFrom.Direction = ParameterDirection.Input;
                paramFrom.DbType = DbType.Int32;
                paramFrom.Value = ToBranch;

                SqlParameter paramTo = new SqlParameter();
                paramTo.ParameterName = "@FromBranch";
                paramTo.Direction = ParameterDirection.Input;
                paramTo.DbType = DbType.Int32;
                paramTo.Value = FromBranch;

                db.command.Parameters.Add("@StandardRate", SqlDbType.Money, 50).Direction = ParameterDirection.Output;
                db.command.Parameters.Add(paramBrnch);
                db.command.Parameters.Add(paramFrom);
                db.command.Parameters.Add(paramTo);
                db.ConnectionOpen();
                db.command.ExecuteNonQuery();
                decimal standardRate = Convert.ToDecimal(db.command.Parameters["@StandardRate"].Value);
                db.ConnectionClose();
                return standardRate;

            }
            catch (Exception x)
            {
                return 0;
            }
        }

        internal static object GetVehicles(bool isVehicleHired)
        {
            try
            {
                if (isVehicleHired == null)
                    isVehicleHired = false;

                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetVehicles";
                SqlParameter paramVehHire = new SqlParameter();
                paramVehHire.ParameterName = "@Ishired";
                paramVehHire.Direction = ParameterDirection.Input;
                paramVehHire.DbType = DbType.Boolean;
                paramVehHire.Value = isVehicleHired;

                cmd.Parameters.Add(paramVehHire);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                List<VehicleMaster> list = new List<VehicleMaster>();
                return list = DatatableHelper.ToList<VehicleMaster>(ds.Tables[0]);

            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetDeliveryTypes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<DeliveryTypeMaster> list = new List<DeliveryTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetDeliveryTypes");
                return list = DatatableHelper.ToList<DeliveryTypeMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetFOV()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<FOVMaster> list = new List<FOVMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetFOV");
                return list = DatatableHelper.ToList<FOVMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetDoorCollections()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<DoorCollectionMaster> list = new List<DoorCollectionMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetDoorCollections");
                return list = DatatableHelper.ToList<DoorCollectionMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetPackagings()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<PackagingMaster> list = new List<PackagingMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetPackagings");
                return list = DatatableHelper.ToList<PackagingMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }



        internal static object GetPaymentTypes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<PaymentTypeMaster> list = new List<PaymentTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetPaymentTypes");
                return list = DatatableHelper.ToList<PaymentTypeMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetTaxes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<TaxMaster> list = new List<TaxMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetTaxes");
                return list = DatatableHelper.ToList<TaxMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        internal static object GetGoods()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<GoodsMaster> list = new List<GoodsMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetGoods");
                return list = DatatableHelper.ToList<GoodsMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        internal static object GetServiceTaxpaidBy()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<ServiceTaxPaidByMaster> list = new List<ServiceTaxPaidByMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetServiceTaxPaidBy");
                return list = DatatableHelper.ToList<ServiceTaxPaidByMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        internal static object GetTransportModes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<TransportmodeMaster> list = new List<TransportmodeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetTransportModes");
                return list = DatatableHelper.ToList<TransportmodeMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }


        internal static object GetTransportTypes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<TransportTypeMaster> list = new List<TransportTypeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetTransportTypes");
                return list = DatatableHelper.ToList<TransportTypeMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetVendors()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<VendorMaster> list = new List<VendorMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetVendors");
                return list = DatatableHelper.ToList<VendorMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        internal static object GetOtherVendors()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<OtherVendorMaster> list = new List<OtherVendorMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetOtherVendors");
                return list = DatatableHelper.ToList<OtherVendorMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetAllVendors()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<VendorMaster> list = new List<VendorMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetAllVendors");
                return list = DatatableHelper.ToList<VendorMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetAllUnPaidBills()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<ConsignmentBilling> list = new List<ConsignmentBilling>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetAllUnPaidBills");
                return list = DatatableHelper.ToList<ConsignmentBilling>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetMHELVendors()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<VendorMaster> list = new List<VendorMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetMHELVendors");
                return list = DatatableHelper.ToList<VendorMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetVehicleVendors()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<VendorMaster> list = new List<VendorMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetVehicleVendors");
                return list = DatatableHelper.ToList<VendorMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetPinCodes()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<PinCodeMaster> list = new List<PinCodeMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetPinCodes");
                return list = DatatableHelper.ToList<PinCodeMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }
        internal static object GetODA()
        {
            try
            {
                DBConnection db = new DBConnection();
                List<ODAMaster> list = new List<ODAMaster>();
                db.command.Parameters.Clear();
                DataTable dt = db.GetDataFromDB("GetODA");
                return list = DatatableHelper.ToList<ODAMaster>(dt);
            }
            catch (Exception x)
            {
                //LogHelper.logException(x, "GetTrainingTypes", "");
                throw x;
            }
        }

        #endregion

        internal static Dictionary<int, List<ConsignmentBookingModel>> GetConsignmentIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetConsignmentBookingIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<ConsignmentBookingModel>(ds.Tables[0]);
                int count = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<ConsignmentBookingModel>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                //LogHelper.logException(ex, "AddTrainingPlan", "");
                throw new Exception(ex.Message);
            }


        }
        internal static DataTable GetConsignmentsForDestination(int branchID, DateTime? fromDate, DateTime? toDate)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentsForDestination";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@DestinationID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                cmd.Parameters.Add(paramBrnch);
                cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
                cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static Dictionary<int, List<ToPayAndPaidConsignment>> GetGSTData(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, DateTime? fromDate, DateTime? toDate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetGSTData";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
                db.command.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<ToPayAndPaidConsignment>(ds.Tables[0]);
                int count = 0;
                if (ds.Tables[1].Rows.Count > 0)
                    count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<ToPayAndPaidConsignment>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static Dictionary<int, List<ToPayAndPaidConsignment>> GetPODData(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, DateTime? dtfrom, DateTime? dtto)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetPODData";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = dtfrom;
                db.command.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = dtto;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<ToPayAndPaidConsignment>(ds.Tables[0]);
                int count = 0;
                if (ds.Tables[1].Rows.Count > 0)
                    count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<ToPayAndPaidConsignment>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static DataTable GetGSTData(int? branchId, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetGSTData";
                cmd.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                SqlParameter paramFromDate = new SqlParameter();
                paramFromDate.ParameterName = "@FromDate";
                paramFromDate.Direction = ParameterDirection.Input;
                paramFromDate.DbType = DbType.DateTime;
                paramFromDate.Value = fromDate;
                cmd.Parameters.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter();
                paramToDate.ParameterName = "@ToDate";
                paramToDate.Direction = ParameterDirection.Input;
                paramToDate.DbType = DbType.DateTime;
                paramToDate.Value = toDate;
                cmd.Parameters.Add(paramToDate);


                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static Dictionary<int, List<ConsignmentBookingReportModel>> GetVendorSalesReportIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandText = "GetVendorSalesReportIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
                db.command.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;
                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<ConsignmentBookingReportModel>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<ConsignmentBookingReportModel>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }
        internal static Dictionary<int, List<ToPayAndPaidConsignment>> GetPaymentWiseVendorConsignments(int? vendorId, int start, int length, string search, int sortColumn, string sortDirection, int? paymentTypeId, DateTime? fromDate, DateTime? toDate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetPaymentTypeWiseVendorConsignments";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pVendorId", SqlDbType.Int).Value = vendorId;
                db.command.Parameters.Add("@pPaymentTypeId", SqlDbType.Int).Value = paymentTypeId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
                db.command.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<ToPayAndPaidConsignment>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<ToPayAndPaidConsignment>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static DataTable GetPaymentWiseVendorConsignmentsReport(int? vendorId, DateTime? dtfrom, DateTime? dtto, int PaymentTypeId)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetPaymentTypeWiseVendorConsignments";
                cmd.Parameters.Add("@pVendorId", SqlDbType.Int).Value = vendorId;
                cmd.Parameters.Add("@pPaymentTypeId", SqlDbType.Int).Value = PaymentTypeId;
                SqlParameter paramFromDate = new SqlParameter();
                paramFromDate.ParameterName = "@FromDate";
                paramFromDate.Direction = ParameterDirection.Input;
                paramFromDate.DbType = DbType.DateTime;
                paramFromDate.Value = dtfrom;
                cmd.Parameters.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter();
                paramToDate.ParameterName = "@ToDate";
                paramToDate.Direction = ParameterDirection.Input;
                paramToDate.DbType = DbType.DateTime;
                paramToDate.Value = dtto;
                cmd.Parameters.Add(paramToDate);


                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        internal static Dictionary<int, List<ToPayAndPaidConsignment>> GetPaymentWiseMHELVendorConsignments(int? vendorId, int start, int length, string search, int sortColumn, string sortDirection, int? paymentTypeId, DateTime? fromDate, DateTime? toDate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetPaymentTypeWiseMHELVendorConsignments";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pVendorId", SqlDbType.Int).Value = vendorId;
                db.command.Parameters.Add("@pPaymentTypeId", SqlDbType.Int).Value = paymentTypeId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
                db.command.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<ToPayAndPaidConsignment>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<ToPayAndPaidConsignment>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }
        }
        internal static DataTable GetPaymentWiseMHELVendorConsignmentsReport(int? vendorId, DateTime? dtfrom, DateTime? dtto, int PaymentTypeId)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetPaymentTypeWiseMHELVendorConsignments";
                cmd.Parameters.Add("@pVendorId", SqlDbType.Int).Value = vendorId;
                cmd.Parameters.Add("@pPaymentTypeId", SqlDbType.Int).Value = PaymentTypeId;
                SqlParameter paramFromDate = new SqlParameter();
                paramFromDate.ParameterName = "@FromDate";
                paramFromDate.Direction = ParameterDirection.Input;
                paramFromDate.DbType = DbType.DateTime;
                paramFromDate.Value = dtfrom;
                cmd.Parameters.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter();
                paramToDate.ParameterName = "@ToDate";
                paramToDate.Direction = ParameterDirection.Input;
                paramToDate.DbType = DbType.DateTime;
                paramToDate.Value = dtto;
                cmd.Parameters.Add(paramToDate);


                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #region Consignment Billing

        internal static Dictionary<int, DataTable> GetConsignmentBillingIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "GetConsignmentBillingIndex";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pBranchId", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;

                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();

                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, DataTable>();
                data.Add(count, ds.Tables[0]);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static int SaveConsignmentBilling(int loginID, ConsignmentBilling newBilling)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                newBilling.CreatedBy = loginID;

                db.command.CommandText = "AddConsignmentBilling";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@pConsignmentBillingID", SqlDbType.Int).Value = newBilling.ConsignmentBillingID;
                db.command.Parameters.Add("@pBillingNo", SqlDbType.VarChar).Value = newBilling.BillingNo;
                db.command.Parameters.Add("@pBranchID", SqlDbType.Int).Value = newBilling.BranchID;
                db.command.Parameters.Add("@pBillingDate", SqlDbType.DateTime).Value = newBilling.BillingDate;
                db.command.Parameters.Add("@pVendorID", SqlDbType.Int).Value = newBilling.VendorID;
                DateTime CurrentDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                TimeSpan timeBilling = CurrentDate.TimeOfDay;
                db.command.Parameters.Add("@pBillingTime", SqlDbType.Time).Value = timeBilling;
                db.command.Parameters.Add("@pRemarks", SqlDbType.VarChar).Value = newBilling.Remarks;
                db.command.Parameters.Add("@pBillingTotalAmount", SqlDbType.Float).Value = newBilling.BillingTotalAmount;
                db.command.Parameters.Add("@pCreatedBy", SqlDbType.Int).Value = newBilling.CreatedBy;

                SqlParameter paramConsID = new SqlParameter();
                paramConsID.ParameterName = "@pBillingID";
                paramConsID.Direction = ParameterDirection.Output;
                paramConsID.DbType = DbType.Int32;
                db.command.Parameters.Add(paramConsID);
                db.command.ExecuteNonQuery();
                db.ConnectionClose();
                int billingID = Convert.ToInt32(paramConsID.Value);
                return billingID;

            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }


        }
        internal static DataTable GetConsignmentsForBilling(int? branchID, int vendorID, int ConsignmentBillingID)
        {

            try
            {
                if (branchID == 0)
                    branchID = null;
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentsForBilling";
                SqlParameter paramBrnch = new SqlParameter();
                paramBrnch.ParameterName = "@BranchID";
                paramBrnch.Direction = ParameterDirection.Input;
                paramBrnch.DbType = DbType.Int32;
                paramBrnch.Value = branchID;

                SqlParameter paramVendorID = new SqlParameter();
                paramVendorID.ParameterName = "@VendorID";
                paramVendorID.Direction = ParameterDirection.Input;
                paramVendorID.DbType = DbType.Int32;
                paramVendorID.Value = vendorID;

                SqlParameter paramConsignmentBillingID = new SqlParameter();
                paramConsignmentBillingID.ParameterName = "@ConsignmentBillingID";
                paramConsignmentBillingID.Direction = ParameterDirection.Input;
                paramConsignmentBillingID.DbType = DbType.Int32;
                paramConsignmentBillingID.Value = ConsignmentBillingID;

                cmd.Parameters.Add(paramBrnch);
                cmd.Parameters.Add(paramVendorID);
                cmd.Parameters.Add(paramConsignmentBillingID);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static DataSet GetBillingConsignmentsWithDetails(int ConsignmentBillingID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetBillingConsignmentsWithDetails";
                SqlParameter paramConsignmentBillingID = new SqlParameter();
                paramConsignmentBillingID.ParameterName = "@ConsignmentBillingID";
                paramConsignmentBillingID.Direction = ParameterDirection.Input;
                paramConsignmentBillingID.DbType = DbType.Int32;
                paramConsignmentBillingID.Value = ConsignmentBillingID;


                cmd.Parameters.Add(paramConsignmentBillingID);


                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static DataTable GetBillingConsignments(int ConsignmentBillingID)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetBillingConsignments";
                SqlParameter paramGetBillingConsignments = new SqlParameter();
                paramGetBillingConsignments.ParameterName = "@ConsignmentBillingID";
                paramGetBillingConsignments.Direction = ParameterDirection.Input;
                paramGetBillingConsignments.DbType = DbType.Int32;
                paramGetBillingConsignments.Value = ConsignmentBillingID;


                cmd.Parameters.Add(paramGetBillingConsignments);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static List<ConsignmentBilling> GetConsignmentBillingByID(int consignmentBillingId)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetConsignmentBillingByID";
                SqlParameter paramCnB = new SqlParameter();
                paramCnB.ParameterName = "@consignmentBillingId";
                paramCnB.Direction = ParameterDirection.Input;
                paramCnB.DbType = DbType.Int32;
                paramCnB.Value = consignmentBillingId;


                cmd.Parameters.Add(paramCnB);

                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();

                List<ConsignmentBilling> list = new List<ConsignmentBilling>();
                return list = DatatableHelper.ToList<ConsignmentBilling>(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal static void SaveBillingConsignments(List<BillingConsignment> billingConsignments)
        {
            string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = connectionStr;
            connection.Open();

            using (SqlTransaction trans = connection.BeginTransaction())
            {
                try
                {

                    SqlCommand delCmd = new SqlCommand();
                    delCmd.CommandText = "DeleteBillingConsignments";
                    delCmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter par = new SqlParameter();
                    delCmd.Parameters.Add("@ConsignmentID", SqlDbType.Int).Value = billingConsignments[0].ConsignmentID;
                    delCmd.Connection = connection;
                    delCmd.Transaction = trans;
                    delCmd.ExecuteNonQuery();

                    foreach (BillingConsignment billingcons in billingConsignments)
                    {


                        SqlCommand addCmd = new SqlCommand();
                        addCmd.CommandText = "AddBillingConsignment";
                        addCmd.CommandType = CommandType.StoredProcedure;
                        addCmd.Parameters.Add("@ConsignmentBillingID", SqlDbType.Int).Value = billingcons.ConsignmentBillingID;
                        addCmd.Parameters.Add("@ConsignmentID", SqlDbType.Int).Value = billingcons.ConsignmentID;
                        addCmd.Connection = connection;
                        addCmd.Transaction = trans;
                        addCmd.ExecuteNonQuery();
                    }

                    trans.Commit();
                    connection.Close();

                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    if (connection.State == ConnectionState.Open)
                        connection.Close();

                    throw new Exception(ex.Message);
                }
            }


        }
        internal static void UpdateConsignmentBillingTotalAmount(int ConsignmentBillingID, float totalAmount)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateConsignmentBillingTotalAmount";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ConsignmentBillingID", SqlDbType.Int).Value = ConsignmentBillingID;
                db.command.Parameters.Add("@totalAmount", SqlDbType.Float).Value = totalAmount;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }
        #endregion

        #region Billed Consignment Report
        internal static DataTable GetBilledConsignmentBookingReport(int? branchId, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetBilledConsignmentBookingReport";
                cmd.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                SqlParameter paramFromDate = new SqlParameter();
                paramFromDate.ParameterName = "@FromDate";
                paramFromDate.Direction = ParameterDirection.Input;
                paramFromDate.DbType = DbType.DateTime;
                paramFromDate.Value = fromDate;
                cmd.Parameters.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter();
                paramToDate.ParameterName = "@ToDate";
                paramToDate.Direction = ParameterDirection.Input;
                paramToDate.DbType = DbType.DateTime;
                paramToDate.Value = toDate;
                cmd.Parameters.Add(paramToDate);


                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static Dictionary<int, List<ConsignmentBookingReportModel>> GetBilledConsignmentBookingReportIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandText = "GetBilledConsignmentBookingReport";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
                db.command.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;
                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<ConsignmentBookingReportModel>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<ConsignmentBookingReportModel>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }
        #endregion

        #region UnBilled Consignment Report
        internal static DataTable GetUnbilledConsignmentBookingReport(int? branchId, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetUnbilledConsignmentBookingReport";
                cmd.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                SqlParameter paramFromDate = new SqlParameter();
                paramFromDate.ParameterName = "@FromDate";
                paramFromDate.Direction = ParameterDirection.Input;
                paramFromDate.DbType = DbType.DateTime;
                paramFromDate.Value = fromDate;
                cmd.Parameters.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter();
                paramToDate.ParameterName = "@ToDate";
                paramToDate.Direction = ParameterDirection.Input;
                paramToDate.DbType = DbType.DateTime;
                paramToDate.Value = toDate;
                cmd.Parameters.Add(paramToDate);


                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static Dictionary<int, List<ConsignmentBookingReportModel>> GetUnbilledConsignmentBookingReportIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandText = "GetUnbilledConsignmentBookingReport";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
                db.command.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;
                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<ConsignmentBookingReportModel>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<ConsignmentBookingReportModel>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }
        #endregion

        #region Cancelled Consignment Report
        internal static DataTable GetCancelledConsignmentBookingReport(int? branchId, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {

            try
            {
                string connectionStr = Convert.ToString(ConfigurationManager.ConnectionStrings["TMSConnStr"]);
                SqlCommand cmd = new SqlCommand();
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = connectionStr;
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GetCancelledConsignmentBookingReport";
                cmd.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                SqlParameter paramFromDate = new SqlParameter();
                paramFromDate.ParameterName = "@FromDate";
                paramFromDate.Direction = ParameterDirection.Input;
                paramFromDate.DbType = DbType.DateTime;
                paramFromDate.Value = fromDate;
                cmd.Parameters.Add(paramFromDate);
                SqlParameter paramToDate = new SqlParameter();
                paramToDate.ParameterName = "@ToDate";
                paramToDate.Direction = ParameterDirection.Input;
                paramToDate.DbType = DbType.DateTime;
                paramToDate.Value = toDate;
                cmd.Parameters.Add(paramToDate);


                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                connection.Close();
                return ds.Tables[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static Dictionary<int, List<ConsignmentBookingReportModel>> GetCancelledConsignmentBookingReportIndex(int? branchId, int start, int length, string search, int sortColumn, string sortDirection, System.Nullable<DateTime> fromDate, System.Nullable<DateTime> toDate)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {
                db.command.CommandText = "GetCancelledConsignmentBookingReport";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchId;
                db.command.Parameters.Add("@pSkip", SqlDbType.Int).Value = start;
                db.command.Parameters.Add("@pTake", SqlDbType.Int).Value = length;
                db.command.Parameters.Add("@pSearchBy", SqlDbType.VarChar).Value = search;
                db.command.Parameters.Add("@pSortColumn", SqlDbType.Int).Value = sortColumn;
                db.command.Parameters.Add("@pSortDirection", SqlDbType.VarChar).Value = sortDirection;
                db.command.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
                db.command.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;
                SqlDataAdapter da = new SqlDataAdapter(db.command);
                DataSet ds = new DataSet();
                da.Fill(ds, "table1");
                db.ConnectionClose();
                var list = DatatableHelper.ToList<ConsignmentBookingReportModel>(ds.Tables[0]);
                int count = Convert.ToInt16(ds.Tables[1].Rows[0][0]);
                var data = new Dictionary<int, List<ConsignmentBookingReportModel>>();
                data.Add(count, list);
                return data;
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }
        #endregion

        internal static void UpdateVendorBalancePayment(int ConsignmentBillingID, float BalanceAmount)
        {
            DBConnection db = new DBConnection();
            db.ConnectoDB();
            try
            {

                db.command.CommandText = "UpdateVendorBalancePayment";
                db.command.CommandType = CommandType.StoredProcedure;
                SqlParameter par = new SqlParameter();
                db.command.Parameters.Add("@ConsignmentBillingID", SqlDbType.Int).Value = ConsignmentBillingID;
                db.command.Parameters.Add("@BalanceAmount", SqlDbType.Float).Value = BalanceAmount;

                db.command.ExecuteNonQuery();
                db.ConnectionClose();
            }
            catch (Exception ex)
            {
                db.ConnectionClose();
                throw new Exception(ex.Message);
            }

        }
    }
}