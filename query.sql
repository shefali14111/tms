
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE AddUser
	@UserName varchar(350),
	@Address varchar(500)='',
	@Pincode varchar(50)='',
	@city varchar(50)='',
	@state varchar(100)='',
	@Nationalty varchar(100)='',
	@EmailID varchar(250),
	@Password varchar(10),
	@ContactNo varchar(50)='',
	@ZoneID int,
	@BranchID int=null,
	@RoleID int,
	@ReportingManagerID int=null,
	@DepartmentID int=null,
	@HO bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO [dbo].[UserMaster]
           ([UserName]
           ,[Address]
           ,[Pincode]
           ,[city]
           ,[state]
           ,[Nationalty]
           ,[EmailID]
           ,[Password]
           ,[ContactNo]
           ,[ZoneID]
           ,[BranchID]
           ,[RoleID]
           ,[ReportingManagerID]
           ,[DepartmentID]
           ,[HO]
           ,[Status])
     VALUES
           (@UserName
           ,@Address
           ,@Pincode
           ,@city
           ,@state
           ,@Nationalty
           ,@EmailID
           ,@Password
           ,@ContactNo
           ,@ZoneID
           ,@BranchID
           ,@RoleID
           ,@ReportingManagerID
           ,@DepartmentID
           ,@HO
           ,1)
END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE DeleteUser 
	@puserID int
AS
BEGIN
	delete from UserMaster where UserID=@puserID
END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE GetUsersByID 
	@UserID int
AS
BEGIN
	select * from UserMaster where UserID=@UserID
END
GO



/****** Object:  StoredProcedure [dbo].[UpdateUser]    Script Date: 8/14/2019 2:08:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[UpdateUser]
@UserID int,
	@UserName varchar(350),
	@Address varchar(500)='',
	@Pincode varchar(50)='',
	@city varchar(50)='',
	@state varchar(100)='',
	@Nationalty varchar(100)='',
	@EmailID varchar(250),
	@Password varchar(10),
	@ContactNo varchar(50)='',
	@ZoneID int,
	@BranchID int=null,
	@RoleID int,
	@ReportingManagerID int=null,
	@DepartmentID int=null,
	@HO bit
AS
BEGIN
	update UserMaster
	set UserName=@UserName,
		Address=@Address,
	Pincode=@Pincode,
	city=@city,
	state=@state,
	Nationalty=@Nationalty,
	EmailID=@EmailID,
	Password=@Password,
	ContactNo=@ContactNo,
	ZoneID=@ZoneID,
	BranchID=@BranchID,
	RoleID=@RoleID,
	ReportingManagerID=@ReportingManagerID,
	DepartmentID=@DepartmentID,
	HO=@HO
END

GO



/****** Object:  StoredProcedure [dbo].[UpdateUser]    Script Date: 8/14/2019 2:08:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[UpdateUser]
@UserID int,
	@UserName varchar(350),
	@Address varchar(500)='',
	@Pincode varchar(50)='',
	@city varchar(50)='',
	@state varchar(100)='',
	@Nationalty varchar(100)='',
	@EmailID varchar(250),
	@Password varchar(10),
	@ContactNo varchar(50)='',
	@ZoneID int,
	@BranchID int=null,
	@RoleID int,
	@ReportingManagerID int=null,
	@DepartmentID int=null,
	@HO bit
AS
BEGIN
	update UserMaster
	set UserName=@UserName,
		Address=@Address,
	Pincode=@Pincode,
	city=@city,
	state=@state,
	Nationalty=@Nationalty,
	EmailID=@EmailID,
	Password=@Password,
	ContactNo=@ContactNo,
	ZoneID=@ZoneID,
	BranchID=@BranchID,
	RoleID=@RoleID,
	ReportingManagerID=@ReportingManagerID,
	DepartmentID=@DepartmentID,
	HO=@HO
	where UserID=@UserID
END

GO


